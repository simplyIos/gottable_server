﻿using GotTable.Dal.Restaurants;
using GotTable.ElasticSearch.Interfaces;
using System;
using System.Threading.Tasks;

namespace GotTable.ElasticSearch.Services
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantService : IIndexService<RestaurantService>
    {
        /// <summary>
        /// 
        /// </summary>
        public RestaurantService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurant"></param>
        public async Task Refresh<T>(T restaurant)
        {
            var restaurantDetail = Convert.ChangeType(restaurant, typeof(RestaurantDto));
            if (restaurantDetail != null && restaurantDetail is RestaurantDto)
            {
                await ESConfiguration.ESClient.IndexDocumentAsync(restaurantDetail);
            }
        }
    }
}
