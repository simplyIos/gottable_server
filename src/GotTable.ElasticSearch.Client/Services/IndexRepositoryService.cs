using Elasticsearch.Net;
using GotTable.ElasticSearch.Interfaces;
using Nest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.ElasticSearch.Services
{
    public class IndexRepositoryService : IIndexRepository
    {
        /// <summary>
        /// 
        /// </summary>
        public IndexRepositoryService()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="shardMultiplier"></param>
        /// <returns></returns>
        public virtual async Task CreateIndex(string name, int? shardMultiplier = 1)
        {
            try
            {
                var existsResponse = await ESConfiguration.ESClient.Indices.ExistsAsync(new IndexExistsRequest(name));
                if (existsResponse.Exists)
                {
                    return;
                }
                var clusterInfo = await ESConfiguration.ESClient.Nodes.StatsAsync(new NodesStatsRequest());
                double dataNodesCount = clusterInfo.Nodes.Values.Where(n => n.Roles.Contains(NodeRole.Data)).Count();

                var indexCreatingRequest = new CreateIndexRequest(name)
                {
                    Settings = new IndexSettings()
                };
                if (dataNodesCount <= 1)
                {
                    indexCreatingRequest.Settings.NumberOfShards = 2 * shardMultiplier;
                    indexCreatingRequest.Settings.NumberOfReplicas = 0;
                }
                else
                {
                    var expansionShards = (int)Math.Min(Math.Floor(dataNodesCount / 3d + dataNodesCount), 6);
                    indexCreatingRequest.Settings.NumberOfShards = expansionShards * shardMultiplier;
                    indexCreatingRequest.Settings.NumberOfReplicas = 1;
                }

                var result = await ESConfiguration.ESClient.Indices.CreateAsync(indexCreatingRequest);
            }
            catch (ElasticsearchClientException ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual async Task DeleteIndex(string name)
        {
            try
            {
                var result = await ESConfiguration.ESClient.Indices.DeleteAsync(name);
            }
            catch (ElasticsearchClientException ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public virtual async Task DeleteTemplate(string templateName)
        {
            var templates = await ESConfiguration.ESClient.Indices.GetTemplateAsync(templateName);
            templates.TemplateMappings.Where(t => t.Key.Equals(templateName, StringComparison.InvariantCultureIgnoreCase)).ToList().ForEach(t =>
            {
                try
                {
                    ESConfiguration.ESClient.Indices.DeleteTemplate(t.Key);
                }
                catch (ElasticsearchClientException e)
                {

                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateName"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public async Task ApplyTemplate(string templateName, string template)
        {
            if (!String.IsNullOrWhiteSpace(template))
            {
                var result = await ESConfiguration.ESClient.LowLevel.Indices.PutTemplateForAllAsync<VoidResponse>(templateName, template);
                if (!result.Success)
                {
                    throw new ApplicationException($"Error during processing of template {templateName}", result.OriginalException);
                }
            }
        }
    }
}