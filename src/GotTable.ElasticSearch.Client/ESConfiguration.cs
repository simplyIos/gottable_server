﻿using Elasticsearch.Net;
using GotTable.ElasticSearch.Interfaces;
using Nest;
using System;
using System.Configuration;

namespace GotTable.ElasticSearch
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ESConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        private const string IndexName = "indiarestaurants";


        /// <summary>
        /// 
        /// </summary>
        public static ElasticClient ESClient;

        /// <summary>
        /// 
        /// </summary>
        public async static void Configure(IIndexRepository indexRepository)
        {
            var url = ConfigurationManager.AppSettings["ElasticSearchUrl"].ToString();
            var pool = new StaticConnectionPool(new[]
            {
                new Uri(url)
            });
            var settings = new ConnectionSettings(pool);
            settings.DefaultIndex(IndexName);
            ESClient = new ElasticClient(settings);
            await indexRepository.CreateIndex(IndexName);
        }
    }
}
