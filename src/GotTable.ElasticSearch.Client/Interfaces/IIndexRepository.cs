﻿using System.Threading.Tasks;

namespace GotTable.ElasticSearch.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IIndexRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="shardMultiplier"></param>
        /// <returns></returns>
        Task CreateIndex(string indexName, int? shardMultiplier = 1);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task DeleteIndex(string indexName);
    }
}