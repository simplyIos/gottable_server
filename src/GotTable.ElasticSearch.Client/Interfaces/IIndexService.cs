﻿using System.Threading.Tasks;

namespace GotTable.ElasticSearch.Interfaces
{
    public interface IIndexService<T1>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataObject"></param>
        /// <returns></returns>
        Task Refresh<T2>(T2 dataObject);
    }
}
