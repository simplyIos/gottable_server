﻿using System;

namespace GotTable.Dal.Reports.Restaurants
{
    /// <summary>
    /// RestaurantDto
    /// </summary>
    [Serializable]
    public sealed class RestaurantDto
    {
        public decimal RestaurantId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}
