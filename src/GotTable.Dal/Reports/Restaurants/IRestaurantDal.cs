﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Reports.Restaurants
{
    /// <summary>
    /// IRestaurantDal
    /// </summary>
    public interface IRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantDto>> FetchExpiredList(int currentPage = 0, int pageSize = 10);
    }
}
