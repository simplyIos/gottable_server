﻿using System;

namespace GotTable.Dal.OperationExceptionLogs
{
    /// <summary>
    /// OperationExceptionLogDto
    /// </summary>
    [Serializable]
    public sealed class OperationExceptionLogDto
    {
        public Guid ExceptionLogId { get; set; }
        public DateTime DateTime { get; set; }
        public string ControllerName { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public bool IsExceptionHandled { get; set; }
        public string InnerException { get; set; }
        public string ActionName { get; set; }
    }
}
