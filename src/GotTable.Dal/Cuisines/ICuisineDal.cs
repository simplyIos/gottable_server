﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Cuisines
{
    /// <summary>
    /// ICuisineDal
    /// </summary>
    public interface ICuisineDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<CuisineDto>> FetchList(int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="onlyEngaged"></param>
        /// <param name="cusineStatus"></param>
        /// <returns></returns>
        Task<List<CuisineDto>> FetchList(int currentPage = 1, int pageSize = 10, bool? cusineStatus = true, bool? onlyEngaged = true);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        Task<CuisineDto> Fetch(decimal cuisineId);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="cuisineName"></param>
        /// <returns></returns>
        Task<CuisineDto> Fetch(string cuisineName);
    }
}
