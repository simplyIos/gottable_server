﻿using System;

namespace GotTable.Dal.RestaurantAmenities
{
    /// <summary>
    /// AmenityDto
    /// </summary>
    [Serializable]
    public sealed class AmenityDto
    {
        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public int AmenityId { get; set; }

        public string AmenityName { get; set; }

        public bool Checked { get; set; }
    }
}
