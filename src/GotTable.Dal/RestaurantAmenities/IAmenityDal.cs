﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantAmenities
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAmenityDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<AmenityDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(AmenityDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityId"></param>
        /// <param name="branchId"></param>
        Task Delete(decimal amenityId, decimal branchId);
    }
}
