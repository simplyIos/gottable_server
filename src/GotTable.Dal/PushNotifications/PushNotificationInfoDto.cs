﻿using System;

namespace GotTable.Dal.PushNotifications
{
    /// <summary>
    /// PushNotificationInfoDto
    /// </summary>
    [Serializable]
    public class PushNotificationInfoDto
    {
        public int NotificationId { get; protected internal set; }

        public decimal UserId { get; protected internal set; }

        public decimal? CityId { get; protected internal set; }

        public string CityName { get; protected internal set; }

        public string Title { get; protected internal set; }

        public string Message { get; protected internal set; }

        public string NotificationType { get; protected internal set; }

        public DateTime ScheduleTime { get; protected internal set; }

        public bool Active { get; protected internal set; }

        public DateTime CreatedDatetime { get; protected internal set; }

        public string OfferTypeName { get; protected internal set; }

        public string CategoryTypeName { get; protected internal set; }

        public int? SuccessCount { get; protected internal set; }

        public int? FailureCount { get; protected internal set; }

        public string AndroidPayLoad { get; protected internal set; }

        public string IosPayLoad { get; protected internal set; }

        public string StatusName { get; protected internal set; }
    }
}
