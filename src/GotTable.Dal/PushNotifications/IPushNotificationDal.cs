﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.PushNotifications
{
    /// <summary>
    /// IPushNotificationDal
    /// </summary>
    public interface IPushNotificationDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="scheduleDate"></param>
        /// <param name="notificationCity"></param>
        /// <param name="notificationType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<PushNotificationInfoDto>> FetchList(decimal userId, DateTime? scheduleDate = null, string notificationCity = "", string notificationType = "", int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        Task<List<NotificationExceptionLogDto>> FetchList(decimal notificationId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        Task<PushNotificationDto> Fetch(decimal notificationId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(PushNotificationDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(List<NotificationExceptionLogDto> dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Update(PushNotificationDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        Task Delete(int notificationId);
    }
}
