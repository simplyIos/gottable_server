﻿using System;

namespace GotTable.Dal.RestaurantInvoices
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class InvoiceDto
    {
        public Guid InvoiceId { get; set; }
        public decimal BranchId { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PrescriptionTypeId { get; set; }
        public string PrescriptionTypeName { get; set; }
        public int? PaymentMethodId { get; set; }
        public string PaymentMethodName { get; set; }
        public bool? IsPaymentDone { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool? DineIn { get; set; }
        public bool? Delivery { get; set; }
        public bool? Takeaway { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public decimal UserId { get; set; }
    }
}
