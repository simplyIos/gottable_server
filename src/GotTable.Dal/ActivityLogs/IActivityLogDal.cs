﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.ActivityLogs
{
    /// <summary>
    /// IApplicationLogDal
    /// </summary>
    public interface IActivityLogDal
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(ActivityLogDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="adminId"></param>
        /// <returns></returns>
        Task<List<ActivityLogDto>> FetchList(decimal restaurantId, decimal? adminId = null);
    }
}
