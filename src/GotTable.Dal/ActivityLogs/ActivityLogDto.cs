﻿
using System;

namespace GotTable.Dal.ActivityLogs
{
    /// <summary>
    /// ApplicationLogDto
    /// </summary>
    public sealed class ActivityLogDto
    {
        public Guid LogId { get; set; }

        public decimal AdminId { get; set; }

        public string AdminName { get; protected internal set; }

        public decimal BranchId { get; set; }

        public string Alteration { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
