﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantRatings
{
    public interface IRatingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RatingDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<AvgRatingDto> FetchAvgRating(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reviewId"></param>
        /// <returns></returns>
        Task<RatingDto> Fetch(decimal reviewId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(RatingDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        Task Delete(decimal ratingId);
    }
}
