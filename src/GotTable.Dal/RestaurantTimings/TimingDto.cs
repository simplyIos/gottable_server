﻿using System;

namespace GotTable.Dal.RestaurantTimings
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class TimingDto
    {
        public decimal Id { get; set; }

        public decimal? BranchId { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int? DayId { get; set; }

        public string DayName { get; set; }

        public string LunchStartTime { get; set; }

        public string LunchEndTime { get; set; }

        public string DinnerStartTime { get; set; }

        public string DinnerEndTime { get; set; }

        public bool? IsClosed { get; set; }
    }
}
