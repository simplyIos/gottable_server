﻿using System;

namespace GotTable.Dal.Cities
{
    /// <summary>
    /// CityInfoDto
    /// </summary>
    [Serializable]
    public class CityInfoDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal Id { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected internal set; }
    }
}
