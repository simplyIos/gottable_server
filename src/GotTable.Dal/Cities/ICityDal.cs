﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Cities
{
    /// <summary>
    /// ICityDal
    /// </summary>
    public interface ICityDal
    {
        /// <summary>
        /// FetchList();
        /// </summary>
        /// <returns></returns>
        Task<List<CityInfoDto>> FetchList();
    }
}
