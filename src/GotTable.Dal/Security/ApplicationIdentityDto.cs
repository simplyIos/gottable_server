﻿using System;

namespace GotTable.Dal.Security
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class ApplicationIdentityDto
    {
        public int Id { get; set; }

        public string Version { get; set; }

        public string SyncName { get; set; }

        public string SyncPassword { get; set; }

        public bool IsActive { get; set; }
    }
}
