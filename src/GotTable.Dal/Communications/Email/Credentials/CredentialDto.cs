﻿using System;

namespace GotTable.Dal.Communications.Email.Credentials
{
    /// <summary>
    /// CredentialDto
    /// </summary>
    [Serializable]
    public class CredentialDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string Username { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string Password { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReplyAddress { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsUsedForTransactions { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsUsedForPromotions { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsDefault { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; protected internal set; }
    }
}
