﻿
using System.Threading.Tasks;

namespace GotTable.Dal.Communications.Email.Credentials
{
    /// <summary>
    /// ICredentialDal
    /// </summary>
    public interface ICredentialDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="credentialTypedId"></param>
        /// <returns></returns>
        Task<CredentialDto> Fetch(int credentialTypedId);
    }
}
