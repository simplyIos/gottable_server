﻿
using System.Threading.Tasks;

namespace GotTable.Dal.Communications.Email.Content
{
    /// <summary>
    /// IContentDal
    /// </summary>
    public interface IContentDal
    {
        Task<ContentDto> Fetch(decimal templateId);
    }
}
