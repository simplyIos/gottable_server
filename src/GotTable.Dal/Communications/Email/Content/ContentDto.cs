﻿using System;

namespace GotTable.Dal.Communications.Email.Content
{
    /// <summary>
    /// ContentDto
    /// </summary>
    [Serializable]
    public class ContentDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal TemplateTypeId { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmailTo { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmailCC { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmailSubject { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmailBody { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; protected internal set; }
    }
}
