﻿

using System.Threading.Tasks;

namespace GotTable.Dal.Communications.Email.Configurations
{
    /// <summary>
    /// IConfigurationDal
    /// </summary>
    public interface IConfigurationDal
    {
        Task<ConfigurationDto> Fetch(int contentTypeId);
    }
}
