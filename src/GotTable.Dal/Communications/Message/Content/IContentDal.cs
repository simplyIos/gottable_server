﻿
using System.Threading.Tasks;

namespace GotTable.Dal.Communications.Message.Content
{
    /// <summary>
    /// IContentDal
    /// </summary>
    public interface IContentDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        Task<ContentDto> Fetch(int templateId);
    }
}
