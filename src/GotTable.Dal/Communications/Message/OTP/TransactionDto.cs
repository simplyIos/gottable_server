﻿using System;

namespace GotTable.Dal.Communications.Message.OTP
{
    /// <summary>
    /// TransactionDto
    /// </summary>
    [Serializable]
    public class TransactionDto
    {
        public decimal TransactionId { get; set; }

        public decimal ExternalId { get; set; }

        public decimal PhoneNumber { get; set; }

        public int TypeId { get; set; }

        public string TypeName { get; set; }

        public string OTP { get; set; }

        public bool? IsUsed { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
