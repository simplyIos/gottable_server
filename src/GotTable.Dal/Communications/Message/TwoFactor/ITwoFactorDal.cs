﻿
using System.Threading.Tasks;

namespace GotTable.Dal.Communications.Message.TwoFactor
{
    /// <summary>
    /// ITwoFactorDal
    /// </summary>
    public interface ITwoFactorDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="templateTypeId"></param>
        /// <returns></returns>
        Task<TwoFactorDto> Fetch(int templateTypeId);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(TwoFactorLogDto dto);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(TwoFactorLogDto dto);
    }
}
