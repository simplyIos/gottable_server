﻿using System;

namespace GotTable.Dal.Communications.Message.TwoFactor
{
    /// <summary>
    /// TwoFactorLogDto
    /// </summary>
    [Serializable]
    public class TwoFactorLogDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal LogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TargetUrlWithParameters { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal ExternalId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal PhoneNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TransactionStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}
