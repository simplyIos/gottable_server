﻿using System;

namespace GotTable.Dal.Communications.Message.TwoFactor
{
    /// <summary>
    /// TwoFactorDto
    /// </summary>
    [Serializable]
    public sealed class TwoFactorDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SenderId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string QueryString { get; set; }
    }
}
