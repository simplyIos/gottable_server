﻿
using System;
using System.Collections.Generic;

namespace GotTable.Dal.RestaurantBookings
{
    [Serializable]
    public class DeliveryandTakeawayListItemDto
    {
        #region Booking

        public decimal BookingId { get; set; }

        public int BookingTypeId { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingTime { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? CurrentStatusId { get; set; }

        public decimal? OfferId { get; set; }

        public string OfferName { get; set; }

        public string PromoCode { get; set; }

        public decimal CartAmount { get; set; }

        public decimal CentralGST { get; set; }

        public decimal StateGST { get; set; }

        public decimal ServiceCharges { get; set; }

        public decimal DeliveryCharges { get; set; }

        public decimal PackingCharges { get; set; }

        #endregion

        #region Branch

        public decimal BranchId { get; set; }

        public string BranchName { get; set; }

        public string BranchAddress { get; set; }

        public string BranchCity { get; set; }

        public string BranchState { get; set; }

        public decimal BranchZipCode { get; set; }

        #endregion

        #region Delivery address

        public DeliveryItemDto Delivery { get; set; }

        #endregion

        #region Contact

        public List<ContactListItemDto> ContactList { get; set; }

        #endregion

        #region CartItem

        public List<CartListItemDto> CartItem { get; set; }

        #endregion
    }
}
