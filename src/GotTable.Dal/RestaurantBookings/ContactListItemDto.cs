﻿using System;

namespace GotTable.Dal.RestaurantBookings
{
    [Serializable]
    public class ContactListItemDto
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
