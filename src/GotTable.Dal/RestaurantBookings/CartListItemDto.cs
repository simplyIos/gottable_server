﻿using System;

namespace GotTable.Dal.RestaurantBookings
{
    [Serializable]
    public class CartListItemDto
    {
        public string MenuName { get; set; }

        public int Quantity { get; set; }

        public decimal Rate { get; set; }
    }
}
