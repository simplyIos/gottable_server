﻿using System;

namespace GotTable.Dal.RestaurantBookings.DeliveryandTakeaway
{
    [Serializable]
    public class DeliveryandTakeawayStatusDto
    {
        #region Active propery

        public decimal Id { get; set; }

        public decimal BookingId { get; set; }

        public decimal? UserId { get; set; }

        public decimal? AdminId { get; set; }

        public int StatusId { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }

        #endregion

        #region Connected property

        public string StatusName { get; set; }

        public string UserEmailAddress { get; set; }

        public string UserName { get; set; }

        public string AdminEmailAddress { get; set; }

        public string AdminName { get; set; }

        #endregion
    }
}
