﻿using System;

namespace GotTable.Dal.RestaurantBookings.DeliveryandTakeaway
{
    [Serializable]
    public class DeliveryAddressDto
    {
        public decimal BookingId { get; set; }

        public decimal? Id { get; set; }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Landmark { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public decimal UserId { get; set; }
    }
}
