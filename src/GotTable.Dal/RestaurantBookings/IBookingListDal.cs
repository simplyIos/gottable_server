﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantBookings
{
    public interface IBookingListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingInfoDto>> FetchRestaurantBookingList(decimal restaurantId, DateTime bookingDate, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingInfoDto>> FetchRestaurantBookingList(decimal restaurantId, DateTime startDate, DateTime endDate, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingInfoDto>> FetchUserBookingList(decimal userId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingInfoDto>> FetchUserBookingList(decimal userId, decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingInfoDto>> FetchDineInBookingList(decimal restaurantTableId, DateTime bookingDate, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<DineInListItemDto>> FetchDineInList(decimal userId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<DeliveryandTakeawayListItemDto>> FetchDeliveryandTakeawayList(decimal userId, int currentPage = 1, int pageSize = 10);
    }
}
