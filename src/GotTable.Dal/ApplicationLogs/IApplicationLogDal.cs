﻿using System.Threading.Tasks;

namespace GotTable.Dal.ApplicationLogs
{
    /// <summary>
    /// IApplicationLogDal
    /// </summary>
    public interface IApplicationLogDal
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(ApplicationLogDto dto);
    }
}
