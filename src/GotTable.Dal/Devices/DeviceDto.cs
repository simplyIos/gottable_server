﻿
using System;

namespace GotTable.Dal.Devices
{
    /// <summary>
    /// DeviceDto
    /// </summary>
    [Serializable]
    public sealed class DeviceDto
    {
        public string Id { get; set; }

        public decimal? UserId { get; set; }

        public int TypeId { get; set; }

        public bool IsActive { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string City { get; set; }
    }
}
