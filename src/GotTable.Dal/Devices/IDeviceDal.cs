﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Devices
{
    /// <summary>
    /// IDeviceDal
    /// </summary>
    public interface IDeviceDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        Task<List<DeviceDto>> FetchList(Enumeration.Device deviceType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        Task<DeviceDto> Fetch(string deviceId);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(DeviceDto dto);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(DeviceDto dto);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="deviceId"></param>
        Task Delete(string deviceId);
    }
}
