﻿using System;

namespace GotTable.Dal.RestaurantConfigurations
{
    /// <summary>
    /// ConfigurationDto
    /// </summary>
    [Serializable]
    public sealed class ConfigurationDto
    {
        public decimal BranchId { get; set; }

        public int? DineInReward { get; set; }

        public string TakeawayStandBy { get; set; }

        public string DeliveryCoverage { get; set; }

        public decimal? ServiceTax { get; set; }

        public decimal? DeliveryCharges { get; set; }

        public decimal? CentralGST { get; set; }

        public decimal? StateGST { get; set; }

        public string DeliveryStandBy { get; set; }

        public bool? DineInRewardActive { get; set; }

        public bool? DineInAutoCompleteActive { get; set; }

        public int? DineInAutoCompleteHours { get; set; }
    }
}
