﻿
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantConfigurations
{
    /// <summary>
    /// IConfigurationDal
    /// </summary>
    public interface IConfigurationDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<ConfigurationDto> Fetch(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(ConfigurationDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(ConfigurationDto dto);
    }
}
