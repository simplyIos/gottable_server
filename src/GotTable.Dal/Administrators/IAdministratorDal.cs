﻿
using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Administrators
{
    /// <summary>
    /// IAdministratorDal
    /// </summary>
    public interface IAdministratorDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<AdminDto> Fetch(decimal userId);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<AdminDto> Fetch(string emailAddress, string password);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        Task<AdminDto> Fetch(string emailAddress);

        /// <summary>
        /// Fetch list
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="adminType"></param>
        /// <param name="emailAddress"></param>
        /// <param name="adminName"></param>
        /// <param name="status"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<AdminDto>> FetchAdminList(Enumeration.AdminType? adminType = null, string emailAddress = null, string adminName = "",
            bool? status = null, int currentPage = 0, int pageSize = 10);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(AdminDto dto);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(AdminDto dto);
    }
}
