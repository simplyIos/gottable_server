﻿using System;

namespace GotTable.Dal.Administrators
{
    /// <summary>
    /// AdminDto
    /// </summary>
    [Serializable]
    public sealed class AdminDto
    {
        public decimal UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int TypeId { get; set; }

        public string TypeName { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public int? PrefixId { get; set; }

        public string PrefixName { get; set; }

        public int? GenderId { get; set; }

        public string GenderName { get; set; }

        public bool? IsActive { get; set; }

        public decimal? CityId { get; set; }

        public string CityName { get; set; }

        public bool? EnableButtonForRestaurantLogin { get; set; }
    }
}
