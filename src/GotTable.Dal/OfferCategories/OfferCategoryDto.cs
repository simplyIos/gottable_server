﻿using System;

namespace GotTable.Dal.OfferCategories
{
    /// <summary>
    /// OfferCategoryDto
    /// </summary>
    [Serializable]
    public sealed class OfferCategoryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public string Extension { get; set; }

        public int? EngagedOffers { get; set; }
    }
}
