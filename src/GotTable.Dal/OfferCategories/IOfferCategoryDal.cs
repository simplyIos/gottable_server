﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.OfferCategories
{
    public interface IOfferCategoryDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        Task<List<OfferCategoryDto>> FetchList(int currentPage = 1, int pageSize = 10, Enumeration.RestaurantTypes? restaurantType = null);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Task<OfferCategoryDto> Fetch(int categoryId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(OfferCategoryDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(OfferCategoryDto dto);
    }
}
