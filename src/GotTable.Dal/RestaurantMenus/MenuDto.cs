﻿using System;

namespace GotTable.Dal.RestaurantMenus
{
    [Serializable]
    public sealed class MenuDto
    {
        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public decimal CuisineId { get; set; }
        public string CuisineName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public bool IsDineAvailable { get; set; }
        public bool IsTakeAwayAvailable { get; set; }
        public bool IsDeliveryAvailable { get; set; }
        public bool? IsActive { get; set; }
        public int? MenuTypeId { get; set; }
        public string MenuTypeName { get; set; }
        public string Description { get; set; }
    }
}
