﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantMenus
{
    public interface IMenuDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<MenuDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<MenuDto>> FetchList(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="typeId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<MenuDto>> FetchList(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType, decimal typeId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        Task<MenuDto> Fetch(decimal menuId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(MenuDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Update(MenuDto dto);
    }
}
