﻿using System.Threading.Tasks;

namespace GotTable.Dal.ApplicationSettings
{
    /// <summary>
    /// IApplicationSettingDal
    /// </summary>
    public interface IApplicationSettingDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <returns></returns>
        Task<ApplicationSettingDto> Fetch();

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(ApplicationSettingDto dto);
    }
}
