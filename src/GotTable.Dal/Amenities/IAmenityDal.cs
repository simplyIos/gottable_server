﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Amenities
{
    /// <summary>
    /// IAmenityDal
    /// </summary>
    public interface IAmenityDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<AmenityDto>> FetchList(int currentPage = 0, int pageSize = 10);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(AmenityDto dto);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(AmenityDto dto);
    }
}
