﻿
namespace GotTable.Dal.Amenities
{
    /// <summary>
    /// AmenityDto
    /// </summary>
    [System.Serializable]
    public sealed class AmenityDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
