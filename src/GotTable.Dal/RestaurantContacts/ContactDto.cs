﻿using System;

namespace GotTable.Dal.RestaurantContacts
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class ContactDto
    {
        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailAlert { get; set; }
        public bool IsPhoneAlert { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string EmailAddress { get; set; }
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
