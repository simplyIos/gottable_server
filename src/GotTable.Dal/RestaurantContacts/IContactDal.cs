﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantContacts
{
    public interface IContactDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<ContactDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        Task<ContactDto> Fetch(decimal contactId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(ContactDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(ContactDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        Task Delete(decimal contactId);
    }
}
