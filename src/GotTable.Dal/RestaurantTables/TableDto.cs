﻿using System;

namespace GotTable.Dal.RestaurantTables
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class TableDto
    {
        public decimal Id { get; set; }

        public decimal RestaurantId { get; set; }

        public decimal TableId { get; set; }

        public string TableName { get; set; }

        public int TableCount { get; set; }

        public bool IsActive { get; set; }
    }
}
