﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantTables
{
    public interface ITableDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<TableDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <returns></returns>
        Task<TableDto> Fetch(decimal restaurantTableId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(TableDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(TableDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <returns></returns>
        Task Delete(decimal restaurantTableId);
    }
}
