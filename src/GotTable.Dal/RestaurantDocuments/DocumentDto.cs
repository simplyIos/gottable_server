﻿using System;

namespace GotTable.Dal.RestaurantDocuments
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class DocumentDto
    {
        public int DocumentId { get; set; }

        public decimal BranchId { get; set; }

        public int? CategoryId { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public bool Active { get; set; }
    }
}
