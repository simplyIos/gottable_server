﻿using System;

namespace GotTable.Dal.Notifications.DeviceNotifications
{
    /// <summary>
    /// NotificationDeviceDto
    /// </summary>
    [Serializable]
    public sealed class NotificationDeviceDto
    {
        public string DeviceId { get; set; }

        public string DeviceType { get; set; }

        public string DeviceLatitude { get; set; }

        public string DeviceLongitude { get; set; }

        public decimal Distance { get; set; }
    }
}
