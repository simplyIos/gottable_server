﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Notifications.DeviceNotifications
{
    public interface INotificationDeviceListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        Task<List<NotificationDeviceDto>> FetchList(decimal? cityId, Enumeration.NotificationType notificationType);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<NotificationDeviceDto>> FetchList();
    }
}
