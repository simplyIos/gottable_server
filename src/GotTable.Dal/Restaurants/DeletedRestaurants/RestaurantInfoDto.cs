﻿
namespace GotTable.Dal.Restaurants.DeletedRestaurants
{
    public class RestaurantInfoDto
    {
        public decimal BranchId { get; protected internal set; }

        public string Name { get; protected internal set; }

        public string Address { get; protected internal set; }
    }
}
