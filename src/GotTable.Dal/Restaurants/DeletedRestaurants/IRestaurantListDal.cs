﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.DeletedRestaurants
{
    public interface IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(int currentPage = 1, int pageSize = 10);
    }
}
