﻿using GotTable.Common;
using System;
using System.Collections.Generic;

namespace GotTable.Dal.Restaurants.Filters
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class FilterDto
    {
        public int Index { get; set; }

        public string Name { get; set; }

        public string Key { get; set; }

        public string IconUrl { get; set; }

        public List<EnumDtoV2<decimal>> List { get; set; }
    }
}
