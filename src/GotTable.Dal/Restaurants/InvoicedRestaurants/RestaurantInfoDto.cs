﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants.InvoicedRestaurants
{
    [Serializable]
    public class RestaurantInfoDto
    {
        public decimal RestaurantId { get; protected internal set; }

        public string RestaurantName { get; protected internal set; }

        public string Line1 { get; protected internal set; }

        public string Line2 { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zip { get; protected internal set; }

        public DateTime? StartDate { get; protected internal set; }

        public DateTime? EndDate { get; protected internal set; }

        public decimal Amount { get; protected internal set; }

        public int SubscriptionTypeId { get; protected internal set; }

        public bool? IsActive { get; protected internal set; }

        public decimal SalesPersonId { get; protected internal set; }

        public string SalesAdminName { get; protected internal set; }

        public string SalesAdminEmailAddress { get; protected internal set; }

        public decimal AccountAdminPersonId { get; protected internal set; }

        public string AccountAdminName { get; protected internal set; }

        public string AccountAdminEmailAddress { get; protected internal set; }

        public Enumeration.RestaurantCategories Category { get; protected internal set; }
    }
}
