﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.InvoicedRestaurants
{
    public interface IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(decimal? salesPersonId = null, int currentPage = 1, int pageSize = 10);
    }
}
