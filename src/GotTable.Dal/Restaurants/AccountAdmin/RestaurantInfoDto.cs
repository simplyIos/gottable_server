﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants.AccountAdmin
{
    [Serializable]
    public class RestaurantInfoDto
    {
        public decimal RestaurantId { get; protected internal set; }

        public string RestaurantName { get; protected internal set; }

        public string Line1 { get; protected internal set; }

        public string Line2 { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zip { get; protected internal set; }

        public bool IsActive { get; protected internal set; }

        public decimal? SalesPersonId { get; protected internal set; }

        public string SalesAdminName { get; protected internal set; }

        public string SalesAdminEmailAddress { get; protected internal set; }

        public bool OpenforAccountAdmin { get; protected internal set; }

        public Enumeration.RestaurantCategories Category { get; protected internal set; }
    }
}
