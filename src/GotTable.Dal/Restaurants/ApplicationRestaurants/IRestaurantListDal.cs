﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.ApplicationRestaurants
{
    public interface IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(ListCriteria criteria);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> Fetch(decimal restaurantId, double latitude, double longitude, Enumeration.RestaurantTypes? restaurantType);
    }
}
