﻿using System;

namespace GotTable.Dal.Restaurants.ApplicationRestaurants
{
    [Serializable]
    public class AutoCompleteDto
    {
        public string Name { get; protected internal set; }

        public decimal Id { get; protected internal set; }
    }
}
