﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants.ApplicationRestaurants
{
    [Serializable]
    public class RestaurantInfoDto
    {
        public decimal RestaurantId { get; protected internal set; }

        public string Name { get; protected internal set; }

        public string Line1 { get; protected internal set; }

        public string Line2 { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zip { get; protected internal set; }

        public decimal Distance { get; protected internal set; }

        public string Longitude { get; protected internal set; }

        public string Latitude { get; protected internal set; }

        public string OfferName { get; protected internal set; }

        public bool IsOfferAvailable { get; protected internal set; }

        public string RestaurantTag { get; protected internal set; }

        public string SEOKeyword { get; protected internal set; }

        public string SEODescription { get; protected internal set; }

        public string SEOTitle { get; protected internal set; }

        public Enumeration.RestaurantTags SelectedTag { get; protected internal set; }

        public Enumeration.RestaurantCategories Category { get; protected internal set; }

        public int ActiveOffers { get; protected internal set; }

        public int CostForTwo { get; protected internal set; }

        public string Description { get; protected internal set; }

        public int MusicRating { get; protected internal set; }

        public int AmbienceRating { get; protected internal set; }

        public int PriceRating { get; protected internal set; }

        public int ServiceRating { get; protected internal set; }

        public int FoodRating { get; protected internal set; }
    }
}
