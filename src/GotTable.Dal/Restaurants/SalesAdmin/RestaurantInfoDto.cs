﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants.SalesAdmin
{
    [Serializable]
    public class RestaurantInfoDto
    {
        public decimal RestaurantId { get; protected internal set; }

        public string RestaurantName { get; protected internal set; }

        public string Line1 { get; protected internal set; }

        public string Line2 { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zip { get; protected internal set; }

        public Guid? InvoiceId { get; protected internal set; }

        public DateTime? StartDate { get; protected internal set; }

        public DateTime? EndDate { get; protected internal set; }

        public int? PrescriptionTypeId { get; protected internal set; }

        public bool? RestaurantStatus { get; protected internal set; }

        public bool? InvoiceStatus { get; protected internal set; }

        public decimal? Amount { get; protected internal set; }

        public bool? DineIn { get; protected internal set; }

        public bool? Delivery { get; protected internal set; }

        public bool? TakeAway { get; protected internal set; }

        public decimal? AccountAdminId { get; protected internal set; }

        public string AccountAdminName { get; protected internal set; }

        public string AccountAdminEmailAddress { get; protected internal set; }

        public Enumeration.RestaurantCategories Category { get; protected internal set; }
    }
}
