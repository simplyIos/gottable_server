﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.PreferredRestaurants
{
    public interface IPreferredRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(double latitude, double longitude, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantDto>> FetchList(decimal? cityId = null, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<RestaurantDto> Fetch(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(RestaurantDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(RestaurantDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantSwapDto"></param>
        /// <returns></returns>
        Task Update(List<decimal> restaurantIds);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        Task Delete(decimal preferredRestaurantId);
    }
}
