﻿using System;

namespace GotTable.Dal.Restaurants.PreferredRestaurants
{
    [Serializable]
    public class RestaurantSwapDto
    {
        /// <summary>
        /// FirstRestaurantId
        /// </summary>
        public decimal FirstRestaurantId { get; set; }

        /// <summary>
        /// FirstRestaurantIndex
        /// </summary>
        public int FirstRestaurantIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal SecondRestaurantId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SecondRestaurantIndex { get; set; }
    }
}
