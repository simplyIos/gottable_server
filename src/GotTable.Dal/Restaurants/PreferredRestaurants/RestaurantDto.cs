﻿using System;

namespace GotTable.Dal.Restaurants.PreferredRestaurants
{
    [Serializable]
    public class RestaurantDto
    {
        public decimal RestaurantId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public bool? Status { get; set; }

        public int IndexValue { get; set; }

        public decimal? CityId { get; set; }

        public string CityName { get; set; }
    }
}
