﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants.PreferredRestaurants
{
    [Serializable]
    public class RestaurantInfoDto
    {
        public decimal RestaurantId { get; protected internal set; }

        public string Name { get; protected internal set; }

        public string Address { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zipcode { get; protected internal set; }

        public string Latitude { get; protected internal set; }

        public string Longitude { get; protected internal set; }

        public decimal? Distance { get; protected internal set; }

        public string TagLine { get; protected internal set; }

        public string SEOKeyword { get; protected internal set; }

        public string SEOTitle { get; protected internal set; }

        public string SEODescription { get; protected internal set; }

        public Enumeration.RestaurantTypes Type { get; protected internal set; }

        public int Rating { get; protected internal set; }
    }
}
