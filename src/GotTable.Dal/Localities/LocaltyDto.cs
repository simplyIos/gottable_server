﻿
using System;

namespace GotTable.Dal.Localities
{
    /// <summary>
    /// LocaltyDto
    /// </summary>
    [Serializable]
    public sealed class LocaltyDto
    {
        public decimal Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
