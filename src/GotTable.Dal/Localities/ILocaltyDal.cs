﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Localities
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILocaltyDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="active"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<LocaltyDto>> FetchList(bool? active = null, int currentPage = default, int pageSize = 10, bool? engagedRestaurant = true);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(LocaltyDto dto);
    }
}
