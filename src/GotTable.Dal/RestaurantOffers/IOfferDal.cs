﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantOffers
{
    public interface IOfferDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="offerType"></param>
        /// <param name="bookingDate"></param>
        /// <param name="expiredOffers"></param>
        /// <param name="valid"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<OfferDto>> FetchList(decimal? restaurantId = null, Enumeration.RestaurantTypes? restaurantType = null, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.OfferType? offerType = null, DateTime? bookingDate = null, bool? expiredOffers = null, bool? valid = null, int currentPage = 0, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        Task<OfferDto> Fetch(decimal offerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(OfferDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Update(OfferDto dto);
    }
}
