﻿
using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.RestaurantOffers
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class OfferDto
    {
        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDelivery { get; set; }

        public bool? IsTakeAway { get; set; }

        public bool? IsDineIn { get; set; }

        public int TypeId { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public Enumeration.OfferType? OfferType { get; set; }
    }
}
