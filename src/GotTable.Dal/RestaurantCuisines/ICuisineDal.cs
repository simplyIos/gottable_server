﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantCuisines
{
    public interface ICuisineDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<CuisineDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantCuisineId"></param>
        /// <returns></returns>
        Task<CuisineDto> Fetch(decimal restaurantCuisineId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterCuisineId"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        Task<CuisineDto> Fetch(decimal masterCuisineId, decimal branchId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(CuisineDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Update(CuisineDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<bool> IsExist(CuisineDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Delete(CuisineDto dto);
    }
}
