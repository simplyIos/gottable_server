﻿using System;

namespace GotTable.Dal.RestaurantCuisines
{
    [Serializable]
    public sealed class CuisineDto
    {
        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public decimal CuisineId { get; set; }

        public string CuisineName { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
