//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdminLogin
    {
        public decimal UserId { get; set; }
        public int TypeId { get; set; }
        public Nullable<int> PrefixId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> GenderId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<decimal> PreferredCityId { get; set; }
        public Nullable<bool> EnableButtonForRestaurantLogin { get; set; }
        public Nullable<bool> Deleted { get; set; }
    }
}
