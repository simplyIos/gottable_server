//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class BranchImage
    {
        public int DocumentId { get; set; }
        public decimal BranchId { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public bool Active { get; set; }
    
        public virtual HotelBranch HotelBranch { get; set; }
    }
}
