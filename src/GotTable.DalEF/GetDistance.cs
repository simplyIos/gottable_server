﻿using System;

namespace GotTable.DalEF
{
    [Serializable]
    internal sealed class GetDistance
    {
        public static decimal Execute(double currentLatitude, double currentLongitude, double restaurantLatitude, double restaurantLongitude)
        {
            if ((currentLatitude == restaurantLatitude) && (currentLongitude == restaurantLongitude))
            {
                return 0;
            }
            else
            {
                double distanceInKms = Radian2Degree(Math.Acos(Math.Sin(Degree2Radian(currentLatitude)) * Math.Sin(Degree2Radian(restaurantLatitude)) + Math.Cos(Degree2Radian(currentLatitude)) * Math.Cos(Degree2Radian(restaurantLatitude)) * Math.Cos(Degree2Radian(currentLongitude - restaurantLongitude)))) * 60 * 1.1515 * 1.609344;
                return (decimal)Math.Round(distanceInKms, 2);
            }
        }

        private static double Degree2Radian(double degree)
        {
            return (degree * Math.PI / 180.0);
        }

        private static double Radian2Degree(double radian)
        {
            return (radian / Math.PI * 180.0);
        }
    }
}
