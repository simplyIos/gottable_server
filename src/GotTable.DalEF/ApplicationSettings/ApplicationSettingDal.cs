﻿using GotTable.Dal.ApplicationSettings;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.ApplicationSettings
{
    /// <summary>
    /// ApplicationSettingDal
    /// </summary>
    [Serializable]
    public sealed class ApplicationSettingDal : IApplicationSettingDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <returns></returns>
        public async Task<ApplicationSettingDto> Fetch()
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from ar in ctx.ApplicationSettings.AsQueryable()
                            select new
                            {
                                ar.ApnsCeritificatePassword,
                                ar.ApnsCertificateContent,
                                ar.GeoUrl,
                                ar.OTPAuthKey,
                                ar.OTPAuthUrl,
                                ar.TwoFactorAPIAuthKey,
                                ar.TwoFactorAPIURl,
                                ar.FCMServerAPIKey,
                                ar.AdminApplicationBaseUrl,
                                ar.UserApplicationBaseUrl,
                                ar.ApplicationIdPackageName,
                                ar.FCMSenderId,
                                ar.FCMUrl,
                                ar.DineInActive,
                                ar.DeliveryActive,
                                ar.TakeawayActive,
                                ar.RestaurantDirectory,
                                ar.OfferDirectory,
                                ar.ServiceBusFeatureEnable,
                                ar.HangfireFeatureEnable,
                                ar.NotificationDirectory,
                                ar.DineInEmailTransmission,
                                ar.DineInMessageTransmission,
                                ar.DeliveryEmailTransmission,
                                ar.DeliveryMessageTransmission,
                                ar.TakeawayEmailTransmission,
                                ar.TakeawayMessageTransmission
                            }).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid application key");
                }

                return new ApplicationSettingDto()
                {
                    ApnsCeritificatePassword = data.ApnsCeritificatePassword,
                    ApnsCertificateContent = data.ApnsCertificateContent,
                    GeoUrl = data.GeoUrl,
                    OTPAuthKey = data.OTPAuthKey,
                    OTPAuthUrl = data.OTPAuthUrl,
                    TwoFactorAPIAuthKey = data.TwoFactorAPIAuthKey,
                    TwoFactorAPIURl = data.TwoFactorAPIURl,
                    FCMServerKey = data.FCMServerAPIKey,
                    AdminApplicationBaseUrl = data.AdminApplicationBaseUrl,
                    UserApplicationBaseUrl = data.UserApplicationBaseUrl,
                    ApplicationIdPackageName = data.ApplicationIdPackageName,
                    FCMSenderId = data.FCMSenderId,
                    FCMUrl = data.FCMUrl,
                    DineInActive = data.DineInActive,
                    TakeawayActive = data.TakeawayActive,
                    DeliveryActive = data.DeliveryActive,
                    RestaurantDirectory = data.RestaurantDirectory,
                    OfferDirectory = data.OfferDirectory,
                    HangfireFeatureEnable = data.HangfireFeatureEnable,
                    ServiceBusFeatureEnable = data.ServiceBusFeatureEnable,
                    NotificationDirectory = data.NotificationDirectory,
                    DineInEmailTransmission = data.DineInEmailTransmission,
                    DineInMessageTransmission = data.DineInMessageTransmission,
                    DeliveryEmailTransmission = data.DeliveryEmailTransmission,
                    DeliveryMessageTransmission = data.DeliveryMessageTransmission,
                    TakeawayEmailTransmission = data.TakeawayEmailTransmission,
                    TakeawayMessageTransmission = data.TakeawayMessageTransmission
                };
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(ApplicationSettingDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.ApplicationSettings
                            select c).SingleOrDefault();

                data.AdminApplicationBaseUrl = dto.AdminApplicationBaseUrl;
                data.ApnsCeritificatePassword = dto.ApnsCeritificatePassword;
                data.ApplicationIdPackageName = dto.ApplicationIdPackageName;
                data.FCMSenderId = dto.FCMSenderId;
                data.FCMServerAPIKey = dto.FCMServerKey;
                data.FCMUrl = dto.FCMUrl;
                data.GeoUrl = dto.GeoUrl;
                data.OTPAuthKey = dto.OTPAuthKey;
                data.OTPAuthUrl = dto.OTPAuthUrl;
                data.TwoFactorAPIAuthKey = dto.TwoFactorAPIAuthKey;
                data.TwoFactorAPIURl = dto.TwoFactorAPIURl;
                data.UserApplicationBaseUrl = dto.UserApplicationBaseUrl;
                data.DineInActive = dto.DineInActive;
                data.DeliveryActive = dto.DeliveryActive;
                data.TakeawayActive = dto.TakeawayActive;
                data.OfferDirectory = dto.OfferDirectory;
                data.RestaurantDirectory = dto.RestaurantDirectory;
                data.NotificationDirectory = dto.NotificationDirectory;
                data.HangfireFeatureEnable = dto.HangfireFeatureEnable;
                data.ServiceBusFeatureEnable = dto.ServiceBusFeatureEnable;
                data.ApnsCertificateContent = dto.ApnsCertificateContent ?? data.ApnsCertificateContent;
                data.DineInEmailTransmission = dto.DineInEmailTransmission;
                data.DineInMessageTransmission = dto.DineInMessageTransmission;
                data.DeliveryEmailTransmission = dto.DeliveryEmailTransmission;
                data.DeliveryMessageTransmission = dto.DeliveryMessageTransmission;
                data.TakeawayEmailTransmission = dto.TakeawayEmailTransmission;
                data.TakeawayMessageTransmission = dto.TakeawayMessageTransmission;
                ctx.SaveChanges();
            }
        }
    }
}
