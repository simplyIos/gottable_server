//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class OTPTransaction
    {
        public decimal TransactionId { get; set; }
        public decimal ExternalId { get; set; }
        public decimal PhoneNumber { get; set; }
        public int TypeId { get; set; }
        public string OTP { get; set; }
        public Nullable<bool> IsUsed { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
