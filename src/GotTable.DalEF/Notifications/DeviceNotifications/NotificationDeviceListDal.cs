﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Notifications.DeviceNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Notifications.DeviceNotifications
{
    [Serializable]
    public sealed class NotificationDeviceListDal : INotificationDeviceListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        public async Task<List<NotificationDeviceDto>> FetchList(decimal? cityId, Enumeration.NotificationType notificationType)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var city = (from c in ctx.Cities
                            where c.CityId == cityId
                            select c).SingleOrDefault();

                var data = (from d in ctx.Devices
                            where d.IsActive == true && d.Latitude != "" && d.Longitude != ""
                            && d.TypeId == (int)notificationType
                            select new NotificationDeviceDto()
                            {
                                DeviceId = d.Id,
                                DeviceLatitude = d.Latitude,
                                DeviceLongitude = d.Longitude,
                                DeviceType = ((Enumeration.Device)d.TypeId).ToString(),
                                Distance = 0
                            }).ToList();

                data.ForEach(m => m.Distance = GetDistance.Execute(double.Parse(city.Latitude), double.Parse(city.Longitude), double.Parse(m.DeviceLatitude), double.Parse(m.DeviceLongitude)));

                return data.Where(x => x.Distance <= 50).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<NotificationDeviceDto>> FetchList()
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.Devices
                            where d.IsActive == true
                            select new NotificationDeviceDto()
                            {
                                DeviceId = d.Id,
                                DeviceLatitude = d.Latitude,
                                DeviceLongitude = d.Longitude,
                                DeviceType = ((Enumeration.Device)d.TypeId).ToString(),
                                Distance = 0
                            }).ToList();

                return data.ToList();
            }
        }
    }
}
