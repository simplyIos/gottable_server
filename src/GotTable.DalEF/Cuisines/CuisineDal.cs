﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Cuisines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Cuisines
{
    [Serializable]
    public sealed class CuisineDal : ICuisineDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="cusineStatus"></param>
        /// <param name="onlyEngaged"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<CuisineDto>> FetchList(int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Cuisines.AsQueryable()
                            where c.IsActive == true
                            orderby c.CategoryId, c.DisplayLevel
                            select new CuisineDto
                            {
                                Name = c.CuisineName,
                                Id = c.Id,
                                CategoryId = c.CategoryId,
                                CategoryName = ((Enumeration.CuisineCategory)c.CategoryId).ToString(),
                                IsActive = c.IsActive,
                                DisplayLevel = c.DisplayLevel,
                                EngagedRestaurant = c.BranchCusinies.GroupBy(x => x.BranchId).Count()
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }

        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="cusineStatus"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="onlyEngaged"></param>
        /// <returns></returns>
        public async Task<List<CuisineDto>> FetchList(int currentPage = 1, int pageSize = 10, bool? cusineStatus = true, bool? onlyEngaged = true)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Cuisines.AsQueryable()
                            where onlyEngaged != null ? c.IsActive == true && c.BranchCusinies.Any(x => x.HotelBranch.IsActive == true) : c.IsActive == cusineStatus
                            orderby c.CuisineName
                            select new CuisineDto
                            {
                                Name = c.CuisineName,
                                Id = c.Id,
                                CategoryId = c.CategoryId,
                                CategoryName = ((Enumeration.CuisineCategory)c.CategoryId).ToString(),
                                IsActive = c.IsActive,
                                DisplayLevel = c.DisplayLevel,
                                EngagedRestaurant = c.BranchCusinies.GroupBy(x => x.BranchId).Count()
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        public async Task<CuisineDto> Fetch(decimal cuisineId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Cuisines.AsQueryable()
                            where c.Id == cuisineId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid cuisine Id");
                }

                return new CuisineDto()
                {
                    Name = data.CuisineName,
                    Id = data.Id,
                    CategoryId = data.CategoryId,
                    CategoryName = ((Enumeration.CuisineCategory)data.CategoryId).ToString(),
                    IsActive = data.IsActive,
                    DisplayLevel = data.DisplayLevel,
                    EngagedRestaurant = 0
                };
            }
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="cuisineName"></param>
        /// <returns></returns>
        public async Task<CuisineDto> Fetch(string cuisineName)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Cuisines.AsQueryable()
                            where c.CuisineName == cuisineName
                            select c).SingleOrDefault();

                if (data == null)
                {
                    return null;
                }

                return new CuisineDto()
                {
                    Name = data.CuisineName,
                    Id = data.Id,
                    CategoryId = data.CategoryId,
                    CategoryName = ((Enumeration.CuisineCategory)data.CategoryId).ToString(),
                    IsActive = data.IsActive,
                    DisplayLevel = data.DisplayLevel,
                    EngagedRestaurant = 0
                };
            }
        }
    }
}
