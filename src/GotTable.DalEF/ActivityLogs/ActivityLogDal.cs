﻿using GotTable.Dal.ActivityLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.ActivityLogs
{
    /// <summary>
    /// IApplicationLog
    /// </summary>
    [Serializable]
    public sealed class ActivityLogDal : IActivityLogDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="adminId"></param>
        /// <returns></returns>
        public async Task<List<ActivityLogDto>> FetchList(decimal restaurantId, decimal? adminId = null)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.ActivityLogs.AsQueryable()
                            where adminId != null ? c.AdminId == adminId && c.BranchId == restaurantId : c.BranchId == restaurantId
                            orderby c.CreatedDate
                            select new ActivityLogDto
                            {
                                AdminId = c.AdminId,
                                AdminName = c.Administrator.FirstName + " " + c.Administrator.LastName,
                                Alteration = c.Alteration,
                                BranchId = c.BranchId,
                                Comment = c.Comment,
                                CreatedDate = c.CreatedDate,
                                LogId = c.LogId
                            }).ToList();

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(ActivityLogDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new ActivityLog()
                {
                    AdminId = dto.AdminId,
                    Alteration = dto.Alteration,
                    BranchId = dto.BranchId,
                    Comment = dto.Comment,
                    CreatedDate = dto.CreatedDate,
                    LogId = Guid.NewGuid()
                };

                ctx.ActivityLogs.Add(data);
                ctx.SaveChanges();
            }
        }
    }
}
