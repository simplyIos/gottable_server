﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Users
{
    [Serializable]
    public sealed class UserDal : IUserDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<UserDto>> FetchList(string query)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var userList = (from c in ctx.Users.AsQueryable()
                            where c.FirstName.Contains(query) || c.LastName.Contains(query)
                            orderby c.CreatedDate descending
                            select new UserDto()
                            {
                                PhoneNumber = c.PhoneNumber,
                                FirstName = c.FirstName,
                                GenderId = c.GenderId,
                                GenderName = ((Enumeration.Gender)c.GenderId).ToString(),
                                IsActive = c.IsActive,
                                IsEmailOptedForCommunication = c.IsEmailOptedForCommunication,
                                IsEmailVerified = c.IsEmailVerified,
                                LastName = c.LastName,
                                PrefixId = c.PrefixId,
                                PrefixName = ((Enumeration.Prefix)c.GenderId).ToString(),
                                TypeId = c.TypeId,
                                TypeName = ((Enumeration.UserType)c.TypeId).ToString(),
                                UserId = c.UserId,
                                Password = c.Password,
                                RewardCount = c.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                                DoubleTheDealBookingId = c.DoubleTheDealBookingId,
                                EmailAddress = c.EmailAddress,
                                Archeived = c.Archeived
                            }).ToList();
                return userList;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(decimal userId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Users.AsQueryable()
                            where c.UserId == userId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid userId");
                }

                return new UserDto()
                {
                    PhoneNumber = data.PhoneNumber,
                    FirstName = data.FirstName,
                    GenderId = data.GenderId,
                    GenderName = ((Enumeration.Gender)data.GenderId).ToString(),
                    IsActive = data.IsActive,
                    IsEmailOptedForCommunication = data.IsEmailOptedForCommunication,
                    IsEmailVerified = data.IsEmailVerified,
                    LastName = data.LastName,
                    PrefixId = data.PrefixId,
                    PrefixName = ((Enumeration.Prefix)data.GenderId).ToString(),
                    TypeId = data.TypeId,
                    TypeName = ((Enumeration.UserType)data.TypeId).ToString(),
                    UserId = data.UserId,
                    Password = data.Password,
                    RewardCount = data.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                    DoubleTheDealBookingId = data.DoubleTheDealBookingId,
                    EmailAddress = data.EmailAddress,
                    Archeived = data.Archeived
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(string phoneNumber, string password)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Users.AsQueryable()
                            where c.PhoneNumber == phoneNumber && c.Password == password
                            select c).SingleOrDefault();

                if (data == null)
                {
                    return null;
                }

                return new UserDto()
                {
                    PhoneNumber = data.PhoneNumber,
                    FirstName = data.FirstName,
                    GenderId = data.GenderId,
                    GenderName = string.Empty,
                    IsActive = data.IsActive,
                    IsEmailOptedForCommunication = data.IsEmailOptedForCommunication,
                    IsEmailVerified = data.IsEmailVerified,
                    LastName = data.LastName,
                    PrefixId = data.PrefixId,
                    PrefixName = string.Empty,
                    TypeId = data.TypeId,
                    TypeName = ((Enumeration.UserType)data.TypeId).ToString(),
                    UserId = data.UserId,
                    Password = data.Password,
                    RewardCount = data.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                    DoubleTheDealBookingId = data.DoubleTheDealBookingId,
                    EmailAddress = data.EmailAddress,
                    Archeived = data.Archeived
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(string phoneNumber)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Users.AsQueryable()
                            where c.PhoneNumber == phoneNumber && (c.TypeId == (int)Enumeration.UserType.EndUser || c.TypeId == (int)Enumeration.UserType.Guest)
                            && c.Archeived != true
                            select c).SingleOrDefault();

                if (data == null)
                {
                    return null;
                }

                return new UserDto()
                {
                    PhoneNumber = data.PhoneNumber,
                    FirstName = data.FirstName,
                    GenderId = data.GenderId,
                    GenderName = data.GenderId != null ? ((Enumeration.Gender)data.GenderId).ToString() : string.Empty,
                    IsActive = data.IsActive,
                    IsEmailOptedForCommunication = data.IsEmailOptedForCommunication,
                    IsEmailVerified = data.IsEmailVerified,
                    LastName = data.LastName,
                    PrefixId = data.PrefixId,
                    PrefixName = string.Empty,
                    TypeId = data.TypeId,
                    TypeName = ((Enumeration.UserType)data.TypeId).ToString(),
                    UserId = data.UserId,
                    Password = data.Password,
                    RewardCount = data.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                    DoubleTheDealBookingId = data.DoubleTheDealBookingId,
                    EmailAddress = data.EmailAddress,
                    Archeived = data.Archeived
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(UserDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new User()
                {
                    PhoneNumber = dto.PhoneNumber,
                    Password = dto.Password ?? Guid.NewGuid().ToString().Substring(1, 10),
                    GenderId = dto.GenderId,
                    IsActive = dto.IsActive,
                    IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication,
                    PrefixId = dto.PrefixId,
                    IsEmailVerified = dto.IsEmailVerified,
                    TypeId = dto.TypeId,
                    LastName = dto.LastName,
                    FirstName = dto.FirstName,
                    CreatedDate = DateTime.Now,
                    DoubleTheDealBookingId = null,
                    EmailAddress = dto.EmailAddress,
                    Archeived = dto.Archeived ?? false
                };
                ctx.Users.Add(data);
                ctx.SaveChanges();

                dto.UserId = data.UserId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(AddressDto dto)
        {
            await Task.FromResult(1);
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(UserDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Users.AsQueryable()
                            where c.UserId == dto.UserId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid userId");
                }

                data.Password = dto.Password ?? data.Password;
                data.LastName = dto.LastName ?? data.LastName;
                data.FirstName = dto.FirstName ?? data.FirstName;
                data.EmailAddress = dto.EmailAddress ?? data.EmailAddress;
                data.IsActive = dto.IsActive ?? data.IsActive;
                data.IsEmailVerified = dto.IsEmailVerified ?? data.IsEmailVerified;
                data.Archeived = dto.Archeived ?? data.Archeived;

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(AddressDto dto)
        {
            await Task.FromResult(1);
            throw new NotImplementedException();
        }
    }
}
