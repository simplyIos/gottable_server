﻿using GotTable.Dal.Reports.Restaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Reports.Restaurants
{
    [Serializable]
    public sealed class RestaurantDal : IRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantDto>> FetchExpiredList(int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from HB in ctx.HotelBranches
                            join BI in ctx.BranchInvoices on HB.BranchId equals BI.BranchId
                            where BI.IsActive == true && BI.EndDate < DateTime.Now
                            select new RestaurantDto()
                            {
                                RestaurantId = BI.BranchId,
                                Address = HB.AddressLine1 + " " + HB.AddressLine2 + " " + HB.City + " " + HB.State + " " + HB.Zipcode,
                                ExpireDate = BI.EndDate,
                                Name = HB.BranchName
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }
    }
}
