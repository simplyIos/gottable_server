//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class BranchTiming
    {
        public decimal Id { get; set; }
        public Nullable<decimal> BranchId { get; set; }
        public Nullable<int> BranchCategoryId { get; set; }
        public Nullable<int> DayId { get; set; }
        public string LunchStartTime { get; set; }
        public string LunchEndTime { get; set; }
        public string DinnerStartTime { get; set; }
        public string DinnerEndTime { get; set; }
        public Nullable<bool> IsClosed { get; set; }
    
        public virtual HotelBranch HotelBranch { get; set; }
    }
}
