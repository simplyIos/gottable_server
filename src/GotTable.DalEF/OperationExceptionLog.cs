//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class OperationExceptionLog
    {
        public System.Guid ExceptionLogId { get; set; }
        public System.DateTime DateTime { get; set; }
        public string ControllerName { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public bool IsExceptionHandled { get; set; }
        public string InnerException { get; set; }
        public string ActionName { get; set; }
    }
}
