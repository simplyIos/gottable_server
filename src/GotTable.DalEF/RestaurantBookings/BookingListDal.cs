﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings
{
    [Serializable]
    public sealed class BookingListDal : IBookingListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingInfoDto>> FetchDineInBookingList(decimal restaurantTableId, DateTime bookingDate, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var formattedBookingDate = bookingDate.Date;
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.DineInBookings.AsQueryable()
                            where d.TableId == restaurantTableId && d.BookingDate == formattedBookingDate
                            select new BookingInfoDto()
                            {
                                BookingDate = d.BookingDate,
                                BranchId = d.BranchId,
                                BookingTime = d.BookingTime,
                                Comment = d.Comment,
                                CreatedDate = d.CreatedDate,
                                CurrentStatusId = d.CurrentStatusId,
                                BookingId = d.BookingId,
                                IsRead = d.IsRead ?? false,
                                OfferDescription = d.BranchOffer.Description ?? null,
                                OfferId = d.OfferId,
                                OfferTitle = d.BranchOffer.Name ?? null,
                                PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                EmailAddress = d.EmailAddress,
                                PromoCode = d.PromoCode,
                                TableId = d.TableId,
                                UserId = d.User.UserId,
                                BookingTypeId = 1,
                                BookingTypeName = Enumeration.BookingType.DineIn.ToString(),
                                BranchName = d.HotelBranch.BranchName,
                                CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                TableName = ((Enumeration.Tables)d.BranchTable.TableId).ToString(),
                                UserName = d.User.FirstName + " " + d.User.LastName,
                                RewardPoint = d.DineInBookingBillUpload != null ? d.DineInBookingBillUpload.AuthorizedRewardPoint ?? 0 : 0
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingInfoDto>> FetchRestaurantBookingList(decimal restaurantId, DateTime bookingDate, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var formattedBookingDate = bookingDate.Date;
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.DineInBookings.AsQueryable()
                            where d.BranchId == restaurantId && d.BookingDate == formattedBookingDate
                            select new BookingInfoDto()
                            {
                                BookingDate = d.BookingDate,
                                BranchId = d.BranchId,
                                BookingTime = d.BookingTime,
                                Comment = d.Comment,
                                CreatedDate = d.CreatedDate,
                                CurrentStatusId = d.CurrentStatusId,
                                BookingId = d.BookingId,
                                IsRead = d.IsRead ?? false,
                                OfferDescription = d.BranchOffer.Description ?? null,
                                OfferId = d.OfferId,
                                OfferTitle = d.BranchOffer.Name ?? null,
                                EmailAddress = d.EmailAddress,
                                PromoCode = d.PromoCode,
                                TableId = d.TableId,
                                UserId = d.User.UserId,
                                BookingTypeId = 1,
                                BookingTypeName = Enumeration.BookingType.DineIn.ToString(),
                                BranchName = d.HotelBranch.BranchName,
                                CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                TableName = ((Enumeration.Tables)d.BranchTable.TableId).ToString(),
                                PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                UserName = d.User.FirstName + " " + d.User.LastName,
                                RewardPoint = d.DineInBookingBillUpload != null ? d.DineInBookingBillUpload.AuthorizedRewardPoint ?? 0 : 0
                            }).ToList();

                var deliveryandTakeawayData = (from d in ctx.DeliveryandTakeawayBookings.AsQueryable()
                                               where d.BranchId == restaurantId && d.BookingDate == formattedBookingDate
                                               select new BookingInfoDto()
                                               {
                                                   BookingDate = d.BookingDate,
                                                   BranchId = d.BranchId,
                                                   BookingTime = d.BookingTime,
                                                   Comment = d.Comment,
                                                   CreatedDate = d.CreatedDate,
                                                   CurrentStatusId = d.CurrentStatusId,
                                                   BookingId = d.BookingId,
                                                   IsRead = d.IsRead,
                                                   OfferDescription = d.BranchOffer.Description ?? null,
                                                   OfferId = d.OfferId,
                                                   OfferTitle = d.BranchOffer.Name ?? null,
                                                   EmailAddress = d.EmailAddress,
                                                   PromoCode = d.PromoCode,
                                                   TableId = 0,
                                                   UserId = d.UserId,
                                                   PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                                   UserName = d.User.FirstName + " " + d.User.LastName,
                                                   BookingTypeId = d.BookingTypeId,
                                                   BookingTypeName = ((Enumeration.BookingType)d.BookingTypeId).ToString(),
                                                   BranchName = d.HotelBranch.BranchName,
                                                   CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                                   TableName = string.Empty
                                               }).ToList();

                data.AddRange(deliveryandTakeawayData);
                if (bookingDate.Date == DateTime.Now.Date)
                {
                    data = data.Where(x => x.IsExpired == false).OrderBy(x => x.BookingDateTime).ToList();
                }
                else
                {
                    data = data.OrderBy(x => x.BookingDateTime).ToList();
                }
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        public async Task<List<BookingInfoDto>> FetchRestaurantBookingList(decimal restaurantId, DateTime startDate, DateTime endDate, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var formattedBookingStartDate = startDate.Date;
            var formattedBookingEndDate = endDate.Date;
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.DineInBookings.AsQueryable()
                            where d.BranchId == restaurantId && d.BookingDate >= formattedBookingStartDate && d.BookingDate <= formattedBookingEndDate
                            select new BookingInfoDto()
                            {
                                BookingDate = d.BookingDate,
                                BranchId = d.BranchId,
                                BookingTime = d.BookingTime,
                                Comment = d.Comment,
                                CreatedDate = d.CreatedDate,
                                CurrentStatusId = d.CurrentStatusId,
                                BookingId = d.BookingId,
                                IsRead = d.IsRead ?? false,
                                OfferDescription = d.BranchOffer.Description ?? null,
                                OfferId = d.OfferId,
                                OfferTitle = d.BranchOffer.Name ?? null,
                                EmailAddress = d.EmailAddress,
                                PromoCode = d.PromoCode,
                                TableId = d.TableId,
                                UserId = d.User.UserId,
                                BookingTypeId = 1,
                                BookingTypeName = Enumeration.BookingType.DineIn.ToString(),
                                BranchName = d.HotelBranch.BranchName,
                                CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                TableName = ((Enumeration.Tables)d.BranchTable.TableId).ToString(),
                                PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                UserName = d.User.FirstName + " " + d.User.LastName,
                                RewardPoint = d.DineInBookingBillUpload != null ? d.DineInBookingBillUpload.AuthorizedRewardPoint ?? 0 : 0
                            }).ToList();

                var deliveryandTakeawayData = (from d in ctx.DeliveryandTakeawayBookings.AsQueryable()
                                               where d.BranchId == restaurantId && d.BookingDate >= formattedBookingStartDate && d.BookingDate <= formattedBookingEndDate
                                               select new BookingInfoDto()
                                               {
                                                   BookingDate = d.BookingDate,
                                                   BranchId = d.BranchId,
                                                   BookingTime = d.BookingTime,
                                                   Comment = d.Comment,
                                                   CreatedDate = d.CreatedDate,
                                                   CurrentStatusId = d.CurrentStatusId,
                                                   BookingId = d.BookingId,
                                                   IsRead = d.IsRead,
                                                   OfferDescription = d.BranchOffer.Description ?? null,
                                                   OfferId = d.OfferId,
                                                   OfferTitle = d.BranchOffer.Name ?? null,
                                                   EmailAddress = d.EmailAddress,
                                                   PromoCode = d.PromoCode,
                                                   TableId = 0,
                                                   UserId = d.UserId,
                                                   PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                                   UserName = d.User.FirstName + " " + d.User.LastName,
                                                   BookingTypeId = d.BookingTypeId,
                                                   BookingTypeName = ((Enumeration.BookingType)d.BookingTypeId).ToString(),
                                                   BranchName = d.HotelBranch.BranchName,
                                                   CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                                   TableName = string.Empty,
                                                   RewardPoint = 0
                                               }).ToList();

                data.AddRange(deliveryandTakeawayData);
                if(startDate < DateTime.Now)
                {
                    data = data.Where(x => x.IsExpired == true).OrderByDescending(x => x.BookingDateTime).ToList();
                }
                else
                {
                    data = data.OrderBy(x => x.BookingDateTime).ToList();
                }
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingInfoDto>> FetchUserBookingList(decimal userId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.DineInBookings.AsQueryable()
                            where d.UserId == userId
                            orderby d.BookingId
                            select new BookingInfoDto()
                            {
                                BookingDate = d.BookingDate,
                                BranchId = d.BranchId,
                                BookingTime = d.BookingTime,
                                Comment = d.Comment,
                                CreatedDate = d.CreatedDate,
                                CurrentStatusId = d.CurrentStatusId,
                                BookingId = d.BookingId,
                                IsRead = d.IsRead ?? false,
                                OfferDescription = d.BranchOffer.Description ?? null,
                                OfferId = d.OfferId,
                                OfferTitle = d.BranchOffer.Name ?? null,
                                EmailAddress = d.EmailAddress,
                                PromoCode = d.PromoCode,
                                TableId = d.TableId,
                                UserId = d.User.UserId,
                                BookingTypeId = 1,
                                BookingTypeName = Enumeration.BookingType.DineIn.ToString(),
                                BranchName = d.HotelBranch.BranchName,
                                CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                TableName = ((Enumeration.Tables)d.BranchTable.TableId).ToString(),
                                PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                UserName = d.User.FirstName + " " + d.User.LastName,
                                RewardPoint = d.DineInBookingBillUpload != null ? d.DineInBookingBillUpload.AuthorizedRewardPoint ?? 0 : 0
                            }).ToList();

                var deliveryandTakeawayData = (from d in ctx.DeliveryandTakeawayBookings.AsQueryable()
                                               where d.UserId == userId
                                               orderby d.BookingId
                                               select new BookingInfoDto()
                                               {
                                                   BookingDate = d.BookingDate,
                                                   BranchId = d.BranchId,
                                                   BookingTime = d.BookingTime,
                                                   Comment = d.Comment,
                                                   CreatedDate = d.CreatedDate,
                                                   CurrentStatusId = d.CurrentStatusId,
                                                   BookingId = d.BookingId,
                                                   IsRead = d.IsRead,
                                                   OfferDescription = d.BranchOffer.Description ?? null,
                                                   OfferId = d.OfferId,
                                                   OfferTitle = d.BranchOffer.Name ?? null,
                                                   EmailAddress = d.EmailAddress,
                                                   PromoCode = d.PromoCode,
                                                   TableId = 0,
                                                   UserId = d.UserId,
                                                   PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                                   UserName = d.User.FirstName + " " + d.User.LastName,
                                                   BookingTypeId = d.BookingTypeId,
                                                   BookingTypeName = ((Enumeration.BookingType)d.BookingTypeId).ToString(),
                                                   BranchName = d.HotelBranch.BranchName,
                                                   CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                                   TableName = string.Empty,
                                                   RewardPoint = 0
                                               }).ToList();

                data.AddRange(deliveryandTakeawayData);
                data = data.OrderByDescending(x => x.BookingId).ToList();
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingInfoDto>> FetchUserBookingList(decimal userId, decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.DineInBookings.AsQueryable()
                            where d.UserId == userId && d.BranchId == restaurantId
                            && (d.CurrentStatusId == (int)Enumeration.DineInStatusType.Complete || d.CurrentStatusId == (int)Enumeration.DineInStatusType.AutoComplete)
                            select new BookingInfoDto()
                            {
                                BookingDate = d.BookingDate,
                                BranchId = d.BranchId,
                                BookingTime = d.BookingTime,
                                Comment = d.Comment,
                                CreatedDate = d.CreatedDate,
                                CurrentStatusId = d.CurrentStatusId,
                                BookingId = d.BookingId,
                                IsRead = d.IsRead ?? false,
                                OfferDescription = d.BranchOffer.Description ?? null,
                                OfferId = d.OfferId,
                                OfferTitle = d.BranchOffer.Name ?? null,
                                EmailAddress = d.EmailAddress,
                                PromoCode = d.PromoCode,
                                TableId = d.TableId,
                                UserId = d.User.UserId,
                                BookingTypeId = 1,
                                BookingTypeName = Enumeration.BookingType.DineIn.ToString(),
                                BranchName = d.HotelBranch.BranchName,
                                CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                TableName = ((Enumeration.Tables)d.BranchTable.TableId).ToString(),
                                PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                UserName = d.User.FirstName + " " + d.User.LastName,
                                RewardPoint = d.DineInBookingBillUpload != null ? d.DineInBookingBillUpload.AuthorizedRewardPoint ?? 0 : 0
                            }).ToList();

                var deliveryandTakeawayData = (from d in ctx.DeliveryandTakeawayBookings.AsQueryable()
                                               where d.UserId == userId && d.BranchId == restaurantId
                                               && d.CurrentStatusId == (int)Enumeration.DeliveryandTakeawayStatusType.Complete
                                               select new BookingInfoDto()
                                               {
                                                   BookingDate = d.BookingDate,
                                                   BranchId = d.BranchId,
                                                   BookingTime = d.BookingTime,
                                                   Comment = d.Comment,
                                                   CreatedDate = d.CreatedDate,
                                                   CurrentStatusId = d.CurrentStatusId,
                                                   BookingId = d.BookingId,
                                                   IsRead = d.IsRead,
                                                   OfferDescription = d.BranchOffer.Description ?? null,
                                                   OfferId = d.OfferId,
                                                   OfferTitle = d.BranchOffer.Name ?? null,
                                                   EmailAddress = d.EmailAddress,
                                                   PromoCode = d.PromoCode,
                                                   TableId = 0,
                                                   UserId = d.UserId,
                                                   PhoneNumber = d.User != null ? d.User.PhoneNumber : string.Empty,
                                                   UserName = d.User.FirstName + " " + d.User.LastName,
                                                   BookingTypeId = d.BookingTypeId,
                                                   BookingTypeName = ((Enumeration.BookingType)d.BookingTypeId).ToString(),
                                                   BranchName = d.HotelBranch.BranchName,
                                                   CurrentStatusName = ((Enumeration.DineInStatusType)d.CurrentStatusId).ToString(),
                                                   TableName = string.Empty,
                                                   RewardPoint = 0
                                               }).ToList();

                data.AddRange(deliveryandTakeawayData);
                data = data.OrderByDescending(x => x.BookingId).ToList();
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<DineInListItemDto>> FetchDineInList(decimal userId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.DineInBookings.AsQueryable()
                            where c.UserId == userId
                            select new DineInListItemDto()
                            {
                                BookingDate = c.BookingDate,
                                BookingId = c.BookingId,
                                BookingTime = c.BookingTime,
                                Comment = c.Comment,
                                CreatedDate = c.CreatedDate,
                                OfferId = c.OfferId ?? 0,
                                OfferName = c.BranchOffer.Name ?? string.Empty,
                                PromoCode = c.PromoCode ?? string.Empty,
                                CurrentStatusId = c.CurrentStatusId,
                                TableId = c.BranchTable.TableId,

                                BranchAddress = c.HotelBranch.AddressLine1 + " " + c.HotelBranch.AddressLine2,
                                BranchCity = c.HotelBranch.City,
                                BranchId = c.BranchId,
                                BranchName = c.HotelBranch.BranchName,
                                BranchState = c.HotelBranch.State,
                                BranchZipCode = c.HotelBranch.Zipcode,

                                ContactList = c.HotelBranch.BranchContacts.Select(m => new ContactListItemDto()
                                {
                                    Email = m.EmailAddress,
                                    Name = m.Name,
                                    PhoneNumber = m.PhoneNumber
                                }).ToList(),

                                BillUploadStatus = c.BillUploadStatus,
                                RewardPoint = c.DineInBookingBillUpload != null ? c.DineInBookingBillUpload.AuthorizedRewardPoint ?? 0 : 0
                            }).ToList();

                data = data.OrderBy(x => x.BookingId).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<DeliveryandTakeawayListItemDto>> FetchDeliveryandTakeawayList(decimal userId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.DeliveryandTakeawayBookings.AsQueryable()
                            where c.UserId == userId
                            select new DeliveryandTakeawayListItemDto()
                            {
                                BookingDate = c.BookingDate,
                                BookingId = c.BookingId,
                                BookingTime = c.BookingTime,

                                Comment = c.Comment,
                                CreatedDate = c.CreatedDate,
                                OfferId = c.OfferId ?? 0,
                                OfferName = c.BranchOffer.Name ?? string.Empty,
                                PromoCode = c.PromoCode ?? string.Empty,
                                CurrentStatusId = c.CurrentStatusId,
                                BookingTypeId = c.BookingTypeId,
                                CartAmount = c.CartAmount,
                                CentralGST = c.CentralGST,
                                DeliveryCharges = c.DeliveryCharges,
                                PackingCharges = c.PackingCharges,
                                ServiceCharges = c.ServiceCharges,
                                StateGST = c.StateGST,

                                BranchAddress = c.HotelBranch.AddressLine1 + " " + c.HotelBranch.AddressLine2,
                                BranchCity = c.HotelBranch.City,
                                BranchId = c.BranchId,
                                BranchName = c.HotelBranch.BranchName,
                                BranchState = c.HotelBranch.State,
                                BranchZipCode = c.HotelBranch.Zipcode,

                                Delivery = c.DeliveryAddresses.Select(m => new DeliveryItemDto()
                                {
                                    Address1 = m.AddressLine1,
                                    Address2 = m.AddressLine2,
                                    City = m.City,
                                    Latitude = m.Latitude,
                                    Longitude = m.Longitude,
                                    State = m.State,
                                    Zip = m.Zip,
                                    Landmark = m.Landmark
                                }).FirstOrDefault(),

                                CartItem = c.DeliveryandTakeawayCartItems.Select(m => new CartListItemDto()
                                {
                                    MenuName = m.BranchMenu.Name,
                                    Quantity = m.Quantity,
                                    Rate = m.BranchMenu.Cost * m.Quantity
                                }).ToList(),

                                ContactList = c.HotelBranch.BranchContacts.Select(m => new ContactListItemDto()
                                {
                                    Email = m.EmailAddress,
                                    Name = m.Name,
                                    PhoneNumber = m.PhoneNumber
                                }).ToList()
                            }).ToList();

                data = data.OrderBy(x => x.BookingId).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }
    }
}
