﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings.Rewards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings.Rewards
{
    [Serializable]
    public sealed class LoyaltyDal : ILoyaltyDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<LoyaltyDto>> FetchList(decimal restaurantId, decimal userId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from d in ctx.DineInBookings.AsQueryable()
                            where d.BranchId == restaurantId && d.UserId == userId && d.CurrentStatusId == (int)Enumeration.DineInStatusType.Complete
                            select new LoyaltyDto()
                            {
                                BookingId = d.BookingId,
                                BranchId = restaurantId,
                                UserId = d.UserId
                            }).ToList();

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(LoyaltyDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new LoyaltyRewardTransactionLog()
                {
                    BookingId = dto.BookingId,
                    BranchId = dto.BranchId,
                    CreatedDate = dto.CreatedDate,
                    UserId = dto.UserId
                };

                ctx.LoyaltyRewardTransactionLogs.Add(data);
                ctx.SaveChanges();

                dto.RewardId = data.RewardId;
            }
        }
    }
}
