﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings.DineIn;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings.DineIn
{
    [Serializable]
    public sealed class DineInDal : IDineInDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DineInDto> Fetch(decimal bookingId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.DineInBookings.AsQueryable()
                            where c.BookingId == bookingId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid Dine In booking Id");
                }

                return new DineInDto()
                {
                    BookingDate = data.BookingDate,
                    BookingTime = data.BookingTime,
                    BranchId = data.BranchId,
                    Comment = data.Comment,
                    CreatedDate = data.CreatedDate,
                    CurrentStatusId = data.CurrentStatusId,
                    Id = data.BookingId,
                    IsRead = data.IsRead,
                    OfferId = data.OfferId,
                    OfferTitle = data.BranchOffer?.Name,
                    OfferDescription = data.BranchOffer?.Description,
                    OfferTypeId = data.BranchOffer?.TypeId,
                    EmailAddress = data.EmailAddress,
                    PromoCode = data.PromoCode,
                    TableId = data.TableId,
                    UserId = data.UserId,
                    TableName = ((Enumeration.Tables)data.BranchTable.TableId).ToString(),
                    StatusList = data.DineInStatus.OrderBy(x => x.CreatedDate).Select(item => new DineInStatusDto()
                    {
                        BookingId = data.BookingId,
                        Comment = item.Comment,
                        UserId = item.UserId,
                        CreatedDate = item.CreatedDate,
                        Id = item.Id,
                        StatusName = ((Enumeration.DeliveryandTakeawayStatusType)item.StatusId).ToString(),
                        StatusId = item.StatusId,
                        UserName = item.User?.FirstName + " " + item.User?.LastName,
                        AdminName = item.Administrator?.FirstName + " " + item.Administrator?.LastName,
                        AdminId = item.AdminId
                    }).ToList(),
                    RewardApplied = data.RewardApplied ?? false,
                    BillUploadStatus = data.BillUploadStatus,
                    RewardPoint = data.DineInBookingBillUpload?.AuthorizedRewardPoint ?? 0,
                    DoubleTheDealActive = data.DoubleTheDealActive
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(DineInDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                dto.RewardApplied = CheckForReward.Execute(dto.UserId, dto.BranchId);
                dto.DoubleTheDealActive = CheckForDoubleTheDeal.Execute(dto.UserId, dto.BranchId, dto.BookingDate, dto.OfferId);

                var data = new DineInBooking()
                {
                    BookingDate = dto.BookingDate,
                    BookingTime = dto.BookingTime,
                    BranchId = dto.BranchId,
                    Comment = dto.Comment,
                    CreatedDate = dto.CreatedDate,
                    CurrentStatusId = dto.CurrentStatusId,
                    IsRead = dto.IsRead,
                    OfferId = dto.OfferId,
                    EmailAddress = dto.EmailAddress,
                    PromoCode = dto.PromoCode,
                    TableId = dto.TableId,
                    UserId = dto.UserId,
                    RewardApplied = dto.RewardApplied,
                    BillUploadStatus = (int)Enumeration.BillUpload.Pending,
                    DoubleTheDealActive = dto.DoubleTheDealActive
                };

                ctx.DineInBookings.Add(data);
                ctx.SaveChanges();

                dto.Id = data.BookingId;

                var dineInBooking = (from c in ctx.DineInBookings
                                     where c.BookingId == data.BookingId
                                     select new
                                     {
                                         c.BranchTable.TableId,
                                         c.BranchOffer.Name
                                     }).SingleOrDefault();

                dto.TableName = ((Enumeration.Tables)dineInBooking.TableId).ToString();
                dto.OfferTitle = dineInBooking.Name;

                var currentStatus = new DineInStatu()
                {
                    BookingId = data.BookingId,
                    Comment = string.Empty,
                    StatusId = (int)Enumeration.DineInStatusType.Confirm,
                    UserId = dto.UserId,
                    CreatedDate = dto.CreatedDate
                };

                ctx.DineInStatus.Add(currentStatus);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(DineInStatusDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var dataBooking = (from c in ctx.DineInBookings
                                   where c.BookingId == dto.BookingId
                                   select c).SingleOrDefault();

                if (dataBooking == null)
                {
                    throw new Exception("Invalid booking Id");
                }

                var dataStatus = new DineInStatu()
                {
                    BookingId = dto.BookingId,
                    Comment = dto.Comment,
                    CreatedDate = DateTime.Now,
                    StatusId = dto.StatusId,
                    UserId = dto.UserId,
                    AdminId = dto.AdminId,
                };
                if (dataStatus.StatusId == (int)Enumeration.DineInStatusType.AutoComplete)
                {
                    dataStatus.AdminId = dataBooking.HotelBranch.Administrator2.UserId;
                }
                dataBooking.DineInStatus.Add(dataStatus);


                dataBooking.CurrentStatusId = (int)dataStatus.StatusId;
                if (dataStatus.StatusId != (int)Enumeration.DineInStatusType.Complete)
                {
                    dataBooking.BillUploadStatus = (int)Enumeration.BillUpload.Done;
                }

                ctx.SaveChanges();
                dto.Id = dataStatus.Id;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<bool> MarkAsRead(decimal bookingId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.DineInBookings.AsQueryable()
                            where c.BookingId == bookingId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid Dine In booking Id");
                }

                data.IsRead = true;

                ctx.SaveChanges();

                return true;
            }
        }
    }
}
