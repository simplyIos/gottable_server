﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Devices
{
    [Serializable]
    public sealed class DeviceDal : IDeviceDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        public async Task<List<DeviceDto>> FetchList(Enumeration.Device deviceType)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Devices
                            where c.TypeId == (int)deviceType
                            select new DeviceDto()
                            {
                                Id = c.Id,
                                TypeId = c.TypeId,
                                IsActive = c.IsActive,
                                UserId = c.UserId,
                                City = c.City,
                                Latitude = c.Latitude,
                                Longitude = c.Longitude
                            }).ToList();

                return data;
            }
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public async Task<DeviceDto> Fetch(string deviceId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Devices
                            where c.Id == deviceId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    return null;
                }

                return new DeviceDto()
                {
                    TypeId = data.TypeId,
                    Id = data.Id,
                    IsActive = data.IsActive,
                    UserId = data.UserId,
                    City = data.City,
                    Latitude = data.Latitude,
                    Longitude = data.Longitude
                };
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(DeviceDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new Device()
                {
                    Id = dto.Id,
                    TypeId = dto.TypeId,
                    IsActive = dto.IsActive,
                    UserId = dto.UserId != 0 ? dto.UserId : null,
                    Latitude = dto.Latitude,
                    Longitude = dto.Longitude,
                    City = dto.City,
                    LastModifiedDate = DateTime.Now
                };

                ctx.Devices.Add(data);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(DeviceDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Devices.AsQueryable()
                            where c.Id == dto.Id
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid device id");
                }

                if (!string.IsNullOrEmpty(dto.Latitude))
                    data.Latitude = dto.Latitude;
                if (!string.IsNullOrEmpty(dto.Longitude))
                    data.Longitude = dto.Longitude;
                if (!string.IsNullOrEmpty(dto.City))
                    data.City = dto.City;

                data.UserId = dto.UserId != 0 ? dto.UserId : null;
                data.IsActive = dto.IsActive;
                data.LastModifiedDate = DateTime.Now;

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public async Task Delete(string deviceId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Devices.AsQueryable()
                            where c.Id == deviceId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid device id");
                }

                ctx.Devices.Remove(data);
                ctx.SaveChanges();
            }
        }
    }
}
