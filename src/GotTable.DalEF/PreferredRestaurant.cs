//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class PreferredRestaurant
    {
        public int IndexValue { get; set; }
        public decimal RestaurantId { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<decimal> CityId { get; set; }
    
        public virtual City City { get; set; }
        public virtual HotelBranch HotelBranch { get; set; }
    }
}
