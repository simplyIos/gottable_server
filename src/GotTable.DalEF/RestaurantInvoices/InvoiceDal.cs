﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantInvoices
{
    [Serializable]
    public sealed class InvoiceDal : IInvoiceDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<InvoiceDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchInvoices.AsQueryable()
                            where c.BranchId == restaurantId
                            orderby c.CreatedDate descending
                            select new InvoiceDto()
                            {
                                Amount = c.Amount,
                                BranchId = restaurantId,
                                CreatedDate = c.CreatedDate,
                                Delivery = c.Delivery,
                                DineIn = c.DineIn,
                                EndDate = c.EndDate,
                                InvoiceId = c.InvoiceId,
                                IsActive = c.IsActive,
                                IsPaymentDone = c.IsPaymentDone,
                                PaymentDate = c.PaymentDate,
                                PaymentMethodId = c.PaymentMethodId,
                                PrescriptionTypeId = c.PrescriptionTypeId,
                                StartDate = c.StartDate,
                                Takeaway = c.Takeaway,
                                PaymentMethodName = c.PaymentMethodId != null ? "" : ((Enumeration.PrescriptionTypes)c.PaymentMethodId).ToString(),
                                PrescriptionTypeName = ((Enumeration.PrescriptionTypes)c.PrescriptionTypeId).ToString(),
                                UserId = c.HotelBranch.Administrator2.UserId,
                                FirstName = c.HotelBranch.Administrator2.FirstName,
                                LastName = c.HotelBranch.Administrator2.LastName,
                                EmailAddress = c.HotelBranch.Administrator2.EmailAddress
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<InvoiceDto> Fetch(Guid invoiceId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchInvoices.AsQueryable()
                            where c.InvoiceId == invoiceId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid invoiceId");
                }

                return new InvoiceDto()
                {
                    Amount = data.Amount,
                    BranchId = data.BranchId,
                    CreatedDate = data.CreatedDate,
                    Delivery = data.Delivery,
                    DineIn = data.DineIn,
                    EndDate = data.EndDate,
                    InvoiceId = data.InvoiceId,
                    IsActive = data.IsActive,
                    IsPaymentDone = data.IsPaymentDone,
                    PaymentDate = data.PaymentDate,
                    PaymentMethodId = data.PaymentMethodId,
                    PrescriptionTypeId = data.PrescriptionTypeId,
                    StartDate = data.StartDate,
                    Takeaway = data.Takeaway,
                    PaymentMethodName = data.PaymentMethodId != null ? string.Empty : ((Enumeration.PrescriptionTypes)data.PrescriptionTypeId).ToString(),
                    PrescriptionTypeName = ((Enumeration.PrescriptionTypes)data.PrescriptionTypeId).ToString(),
                    UserId = data.HotelBranch.Administrator2.UserId,
                    FirstName = data.HotelBranch.Administrator2.FirstName,
                    LastName = data.HotelBranch.Administrator2.LastName,
                    EmailAddress = data.HotelBranch.Administrator2.EmailAddress
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<InvoiceDto> Fetch(decimal restaurantId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchInvoices.AsQueryable()
                            where c.BranchId == restaurantId && c.IsActive == true
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid invoiceId");
                }

                return new InvoiceDto()
                {
                    Amount = data.Amount,
                    BranchId = data.BranchId,
                    CreatedDate = data.CreatedDate,
                    Delivery = data.Delivery,
                    DineIn = data.DineIn,
                    EndDate = data.EndDate,
                    InvoiceId = data.InvoiceId,
                    IsActive = data.IsActive,
                    IsPaymentDone = data.IsPaymentDone,
                    PaymentDate = data.PaymentDate,
                    PaymentMethodId = data.PaymentMethodId,
                    PrescriptionTypeId = data.PrescriptionTypeId,
                    StartDate = data.StartDate,
                    Takeaway = data.Takeaway,
                    PaymentMethodName = data.PaymentMethodId != null ? string.Empty : ((Enumeration.PrescriptionTypes)data.PrescriptionTypeId).ToString(),
                    PrescriptionTypeName = ((Enumeration.PrescriptionTypes)data.PrescriptionTypeId).ToString(),
                    UserId = data.HotelBranch.Administrator2.UserId,
                    FirstName = data.HotelBranch.Administrator2.FirstName,
                    LastName = data.HotelBranch.Administrator2.LastName,
                    EmailAddress = data.HotelBranch.Administrator2.EmailAddress
                };
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(InvoiceDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var invoiceList = ctx.BranchInvoices.Where(x => x.BranchId == dto.BranchId).ToList();

                foreach (var item in invoiceList)
                {
                    item.IsActive = false;
                }

                ctx.SaveChanges();

                var data = new BranchInvoice()
                {
                    Amount = dto.Amount,
                    BranchId = dto.BranchId,
                    CreatedDate = dto.CreatedDate,
                    Delivery = dto.Delivery,
                    DineIn = dto.DineIn,
                    EndDate = dto.EndDate,
                    InvoiceId = Guid.NewGuid(),
                    IsActive = dto.IsActive,
                    IsPaymentDone = dto.IsPaymentDone,
                    PaymentDate = dto.PaymentDate,
                    PaymentMethodId = null,
                    PrescriptionTypeId = dto.PrescriptionTypeId,
                    StartDate = dto.StartDate,
                    Takeaway = dto.Takeaway
                };
                ctx.BranchInvoices.Add(data);

                ctx.SaveChanges();

                dto.InvoiceId = data.InvoiceId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(InvoiceDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchInvoices.AsQueryable()
                            where c.InvoiceId == dto.InvoiceId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid invoiceId");
                }

                data.IsPaymentDone = dto.IsPaymentDone;
                data.IsActive = dto.IsActive;
                data.PaymentDate = dto.PaymentDate;
                data.Amount = dto.Amount;
                data.Delivery = dto.Delivery;
                data.DineIn = dto.DineIn;
                data.EndDate = dto.EndDate;
                data.PaymentMethodId = dto.PaymentMethodId;
                data.StartDate = dto.StartDate;
                data.Takeaway = dto.Takeaway;

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task Delete(Guid invoiceId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchInvoices
                            where c.InvoiceId == invoiceId
                            select c).SingleOrDefault();

                if (data != null)
                {
                    ctx.BranchInvoices.Remove(data);
                }
                ctx.SaveChanges();
            }
        }
    }
}
