﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.InvoicedRestaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.InvoicedRestaurants
{
    [Serializable]
    public sealed class RestaurantListDal : IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal? salesPersonId = null, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from HB in ctx.HotelBranches.AsQueryable()
                            join BI in ctx.BranchInvoices.AsQueryable()
                            on HB.BranchId equals BI.BranchId
                            where BI.IsActive == true && HB.Deleted == false
                            orderby HB.BranchName
                            select new RestaurantInfoDto()
                            {
                                AccountAdminEmailAddress = HB.Administrator != null ? HB.Administrator.EmailAddress : string.Empty,
                                AccountAdminName = HB.Administrator != null ? HB.Administrator.FirstName + " " + HB.Administrator.LastName : string.Empty,
                                AccountAdminPersonId = HB.AccountAdminPersonId ?? 0,
                                Amount = BI.Amount,
                                City = HB.City,
                                EndDate = BI.EndDate,
                                IsActive = HB.IsActive,
                                Line1 = HB.AddressLine1,
                                Line2 = HB.AddressLine2,
                                RestaurantId = HB.BranchId,
                                RestaurantName = HB.BranchName,
                                SalesAdminEmailAddress = HB.Administrator1 != null ? HB.Administrator1.EmailAddress : string.Empty,
                                SalesAdminName = HB.Administrator1 != null ? HB.Administrator1.FirstName + " " + HB.Administrator1.LastName : string.Empty,
                                SalesPersonId = HB.SalesPersonId ?? 0,
                                StartDate = BI.StartDate,
                                State = HB.State,
                                SubscriptionTypeId = BI.PrescriptionTypeId,
                                Zip = HB.Zipcode,
                                Category = (Enumeration.RestaurantCategories)HB.CategoryId
                            }).ToList();

                if (salesPersonId != null)
                {
                    data = data.Where(x => x.SalesPersonId == salesPersonId.Value).ToList();
                }
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }
    }
}
