﻿using GotTable.Dal.Restaurants.PreferredRestaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GotTable.Common.Extensions;

namespace GotTable.DalEF.Restaurants.PreferredRestaurants
{
    [Serializable]
    public sealed class PreferredRestaurantDal : IPreferredRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(double latitude, double longitude, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var ratingSource = (from c in ctx.BranchAvgRatings
                                    select new
                                    {
                                        c.AvgRating,
                                        c.BranchId
                                    });

                var data = (from c in ctx.FindPreferredRestaurant(latitude, longitude).AsQueryable()
                            select new RestaurantInfoDto()
                            {
                                RestaurantId = c.RestaurantId,
                                Address = c.Address,
                                City = c.City,
                                Latitude = c.Latitude,
                                Longitude = c.Longitude,
                                Name = c.RestaurantName,
                                State = c.State,
                                Zipcode = c.ZipCode,
                                Distance = c.Distance,
                                SEODescription = c.SEODescription,
                                SEOKeyword = c.SEOKeyword,
                                SEOTitle = c.SEOTitle,
                                TagLine = c.TagLine,
                                Rating = ratingSource.Count() > 0 ? ratingSource.EmptyIfNull().SingleOrDefault(x => x.BranchId == c.RestaurantId).AvgRating ?? 0 : 0
                            }).ToList();
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantDto>> FetchList(decimal? cityId = null, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var query = (from c in ctx.PreferredRestaurants select c).AsQueryable();
                if (cityId != null)
                {
                    query = query.Where(x => x.CityId == cityId);
                }
                var data = (from pR in query
                            orderby pR.IndexValue
                            select new RestaurantDto()
                            {
                                RestaurantId = pR.RestaurantId,
                                Address = pR.HotelBranch.AddressLine1 + " " + pR.HotelBranch.AddressLine1,
                                CityId = pR.CityId,
                                CityName = pR.City.Name,
                                Name = pR.HotelBranch.BranchName
                            }).ToList();
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<RestaurantDto> Fetch(decimal restaurantId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.PreferredRestaurants
                            where c.RestaurantId == restaurantId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    return null;
                }

                return new RestaurantDto()
                {
                    Address = data.HotelBranch.AddressLine1 + " " + data.HotelBranch.AddressLine2,
                    RestaurantId = data.RestaurantId,
                    CityId = data.CityId,
                    CityName = data.City.Name,
                    IndexValue = data.IndexValue,
                    Name = data.HotelBranch.BranchName,
                    Status = data.Status
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(RestaurantDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var indexValue = 1;
                var restaurant = (from p in ctx.PreferredRestaurants
                                  orderby p.IndexValue descending
                                  select p).Take(1).FirstOrDefault();
                if (restaurant != null)
                {
                    indexValue = restaurant.IndexValue + 1;
                }
                var data = new PreferredRestaurant()
                {
                    CityId = dto.CityId,
                    Comment = string.Empty,
                    RestaurantId = dto.RestaurantId,
                    Status = dto.Status,
                    IndexValue = indexValue
                };
                ctx.PreferredRestaurants.Add(data);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(RestaurantDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.PreferredRestaurants
                            where c.RestaurantId == dto.RestaurantId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid preferred restaurantId");
                }

                data.Status = dto.Status;
                data.IndexValue = dto.IndexValue;

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        public async Task Delete(decimal preferredRestaurantId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                int indexer = 1;
                var preferredRestaurants = (from c in ctx.PreferredRestaurants
                                            select c).ToList();

                if (preferredRestaurants == null)
                {
                    throw new System.Exception("Invalid preferred restaurantId");
                }

                preferredRestaurants.ForEach(item =>
                {
                    if (item.RestaurantId == preferredRestaurantId)
                    {
                        ctx.PreferredRestaurants.Remove(item);
                    }
                    else
                    {
                        item.IndexValue = indexer++;
                    }
                });

                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantIds"></param>
        /// <returns></returns>
        public async Task Update(List<decimal> restaurantIds)
        {
            await Task.FromResult(1);
            var indexValue = 1;
            using (var ctx = new GotTableRepository())
            {
                var data = ctx.PreferredRestaurants.Where(x => restaurantIds.Contains(x.RestaurantId)).ToList();

                restaurantIds.ForEach(item =>
                {
                    data.Where(x => x.RestaurantId == item).SingleOrDefault().IndexValue = indexValue;
                    indexValue++;
                });

                ctx.SaveChanges();
            }
        }
    }
}
