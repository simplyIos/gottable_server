﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.ApplicationRestaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.ApplicationRestaurants
{
    [Serializable]
    public sealed class AutoCompleteDal : IAutoCompleteDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<List<AutoCompleteDto>> FetchList(string queryExpression = "", Enumeration.RestaurantAutoComplete? type = null)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                if (type == Enumeration.RestaurantAutoComplete.RestaurantName)
                {
                    return (from c in ctx.HotelBranches
                            where c.BranchName.ToLower().Contains(queryExpression.ToLower()) && c.IsActive == true
                            select new AutoCompleteDto()
                            {
                                Id = c.BranchId,
                                Name = c.BranchName
                            }).Distinct().ToList();
                }
                else if (type == Enumeration.RestaurantAutoComplete.Cuisine)
                {
                    return (from c in ctx.Cuisines
                            where c.CuisineName.ToLower().Contains(queryExpression.ToLower()) && c.IsActive == true
                            select new AutoCompleteDto()
                            {
                                Id = c.Id,
                                Name = c.CuisineName
                            }).Distinct().ToList();
                }
                else if (type == Enumeration.RestaurantAutoComplete.Location)
                {
                    var localityList = (from c in ctx.Localities
                                        where c.Name.ToLower().Contains(queryExpression.ToLower()) && c.Active == true
                                        select new AutoCompleteDto()
                                        {
                                            Id = c.Id,
                                            Name = c.Name
                                        }).ToList();

                    return localityList.Where(l => ctx.HotelBranches.Any(x => x.Deleted != true && x.IsActive == true
                                              && (x.LocalityId1 == l.Id || x.LocalityId2 == l.Id
                                              || x.LocalityId3 == l.Id || x.LocalityId4 == l.Id))).ToList();
                }

                return default;
            }
        }
    }
}
