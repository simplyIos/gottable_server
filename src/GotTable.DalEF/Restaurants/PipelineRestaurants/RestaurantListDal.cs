﻿using GotTable.Common.Enumerations;
using GotTable.Common.Extensions;
using GotTable.Dal.Restaurants.PipelineRestaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.PipelineRestaurants
{
    [Serializable]
    public sealed class RestaurantListDal : IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from HB in ctx.HotelBranches
                            where !ctx.BranchInvoices.Any(x => x.BranchId == HB.BranchId)
                            orderby HB.BranchName
                            select new RestaurantInfoDto()
                            {
                                AccountAdminEmailAddress = HB.Administrator != null ? HB.Administrator.EmailAddress : "NA",
                                AccountAdminId = HB.AccountAdminPersonId ?? 0,
                                AccountAdminName = HB.Administrator != null ? HB.Administrator.FirstName + " " + HB.Administrator.LastName : "NA",
                                Line1 = HB.AddressLine1,
                                Line2 = HB.AddressLine2,
                                City = HB.City,
                                RestaurantId = HB.BranchId,
                                RestaurantName = HB.BranchName,
                                SalesAdminEmailAddress = HB.Administrator1.EmailAddress,
                                SalesAdminPersonId = HB.Administrator1.UserId,
                                SalesAdminName = HB.Administrator1.FirstName ?? string.Empty + " " + HB.Administrator1.LastName ?? string.Empty,
                                State = HB.State,
                                Zip = HB.Zipcode,
                                Category = (Enumeration.RestaurantCategories)HB.CategoryId,
                                ContactCount = 0,
                                CuisineCount = 0,
                                MenuCount = 0,
                                TablesCount = 0,
                                TimingCount = 0,
                                OpenForAccountAdmin = HB.OpenforAccountAdmin ?? false,
                                AmenitiesCount = 0,
                                GalleryImageCount = 0
                            }).ToList();
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal salesPersonId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from HB in ctx.HotelBranches.AsQueryable()
                            where HB.SalesPersonId == salesPersonId && !ctx.BranchInvoices.Any(x => x.BranchId == HB.BranchId)
                            orderby HB.BranchName
                            select new RestaurantInfoDto()
                            {
                                AccountAdminEmailAddress = HB.Administrator != null ? HB.Administrator.EmailAddress : "NA",
                                AccountAdminId = HB.AccountAdminPersonId ?? 0,
                                AccountAdminName = HB.Administrator != null ? HB.Administrator.FirstName + " " + HB.Administrator.LastName : "NA",
                                Line1 = HB.AddressLine1,
                                Line2 = HB.AddressLine2,
                                City = HB.City,
                                RestaurantId = HB.BranchId,
                                RestaurantName = HB.BranchName,
                                SalesAdminEmailAddress = HB.Administrator1.EmailAddress,
                                SalesAdminPersonId = HB.Administrator1.UserId,
                                SalesAdminName = HB.Administrator1.FirstName ?? string.Empty + " " + HB.Administrator1.LastName ?? string.Empty,
                                State = HB.State,
                                Zip = HB.Zipcode,
                                Category = (Enumeration.RestaurantCategories)HB.CategoryId,
                                ContactCount = 0,
                                CuisineCount = 0,
                                MenuCount = 0,
                                TablesCount = 0,
                                TimingCount = 0,
                                OpenForAccountAdmin = HB.OpenforAccountAdmin ?? false
                            }).ToList();
                return data;
            }
        }
    }
}
