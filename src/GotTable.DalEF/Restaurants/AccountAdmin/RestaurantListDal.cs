﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.AccountAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.AccountAdmin
{
    [Serializable]
    public sealed class RestaurantListDal : IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountAdminPersonId"></param>
        /// <param name="searchExpression"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal accountAdminPersonId, string searchExpression, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from HB in ctx.HotelBranches.AsQueryable()
                            where HB.AccountAdminPersonId == accountAdminPersonId
                            && (!string.IsNullOrEmpty(searchExpression) ?
                                HB.BranchName.Contains(searchExpression) && HB.OpenforAccountAdmin == true && HB.Deleted == false
                                : HB.OpenforAccountAdmin == true && HB.Deleted == false)
                            orderby HB.BranchName
                            select new RestaurantInfoDto()
                            {
                                City = HB.City,
                                Line1 = HB.AddressLine1,
                                Line2 = HB.AddressLine2,
                                RestaurantId = HB.BranchId,
                                RestaurantName = HB.BranchName,
                                SalesPersonId = HB.SalesPersonId,
                                SalesAdminEmailAddress = HB.Administrator1.EmailAddress,
                                SalesAdminName = HB.Administrator1.FirstName + " " + HB.Administrator1.LastName,
                                State = HB.State,
                                Zip = HB.Zipcode,
                                IsActive = HB.IsActive ?? false,
                                OpenforAccountAdmin = HB.OpenforAccountAdmin ?? false,
                                Category = (Enumeration.RestaurantCategories)HB.CategoryId
                            }).ToList();
                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }
    }
}
