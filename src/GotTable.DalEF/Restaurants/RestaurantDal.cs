﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants
{
    [Serializable]
    public sealed class RestaurantDal : IRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal? cityId = null)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from HB in ctx.HotelBranches
                            join BI in ctx.BranchInvoices on HB.BranchId equals BI.BranchId
                            where !ctx.PreferredRestaurants.Any(d => d.RestaurantId == HB.BranchId)
                            && HB.IsActive == true && BI.IsActive == true && BI.EndDate > DateTime.Now
                            select new RestaurantInfoDto()
                            {
                                BranchId = HB.BranchId,
                                AddressLine1 = HB.AddressLine1,
                                AddressLine2 = HB.AddressLine2,
                                City = HB.City,
                                Name = HB.BranchName
                            }).ToList();

                return data;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<RestaurantDto> Fetch(decimal restaurantId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.HotelBranches.AsQueryable()
                            where c.BranchId == restaurantId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid restaurantId");
                }

                return new RestaurantDto()
                {
                    AccountAdminPersonId = data.AccountAdminPersonId,
                    AddressLine1 = data.AddressLine1,
                    AddressLine2 = data.AddressLine2,
                    BranchId = data.BranchId,
                    City = data.City,
                    IsActive = data.IsActive,
                    Latitude = data.Latitude,
                    Longitude = data.Longitude,
                    Name = data.BranchName,
                    SalesAdminPersonId = data.SalesPersonId,
                    State = data.State,
                    Zip = data.Zipcode,
                    OpenforAccountAdmin = data.OpenforAccountAdmin,

                    SalesAdminName = data.Administrator1.FirstName + " " + data.Administrator1.LastName,
                    SalesAdminEmailAddress = data.Administrator1.EmailAddress,

                    AccountAdminEmailAddress = data.Administrator != null ? data.Administrator.EmailAddress : string.Empty,
                    AccountAdminName = data.Administrator != null ? data.Administrator.FirstName + " " + data.Administrator.LastName : string.Empty,

                    RestaurantAdminPersonId = data.AdminId,
                    RestaurantAdminEmailAddress = data.Administrator2?.EmailAddress ?? string.Empty,
                    RestaurantAdminName = data.Administrator2?.FirstName + " " + data.Administrator2?.LastName,

                    ContactCount = data.BranchContacts != null ? data.BranchContacts.Count : 0,
                    CuisineCount = data.BranchCusinies != null ? data.BranchCusinies.Count : 0,
                    TableCount = data.BranchTables != null ? data.BranchTables.Count : 0,
                    TimingCount = data.BranchTimings != null ? data.BranchTimings.Count : 0,
                    GalleryImagesCount = data.BranchImages != null ? data.BranchImages.Count : 0,
                    AmenitiesCount = data.BranchAmenities != null ? data.BranchAmenities.Count : 0,

                    TagLine = data.TagLine,
                    SEOTitle = data.SEOTitle ?? string.Empty,
                    SEOKeyword = data.SEOKeyword ?? string.Empty,
                    SEODescription = data.SEODescription ?? string.Empty,
                    Category = (Enumeration.RestaurantCategories)data.CategoryId,

                    LocalityId1 = data.LocalityId1,
                    LocalityName1 = data.Locality != null ? data.Locality.Name : "",

                    LocalityId2 = data.LocalityId2,
                    LocalityName2 = data.Locality1 != null ? data.Locality1.Name : "",

                    LocalityId3 = data.LocalityId3,
                    LocalityName3 = data.Locality2 != null ? data.Locality2.Name : "",

                    LocalityId4 = data.LocalityId4,
                    LocalityName4 = data.Locality3 != null ? data.Locality3.Name : "",

                    CostForTwo = data.CostForTwo,
                    Description = data.Description
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(RestaurantDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new HotelBranch()
                {
                    AccountAdminPersonId = null,
                    AddressLine1 = dto.AddressLine1,
                    AddressLine2 = dto.AddressLine2,
                    City = dto.City,
                    IsActive = dto.IsActive,
                    Latitude = dto.Latitude,
                    Longitude = dto.Longitude,
                    SalesPersonId = dto.SalesAdminPersonId,
                    State = dto.State,
                    Zipcode = dto.Zip,
                    OpenforAccountAdmin = false,
                    BranchName = dto.Name,
                    TagLine = dto.TagLine,
                    SEODescription = dto.SEODescription ?? string.Empty,
                    SEOKeyword = dto.SEOKeyword ?? string.Empty,
                    SEOTitle = dto.SEOTitle ?? dto.Name ?? string.Empty,
                    CategoryId = (int)dto.Category,
                    LocalityId1 = dto.LocalityId1,
                    LocalityId2 = dto.LocalityId2,
                    LocalityId3 = dto.LocalityId3,
                    LocalityId4 = dto.LocalityId4,
                    CostForTwo = dto.CostForTwo,
                    Description = dto.Description,
                    Deleted = false,
                    AdminId = null
                };
                ctx.HotelBranches.Add(data);
                ctx.SaveChanges();

                dto.BranchId = data.BranchId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(RestaurantDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.HotelBranches.AsQueryable()
                            where c.BranchId == dto.BranchId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid Restaurant Id");
                }

                data.AccountAdminPersonId = dto.AccountAdminPersonId;
                data.AddressLine1 = dto.AddressLine1;
                data.AddressLine2 = dto.AddressLine2;
                data.City = dto.City;
                data.IsActive = dto.IsActive;
                data.Latitude = dto.Latitude;
                data.Longitude = dto.Longitude;
                data.SalesPersonId = dto.SalesAdminPersonId;
                data.State = dto.State;
                data.Zipcode = dto.Zip;
                data.BranchName = dto.Name;
                data.OpenforAccountAdmin = dto.OpenforAccountAdmin;
                data.TagLine = dto.TagLine ?? string.Empty;
                data.SEODescription = dto.SEODescription ?? string.Empty;
                data.SEOKeyword = dto.SEOKeyword ?? string.Empty;
                data.SEOTitle = dto.SEOTitle ?? string.Empty;
                data.CategoryId = (int)dto.Category;

                data.LocalityId1 = dto.LocalityId1;
                data.LocalityId2 = dto.LocalityId2;
                data.LocalityId3 = dto.LocalityId3;
                data.LocalityId4 = dto.LocalityId4;

                data.CostForTwo = dto.CostForTwo;
                data.Description = dto.Description;

                data.AdminId = dto.RestaurantAdminPersonId ?? data.AdminId;
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantName"></param>
        /// <param name="cityName"></param>
        /// <param name="addressLine1"></param>
        /// <returns></returns>
        public async Task<bool> IsExist(string restaurantName, string cityName, string addressLine1)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.HotelBranches
                            where c.BranchName == restaurantName && c.City == cityName && c.AddressLine1 == addressLine1
                            select c).FirstOrDefault();

                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="adminId"></param>
        /// <returns></returns>
        public async Task<RestaurantInfoDto> FetchInfo(decimal? restaurantId = null, decimal? adminId = null)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var query = (from HB in ctx.HotelBranches
                             select HB).AsQueryable();

                if (restaurantId != null)
                {
                    query = query.Where(x => x.BranchId == restaurantId);
                }
                if (adminId != null)
                {
                    query = query.Where(x => x.AdminId == adminId);
                }

                var data = (from HB in query
                            select new RestaurantInfoDto()
                            {
                                AccountAdminPersonId = HB.AccountAdminPersonId,
                                AddressLine1 = HB.AddressLine1,
                                AddressLine2 = HB.AddressLine2,
                                BranchId = HB.BranchId,
                                City = HB.City,
                                IsActive = HB.IsActive,
                                Latitude = HB.Latitude,
                                Longitude = HB.Longitude,
                                Name = HB.BranchName,
                                SalesAdminPersonId = HB.SalesPersonId,
                                State = HB.State,
                                Zip = HB.Zipcode,
                                OpenforAccountAdmin = HB.OpenforAccountAdmin,

                                SalesAdminName = HB.Administrator1.FirstName + " " + HB.Administrator1.LastName,
                                SalesAdminEmailAddress = HB.Administrator1.EmailAddress,

                                AccountAdminEmailAddress = HB.Administrator != null ? HB.Administrator.EmailAddress : string.Empty,
                                AccountAdminName = HB.Administrator != null ? HB.Administrator.FirstName + " " + HB.Administrator.LastName : string.Empty,

                                RestaurantAdminPersonId = HB.AdminId,
                                RestaurantAdminEmailAddress = HB.Administrator2 != null ? HB.Administrator2.EmailAddress : string.Empty,
                                RestaurantAdminName = HB.Administrator2 != null ? HB.Administrator2.FirstName + " " + HB.Administrator2.LastName : string.Empty,

                                TagLine = HB.TagLine,
                                SEOTitle = HB.SEOTitle ?? string.Empty,
                                SEOKeyword = HB.SEOKeyword ?? string.Empty,
                                SEODescription = HB.SEODescription ?? string.Empty,
                                Category = (Enumeration.RestaurantCategories)HB.CategoryId,

                                LocalityId1 = HB.LocalityId1,
                                LocalityName1 = HB.Locality != null ? HB.Locality.Name : "",

                                LocalityId2 = HB.LocalityId2,
                                LocalityName2 = HB.Locality1 != null ? HB.Locality1.Name : "",

                                LocalityId3 = HB.LocalityId3,
                                LocalityName3 = HB.Locality2 != null ? HB.Locality2.Name : "",

                                LocalityId4 = HB.LocalityId4,
                                LocalityName4 = HB.Locality3 != null ? HB.Locality3.Name : "",

                                CostForTwo = HB.CostForTwo,
                                Description = HB.Description,

                                DineInActive = HB.BranchInvoices.Where(x => x.IsActive == true && x.DineIn == true).FirstOrDefault() != null ?
                                               HB.BranchInvoices.Where(x => x.IsActive == true && x.DineIn == true).FirstOrDefault().DineIn ?? false : false,
                                DeliveryActive = HB.BranchInvoices.Where(x => x.IsActive == true && x.Delivery == true).FirstOrDefault() != null ?
                                                 HB.BranchInvoices.Where(x => x.IsActive == true && x.Delivery == true).FirstOrDefault().Delivery ?? false : false,
                                TakeawayActive = HB.BranchInvoices.Where(x => x.IsActive == true && x.Takeaway == true).FirstOrDefault() != null ?
                                                 HB.BranchInvoices.Where(x => x.IsActive == true && x.Takeaway == true).FirstOrDefault().Takeaway ?? false : false,
                            }).SingleOrDefault();

                return data;
            }
        }
    }
}
