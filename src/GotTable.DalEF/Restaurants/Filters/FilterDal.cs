﻿using GotTable.Common;
using GotTable.Common.Extensions;
using GotTable.Dal.Restaurants.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.Filters
{
    public sealed class FilterDal : IFilterDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<FilterDto>> FetchList()
        {
            await Task.FromResult(1);

            var list = new List<FilterDto>();

            using (var ctx = new GotTableRepository())
            {
                var cuisineList = (from c in ctx.Cuisines
                                   where c.BranchCusinies.Any(x => x.HotelBranch.IsActive == true)
                                   select new EnumDtoV2<decimal>()
                                   {
                                       Id = c.Id,
                                       Name = c.CuisineName
                                   }).ToList();

                var localtyList = (from c in ctx.Localities
                                   where c.HotelBranches.Any(x => x.IsActive == true)
                                   select new EnumDtoV2<decimal>()
                                   {
                                       Id = c.Id,
                                       Name = c.Name
                                   }).ToList();

                var offerCategoryList = (from c in ctx.OfferCategories
                                         where c.BranchOffers.Any(x => x.HotelBranch.IsActive == true)
                                         select new EnumDtoV2<decimal>()
                                         {
                                             Id = c.Id,
                                             Name = c.Name
                                         }).ToList();


                list.Add(new FilterDto()
                {
                    Index = 1,
                    List = cuisineList,
                    Name = Common.Enumerations.Enumeration.Filter.Cuisines.ToString(),
                    Key = Common.Enumerations.Enumeration.Filter.Cuisines.GetDisplayName(),
                    IconUrl = Common.Enumerations.Enumeration.Filter.Cuisines.GetPromptString()
                });

                list.Add(new FilterDto()
                {
                    Index = 2,
                    List = offerCategoryList,
                    Name = Common.Enumerations.Enumeration.Filter.OfferCategories.ToString(),
                    Key = Common.Enumerations.Enumeration.Filter.OfferCategories.GetDisplayName(),
                    IconUrl = Common.Enumerations.Enumeration.Filter.OfferCategories.GetPromptString()
                });

                list.Add(new FilterDto()
                {
                    Index = 3,
                    List = localtyList,
                    Name = Common.Enumerations.Enumeration.Filter.Localities.ToString(),
                    Key = Common.Enumerations.Enumeration.Filter.Localities.GetDisplayName(),
                    IconUrl = Common.Enumerations.Enumeration.Filter.Localities.GetPromptString()
                });
            }

            return list;
        }
    }
}
