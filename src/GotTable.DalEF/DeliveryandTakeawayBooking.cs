//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeliveryandTakeawayBooking
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeliveryandTakeawayBooking()
        {
            this.DeliveryAddresses = new HashSet<DeliveryAddress>();
            this.DeliveryandTakeawayCartItems = new HashSet<DeliveryandTakeawayCartItem>();
            this.DeliveryandTakeawayStatus = new HashSet<DeliveryandTakeawayStatu>();
        }
    
        public decimal BookingId { get; set; }
        public decimal BranchId { get; set; }
        public decimal UserId { get; set; }
        public Nullable<decimal> OfferId { get; set; }
        public int BookingTypeId { get; set; }
        public System.DateTime BookingDate { get; set; }
        public string BookingTime { get; set; }
        public string EmailAddress { get; set; }
        public int CurrentStatusId { get; set; }
        public decimal CartAmount { get; set; }
        public decimal CentralGST { get; set; }
        public decimal StateGST { get; set; }
        public decimal ServiceCharges { get; set; }
        public decimal DeliveryCharges { get; set; }
        public decimal PackingCharges { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsRead { get; set; }
        public string PromoCode { get; set; }
    
        public virtual BranchOffer BranchOffer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeliveryAddress> DeliveryAddresses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeliveryandTakeawayCartItem> DeliveryandTakeawayCartItems { get; set; }
        public virtual HotelBranch HotelBranch { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeliveryandTakeawayStatu> DeliveryandTakeawayStatus { get; set; }
        public virtual User User { get; set; }
    }
}
