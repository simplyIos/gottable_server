﻿using GotTable.Dal.RestaurantRatings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantRatings
{
    [Serializable]
    public sealed class RatingDal : IRatingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RatingDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchRatings.AsQueryable()
                            where c.BranchId == restaurantId
                            orderby c.Id descending
                            select new RatingDto()
                            {
                                BranchId = c.BranchId,
                                Comment = c.Comment,
                                CreatedDate = c.CreatedDate,
                                Id = c.Id,
                                IsActive = c.IsActive,
                                Rating = c.Rating,
                                Title = c.Title,
                                TypeId = c.User.TypeId,
                                UserId = c.User.UserId,
                                UserName = c.User.FirstName + " " + c.User.LastName,
                                UserType = string.Empty,
                                AmbienceRating = c.AmbienceRating,
                                PhoneNumber = c.User.PhoneNumber,
                                FoodRating = c.FoodRating,
                                MusicRating = c.MusicRating,
                                PriceRating = c.PriceRating,
                                ServiceRating = c.ServiceRating
                            }).ToList();

                if (data != null)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<AvgRatingDto> FetchAvgRating(decimal restaurantId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchAvgRatings
                            where c.BranchId == restaurantId
                            select c).SingleOrDefault();

                if (data != null)
                {
                    return new AvgRatingDto()
                    {
                        AmbienceRating = data.AvgAmbienceRating ?? 0,
                        FoodRating = data.AvgFoodRating ?? 0,
                        MusicRating = data.AvgMusicRating ?? 0,
                        PriceRating = data.AvgPriceRating ?? 0,
                        Rating = data.AvgRating ?? 0,
                        ServiceRating = data.AvgServiceRating ?? 0
                    };
                }
                else
                {
                    return new AvgRatingDto()
                    {
                        AmbienceRating = 0,
                        FoodRating = 0,
                        MusicRating = 0,
                        PriceRating = 0,
                        Rating = 0,
                        ServiceRating = 0
                    };
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reviewId"></param>
        /// <returns></returns>
        public async Task<RatingDto> Fetch(decimal reviewId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchRatings.AsQueryable()
                            where c.Id == reviewId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid review Id");
                }

                return new RatingDto()
                {
                    BranchId = data.BranchId,
                    Comment = data.Comment,
                    CreatedDate = data.CreatedDate,
                    Id = data.Id,
                    IsActive = data.IsActive,
                    Rating = data.Rating,
                    Title = data.Title,
                    TypeId = data.User.TypeId,
                    UserId = data.User.UserId,
                    UserName = data.User.FirstName + " " + data.User.LastName,
                    UserType = string.Empty,
                    AmbienceRating = data.AmbienceRating,
                    PhoneNumber = data.User.PhoneNumber,
                    MusicRating = data.MusicRating,
                    FoodRating = data.FoodRating,
                    PriceRating = data.PriceRating,
                    ServiceRating = data.ServiceRating
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(RatingDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new BranchRating()
                {
                    BranchId = dto.BranchId,
                    Comment = dto.Comment,
                    CreatedDate = dto.CreatedDate,
                    IsActive = dto.IsActive,
                    Rating = dto.Rating,
                    Title = dto.Title,
                    UserId = dto.UserId,
                    AmbienceRating = dto.AmbienceRating,
                    FoodRating = dto.FoodRating,
                    MusicRating = dto.MusicRating,
                    PriceRating = dto.PriceRating,
                    ServiceRating = dto.ServiceRating
                };

                ctx.BranchRatings.Add(data);
                ctx.SaveChanges();

                dto.Id = data.Id;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        public async Task Delete(decimal ratingId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.BranchRatings
                            where c.Id == ratingId
                            select c).SingleOrDefault();

                if (data != null)
                {
                    ctx.BranchRatings.Remove(data);
                }
                ctx.SaveChanges();
            }
        }
    }
}
