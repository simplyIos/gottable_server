﻿using GotTable.Dal.Security;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Security
{
    [Serializable]
    public sealed class ApplicationIdentityDal : IApplicationIdentityDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="syncName"></param>
        /// <param name="syncPassword"></param>
        /// <returns></returns>
        public async Task<ApplicationIdentityDto> Get(string syncName, string syncPassword)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Versions
                            where c.SyncName == syncName && c.SyncPassword == syncPassword && c.Active == true
                            select c).SingleOrDefault();

                if (data == null)
                {
                    return null;
                }

                return new ApplicationIdentityDto()
                {
                    SyncName = data.SyncName,
                    SyncPassword = data.SyncPassword,
                    IsActive = data.Active,
                    Id = data.VersionId,
                    Version = data.VersionNumber
                };
            }
        }
    }
}
