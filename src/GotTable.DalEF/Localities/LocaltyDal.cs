﻿using GotTable.Dal.Localities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Localities
{
    [Serializable]
    public sealed class LocaltyDal : ILocaltyDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="active"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<LocaltyDto>> FetchList(bool? active = null, int currentPage = default, int pageSize = 10, bool? engagedRestaurant = true)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var query = (from c in ctx.Localities.AsQueryable()
                             select c);

                if (active != null)
                {
                    query = query.Where(x => x.Active == active);
                }

                if (engagedRestaurant == true)
                {
                    query = query.Where(x => x.HotelBranches.Any(c => c.IsActive == true) || x.HotelBranches1.Any(c => c.IsActive == true) || x.HotelBranches2.Any(c => c.IsActive == true) || x.HotelBranches3.Any(c => c.IsActive == true));
                }

                var data = (from c in query
                            select new LocaltyDto()
                            {
                                Id = c.Id,
                                Active = c.Active,
                                Name = c.Name
                            }).ToList();

                if (data != null && currentPage != default)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(LocaltyDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new Locality()
                {
                    Name = dto.Name,
                    Active = true
                };

                ctx.Localities.Add(data);
                ctx.SaveChanges();
            }
        }
    }
}
