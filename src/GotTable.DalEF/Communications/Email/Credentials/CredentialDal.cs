﻿using GotTable.Dal.Communications.Email.Credentials;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Email.Credentials
{
    /// <summary>
    /// CredentialDal
    /// </summary>
    [Serializable]
    public sealed class CredentialDal : ICredentialDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="credentialId"></param>
        /// <returns></returns>
        public async Task<CredentialDto> Fetch(int credentialId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.EmailCredentials.AsQueryable()
                            where c.Id == credentialId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid credentialdId");
                }

                return new CredentialDto()
                {
                    DisplayName = data.DisplayName,
                    Id = data.Id,
                    IsActive = data.IsActive,
                    IsDefault = data.IsDefault,
                    IsUsedForPromotions = data.IsUsedForPromotions,
                    IsUsedForTransactions = data.IsUsedForTransactions,
                    Password = data.Password,
                    ReplyAddress = data.EmailReply_To,
                    Username = data.Username
                };
            }
        }
    }
}
