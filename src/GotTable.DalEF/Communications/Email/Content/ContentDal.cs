﻿using GotTable.Dal.Communications.Email.Content;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Email.Content
{
    /// <summary>
    /// ContentDal
    /// </summary>
    [Serializable]
    public sealed class ContentDal : IContentDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public async Task<ContentDto> Fetch(decimal templateId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.EmailContents.AsQueryable()
                            where c.TemplateTypeId == templateId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid templateId");
                }

                return new ContentDto()
                {
                    EmailBody = data.Email_Body,
                    EmailCC = data.Email_CC,
                    EmailSubject = data.Email_Subject,
                    EmailTo = data.Email_To,
                    IsActive = data.IsActive,
                    TemplateTypeId = data.TemplateTypeId
                };
            }
        }
    }
}
