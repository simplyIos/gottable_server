﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Communications.Message.OTP;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Message.OTP
{
    /// <summary>
    /// TransactionDal
    /// </summary>
    [Serializable]
    public sealed class TransactionDal : ITransactionDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public async Task<TransactionDto> Fetch(decimal transactionId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.OTPTransactions.AsQueryable()
                            where c.TransactionId == transactionId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid transactionId");
                }

                return new TransactionDto()
                {
                    CreatedDate = data.CreatedDate,
                    ExternalId = data.ExternalId,
                    IsUsed = data.IsUsed,
                    OTP = data.OTP,
                    PhoneNumber = data.PhoneNumber,
                    TransactionId = data.TransactionId,
                    TypeId = data.TypeId,
                    TypeName = ((Enumeration.OTPTransaction)data.TypeId).ToString()
                };
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(TransactionDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new OTPTransaction()
                {
                    CreatedDate = dto.CreatedDate,
                    ExternalId = dto.ExternalId,
                    OTP = dto.OTP,
                    PhoneNumber = dto.PhoneNumber,
                    TypeId = dto.TypeId,
                    IsUsed = false
                };
                ctx.OTPTransactions.Add(data);
                ctx.SaveChanges();

                dto.TransactionId = data.TransactionId;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(TransactionDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.OTPTransactions.AsQueryable()
                            where c.TransactionId == dto.TransactionId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid exceptionId");
                }

                data.IsUsed = dto.IsUsed;
                ctx.SaveChanges();
            }
        }
    }
}
