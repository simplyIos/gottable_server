﻿using GotTable.Dal.Communications.Message.Content;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Message.Content
{
    /// <summary>
    /// ContentDal
    /// </summary>
    [Serializable]
    public sealed class ContentDal : IContentDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public async Task<ContentDto> Fetch(int templateId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.TransmissionContents.AsQueryable()
                            where c.TypeId == templateId
                            select c).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("Invalid templateId");
                }

                return new ContentDto()
                {
                    ContentTypeId = data.TypeId,
                    IsActive = data.isActive ?? false,
                    Message = data.Message
                };
            }
        }
    }
}
