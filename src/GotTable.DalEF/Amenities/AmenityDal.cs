﻿using GotTable.Dal.Amenities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Amenities
{
    /// <summary>
    /// AmenityDal
    /// </summary>
    public sealed class AmenityDal : IAmenityDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<AmenityDto>> FetchList(int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Amenities
                            where c.Active == true
                            select new AmenityDto()
                            {
                                Active = c.Active,
                                Id = c.Id,
                                Name = c.Name
                            }).ToList();
                return data;
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(AmenityDto dto)
        {
            await Task.FromResult(1);
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(AmenityDto dto)
        {
            await Task.FromResult(1);
            throw new System.NotImplementedException();
        }
    }
}
