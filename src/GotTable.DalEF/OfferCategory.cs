//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GotTable.DalEF
{
    using System;
    using System.Collections.Generic;
    
    public partial class OfferCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OfferCategory()
        {
            this.BranchOffers = new HashSet<BranchOffer>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Extension { get; set; }
        public Nullable<bool> DineInActive { get; set; }
        public Nullable<bool> DeliveryActive { get; set; }
        public Nullable<bool> TakeawayActive { get; set; }
        public Nullable<bool> Deleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BranchOffer> BranchOffers { get; set; }
    }
}
