﻿using GotTable.Dal.OperationExceptionLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.OperationExceptionLogs
{
    [Serializable]
    public sealed class OperationExceptionLogDal : IOperationExceptionLogDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<OperationExceptionLogDto>> FetchList(int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from opl in ctx.OperationExceptionLogs.AsQueryable()
                            select new OperationExceptionLogDto()
                            {
                                ExceptionLogId = opl.ExceptionLogId,
                                ActionName = opl.ActionName,
                                ControllerName = opl.ControllerName,
                                Message = opl.Message,
                                StackTrace = opl.StackTrace,
                                InnerException = opl.InnerException,
                                DateTime = opl.DateTime,
                                IsExceptionHandled = opl.IsExceptionHandled
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionId"></param>
        /// <returns></returns>
        public async Task<OperationExceptionLogDto> Fetch(Guid exceptionId)
        {
            await Task.FromResult(1);
            OperationExceptionLogDto exceptionDto = new OperationExceptionLogDto();
            using (var ctx = new GotTableRepository())
            {
                var data = (from opl in ctx.OperationExceptionLogs.AsQueryable()
                            where opl.ExceptionLogId == exceptionId
                            select new OperationExceptionLogDto()
                            {
                                ExceptionLogId = opl.ExceptionLogId,
                                ActionName = opl.ActionName,
                                ControllerName = opl.ControllerName,
                                Message = opl.Message,
                                StackTrace = opl.StackTrace,
                                InnerException = opl.InnerException,
                                DateTime = opl.DateTime,
                                IsExceptionHandled = opl.IsExceptionHandled
                            }).SingleOrDefault();
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(OperationExceptionLogDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new OperationExceptionLog()
                {
                    ExceptionLogId = Guid.NewGuid(),
                    ActionName = dto.ActionName,
                    ControllerName = dto.ControllerName,
                    DateTime = DateTime.Now,
                    InnerException = dto.InnerException,
                    IsExceptionHandled = dto.IsExceptionHandled,
                    Message = dto.Message,
                    StackTrace = dto.StackTrace
                };
                ctx.OperationExceptionLogs.Add(data);
                ctx.SaveChanges();

                dto.ExceptionLogId = data.ExceptionLogId;
            }
        }
    }
}
