﻿using GotTable.Dal.Cities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Cities
{
    [Serializable]
    public sealed class CityDal : ICityDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <returns></returns>
        public async Task<List<CityInfoDto>> FetchList()
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.Cities
                            where c.Active == true
                            orderby c.Name
                            select new CityInfoDto()
                            {
                                Id = c.CityId,
                                Name = c.Name
                            }).ToList();

                return data;
            }
        }
    }
}
