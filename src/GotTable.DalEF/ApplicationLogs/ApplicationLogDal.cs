﻿using GotTable.Dal.ApplicationLogs;
using System;
using System.Threading.Tasks;

namespace GotTable.DalEF.ApplicationLogs
{
    /// <summary>
    /// IApplicationLog
    /// </summary>
    [Serializable]
    public sealed class ApplicationLogDal : IApplicationLogDal
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(ApplicationLogDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new ApplicationLog()
                {
                    DeviceId = dto.DeviceId,
                    DeviceTypeId = dto.DeviceTypeId,
                    Error = dto.Error,
                    Params = dto.Params,
                    StatusCode = dto.StatusCode,
                    Url = dto.Url,
                    UserId = dto.UserId,
                    ControllerName = dto.ControllerName
                };

                ctx.ApplicationLogs.Add(data);
                ctx.SaveChanges();
            }
        }
    }
}
