﻿using GotTable.Common.Enumerations;
using GotTable.Dal.OfferCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.OfferCategories
{
    [Serializable]
    public sealed class OfferCategoryDal : IOfferCategoryDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        public async Task<List<OfferCategoryDto>> FetchList(int currentPage = 1, int pageSize = 10, Enumeration.RestaurantTypes? restaurantType = null)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var query = (from c in ctx.OfferCategories
                             where c.BranchOffers.Any(x => x.HotelBranch.IsActive == true) && c.Deleted == false
                             select c);

                if (restaurantType != null)
                {
                    if (restaurantType == Enumeration.RestaurantTypes.DineIn)
                    {
                        query = query.Where(x => x.DineInActive == true);
                    }
                    else if (restaurantType == Enumeration.RestaurantTypes.Delivery)
                    {
                        query = query.Where(x => x.DeliveryActive == true);
                    }
                    else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
                    {
                        query = query.Where(x => x.TakeawayActive == true);
                    }
                }

                var data = (from c in ctx.OfferCategories
                            orderby c.Name
                            select new OfferCategoryDto
                            {
                                Name = c.Name,
                                Id = c.Id,
                                Active = c.Active,
                                Extension = c.Extension,
                                EngagedOffers = c.BranchOffers.Count()
                            }).ToList();

                if (data != null && currentPage > 0)
                {
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                }

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public async Task<OfferCategoryDto> Fetch(int categoryId)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.OfferCategories
                            where c.Id == categoryId
                            select new
                            {
                                c.Id,
                                c.Active,
                                c.Name,
                                c.Extension
                            }).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid offer category Id");
                }

                return new OfferCategoryDto()
                {
                    Active = data.Active,
                    Extension = data.Extension,
                    Id = data.Id,
                    Name = data.Name
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(OfferCategoryDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = new OfferCategory()
                {
                    Active = dto.Active,
                    Id = dto.Id,
                    Name = dto.Name,
                    Extension = dto.Extension
                };

                ctx.OfferCategories.Add(data);
                ctx.SaveChanges();

                dto.Id = data.Id;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(OfferCategoryDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new GotTableRepository())
            {
                var data = (from c in ctx.OfferCategories
                            where c.Id == dto.Id
                            select new OfferCategoryDto()
                            {
                                Id = c.Id,
                                Active = c.Active,
                                Name = c.Name,
                                Extension = c.Extension
                            }).SingleOrDefault();

                if (data == null)
                {
                    throw new System.Exception("Invalid offer category Id");
                }

                data.Active = dto.Active;

                ctx.SaveChanges();
            }
        }
    }
}
