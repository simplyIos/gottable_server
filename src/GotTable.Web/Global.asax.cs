﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Extensions;
using GotTable.Library.Shared;
using Hangfire;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GotTable.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// 
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(System.Web.Http.GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            _ = AppSettingKeys.Init();

            if (AppSettingKeys.HangfireFeatureEnable)
            {
                HangfireAspNet.Use(HangfireInstance.GetHangfireServers);
            }

            if (AppSettingKeys.ServiceBusFeatureEnable)
            {
                ServiceBus.Init();
            }

            #region Injecting dependecies

            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(System.Web.Http.GlobalConfiguration.Configuration);
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            ContainerExtension.Add(builder);

            var container = builder.Build();
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            #endregion
        }
    }
}