﻿using GotTable.Common.Enumerations;
using GotTable.Web.Models;
using System.Web.Mvc;

namespace GotTable.Web.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Permission : AuthorizeAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="allowedRoles"></param>
        public Permission(string allowedRoles)
        {
            AllowedRoles = allowedRoles;
        }

        /// <summary>
        /// 
        /// </summary>
        public string AllowedRoles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnAuthorization(AuthorizationContext context)
        {
            if (context == null)
            {
                throw new System.Exception("filterContext");
            }
            if (Identity.UserType != null)
            {
                bool returnStatus = Identity.UserType == Enumeration.AdminType.AccountAdmin.ToString() || Identity.UserType == Enumeration.AdminType.SalesAdmin.ToString() || Identity.UserType == Enumeration.AdminType.SuperAdmin.ToString() || Identity.UserType == Enumeration.AdminType.HotelAdmin.ToString();
                if (!returnStatus)
                {
                    throw new System.Exception("You are not authorized.");
                }

                if (!AllowedRoles.Contains(Identity.UserType))
                {
                    throw new System.Exception("You are not authroized.");
                }
                base.OnAuthorization(context);
            }
        }
    }
}