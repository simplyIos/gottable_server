﻿using GotTable.Library.OperationExceptionLogs;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace GotTable.Web.Filters
{
    /// <summary>
    /// ExceptionAttribute
    /// </summary>
    public sealed class ExceptionAttribute : Attribute, IExceptionFilter
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public ExceptionAttribute()
        {
            this.operationExceptionLog = new OperationExceptionLog();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool AllowMultiple
        {
            get { return true; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            await operationExceptionLog.New(actionExecutedContext.Exception);
        }
    }
}