﻿using GotTable.Web.Filters;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartup(typeof(GotTable.Web.Startup))]

namespace GotTable.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        [Obsolete]
        public void Configuration(IAppBuilder app)
        {
            app.UseHangfireServer();
            var options = new DashboardOptions
            {
                Authorization = new[] { new HangfireCustomAuthrizationFilter() }
            };
            app.UseHangfireDashboard("/hangfire", options);
        }
    }
}
