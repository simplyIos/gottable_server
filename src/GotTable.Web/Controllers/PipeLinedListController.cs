﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class PipeLinedListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IPipelineRestaurantList pipelineRestaurantList;


        public PipeLinedListController(IPipelineRestaurantList pipelineRestaurantList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.pipelineRestaurantList = pipelineRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal? userId = null, int currentPage = 0, int pageSize = 10)
        {
            dynamic restaurantList;
            if (userId == null)
            {
                restaurantList = await pipelineRestaurantList.Get(currentPage, pageSize);
            }
            else
            {
                restaurantList = await pipelineRestaurantList.Get((decimal)userId, currentPage, pageSize);
            }
            return View(restaurantList);
        }
    }
}
