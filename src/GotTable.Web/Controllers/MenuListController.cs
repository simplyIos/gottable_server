﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantMenus;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class MenuListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IMenuList menuList;

        public MenuListController(IMenuList menuList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.menuList = menuList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var menuDAOs = await menuList.Get(restaurantId, currentPage, pageSize);
            return View(menuDAOs);
        }
    }
}
