﻿using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class RestaurantListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IAccountAdminRestaurantList accountAdminRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ISalesAdminRestaurantList salesAdminRestaurantList;

        public RestaurantListController(IAccountAdminRestaurantList accountAdminRestaurantList, IOperationExceptionLog operationExceptionLog, ISalesAdminRestaurantList salesAdminRestaurantList) : base(operationExceptionLog)
        {
            this.accountAdminRestaurantList = accountAdminRestaurantList;
            this.salesAdminRestaurantList = salesAdminRestaurantList;
        }

        [HttpGet]
        public async Task<ActionResult> Index(decimal userId, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.RestaurantTypes? restaurantType = null, string searchExpression = "", string restaurantOwnerEmailAddress = "", bool? expiredRestaurant = null, bool? inactiveRestaurant = null, int currentPage = 0, int pageSize = 10)
        {
            ViewBag.RestaurantCategory = restaurantCategory.ToString();
            ViewBag.SearchExpression = searchExpression;
            ViewBag.RestaurantOwnerEmailAddress = restaurantOwnerEmailAddress;
            ViewBag.ExpiredRestaurant = expiredRestaurant;
            ViewBag.InactiveRestaurant = inactiveRestaurant;
            if (Identity.UserType == Enumeration.AdminType.AccountAdmin.ToString())
            {
                var restaurantList = await accountAdminRestaurantList.Get(userId, searchExpression, currentPage, pageSize);
                return View(restaurantList);
            }
            else if (Identity.UserType == Enumeration.AdminType.SalesAdmin.ToString())
            {
                var restaurantList = await salesAdminRestaurantList.Get(userId, restaurantCategory, restaurantType, searchExpression, restaurantOwnerEmailAddress, expiredRestaurant, inactiveRestaurant, currentPage, pageSize);
                ViewBag.Total = restaurantList.Count();
                ViewBag.Pub = restaurantList.Where(x => x.Category.ToString() == "Pub").Count();
                ViewBag.Premium = restaurantList.Where(x => x.Category.ToString() == "Premium").Count();
                ViewBag.Microbrewery = restaurantList.Where(x => x.Category.ToString() == "Microbrewery").Count();
                ViewBag.CasualDining = restaurantList.Where(x => x.Category.ToString() == "Casual_Dining").Count();
                return View(restaurantList);
            }
            return View();
        }
    }
}
