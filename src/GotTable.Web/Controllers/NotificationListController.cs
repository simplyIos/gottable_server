﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.PushNotifications;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class NotificationListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SalesAdmin, SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly INotificationList notificationList;

        public NotificationListController(INotificationList notificationList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.notificationList = notificationList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="scheduleDate"></param>
        /// <param name="notificationCity"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(int currentPage = 0, int pageSize = 10, DateTime? scheduleDate = null, string notificationCity = "", string notificationType = "")
        {
            decimal userId = Convert.ToDecimal(Identity.UserId);
            var notificationDAOS = await notificationList.Get(userId, scheduleDate, notificationCity, notificationType, currentPage, pageSize);
            ViewBag.NotificationType = notificationType;
            ViewBag.NotficationCity = notificationCity;
            ViewBag.ScheduleDate = scheduleDate.HasValue ? scheduleDate.Value.ToString("dd-MM-yyyy") : null;
            return View(notificationDAOS);
        }
    }
}
