﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DineIn;
using GotTable.DAO.Users;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Library.RestaurantOffers;
using GotTable.Library.Restaurants;
using GotTable.Library.RestaurantTables;
using GotTable.Web.Filters;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class DineInBookingController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDineIn dineIn;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// 
        /// </summary>
        private readonly ITableList tableList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        public DineInBookingController(IDineIn dineIn, IRestaurant restaurant, ITableList tableList, IOfferList offerList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.dineIn = dineIn;
            this.restaurant = restaurant;
            this.tableList = tableList;
            this.offerList = offerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId)
        {
            var branchDetail = await restaurant.Get(restaurantId);
            var dineInBooking = new DineInBookingBySalesAdminDAO()
            {
                BookingDateTime = DateTime.Now.ToString("MM-dd-yyyy hh:mm tt"),
                BookingId = 0,
                Comment = string.Empty,
                ConfirmationMessage = string.Empty,
                EmailAddress = string.Empty,
                ErrorMessage = string.Empty,
                FirstName = string.Empty,
                LastName = string.Empty,
                PhoneNumber = 0,
                PromoCode = string.Empty,
                RestaurantId = restaurantId,
                RestaurantName = branchDetail.BranchName + " " + branchDetail.AddressLine1 + " " + branchDetail.AddressLine2 + " " + branchDetail.City + " " + branchDetail.State + " " + branchDetail.Zip,
                SelectedOfferId = 0,
                SelectedTableId = 0,
                StatusId = 1,
                UserId = 0
            };
            ViewBag.OfferList = await offerList.Get(restaurantId, Enumeration.RestaurantTypes.DineIn, null, null, null, null, true, 0, 10);
            ViewBag.TableList = await tableList.Get(restaurantId, 0, 10);
            return View(dineInBooking);
        }

        [HttpPost]
        public async Task<ActionResult> Index(DineInBookingBySalesAdminDAO dao)
        {
            var dineInBooking = new DineInBookingDAO()
            {
                BookingDate = DateTime.ParseExact(dao.BookingDate, "MM-dd-yyyy", null),
                BookingTime = dao.BookingTime,
                BranchId = dao.RestaurantId,
                Comment = dao.Comment ?? string.Empty,
                ConfirmationMessage = string.Empty,
                CreatedDate = DateTime.Now,
                ErrorMessage = string.Empty,
                Id = 0,
                OfferId = dao.SelectedOfferId,
                TableId = dao.SelectedTableId,
                EmailAddress = dao.EmailAddress,
                UserDetail = new UserDetailDAO()
                {
                    FirstName = dao.FirstName,
                    LastName = dao.LastName,
                    PhoneNumber = dao.PhoneNumber.ToString()
                },
                UserId = 0,
                PromoCode = dao.PromoCode ?? string.Empty
            };

            await dineIn.New(dineInBooking);

            dao.ConfirmationMessage = dineInBooking.ConfirmationMessage;
            dao.ErrorMessage = dineInBooking.ErrorMessage;

            if (!dineInBooking.Status)
            {
                var branchDetail = await restaurant.Get(dineInBooking.BranchId);
                ViewBag.OfferList = await offerList.Get(dineInBooking.BranchId, Enumeration.RestaurantTypes.DineIn, null, null, null, null, true, 0, 10);
                ViewBag.TableList = await tableList.Get(dineInBooking.BranchId, 0, 10);
                dao.RestaurantId = dineInBooking.BranchId;
                dao.RestaurantName = branchDetail.BranchName + " " + branchDetail.AddressLine1 + " " + branchDetail.AddressLine2 + " " + branchDetail.City + " " + branchDetail.State + " " + branchDetail.Zip;
            }

            return View(dao);
        }
    }
}
