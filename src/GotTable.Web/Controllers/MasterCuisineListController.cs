﻿using GotTable.Library.Cuisines;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class MasterCuisineListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        public MasterCuisineListController(IOperationExceptionLog operationExceptionLog, ICuisineList cuisineList) : base(operationExceptionLog)
        {
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(int currentPage = 0, int pageSize = 10, bool status = true)
        {
            var masterCuisineList = await cuisineList.Get(currentPage, pageSize, status);
            return View(masterCuisineList);
        }
    }
}
