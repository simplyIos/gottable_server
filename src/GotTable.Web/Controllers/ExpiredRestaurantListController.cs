﻿using GotTable.DAO.Reports;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Reports;
using GotTable.Library.Reports.Criterias;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ExpiredRestaurantListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IReportList<ExpiredRestaurantDAO, ExpiredRestaurantListCriteria> reportList;

        public ExpiredRestaurantListController(IReportList<ExpiredRestaurantDAO, ExpiredRestaurantListCriteria> reportList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.reportList = reportList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(int currentPage = 0, int pageSize = 10)
        {
            var criteria = new ExpiredRestaurantListCriteria(currentPage, pageSize);
            var restaurants = await reportList.Get(criteria);
            return View(restaurants);
        }
    }
}
