﻿using GotTable.Library.OperationExceptionLogs;
using System.Web.Mvc;
using System.Web.Security;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class UserBaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public UserBaseController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        protected async override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            var exceptionId = await operationExceptionLog.New(exception);
            FormsAuthentication.SignOut();
            filterContext.Result = RedirectToAction("Index", "Error", new { @Id = exceptionId });
        }
    }
}
