﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.PushNotifications;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class NotificationDetailController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin, AccountAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IPushNotification pushNotification;

        public NotificationDetailController(IPushNotification pushNotification, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.pushNotification = pushNotification;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(int notificationId)
        {
            var model = await pushNotification.Get(notificationId);
            return View(model);
        }
    }
}
