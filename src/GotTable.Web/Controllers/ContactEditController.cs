﻿using GotTable.Common.Enumerations;
using GotTable.Common.List;
using GotTable.DAO.RestaurantContacts;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantContacts;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ContactEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchContact branchContact;

        public ContactEditController(IBranchContact branchContact, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.branchContact = branchContact;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal contactId)
        {
            BranchContactDAO branchContactDAO;
            if (contactId == 0)
            {
                var restaurantId = CurrentRestaurant.Id ?? Identity.BranchId;
                branchContactDAO = await branchContact.Create(Convert.ToDecimal(restaurantId));
            }
            else
            {
                branchContactDAO = await branchContact.Get(contactId);
            }
            ViewBag.ContactTypeList = ContactTypeList.Get(Identity.UserType != Enumeration.AdminType.HotelAdmin.ToString());
            return View(branchContactDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(BranchContactDAO model)
        {
            if (model.Id == 0)
            {
                await branchContact.New(model);
            }
            else
            {
                await branchContact.Save(model);
            }

            if (model.Status)
            {
                return RedirectToAction("Index", "ContactList", new { @restaurantId = model.BranchId });
            }
            else
            {
                ViewBag.ContactTypeList = ContactTypeList.Get(Identity.UserType != Enumeration.AdminType.HotelAdmin.ToString());
                return View(model);
            }
        }
    }
}
