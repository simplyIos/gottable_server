﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.PushNotifications;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class NotificationExceptionLogListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SalesAdmin, SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly INotificationExceptionLogList notificationExceptionLogList;

        public NotificationExceptionLogListController(INotificationExceptionLogList notificationExceptionLogList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.notificationExceptionLogList = notificationExceptionLogList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(decimal notificationId)
        {
            var model = await notificationExceptionLogList.Get(notificationId);
            return View(model);
        }
    }
}
