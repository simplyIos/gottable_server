﻿using GotTable.DAO.ApplicationSettings;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class AppSettingEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationSetting applicationSetting;

        public AppSettingEditController(IApplicationSetting applicationSetting, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.applicationSetting = applicationSetting;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var model = await applicationSetting.Get();
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(ApplicationSettingDAO model)
        {
            await applicationSetting.Save(model);
            return View(model);
        }
    }
}
