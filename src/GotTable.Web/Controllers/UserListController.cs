﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Users;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class UserListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IUserList userList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userList"></param>
        /// <param name="operationExceptionLog"></param>
        public UserListController(IUserList userList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.userList = userList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminType"></param>
        /// <param name="emailAddress"></param>
        /// <param name="adminName"></param>
        /// <param name="status"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(string query = "", bool? status = null, int currentPage = 0, int pageSize = 10)
        {
            var list = await userList.Get(query);
            return View(list);
        }
    }
}
