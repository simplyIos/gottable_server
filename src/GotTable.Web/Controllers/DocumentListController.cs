﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantDocuments;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantDocuments;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public class DocumentListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocumentList documentList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        public DocumentListController(IOperationExceptionLog operationExceptionLog, IDocumentList documentList, IDocument document) : base(operationExceptionLog)
        {
            this.documentList = documentList;
            this.document = document;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategoryId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(decimal restaurantId, Enumeration.ImageCategory? imageCategoryId = default, int currentPage = default, int pageSize = default)
        {
            var model = await documentList.Get(restaurantId, imageCategoryId, currentPage, pageSize);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentDAO"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Upload(DocumentDAO documentDAO)
        {
            await document.Post(documentDAO);
            return RedirectToAction("index", new { restaurantId = CurrentRestaurant.Id });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Delete(int documentId)
        {
            await document.Delete(documentId);
            return RedirectToAction("index", new { restaurantId = CurrentRestaurant.Id });
        }
    }
}
