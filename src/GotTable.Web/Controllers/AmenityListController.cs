﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantAmenities;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// AmenityList
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class AmenityListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, AccountAdmin, HotelAdmin";

        /// <summary>
        /// IAmenityList
        /// </summary>
        private readonly IAmenityList amenityList;

        public AmenityListController(IAmenityList amenityList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.amenityList = amenityList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var amenityDAOS = await amenityList.Get(restaurantId, currentPage, pageSize);
            return View(amenityDAOS);
        }
    }
}