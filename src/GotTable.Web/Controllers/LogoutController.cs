﻿using GotTable.Library.OperationExceptionLogs;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LogoutController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public LogoutController(IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            await Task.FromResult(1);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}
