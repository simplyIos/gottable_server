﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Administrators;
using GotTable.DAO.RestaurantSettings;
using GotTable.Library.Administrators;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class RestaurantSettingController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurantSetting restaurantSetting;

        /// <summary>
        /// 
        /// </summary>
        private readonly IAdministratorList<AdminInfoDAO, ListCriteria> administratorList;

        public RestaurantSettingController(IAdministratorList<AdminInfoDAO, ListCriteria> administratorList, IOperationExceptionLog operationExceptionLog, IRestaurantSetting restaurantSetting) : base(operationExceptionLog)
        {
            this.administratorList = administratorList;
            this.restaurantSetting = restaurantSetting;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId)
        {
            var model = await restaurantSetting.Get(restaurantId);
            var criteria = new ListCriteria(adminType: Enumeration.AdminType.AccountAdmin);
            ViewBag.UserList = await administratorList.Get(criteria);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(RestaurantSettingDAO model)
        {
            model.AdminId = decimal.Parse(Identity.UserId);
            await restaurantSetting.Save(model);
            var criteria = new ListCriteria(adminType: Enumeration.AdminType.AccountAdmin);
            ViewBag.UserList = await administratorList.Get(criteria);
            return View(model);
        }
    }
}
