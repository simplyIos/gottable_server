﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTables;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TableListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ITableList tableList;

        public TableListController(ITableList tableList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.tableList = tableList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var tableDAOs = await tableList.Get(restaurantId, currentPage, pageSize);
            return View(tableDAOs);
        }
    }
}
