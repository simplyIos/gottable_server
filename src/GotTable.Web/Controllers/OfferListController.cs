﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantOffers;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class OfferListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        public OfferListController(IOfferList offerList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.offerList = offerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="displayExpiredOffers"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, bool? displayExpiredOffers = null, int currentPage = 0, int pageSize = 10)
        {
            ViewBag.ActiveSubMenu = displayExpiredOffers == true ? "History" : "Current";
            var offerDAOs = await offerList.Get(restaurantId, null, null, null, null, displayExpiredOffers, null, currentPage, pageSize);
            return View(offerDAOs);
        }
    }
}
