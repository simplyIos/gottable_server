﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantOffers;
using GotTable.Library.OfferCategories;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantOffers;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class OfferEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchOffer branchOffer;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        public OfferEditController(IBranchOffer branchOffer, IOperationExceptionLog operationExceptionLog, ICategoryList categoryList) : base(operationExceptionLog)
        {
            this.branchOffer = branchOffer;
            this.categoryList = categoryList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(decimal offerId)
        {
            BranchOfferDAO branchOfferDAO;
            if (offerId == 0)
            {
                var restaurantId = Identity.UserType == Enumeration.AdminType.HotelAdmin.ToString() ? Identity.BranchId : CurrentRestaurant.Id;
                branchOfferDAO = await branchOffer.Create(Convert.ToDecimal(restaurantId));
            }
            else
            {
                branchOfferDAO = await branchOffer.Get(offerId);
            }
            ViewBag.CategoryList = await categoryList.Get(0, 10);
            return View(branchOfferDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchOfferDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(BranchOfferDAO branchOfferDAO)
        {
            if (branchOfferDAO.Id == 0)
            {
                await branchOffer.New(branchOfferDAO);
            }
            else
            {
                await branchOffer.Save(branchOfferDAO);
            }
            if (branchOfferDAO.Status)
            {
                return RedirectToAction("Index", "OfferList", new { restaurantId = branchOfferDAO.BranchId, displayExpiredOffers = false });
            }
            ViewBag.CategoryList = await categoryList.Get(0, 10);
            return View(branchOfferDAO);
        }
    }
}
