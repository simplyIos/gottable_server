﻿using GotTable.DAO.RestaurantInvoices;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantInvoices;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class InvoiceEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchInvoice branchInvoice;

        public InvoiceEditController(IBranchInvoice branchInvoice, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.branchInvoice = branchInvoice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(Guid invoiceId)
        {
            dynamic invoiceDAO;
            if (invoiceId == Guid.Empty)
            {
                invoiceDAO = await branchInvoice.Create(Convert.ToDecimal(CurrentRestaurant.Id));
            }
            else
            {
                invoiceDAO = await branchInvoice.Get(invoiceId);
            }
            ViewBag.SusbscriptionList = Common.List.PrescriptionMethodList.Get();
            return View(invoiceDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(BranchInvoiceDAO invoiceDAO)
        {
            await branchInvoice.New(invoiceDAO);
            if (invoiceDAO.Status)
            {
                return RedirectToAction("Index", "InvoiceList", new { restaurantId = invoiceDAO.BranchId });
            }
            ViewBag.SusbscriptionList = Common.List.PrescriptionMethodList.Get();
            return View(invoiceDAO);
        }
    }
}
