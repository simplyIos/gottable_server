﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Administrators;
using GotTable.Library.Administrators;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// HomeController
    /// </summary>
    [AllowAnonymous]
    public sealed class HomeController : Controller
    {
        /// <summary>
        /// IAdministrator
        /// </summary>
        private readonly IAdministrator<AdminDAO> administrator;

        /// <summary>
        /// IBranchStaff
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// IOperationExceptionLog
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="administrator"></param>
        /// <param name="restaurant"></param>
        /// <param name="operationExceptionLog"></param>
        public HomeController(IAdministrator<AdminDAO> administrator, IRestaurant restaurant, IOperationExceptionLog operationExceptionLog)
        {
            this.administrator = administrator;
            this.operationExceptionLog = operationExceptionLog;
            this.restaurant = restaurant;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            await Task.FromResult(1);
            if (Identity.UserId != "0" && Identity.UserId != "")
            {
                if (Identity.UserType == Enumeration.AdminType.HotelAdmin.ToString())
                {
                    return RedirectToAction("Index", "Transaction", new { @restaurantId = Identity.BranchId, @bookingDate = DateTime.Now, @bookingType = "Current" });
                }
                else if (Identity.UserType == Enumeration.AdminType.SuperAdmin.ToString())
                {
                    return RedirectToAction("Index", "InvoicedList", new { @userId = "" });
                }
                else if (Identity.UserType == Enumeration.AdminType.SalesAdmin.ToString())
                {
                    return RedirectToAction("Index", "RestaurantList", new { @userId = Identity.UserId });
                }
                else if (Identity.UserType == Enumeration.AdminType.AccountAdmin.ToString())
                {
                    return RedirectToAction("Index", "RestaurantList", new { @userId = Identity.UserId });
                }
                else
                {
                    var user = new AdminDAO()
                    {
                        EmailAddress = string.Empty,
                        Password = string.Empty,
                        StatusId = 1,
                        ErrorMessage = string.Empty
                    };
                    return View(user);
                }
            }
            else
            {
                var user = new AdminDAO()
                {
                    EmailAddress = string.Empty,
                    Password = string.Empty,
                    StatusId = 1,
                    ErrorMessage = string.Empty
                };
                return View(user);
            }
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(AdminDAO model)
        {
            model = await administrator.Get(model.EmailAddress, model.Password);
            if (model != null && model.UserId == 0)
            {
                model.ErrorMessage = "Please enter valid credentials";
                return View(model);
            }
            else
            {
                if (model.IsActive)
                {
                    string cookiesDetail = model.UserId + "~" + model.FirstName + " " + model.LastName + "~" + model.UserType + "~" + model.CityId + "~" + model.EnableButtonForRestaurantLogin;
                    if (model.UserType == Enumeration.AdminType.HotelAdmin.ToString())
                    {
                        var restaurantInfo = await restaurant.GetInfo(adminId: model.UserId);
                        cookiesDetail += "~" + restaurantInfo.BranchId + "~" + restaurantInfo.BranchName + "~" + restaurantInfo.AddressLine1 + ", " + restaurantInfo.AddressLine2 + "~" + restaurantInfo.DineInActive + "~" + restaurantInfo.DeliveryActive + "~" + restaurantInfo.TakeawayActive;
                        FormsAuthentication.SetAuthCookie(cookiesDetail, false);
                        return RedirectToAction("Index", "Transaction", new { @restaurantId = restaurantInfo.BranchId, @bookingDate = DateTime.Now, @bookingType = "Current" });
                    }
                    else if (model.UserType == Enumeration.AdminType.AccountAdmin.ToString())
                    {
                        FormsAuthentication.SetAuthCookie(cookiesDetail, false);
                        return RedirectToAction("Index", "RestaurantList", new { @userId = model.UserId });
                    }
                    else if (model.UserType == Enumeration.AdminType.SalesAdmin.ToString())
                    {
                        FormsAuthentication.SetAuthCookie(cookiesDetail, false);
                        return RedirectToAction("Index", "RestaurantList", new { @userId = model.UserId });
                    }
                    else if (model.UserType == Enumeration.AdminType.SuperAdmin.ToString())
                    {
                        FormsAuthentication.SetAuthCookie(cookiesDetail, false);
                        return RedirectToAction("Index", "InvoicedList", new { @userId = "" });
                    }
                    else
                    {
                        model.StatusId = 0;
                        model.ErrorMessage = "You are not authorized for login, Please contact support team";
                        return View(model);
                    }
                }
                else
                {
                    model.StatusId = 0;
                    model.ErrorMessage = "You are not authorized for login, Please contact support team";
                    return View(model);
                }
            }
        }

        protected async override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            var exceptionId = await operationExceptionLog.New(exception);
            FormsAuthentication.SignOut();
            filterContext.Result = RedirectToAction("Index", "Error", new { @Id = exceptionId });
        }
    }
}
