﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTimings;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TimingListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ITimingList timingList;

        public TimingListController(ITimingList timingList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.timingList = timingList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId)
        {
            var timingDAOS = await timingList.Get(restaurantId);
            return View(timingDAOS);
        }
    }
}
