﻿using GotTable.DAO.Restaurants.PreferredRestaurants;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Library.Restaurants.PreferredRestaurants;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class PreferredRestaurantsController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IPreferredRestaurantList preferredRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPreferredRestaurant preferredRestaurant;

        /// <summary>
        /// 
        /// </summary>
        private readonly IInCityRestaurantList inCityRestaurantList;

        public PreferredRestaurantsController(IPreferredRestaurantList preferredRestaurantList, IOperationExceptionLog operationExceptionLog, IPreferredRestaurant preferredRestaurant, IInCityRestaurantList inCityRestaurantList) : base(operationExceptionLog)
        {
            this.preferredRestaurantList = preferredRestaurantList;
            this.preferredRestaurant = preferredRestaurant;
            this.inCityRestaurantList = inCityRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public async Task<ActionResult> Index(decimal? cityId = null)
        {
            ViewBag.RestaurantList = await inCityRestaurantList.Get(cityId);
            var model = await preferredRestaurantList.Get(cityId, 0, 10);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public async Task<ActionResult> Add(PreferredRestaurantDAO model)
        {
            if (model.RestaurantId != 0)
            {
                await preferredRestaurant.New(model);
            }
            return RedirectToAction("Index", new { @cityId = Identity.CityId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public async Task<ActionResult> Remove(decimal preferredRestaurantId, decimal cityId)
        {
            if (preferredRestaurantId != 0)
            {
                await preferredRestaurant.Delete(preferredRestaurantId);
            }
            return RedirectToAction("Index", new { @cityId = cityId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantIds"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public async Task<JsonResult> Update(List<decimal> restaurantIds)
        {
            await preferredRestaurant.Update(restaurantIds);
            return null;
        }
    }
}
