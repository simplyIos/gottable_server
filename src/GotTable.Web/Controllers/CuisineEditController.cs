﻿using GotTable.DAO.RestaurantCuisines;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantCuisines;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class CuisineEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchCuisine branchCuisine;

        /// <summary>
        /// 
        /// </summary>
        private readonly Library.Cuisines.ICuisineList cuisineList;

        public CuisineEditController(IBranchCuisine branchCuisine, IOperationExceptionLog operationExceptionLog, Library.Cuisines.ICuisineList cuisineList) : base(operationExceptionLog)
        {
            this.branchCuisine = branchCuisine;
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal cuisineId)
        {
            BranchCuisineDAO branchCuisineDAO;
            if (cuisineId == 0)
            {
                var restaurantId = CurrentRestaurant.Id ?? Identity.BranchId;
                branchCuisineDAO = await branchCuisine.Create(Convert.ToDecimal(restaurantId));
            }
            else
            {
                branchCuisineDAO = await branchCuisine.Get(cuisineId);
            }
            ViewBag.CuisineList = await cuisineList.Get(0, 10);
            return View(branchCuisineDAO);
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(BranchCuisineDAO model)
        {
            if (model.Id == 0)
            {
                await branchCuisine.New(model);
            }
            else
            {
                await branchCuisine.Save(model);
            }
            if (model.Status)
            {
                return RedirectToAction("Index", "CuisineList", new { @restaurantId = model.BranchId });
            }
            else
            {
                ViewBag.CuisineList = await cuisineList.Get(0, 10);
                return View(model);
            }
        }
    }
}
