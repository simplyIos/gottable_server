﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TransactionController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurantBookingList restaurantBookingList;

        public TransactionController(IRestaurantBookingList restaurantBookingList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.restaurantBookingList = restaurantBookingList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <param name="bookingType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, DateTime? bookingDate, DateTime? dtStartDate, DateTime? dtEndDate, string bookingType = "", int currentPage = 0, int pageSize = 10)
        {
            if (bookingDate.HasValue)
            {
                var model = await restaurantBookingList.Get(restaurantId, (DateTime)bookingDate, currentPage, pageSize);
                ViewBag.ActiveSubMenu = bookingType;
                return View(model);
            }
            else
            {
                var model = await restaurantBookingList.Get(restaurantId, (DateTime)dtStartDate, (DateTime)dtEndDate, currentPage, pageSize);
                ViewBag.ActiveSubMenu = bookingType;
                return View(model);
            }
        }
    }
}
