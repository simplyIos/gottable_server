﻿using GotTable.DAO.Restaurants;
using GotTable.Library.Localities;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class RestaurantEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin, SuperAdmin, HotelAdmin, AccountAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILocaltyList localtyList;

        public RestaurantEditController(IRestaurant restaurant, ILocaltyList localtyList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.restaurant = restaurant;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId)
        {
            dynamic branchDetail;
            if (restaurantId == 0)
            {
                CurrentRestaurant.Delete();
                branchDetail = await restaurant.Create(Convert.ToDecimal(Identity.UserId));
            }
            else
            {
                CurrentRestaurant.Delete();
                branchDetail = await restaurant.Get(restaurantId);
                CurrentRestaurant.Set(branchDetail?.BranchId, branchDetail?.BranchName);
            }
            ViewBag.LocaltyList = await localtyList.Get(true);
            return View(branchDetail);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchDetail"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(BranchDetailDAO branchDetail)
        {
            if (branchDetail.BranchId == 0)
            {
                await restaurant.New(branchDetail);
                if (branchDetail.Status)
                {
                    CurrentRestaurant.Delete();
                    return RedirectToAction("Index", "PipeLinedList", new { @userId = Identity.UserId });
                }
                else
                {
                    return View(branchDetail);
                }
            }
            else
            {
                await restaurant.Save(branchDetail);
            }
            ViewBag.LocaltyList = await localtyList.Get(true);
            return RedirectToAction("Index", "RestaurantEdit", new { @restaurantId = branchDetail.BranchId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> ToggleRestaurant(decimal restaurantId)
        {
            var restaurantDAO = await restaurant.Get(restaurantId);
            restaurantDAO.IsActive = !restaurantDAO.IsActive;
            await restaurant.Save(restaurantDAO);
            return RedirectToAction("Index", "InvoicedList");
        }
    }
}
