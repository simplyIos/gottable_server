﻿using GotTable.Common.List;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class DeliveryEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeaway deliveryandTakeaway;

        public DeliveryEditController(IDeliveryandTakeaway deliveryandTakeaway, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.deliveryandTakeaway = deliveryandTakeaway;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="isRead"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal bookingId, bool isRead)
        {
            if (isRead)
            {
                await deliveryandTakeaway.MarkAsRead(bookingId);
            }
            var deliveryModel = await deliveryandTakeaway.Get(bookingId);
            ViewBag.DeliveryStatusList = DeliveryandTakeawayStatusList.Get();
            return View(deliveryModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> DeliveryStatus(DeliveryandTakeawayDAO Model)
        {
            var statusModel = Model.NewStatus;
            statusModel.BookingId = Model.BookingId;
            statusModel.UserId = Convert.ToDecimal(Identity.UserId);
            statusModel.Comment = Model.NewStatus.Comment ?? string.Empty;
            await deliveryandTakeaway.New(statusModel);
            return RedirectToAction("Index", new { bookingId = Model.NewStatus.BookingId, isRead = false });
        }
    }
}
