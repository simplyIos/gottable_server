﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantInvoices;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class InvoiceListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoiceList invoiceList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceList"></param>
        /// <param name="operationExceptionLog"></param>
        public InvoiceListController(IInvoiceList invoiceList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.invoiceList = invoiceList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var invoiceDAOs = await invoiceList.Get(restaurantId, currentPage, pageSize);
            return View(invoiceDAOs);
        }
    }
}
