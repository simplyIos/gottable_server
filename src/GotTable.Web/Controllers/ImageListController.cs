﻿using GotTable.DAO.RestaurantDocuments;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantDocuments;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ImageListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SalesAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        public ImageListController(IDocument document, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.document = document;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId)
        {
            var imageList = await document.Get(restaurantId);
            return View(imageList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(FileModelDAO model)
        {
            await document.Save(model);
            if (model.Status)
            {
                return RedirectToAction("Index", "ImageList", new { @restaurantId = model.RestaurantId });
            }
            return View(model);
        }
    }
}
