﻿using GotTable.Library.Cuisines;
using GotTable.Library.Localities;
using GotTable.Library.OfferCategories;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ApplicationRestaurantListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILocaltyList localtyList;

        public ApplicationRestaurantListController(IOperationExceptionLog operationExceptionLog, ICategoryList categoryList, ICuisineList cuisineList, ILocaltyList localtyList) : base(operationExceptionLog)
        {
            this.categoryList = categoryList;
            this.cuisineList = cuisineList;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            ViewBag.CategoryList = await categoryList.Get();
            ViewBag.CuisineList = await cuisineList.Get(currentPage: 0, pageSize: 10);
            ViewBag.LocaltyList = await localtyList.Get(true);
            return View();
        }
    }
}