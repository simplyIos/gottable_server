﻿using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class BillUploadListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInBookingBillUploadList dineInBookingBillUploadList;

        public BillUploadListController(IDineInBookingBillUploadList dineInBookingBillUploadList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.dineInBookingBillUploadList = dineInBookingBillUploadList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadStatus"></param>
        /// <param name="emailAddress"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(Enumeration.BillUpload? uploadStatus = Enumeration.BillUpload.InProcessing, string emailAddress = "", string phoneNumber = "", int currentPage = 0, int pageSize = 10)
        {
            ViewBag.EmailAddress = emailAddress;
            ViewBag.PhoneNumber = phoneNumber;
            ViewBag.UploadStatus = uploadStatus?.ToString();
            var model = await dineInBookingBillUploadList.Get(uploadStatus, emailAddress, phoneNumber, currentPage, pageSize);
            return View(model);
        }
    }
}
