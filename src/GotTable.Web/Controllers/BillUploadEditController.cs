﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantBookings.BillUploads;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class BillUploadEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInBookingBillUpload dineInBookingBillUpload;

        public BillUploadEditController(IDineInBookingBillUpload dineInBookingBillUpload, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.dineInBookingBillUpload = dineInBookingBillUpload;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(int bookingId = 0)
        {
            var model = await dineInBookingBillUpload.Get(bookingId);
            model.AdminId = decimal.Parse(Identity.UserId);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(DineInBookingBillUploadDAO model)
        {
            await dineInBookingBillUpload.Save(model);
            if (model.Status)
            {
                return RedirectToAction("Index", "BillUploadList", new { userId = Identity.UserId, uploadStatus = Enumeration.BillUpload.Pending });
            }
            model.Authorized = null;
            return View(model);
        }
    }
}
