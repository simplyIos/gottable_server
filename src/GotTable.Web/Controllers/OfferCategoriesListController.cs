﻿using GotTable.DAO.OfferCategories;
using GotTable.Library.OfferCategories;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class OfferCategoriesListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferCategory offerCategory;

        public OfferCategoriesListController(ICategoryList categoryList, IOfferCategory offerCategory, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.categoryList = categoryList;
            this.offerCategory = offerCategory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(int currentPage = default, int pageSize = default)
        {
            var categoryDAOs = await categoryList.Get(currentPage, pageSize);
            return View(categoryDAOs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDAO"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Save(OfferCategoryDAO categoryDAO)
        {
            await offerCategory.Save(categoryDAO);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerCategoryId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Update(int offerCategoryId)
        {
            await offerCategory.Update(offerCategoryId);
            return RedirectToAction("Index");
        }
    }
}
