﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantRatings;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class RatingListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IRatingList ratingList;

        public RatingListController(IRatingList ratingList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.ratingList = ratingList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var ratingDAOs = await ratingList.Get(restaurantId, currentPage, pageSize);
            return View(ratingDAOs);
        }
    }
}
