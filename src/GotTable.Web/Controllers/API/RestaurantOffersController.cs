﻿using GotTable.API.Model.RestaurantOffers;
using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantOffers;
using GotTable.Library.RestaurantOffers;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantOffersController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerList"></param>
        public RestaurantOffersController(IOfferList offerList)
        {
            this.offerList = offerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resturantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="offerType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(decimal? resturantId = null, Enumeration.RestaurantTypes? restaurantType = null, Enumeration.OfferType? offerType = null, int currentPage = 0, int pageSize = 10)
        {
            if (resturantId == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { errorMessage = "Missing restaurantId" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            var offerDAOs = await offerList.Get(resturantId, restaurantType, null, offerType, null, null, true, currentPage, pageSize);
            var model = MapObjectToModel(offerDAOs, offerType);
            return Request.CreateResponse(HttpStatusCode.OK, new { model = model }, JsonMediaTypeFormatter.DefaultMediaType);
        }

        #region Helpers method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerList"></param>
        /// <param name="offerType"></param>
        /// <returns></returns>
        internal List<OfferListModel> MapObjectToModel(List<BranchOfferDAO> offerList, Enumeration.OfferType? offerType)
        {
            var model = new List<OfferListModel>();

            if (offerType == null)
            {
                model.Add(new OfferListModel()
                {
                    Title = Enumeration.OfferType.Todays.ToString(),
                    List = offerList.Where(x => x.IsActive == true && x.TypeId == (int)Enumeration.OfferType.Todays).Select(item => new OfferModel()
                    {
                        Description = item.Description,
                        EndDate = item.EndDate.ToString(),
                        Name = item.Name,
                        OfferId = item.Id.ToString(),
                        StartDate = item.StartDate.ToString(),
                        Tag = string.Empty
                    }).ToList()
                });
                model.Add(new OfferListModel()
                {
                    Title = Enumeration.OfferType.Special.ToString(),
                    List = offerList.Where(x => x.IsActive == true && x.TypeId == (int)Enumeration.OfferType.Special).Select(item => new OfferModel()
                    {
                        Description = item.Description,
                        EndDate = item.EndDate.ToString(),
                        Name = item.Name,
                        OfferId = item.Id.ToString(),
                        StartDate = item.StartDate.ToString(),
                        Tag = string.Empty
                    }).ToList()
                });
            }
            else
            {
                model.Add(new OfferListModel()
                {
                    Title = offerType.ToString(),
                    List = offerList.Where(x => x.IsActive == true && x.TypeId == (int)offerType).Select(item => new OfferModel()
                    {
                        Description = item.Description,
                        EndDate = item.EndDate.ToString(),
                        Name = item.Name,
                        OfferId = item.Id.ToString(),
                        StartDate = item.StartDate.ToString(),
                        Tag = string.Empty
                    }).ToList()
                });
            }
            return model;
        }

        #endregion
    }
}
