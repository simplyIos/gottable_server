﻿using GotTable.API.Model.RestaurantCategories;
using GotTable.Common.Models;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantCategoryListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public RestaurantCategoryListController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var model = MapListToModel(Common.List.RestaurantCategoryList.Get());
                return Request.CreateResponse(HttpStatusCode.OK, new { list = model }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { errorMessage = exception.Message, list = "" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        #region Helper method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal List<RestaurantCategoryModel> MapListToModel(List<EnumDto> list)
        {
            var returnlist = new List<RestaurantCategoryModel>();
            foreach (var item in list)
            {
                returnlist.Add(new RestaurantCategoryModel()
                {
                    Id = item.Id,
                    Name = item.Name.Replace("_", " ")
                });
            }
            return returnlist;
        }

        #endregion
    }
}
