﻿using AttributeRouting.Web.Http;
using GotTable.API.Model.Restaurants;
using GotTable.DAO.Restaurants;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class FilterListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IFilterList filterList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public FilterListController(IFilterList filterList, IOperationExceptionLog operationExceptionLog)
        {
            this.filterList = filterList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpRoute("api/filter/V1/Get")]
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var model = await filterList.Get();
                return Ok(MapObjectToModel(model));
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return InternalServerError(exception);
            }
        }

        #region Helpers method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dAOs"></param>
        /// <returns></returns>
        private List<FilterModel> MapObjectToModel(List<FilterDAO> dAOs)
        {
            return dAOs.Select(x => new FilterModel()
            {
                Name = x.Name,
                Index = x.Index,
                Key = x.Key,
                IconUrl = x.IconUrl,
                List = x.List.Select(z => new FilterItemModel<decimal>()
                {
                    Id = z.Id,
                    Name = z.Name
                }).ToList()
            }).ToList();
        }

        #endregion
    }
}
