﻿using GotTable.Library.Users;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CheckUserController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        public CheckUserController(IUser user)
        {
            this.user = user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Missing phone number" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            var userDAO = await user.Get(phoneNumber);
            if (userDAO != null && userDAO.UserId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "INVALID_USER" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (userDAO.UserType.ToLower() != "enduser")
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "INVALID_USER" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "VALID_USER" }, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
