﻿using GotTable.API.Model.RestaurantBookings.BillUploads;
using GotTable.DAO.RestaurantBookings.BillUploads;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class BillUploadController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInBookingBillUpload dineInBookingBillUpload;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public BillUploadController(IDineInBookingBillUpload dineInBookingBillUpload, IOperationExceptionLog operationExceptionLog)
        {
            this.dineInBookingBillUpload = dineInBookingBillUpload;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody] BillUploadModel model)
        {
            try
            {
                var dineInBookingBillUploadDAO = new DineInBookingBillUploadDAO()
                {
                    Amount = model.Amount,
                    BookingId = model.BookingId,
                    UserId = model.UserId,
                    ImagePath = model.BillPath
                };

                await dineInBookingBillUpload.New(dineInBookingBillUploadDAO);

                if (dineInBookingBillUploadDAO.Status)
                {
                    model.Status = true;
                    model.SuccessMessage = "Bill uploaded successfully.";
                    return Request.CreateResponse(HttpStatusCode.OK, model, JsonMediaTypeFormatter.DefaultMediaType);
                }
                else
                {
                    model.Status = false;
                    model.ErrorMessage = dineInBookingBillUploadDAO.ErrorMessage;
                    return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                model.ErrorMessage = "There is some issue while processing your request, Please try after some time.";
                model.SuccessMessage = string.Empty;
                model.Status = false;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, model, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
