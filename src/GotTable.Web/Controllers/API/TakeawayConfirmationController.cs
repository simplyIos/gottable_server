﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantBookings;
using GotTable.API.Model.RestaurantBookings.DeliveryandTakeaway;
using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Library.RestaurantMenus;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class TakeawayConfirmationController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeaway deliveryandTakeaway;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchMenu branchMenu;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deliveryandTakeaway"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="branchMenu"></param>
        /// <param name="oTPCommand"></param>
        public TakeawayConfirmationController(IDeliveryandTakeaway deliveryandTakeaway, IOperationExceptionLog operationExceptionLog, IBranchMenu branchMenu, IOTPCommand oTPCommand)
        {
            this.deliveryandTakeaway = deliveryandTakeaway;
            this.operationExceptionLog = operationExceptionLog;
            this.branchMenu = branchMenu;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Post(Request<DeliveryandTakeawayBookingModel> request)
        {
            var bookingModel = new BookingResponse
            {
                BookingId = "0",
                BookingType = Enumeration.BookingType.Takeaway.ToString(),
                ConfirmationMessage = string.Empty,
                ErrorMessage = string.Empty
            };

            if (string.IsNullOrEmpty(request.Detail.TransactionId))
            {
                bookingModel.ErrorMessage = "Invalid request, missing transactionId";
                return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                bookingModel.ErrorMessage = "Invalid request, missing OTP number";
                return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
            }

            try
            {
                var otpDetail = await oTPCommand.Get(Convert.ToDecimal(request.Detail.TransactionId));
                if (otpDetail == null)
                {
                    bookingModel.ErrorMessage = "Invalid OTP";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (otpDetail.TransactionId == 0)
                {
                    bookingModel.ErrorMessage = "Invalid OTP";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (otpDetail.IsUsed)
                {
                    bookingModel.ErrorMessage = "This OTP is already used";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (otpDetail.OTP != request.Detail.OtpNumber)
                {
                    bookingModel.ErrorMessage = "Invalid OTP";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
                }

                var dao = new DeliveryandTakeawayDAO()
                {
                    BookingDate = DateTime.ParseExact(request.Detail.BookingDate, "MM-dd-yyyy", null),
                    BookingTime = request.Detail.BookingTime,
                    BookingId = request.Detail.BookingId == "" ? 0 : decimal.Parse(request.Detail.BookingId),
                    BookingTypeId = (int)Enumeration.BookingType.Takeaway,
                    BranchId = request.Detail.RestaurantId == "" ? 0 : decimal.Parse(request.Detail.RestaurantId),
                    OfferId = request.Detail.OfferId == "" ? 0 : decimal.Parse(request.Detail.OfferId),
                    EmailAddress = request.Detail.EmailAddress.ToString(),
                    StateGST = String.IsNullOrEmpty(request.Detail.StateGst) ? 0 : decimal.Parse(request.Detail.StateGst),
                    CentralGST = String.IsNullOrEmpty(request.Detail.CentralGst) ? 0 : decimal.Parse(request.Detail.CentralGst),
                    UserId = String.IsNullOrEmpty(request.Detail.UserId) ? 0 : decimal.Parse(request.Detail.UserId),
                    CartItem = await MapObject2Model(request.Detail.CartItem),
                    PromoCode = request.Detail.PromoCode ?? string.Empty,
                    UserDetail = new UserDetailDAO()
                    {
                        FirstName = request.Detail.FirstName,
                        LastName = request.Detail.LastName,
                        PhoneNumber = request.Detail.PhoneNumber.ToString(),
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        UserType = Enumeration.UserType.Guest.ToString(),
                        IsActive = true
                    }
                };

                await deliveryandTakeaway.New(dao);

                if (!dao.Status)
                {
                    bookingModel.ErrorMessage = dao.ErrorMessage;
                    return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
                }
                bookingModel.BookingId = dao.BookingId.ToString("G29");
                bookingModel.ConfirmationMessage = dao.ConfirmationMessage;
                otpDetail.IsUsed = true;
                await oTPCommand.Save(otpDetail);
                return Request.CreateResponse(HttpStatusCode.OK, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception ee)
            {
                bookingModel.ErrorMessage = ee.Message.ToString();
                await operationExceptionLog.New(ee);
                return Request.CreateResponse(HttpStatusCode.BadRequest, bookingModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task<List<DeliveryandTakeawayCartItemDAO>> MapObject2Model(List<CartModel> request)
        {
            var model = new List<DeliveryandTakeawayCartItemDAO>();
            foreach (var cartItem in request)
            {
                var menuItem = await branchMenu.Get(decimal.Parse(cartItem.MenuId));
                model.Add(new DeliveryandTakeawayCartItemDAO()
                {
                    MenuId = string.IsNullOrEmpty(cartItem.MenuId) ? 0 : decimal.Parse(cartItem.MenuId),
                    Quantity = cartItem.Quantity,
                    ItemId = 0,
                    MenuName = menuItem.Name,
                    Price = menuItem.Cost
                });
            }
            return model;
        }
    }
}
