﻿using GotTable.API.Model.Categories;
using GotTable.API.Model.Cuisines;
using GotTable.API.Model.Cuisines.V2;
using GotTable.Library.Cuisines;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class MasterCuisinesController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public MasterCuisinesController(ICuisineList cuisineList, IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(int currentPage = 0, int pageSize = 10)
        {
            var cuisineModel = new CuisinesListResponse<List<CategoryModel>>
            {
                CuisinesList = new List<CategoryModel>(),
                ErrorMessage = string.Empty
            };
            try
            {
                var masterCuisineList = await cuisineList.Get(currentPage, pageSize);
                var cuisineDAOs = masterCuisineList.Where(x => x.CategoryId == 1 && x.DisplayLevel == 1 && x.EngagedRestaurant != 0).ToList();
                foreach (var item in cuisineDAOs)
                {
                    cuisineModel.CuisinesList.Add(new CategoryModel()
                    {
                        Id = item.Id.ToString("G29"),
                        Name = item.CuisineName,
                        List = new List<CuisineType1>()
                    });
                }

                cuisineDAOs = null;
                cuisineDAOs = masterCuisineList.Where(x => x.CategoryId == 1 && x.DisplayLevel == 2 && x.EngagedRestaurant != 0).OrderBy
                    (x => x.CuisineName).ToList();
                if (cuisineDAOs != null)
                {
                    var categoryItem = new CategoryModel()
                    {
                        Id = "0",
                        Name = "Regional",
                        List = cuisineDAOs.Select(m => new CuisineType1()
                        {
                            Id = m.Id.ToString("G29"),
                            Name = m.CuisineName
                        }).ToList()
                    };
                    cuisineModel.CuisinesList.Add(categoryItem);
                }

                cuisineDAOs = null;
                cuisineDAOs = masterCuisineList.Where(x => x.CategoryId == 2 && x.DisplayLevel == 3 && x.EngagedRestaurant != 0).OrderBy(x => x.CuisineName).ToList();

                if (cuisineDAOs != null)
                {
                    var categoryItem = new CategoryModel
                    {
                        Id = "0",
                        Name = "International",
                        List = new List<CuisineType2>()
                    };

                    categoryItem.List.Add(new CuisineType2()
                    {
                        Id = "0",
                        Name = "Asian",
                        List = cuisineDAOs.Select(m => new CuisineType1()
                        {
                            Id = m.Id.ToString("G29"),
                            Name = m.CuisineName,
                        }).ToList()
                    });

                    cuisineDAOs = null;
                    cuisineDAOs = masterCuisineList.Where(x => x.CategoryId == 2 && x.DisplayLevel == 1 && x.EngagedRestaurant != 0).OrderBy(x => x.CuisineName).ToList();

                    foreach (var item in cuisineDAOs)
                    {
                        categoryItem.List.Add(new CuisineType2()
                        {
                            Id = item.Id.ToString("G29"),
                            List = new List<CuisineType1>(),
                            Name = item.CuisineName
                        });
                    }

                    cuisineModel.CuisinesList.Add(categoryItem);
                }
                return Request.CreateResponse(HttpStatusCode.OK, cuisineModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception exceptionLog)
            {
                cuisineModel.CuisinesList = null;
                await operationExceptionLog.New(exceptionLog);
                cuisineModel.ErrorMessage = exceptionLog.Message;
                return Request.CreateResponse(HttpStatusCode.BadRequest, cuisineModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
