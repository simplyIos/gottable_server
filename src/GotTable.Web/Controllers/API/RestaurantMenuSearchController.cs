﻿using GotTable.API.Model.Images;
using GotTable.API.Model.RestaurantMenus;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantMenus;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantMenuSearchController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMenuList menuList;

        public RestaurantMenuSearchController(IOperationExceptionLog operationExceptionLog, IMenuList menuList)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.menuList = menuList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType = null, int currentPage = 0, int pageSize = 10)
        {
            var menuModel = new ResturantMenuModel<List<MenuModel>>();
            if (restaurantId == 0)
            {
                menuModel.ErrorMessage = "missing restaurantId";
                return Request.CreateResponse(HttpStatusCode.BadRequest, menuModel, JsonMediaTypeFormatter.DefaultMediaType);
            }

            if (restaurantType == null && restaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                menuModel.ErrorMessage = "missing restaurantType";
                return Request.CreateResponse(HttpStatusCode.BadRequest, menuModel, JsonMediaTypeFormatter.DefaultMediaType);
            }

            menuModel.List = new List<MenuModel>();
            var menuDAOs = await menuList.Get(restaurantId, restaurantType, currentPage, pageSize);
            foreach (var menuItem in menuDAOs)
            {
                menuModel.List.Add(new MenuModel()
                {
                    CategoryName = menuItem.SelectedCategory,
                    MenuId = menuItem.Id.ToString("G29"),
                    Name = menuItem.Name,
                    Price = menuItem.Cost,
                    Description = menuItem.Description,
                    Logo = new ImageModel()
                    {
                        Name = menuItem.MenuLogo.Name,
                        Url = menuItem.MenuLogo.Url ?? "http://67.209.123.59/images/logo.png"
                    }
                });
            }

            menuModel.ErrorMessage = string.Empty;
            return Request.CreateResponse(HttpStatusCode.OK, menuModel, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
