﻿using GotTable.API.Model.PreferredRestaurants;
using GotTable.DAO.Restaurants.PreferredRestaurants;
using GotTable.Library.RestaurantDocuments;
using GotTable.Library.Restaurants;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PreferredRestaurantController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPreferredRestaurantList preferredRestaurantList;

        public PreferredRestaurantController(IPreferredRestaurantList preferredRestaurantList, IDocument document)
        {
            this.document = document;
            this.preferredRestaurantList = preferredRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(double latitude, double longitude, int currentPage = 0, int pageSize = 10)
        {
            var restaurantListModel = new PreferredRestaurantListModel<List<PreferredRestaurantModel>>
            {
                ErrorMessage = string.Empty
            };
            if (latitude == 0.0)
            {
                restaurantListModel.ErrorMessage = "Invalid request, Please check.!";
                return Request.CreateResponse(HttpStatusCode.BadRequest, restaurantListModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (longitude == 0.0)
            {
                restaurantListModel.ErrorMessage = "Invalid request, Please check.!";
                return Request.CreateResponse(HttpStatusCode.BadRequest, restaurantListModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
            restaurantListModel.RestaurantList = new List<PreferredRestaurantModel>();
            var listModel = await preferredRestaurantList.Get(latitude, longitude, currentPage, pageSize);
            restaurantListModel.RestaurantList = await MapObjectToModel(listModel);
            return Request.CreateResponse(HttpStatusCode.OK, restaurantListModel, JsonMediaTypeFormatter.DefaultMediaType);
        }

        #region Helpers method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurants"></param>
        /// <returns></returns>
        private async Task<List<PreferredRestaurantModel>> MapObjectToModel(List<PreferredRestaurantInfoDAO> preferredRestaurants)
        {
            var model = new List<PreferredRestaurantModel>();

            foreach (var restaurantItem in preferredRestaurants)
            {
                var restaurantImages = await document.Get(restaurantItem.RestaurantId);
                model.Add(new PreferredRestaurantModel()
                {
                    Address = restaurantItem.Address,
                    City = restaurantItem.City,
                    Distance = restaurantItem.Distance.Value.ToString("G29"),
                    Latitude = restaurantItem.Latitude,
                    Longitude = restaurantItem.Longitude,
                    Name = restaurantItem.Name,
                    RestaurantId = restaurantItem.RestaurantId.ToString("G29"),
                    State = restaurantItem.State,
                    Zipcode = restaurantItem.RestaurantId.ToString("G29"),
                    SEODescription = restaurantItem.SEODescription,
                    SEOKeyword = restaurantItem.SEOKeyword,
                    SEOTitle = restaurantItem.SEOTitle,
                    TagLine = restaurantItem.TagLine,
                    ImageUrl = restaurantImages.FrontImage != null && restaurantImages.FrontImage.FilePath != null ? restaurantImages.FrontImage.FilePath : "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg",
                    Rating = restaurantItem.Rating
                });
            }

            return model;
        }

        #endregion
    }
}
