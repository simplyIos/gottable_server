﻿using GotTable.API.Model.ApplicationSettings;
using GotTable.Library.ApplicationSettings;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// ApplicationSettingController
    /// </summary>
    public sealed class ApplicationSettingController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationSetting applicationSetting;

        public ApplicationSettingController(IApplicationSetting applicationSetting)
        {
            this.applicationSetting = applicationSetting;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var model = new ApplicationSettingModel();
            var applicationSettingDAO = await applicationSetting.Get();
            model.AdminApplicationBaseUrl = applicationSettingDAO.AdminApplicationBaseUrl;
            model.UserApplicationBaseUrl = applicationSettingDAO.UserApplicationBaseUrl;
            model.DineInActive = applicationSettingDAO.DineInActive;
            model.DeliveryActive = applicationSettingDAO.DeliveryActive;
            model.TakeawayActive = applicationSettingDAO.TakeawayActive;
            return Request.CreateResponse(HttpStatusCode.OK, model, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
