﻿using GotTable.API.Model.OTPTransactions;
using GotTable.DAO.Communications.Message;
using GotTable.Library.Communications.Message;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    public sealed class OTPTransactionController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        public OTPTransactionController(IOTPCommand oTPCommand)
        {
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Post(RequestOTPModel request)
        {
            if (String.IsNullOrEmpty(request.ExternalId))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "missing ExternalId" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (request.PhoneNumber == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "missing PhoneNumber" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (request.Type == "")
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "missing Type" }, JsonMediaTypeFormatter.DefaultMediaType);
            }

            var otpDetail = new OTPDetailDAO()
            {
                TransactionId = 0,
                IsUsed = false,
                OTP = string.Empty,
                PhoneNumber = request.PhoneNumber,
                Type = request.Type,
                ExternalId = String.IsNullOrEmpty(request.ExternalId) ? 0 : decimal.Parse(request.ExternalId),
                CreatedDate = DateTime.Now
            };

            await oTPCommand.New(otpDetail);
            if (otpDetail.TransactionId > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { transactionid = otpDetail.TransactionId.ToString("G29"), message = "OTP Sent" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Issue" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
