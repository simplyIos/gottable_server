﻿using GotTable.API.Model.OfferCategories;
using GotTable.Common.Enumerations;
using GotTable.DAO.OfferCategories;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.OfferCategories;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class OfferCategoryListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public OfferCategoryListController(ICategoryList categoryList, IOperationExceptionLog operationExceptionLog)
        {
            this.categoryList = categoryList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(Enumeration.RestaurantTypes? restaurantType = null)
        {
            try
            {
                var model = MapListToModel(await categoryList.Get(0, 10, restaurantType));
                return Request.CreateResponse(HttpStatusCode.OK, new { list = model }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception exceptionLog)
            {
                await operationExceptionLog.New(exceptionLog);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { errorMessage = exceptionLog.Message }, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        #region Helper method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal List<OfferCategoryModel> MapListToModel(List<OfferCategoryDAO> list)
        {
            return list.Where(x => x.Active == true).Select(m => new OfferCategoryModel()
            {
                Name = m.Name,
                Id = m.Id,
                ImagePath = AppSettingKeys.AdminApplicationBaseUrl + "/" + m.ImagePath
            }).ToList();
        }

        #endregion

    }
}
