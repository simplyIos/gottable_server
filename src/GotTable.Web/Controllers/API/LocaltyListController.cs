﻿using GotTable.DalEF.Localities;
using GotTable.Library.Localities;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using WebGrease.Css.Extensions;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class LocaltyListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        public readonly ILocaltyList localtyList;

        public LocaltyListController(ILocaltyList localtyList, IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="active"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(bool? active = null, int? cityId = null)
        {
            var model = new List<LocaltyModel>();
            var localtyDAOs = await localtyList.Get(active, true);
            localtyDAOs.ForEach(item =>
                model.Add(new LocaltyModel()
                {
                    Id = int.Parse(item.Id.ToString()),
                    Name = item.Name
                }));
            return Request.CreateResponse(HttpStatusCode.OK, model, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
