﻿using GotTable.Library.Restaurants;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantAutoCompleteListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAutoCompleteList autoCompleteList;

        public RestaurantAutoCompleteListController(IAutoCompleteList autoCompleteList)
        {
            this.autoCompleteList = autoCompleteList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(string queryExpression)
        {
            var model = await autoCompleteList.Get(queryExpression);
            return Request.CreateResponse(HttpStatusCode.OK, new { list = model }, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
