﻿using GotTable.API.Model.RestaurantTables;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTables;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class TableAvailabilityController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITableList tableList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTable"></param>
        /// <param name="tableList"></param>
        /// <param name="operationExceptionLog"></param>
        public TableAvailabilityController(IBranchTable branchTable, ITableList tableList, IOperationExceptionLog operationExceptionLog)
        {
            this.branchTable = branchTable;
            this.tableList = tableList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(decimal restaurantId = default, string bookingDate = default, string bookingTime = default)
        {
            var model = new ResturantTablesResponse<List<TableModel>>();
            try
            {
                if (restaurantId == default)
                {
                    model.ErrorMessage = "missing restaurantId";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (string.IsNullOrEmpty(bookingDate))
                {
                    model.ErrorMessage = "missing booking date";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (string.IsNullOrEmpty(bookingTime))
                {
                    model.ErrorMessage = "missing booking time";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
                }

                var dtBookingDate = DateTime.ParseExact(bookingDate, "MM-dd-yyyy", null);

                model.ErrorMessage = string.Empty;
                model.TableList = new List<TableModel>();
                foreach (var table in await tableList.Get(restaurantId, 0, 10))
                {
                    model.TableList.Add(new TableModel()
                    {
                        Id = table.Id.ToString("G29"),
                        Name = table.SelectedTable,
                        Total = table.Active ? Convert.ToInt16(table.Value) : 0,
                        Vacant = await branchTable.CheckAvailablity(table, dtBookingDate, bookingTime)
                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK, model, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception ee)
            {
                await operationExceptionLog.New(ee);
                model.ErrorMessage = ee.Message.ToString();
                return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
