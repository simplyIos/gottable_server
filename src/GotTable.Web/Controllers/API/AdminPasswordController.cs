﻿using GotTable.API.Model.Users;
using GotTable.Library.Administrators;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class AdminPasswordController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAdministrator<DAO.Administrators.AdminDAO> administrator;

        public AdminPasswordController(IAdministrator<DAO.Administrators.AdminDAO> administrator)
        {
            this.administrator = administrator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Post(PasswordModel model)
        {
            if (string.IsNullOrEmpty(model.UserId))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Missing userid" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (string.IsNullOrEmpty(model.OldPassword) || string.IsNullOrEmpty(model.NewPassword))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Missing password" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (model.NewPassword.Length <= 5)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new { message = "Password minimum of 6 characters" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            var adminDAO = await administrator.Get(decimal.Parse(model.UserId));
            if (adminDAO != null && adminDAO.UserId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Invalid user" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            if (adminDAO.Password != model.OldPassword)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Invalid Old Password" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            adminDAO.Password = model.NewPassword;
            await administrator.Save(adminDAO);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Password changed" }, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
