﻿using GotTable.API.Model;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class FileUploadController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public FileUploadController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Post(FileUploadModel model)
        {
            try
            {
                string errorMessage = "Invalid model properties, Please check";
                if (model.Content == null && string.IsNullOrEmpty(model.StringContent))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty }, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (string.IsNullOrEmpty(model.FileExtension))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty }, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (string.IsNullOrEmpty(model.FileName))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty }, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (string.IsNullOrEmpty(model.FileName))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty }, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (model.Content == null && model.StringContent != null)
                {
                    model.Content = Convert.FromBase64String(model.StringContent);
                }
                string directoryPath = HttpContext.Current.Server.MapPath("~/Uploads/" + model.UploadType + @"//");
                string filePath = directoryPath + model.FileName;
                File.WriteAllBytes(filePath, model.Content);
                string imagePath = @"/Uploads/" + model.UploadType + @"/" + model.FileName;
                return Request.CreateResponse(HttpStatusCode.OK, new { Status = 1, FilePath = imagePath, ErrorMessage = string.Empty }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Status = 0, FilePath = string.Empty, ErrorMessage = exception.Message }, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
