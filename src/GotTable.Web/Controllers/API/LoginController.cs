﻿using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.Users;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Users;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LoginController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="operationExceptionLog"></param>
        public LoginController(IUser user, IOperationExceptionLog operationExceptionLog)
        {
            this.user = user;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(string phoneNumber, string password)
        {
            var model = new UserResponse<DetailModel>()
            {
                UserDetail = new DetailModel()
            };
            if (string.IsNullOrEmpty(phoneNumber) || string.IsNullOrEmpty(password))
            {
                model.ErrorMessage = "Phone number and password cannot be blank";
                return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
            }
            try
            {
                var dao = await user.Get(phoneNumber, password);
                if (dao == null || dao.UserId == 0)
                {
                    model.ErrorMessage = "Invalid credentials";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (!dao.IsActive)
                {
                    model.ErrorMessage = "Your credentials has been locked.";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
                }
                if (dao.UserType == Enumeration.UserType.Guest.ToString())
                {
                    dao.UserType = Enumeration.UserType.EndUser.ToString();
                    await user.Save(dao);
                }

                model.ErrorMessage = string.Empty;
                model.UserDetail.Password = dao.Password;
                model.UserDetail.UserId = dao.UserId.ToString("G29");
                model.UserDetail.UserType = dao.UserType;
                model.UserDetail.FirstName = dao.FirstName;
                model.UserDetail.Gender = dao.Gender;
                model.UserDetail.Prefix = dao.Prefix;
                model.UserDetail.LastName = dao.LastName;
                model.UserDetail.PhoneNumber = dao.PhoneNumber;
                model.UserDetail.EmailAddress = dao.EmailAddress;
                model.UserDetail.AddressList = new System.Collections.Generic.List<AddressModel>();

                return Request.CreateResponse(HttpStatusCode.OK, model, JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                model.ErrorMessage = exception.Message;
                model.UserDetail = new DetailModel();
                return Request.CreateResponse(HttpStatusCode.BadRequest, model, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
