﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantReviews;
using GotTable.DAO.RestaurantRatings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantRatings;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ReviewController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchRating branchRating;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public ReviewController(IBranchRating branchRating, IOperationExceptionLog operationExceptionLog)
        {
            this.branchRating = branchRating;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Post(Request<ReviewModel> request)
        {
            var reviewModel = new ReviewResponse();
            var dao = new BranchRatingDAO()
            {
                BranchId = Convert.ToDecimal(request.Detail.BranchId),
                Comment = request.Detail.Comment,
                CreationDate = DateTime.Now,
                Id = Convert.ToDecimal(request.Detail.Id),
                IsActive = true,
                Rating = request.Detail.Rating,
                Title = request.Detail.Title,
                AmbienceRating = request.Detail.AmbienceRating,
                FoodRating = request.Detail.FoodRating,
                MusicRating = request.Detail.MusicRating,
                PriceRating = request.Detail.PriceRating,
                ServiceRating = request.Detail.ServiceRating,
                UserId = Convert.ToDecimal(request.Detail.UserId)
            };
            await branchRating.New(dao);
            if (dao.Status)
            {
                reviewModel.ReviewId = dao.StatusId.ToString();
                reviewModel.ErrorMessage = string.Empty;
                return Request.CreateResponse(HttpStatusCode.OK, reviewModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                reviewModel.ReviewId = string.Empty;
                reviewModel.ErrorMessage = dao.ErrorMessage;
                return Request.CreateResponse(HttpStatusCode.BadRequest, reviewModel, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restuarantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Check(decimal restuarantId, decimal userId)
        {
            if (restuarantId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { errormessage = "Missing restaurantId" }, JsonMediaTypeFormatter.DefaultMediaType);
            }

            if (userId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { errormessage = "Missing userId" }, JsonMediaTypeFormatter.DefaultMediaType);
            }

            if (await branchRating.CheckAuthencity(userId, restuarantId))
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "true" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "false" }, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }
    }
}
