﻿using GotTable.API.Model.Events;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.Web.Controllers.API
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class EventListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            await Task.FromResult(1);
            var model = new EventModel();
            model = new EventModel
            {
                Active = false,
                ImageUrl = "https://gottableindia.com/Uploads/Events/1.jpg"
            };
            return Request.CreateResponse(HttpStatusCode.OK, model, JsonMediaTypeFormatter.DefaultMediaType);
        }
    }
}
