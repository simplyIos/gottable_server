﻿using GotTable.Common.List;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TakeawayEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeaway deliveryandTakeaway;

        public TakeawayEditController(IDeliveryandTakeaway deliveryandTakeaway, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.deliveryandTakeaway = deliveryandTakeaway;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="isRead"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(decimal bookingId, bool isRead)
        {
            if (isRead)
            {
                await deliveryandTakeaway.MarkAsRead(bookingId);
            }
            var takeawayModel = await deliveryandTakeaway.Get(bookingId);
            ViewBag.TakeawayStatusList = DeliveryandTakeawayStatusList.Get();
            return View(takeawayModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> NewStatus(DeliveryandTakeawayStatusDAO model)
        {
            await deliveryandTakeaway.New(model);
            return RedirectToAction("Index", new { bookingId = model.BookingId, isRead = false });
        }
    }
}
