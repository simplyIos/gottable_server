﻿using GotTable.DAO.RestaurantTables;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTables;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TableEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        public TableEditController(IBranchTable branchTable, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.branchTable = branchTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal tableId)
        {
            var branchTableDAO = await branchTable.Get(tableId);
            ViewBag.TableList = Common.List.TableList.Get();
            return View(branchTableDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTableDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(BranchTableDAO branchTableDAO)
        {
            await branchTable.Save(branchTableDAO);
            if (branchTableDAO.Status)
            {
                return RedirectToAction("Index", "TableList", new { @restaurantId = branchTableDAO.BranchId });
            }
            ViewBag.TableList = Common.List.TableList.Get();
            return View(branchTableDAO);
        }
    }
}
