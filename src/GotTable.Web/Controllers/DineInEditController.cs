﻿using GotTable.Common.List;
using GotTable.DAO.Bookings.DineIn;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class DineInEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDineIn dineIn;

        public DineInEditController(IOperationExceptionLog operationExceptionLog, IDineIn dineIn) : base(operationExceptionLog)
        {
            this.dineIn = dineIn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="isRead"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal bookingId, bool isRead)
        {
            if (isRead)
            {
                await dineIn.MarkAsRead(bookingId);
            }
            var dineInModel = await dineIn.Get(bookingId);
            ViewBag.DineInStatusList = DineInStatusList.Get();
            return View(dineInModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> NewStatus(DineInStatusDAO model)
        {
            await dineIn.New(model);
            return RedirectToAction("Index", new { bookingId = model.BookingId, isRead = false });
        }
    }
}
