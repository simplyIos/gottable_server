﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantContacts;
using GotTable.Web.Filters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ContactListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IContactList contactList;

        public ContactListController(IContactList contactList, IOperationExceptionLog operationExceptionLog) : base(operationExceptionLog)
        {
            this.contactList = contactList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var contactDAOs = await contactList.Get(restaurantId, currentPage, pageSize);
            return View(contactDAOs);
        }
    }
}
