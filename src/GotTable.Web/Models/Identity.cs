﻿using System;

namespace GotTable.Web.Models
{
    /// <summary>
    /// Identity
    /// </summary>
    public static class Identity
    {
        /// <summary>
        /// UserId
        /// </summary>
        public static string UserId
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Name
        /// </summary>
        public static string Name
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return System.Web.HttpContext.Current.User.Identity.Name.Split('~')[1];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// UserType
        /// </summary>
        public static string UserType
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return System.Web.HttpContext.Current.User.Identity.Name.Split('~')[2];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// CityId
        /// </summary>
        public static decimal? CityId
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return Convert.ToDecimal(System.Web.HttpContext.Current.User.Identity.Name.Split('~')[3]);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// EnableButtonForRestaurantLogin
        /// </summary>
        public static bool EnableButtonForRestaurantLogin
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }


        #region Restaurant related properties

        public static string BranchId
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return System.Web.HttpContext.Current.User.Identity.Name.Split('~')[5];
                }
                else
                {
                    return null;
                }
            }
        }

        public static string BranchName
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return System.Web.HttpContext.Current.User.Identity.Name.Split('~')[6];
                }
                else
                {
                    return null;
                }
            }
        }

        public static string AddressName
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return System.Web.HttpContext.Current.User.Identity.Name.Split('~')[7];
                }
                else
                {
                    return null;
                }
            }
        }

        public static bool DineIn
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return Convert.ToBoolean(System.Web.HttpContext.Current.User.Identity.Name.Split('~')[8]);
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool Delivery
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return Convert.ToBoolean(System.Web.HttpContext.Current.User.Identity.Name.Split('~')[9]);
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool Takeaway
        {
            get
            {
                if (!String.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    return Convert.ToBoolean(System.Web.HttpContext.Current.User.Identity.Name.Split('~')[10]);
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion
    }
}