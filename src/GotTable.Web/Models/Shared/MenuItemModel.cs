﻿
namespace GotTable.Web.Models.Shared
{
    /// <summary>
    /// MenuItemModel
    /// </summary>
    public class MenuItemModel
    {
        /// <summary>
        /// OptionKey
        /// </summary>
        public string OptionKey { get; set; }

        /// <summary>
        /// OptionEnabled
        /// </summary>
        public bool OptionEnabled { get; set; }

        /// <summary>
        /// OptionName
        /// </summary>
        public string OptionName { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        public string Url { get; set; }
    }
}