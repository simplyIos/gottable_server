﻿using System.Web;

namespace GotTable.Web.Models
{
    /// <summary>
    /// CurrentRestaurant
    /// </summary>
    public sealed class CurrentRestaurant
    {
        /// <summary>
        /// Id
        /// </summary>
        public static string Id
        {
            get { return HttpContext.Current.Session["RestaurantId"] != null ? HttpContext.Current.Session["RestaurantId"].ToString() : null; }
        }

        /// <summary>
        /// Name
        /// </summary>
        public static string Name
        {
            get { return HttpContext.Current.Session["RestaurantName"] != null ? HttpContext.Current.Session["RestaurantName"].ToString() : null; }
        }

        /// <summary>
        /// Delete method
        /// </summary>
        internal static void Delete()
        {
            HttpContext.Current.Session.Clear();
        }

        /// <summary>
        /// Set
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantName"></param>
        internal static void Set(decimal restaurantId, string restaurantName)
        {
            HttpContext.Current.Session["RestaurantId"] = restaurantId;
            HttpContext.Current.Session["RestaurantName"] = restaurantName;
        }
    }
}