﻿
namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum Prefix : int
        {
            Mr = 1,
            Mrs = 2,
            Miss = 3,
            Dr = 4,
            NotDefined = 5
        };
    }
}
