﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum Tables : int
        {
            [Display(Name = "Table for 2")]
            TableFor2 = 1,
            [Display(Name = "Table for 4")]
            TableFor4 = 2,
            [Display(Name = "Table for 6")]
            TableFor6 = 3,
            [Display(Name = "Table for 8")]
            TableFor8 = 4,
            [Display(Name = "Table for 10")]
            TableFor10 = 5,
            [Display(Name = "Table for 12")]
            TableFor12 = 6
        };
    }
}
