﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum PhoneNumberType : int
        {
            [Display(Name = "Cell Phone")]
            Cell = 1,

            [Display(Name = "LandLine")]
            LandLine = 2
        };

        public enum EmailAddressType : int
        {
            [Display(Name = "Primary")]
            Primary = 1,

            [Display(Name = "Secondary")]
            Secondary = 2
        };

        public enum AddressType : int
        {
            [Display(Name = "Home")]
            Home = 1,

            [Display(Name = "Work")]
            Work = 2
        };
    }
}
