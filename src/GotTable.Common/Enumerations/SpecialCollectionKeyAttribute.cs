﻿
namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum SpecialCollectionKeyAttribute : int
        {
            DineInContactEmailAddress = 0,
            DeliveryContactEmailAddress = 1,
            TakeawayInContactEmailAddress = 2,
        }
    }
}
