﻿namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum SocialMediaType : int
        {
            Facebook = 1,
            Google = 2,
            Twitter = 3
        }
    }
}
