﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum PrescriptionTypes : int
        {
            [Display(Name = "Trial Period")]
            TrialPackage_ForOneMonth = 1,
            [Display(Name = "6 Month Prescription")]
            PackageOne_ForSixMonths = 2,
            [Display(Name = "12 Month Prescription")]
            PackageTwo_ForTwelveMonths = 3,
            [Display(Name = "24 Month Prescription")]
            PackageThree_For24Months = 4
        };
    }
}
