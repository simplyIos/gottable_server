﻿namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum ImageCategory : int
        {
            FrontView = 1,

            InsideView = 2,

            Logo = 3,

            Ambience = 4,

            MenuCard = 5
        };
    }
}
