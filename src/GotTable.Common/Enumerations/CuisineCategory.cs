﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum CuisineCategory : int
        {
            [Display(Name = "Regional")]
            Regional = 1,

            [Display(Name = "International")]
            International = 2
        };
    }
}
