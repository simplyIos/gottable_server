﻿namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum TransmissionContent : int
        {
            // OTP Messages
            NewRegistration = 1,
            NewDineIn = 2,
            NewDelivery = 3,
            NewTakeaway = 4,

            // DineIn 
            NewDineConfirmation2User = 5,
            NewDineConfirmation2Restaurant = 6,
            DineInLoyalty4User = 7,
            DineInReminder4User = 8,

            // Delivery
            NewDeliveryConfirmation2User = 9,
            NewDeliveryConfirmation2Restaurant = 10,
            DeliveryLoyalty4User = 11,

            // Takeaway
            NewTakeawayConfirmation2User = 12,
            NewTakeawayConfirmation2Restaurant = 13,
            TakeawayLoyalty4User = 14
        };
    }
}
