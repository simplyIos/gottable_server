﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum BookingType : int
        {
            [Display(Name = "DineIn")]
            DineIn = 1,

            [Display(Name = "Delivery")]
            Delivery = 2,

            [Display(Name = "Takeaway")]
            Takeaway = 3
        };
    }
}
