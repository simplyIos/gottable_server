﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum DineInStatusType : int
        {
            [Display(Name = "Confirm")]
            Confirm = 1,

            [Display(Name = "Complete")]
            Complete = 2,

            [Display(Name = "NoShow")]
            NoShow = 3,

            [Display(Name = "CancelByResturant")]
            CancelByResturant = 4,

            [Display(Name = "CancelByUser")]
            CancelByUser = 5,

            [Display(Name = "AutoComplete")]
            AutoComplete = 6
        };
    }
}
