﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum RestaurantCategories : int
        {
            [Display(Name = "Casual Dining")]
            Casual_Dining = 1,

            [Display(Name = "Pub")]
            Pub = 2,

            [Display(Name = "Microbrewery")]
            Microbrewery = 3
        };
    }
}
