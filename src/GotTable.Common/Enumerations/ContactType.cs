﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum ContactType : int
        {
            [Display(Name = "Operation Manager")]
            OperationManager = 1,

            [Display(Name = "Floor Manager")]
            FloorManager = 2,

            [Display(Name = "Restaurant Staff")]
            RestaurantStaff = 3,

            [Display(Name = "GotTable Repersentative")]
            GotTableRepersentative = 4
        }
    }
}
