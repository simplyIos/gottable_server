﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum RestaurantTypes : int
        {
            [Display(Name = "Dine In")]
            DineIn = 1,

            [Display(Name = "Delivery")]
            Delivery = 2,

            [Display(Name = "Takeaway")]
            Takeaway = 3
        };
    }
}
