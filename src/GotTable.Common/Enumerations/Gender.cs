﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum Gender : int
        {
            [Display(Name = "NotDefined")]
            NotDefined = 1,

            [Display(Name = "Male")]
            Male = 2,

            [Display(Name = "Female")]
            Female = 3,

            [Display(Name = "Other")]
            Other = 4
        };
    }
}
