﻿
namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum JobType : int
        {
            AndroidNotification = 1,
            IOSNotification = 2,
            DineInAutoComplete = 3
        };
    }
}
