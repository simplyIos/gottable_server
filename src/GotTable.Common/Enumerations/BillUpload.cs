﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Enumeration
    {
        /// <summary>
        /// 
        /// </summary>
        public enum BillUpload : int
        {
            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Pending")]
            Pending = 1,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "InProcessing")]
            InProcessing = 2,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Error")]
            Error = 3,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Done")]
            Done = 4,
        };
    }
}
