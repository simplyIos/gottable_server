﻿
namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum MenuType : int
        {
            Veg = 1,
            NonVeg = 2,
            NotDefined = 3
        };
    }
}
