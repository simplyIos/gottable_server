﻿
namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum PaymentMethod : int
        {
            Pending = 1,
            Cash = 2,
            Cheque = 3,
            DemandDraft = 4,
            OnlineTransaction = 5
        };
    }
}
