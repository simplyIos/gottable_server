﻿namespace GotTable.Common.Enumerations
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Enumeration
    {
        /// <summary>
        /// 
        /// </summary>
        public enum ClaimTypes : int
        {
            Name,
            Email,
            Role,
            Identifier,
            PreferredCityId,
            EnableRestaurantLogin,
            RestaurantId,
            RestaurantName,
            Address,
            DineIn,
            Delivery,
            Takeaway
        };
    }
}
