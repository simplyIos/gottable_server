﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum RestaurantTags : int
        {
            [Display(Name = "No Tag")]
            NoTag = 0,

            [Display(Name = "No tables available")]
            Red = 1,

            [Display(Name = "Table available with out offer")]
            Green = 2,

            [Display(Name = "Table available with offer")]
            Yellow = 3
        };
    }
}
