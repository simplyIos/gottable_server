﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum CredentialType : int
        {
            [Display(Name = "NoReply")]
            NoReply = 1,

            [Display(Name = "Info")]
            Info = 2,

            [Display(Name = "Admin")]
            Admin = 3
        }
    }
}
