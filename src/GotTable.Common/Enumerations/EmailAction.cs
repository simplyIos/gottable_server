﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum EmailAction : int
        {
            [Display(Name = "PaymentReminder")]
            PaymentReminder = 1,

            [Display(Name = "ResendPassword")]
            ResendPassword = 2,

            [Display(Name = "RestaurantCredential")]
            RestaurantCredential = 3,

            [Display(Name = "UserRegistration")]
            UserRegistration = 4,

            [Display(Name = "DineInConfirmationToUser")]
            DineInConfirmationToUser = 5,

            [Display(Name = "DineInConfirmationToRestaurant")]
            DineInConfirmationToRestaurant = 6,

            [Display(Name = "DeliveryConfirmationToUser")]
            DeliveryConfirmationToUser = 7,

            [Display(Name = "DeliveryConfirmationToRestaurant")]
            DeliveryConfirmationToRestaurant = 8,

            [Display(Name = "TakeawayConfirmationToUser")]
            TakeawayConfirmationToUser = 9,

            [Display(Name = "TakeawayConfirmationToRestaurant")]
            TakeawayConfirmationToRestaurant = 10,

            [Display(Name = "SocialMediaSignUpRegistration")]
            SocialMediaSignUpRegistration = 11
        };
    }
}
