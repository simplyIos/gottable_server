﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum Filter : int
        {
            [Display(Name = "cuisineids", Prompt = "")]
            Cuisines = 1,

            [Display(Name = "offerCategory", Prompt = "")]
            OfferCategories = 2,

            [Display(Name = "localtyId", Prompt = "")]
            Localities = 3
        };
    }
}
