﻿namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum UserType : int
        {
            EndUser = 5,
            Guest = 8,
            DeviceUser = 9
        };
    }
}
