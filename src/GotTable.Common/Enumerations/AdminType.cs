﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Enumeration
    {
        /// <summary>
        /// 
        /// </summary>
        public enum AdminType : int
        {
            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Super Admin")]
            SuperAdmin = 1,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Sales Admin")]
            SalesAdmin = 2,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Account Admin")]
            AccountAdmin = 3,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Application Support")]
            ApplicationSupport = 4,

            /// <summary>
            /// 
            /// </summary>
            [Display(Name = "Restaurant Admin")]
            HotelAdmin = 6
        };
    }
}
