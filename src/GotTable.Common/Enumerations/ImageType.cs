﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum ImageType : int
        {
            [Display(Name = "Front View")]
            Front = 1,

            [Display(Name = "Inside View")]
            Inside = 2,

            [Display(Name = "Logo Image")]
            Logo = 3
        };
    }
}
