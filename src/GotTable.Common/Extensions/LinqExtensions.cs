﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Common.Extensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<T> Should<T>(this IEnumerable<T> seq, params Func<T, bool>[] shoulds)
        {
            return seq.EmptyIfNull().Where(elt => shoulds.EmptyIfNull().Any(condition => condition(elt)));
        }

        public static IEnumerable<T> Must<T>(this IEnumerable<T> seq, params Func<T, bool>[] musts)
        {
            return seq.EmptyIfNull().Where(elt => musts.EmptyIfNull().All(condition => condition(elt)));
        }

        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> e)
        {
            return e ?? Enumerable.Empty<T>();
        }

        public static IEnumerable<T> NullIfEmpty<T>(this IEnumerable<T> e)
        {
            return e.Any() ? e : null;
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> e, bool throwOnDuplicates = false)
        {
            if (throwOnDuplicates)
                return new HashSet<T>(e);
            var ret = new HashSet<T>();
            foreach (var t in e)
                ret.Add(t);
            return ret;
        }

        public static IEnumerable<TSource> IntersectBy<TSource, TKey>(this IEnumerable<TSource> source1, IEnumerable<TSource> source2, Func<TSource, TKey> keySelector)
        {
            var hashSource = source1.Select(keySelector).ToHashSet(false);
            return source2.Where(e => hashSource.Contains(keySelector(e)));
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var elements = new HashSet<TKey>();
            foreach (var item in source)
            {
                var key = keySelector(item);
                if (elements.Add(key))
                    yield return item;
            }
        }

        public static IEnumerable<TSource> ExceptBy<TSource, TKey>(this IEnumerable<TSource> source1, IEnumerable<TSource> source2, Func<TSource, TKey> keySelector)
        {
            var sourceKeys = source2.Select(keySelector).ToHashSet();
            return source1.Where(item => !sourceKeys.Contains(keySelector(item)));
        }

        public static IEnumerable<Tuple<int, T>> WithPosition<T>(this IEnumerable<T> values)
        {
            var idx = 0;
            foreach (var v in values)
                yield return Tuple.Create(idx++, v);
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue def = default)
        {
            TValue ret;
            if (dic.TryGetValue(key, out ret))
                return ret;
            return def;
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.Shuffle(new Random());
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (rng == null) throw new ArgumentNullException(nameof(rng));

            return source.ShuffleIterator(rng);
        }

        private static IEnumerable<T> ShuffleIterator<T>(this IEnumerable<T> source, Random rng)
        {
            var buffer = source.ToList();
            for (int i = 0; i < buffer.Count; i++)
            {
                int j = rng.Next(i, buffer.Count);
                yield return buffer[j];

                buffer[j] = buffer[i];
            }
        }

        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int size)
        {
            var partition = new List<T>(size);
            var counter = 0;

            if (source.Any())
            {
                using (var enumerator = source.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        partition.Add(enumerator.Current);
                        counter++;
                        if (counter % size == 0)
                        {
                            yield return partition.ToList();
                            partition.Clear();
                            counter = 0;
                        }
                    }

                    if (counter != 0)
                        yield return partition;
                }
            }
        }

        public static void ForEach<T>(this IEnumerable<T> sequence, Action<T> action)
        {
            if (sequence != null)
            {
                foreach (T item in sequence) action(item);
            }
        }

        public static async Task ForEachAsync<T>(this IEnumerable<T> sequence, Func<T, Task> action)
        {
            if (sequence != null)
            {
                foreach (T item in sequence) await action(item);
            }
        }

        public static void ForEachParallel<T>(this IEnumerable<T> sequence, Action<T> action)
        {
            sequence.AsParallel().ForEach(action);
        }

        /// <summary>
        /// This is globaly a really bad idea to call it directly... use the by range for limiting the number of tasks generated.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static Task ForEachParallelAsync<T>(this IEnumerable<T> sequence, Func<T, Task> action)
        {
            //For now it s seems the best implementation
            //This is avoiding a big number of tasks to be generated upfront.
            //Parallel.ForEach(sequence, (s) =>
            //{
            //    action(s).GetAwaiter().GetResult();
            //});
            //return Task.FromResult<Object>(null);
            //Other solution
            //The task run allow to also execute the immediate code in parallel.
            var tasks = sequence.Select(s => Task.Run(() => action(s))).ToList();
            return Task.WhenAll(tasks);
        }

        public static async Task ForEachByRangeAsync<T>(this IEnumerable<T> sequence, Func<IEnumerable<T>, Task> action, int rangeSize)
        {
            await sequence.Partition(rangeSize).ForEachAsync(action);
        }

        public static Task ForEachParallelByRangeAsync<T>(this IEnumerable<T> sequence, Func<IEnumerable<T>, Task> action, int rangeSize)
        {
            return sequence.Partition(rangeSize).ForEachParallelAsync(action);
        }

        public static void ForEachParallelByRange<T>(this IEnumerable<T> sequence, Action<IEnumerable<T>> action, int rangeSize)
        {
            sequence.Partition(rangeSize).ForEachParallel(action);
        }

        public static void ForEachByRange<T>(this IEnumerable<T> sequence, Action<IEnumerable<T>> action, int rangeSize)
        {
            sequence.Partition(rangeSize).ForEach(action);
        }

        public static int IndexOf<T>(this IOrderedEnumerable<T> sequence, T value)
        {
            return Array.IndexOf(sequence.ToArray(), value);
        }

        public static IEnumerable<T> Remove<T>(this IEnumerable<T> sequence, Func<T, bool> predicate)
        {
            return sequence.Where(s => !predicate(s)).ToArray();
        }

        public static Task ForEachParallelByEvenRangeAsync<T>(this IEnumerable<T> sequence, Func<IEnumerable<T>, Task> action, int rangeSize)
        {
            return sequence.PartitionEven(rangeSize).ForEachParallelAsync(action);
        }

        public static IEnumerable<IEnumerable<T>> PartitionEven<T>(this IEnumerable<T> sequence, int rangeSize)
        {
            var batchCount = Convert.ToInt32(Math.Round(Math.Ceiling((decimal)sequence.Count() / rangeSize)));
            var range = sequence.Count() / batchCount;
            var leftItems = sequence.Count() % batchCount;
            int index = 0;

            while (index < sequence.Count())
            {
                int currentRangeSize = range + (leftItems > 0 ? 1 : 0);
                yield return sequence.ToList().GetRange(index, currentRangeSize);
                index += currentRangeSize;
                leftItems--;
            }
        }

        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> childPropertySelector)
        {
            return source.Flatten((itemBeingFlattened, objectsBeingFlattened) => childPropertySelector(itemBeingFlattened));
        }

        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>, IEnumerable<T>> childPropertySelector)
        {
            return source.Concat(source.Where(item => childPropertySelector(item, source) != null)
                .SelectMany(itemBeingFlattened => childPropertySelector(itemBeingFlattened, source).Flatten(childPropertySelector)));
        }

        public static IEnumerable<IEnumerable<T>> SmartGroup<T>(this IEnumerable<T> source, Func<T, T, bool> predicate)
        {
            using (var iterator = source.GetEnumerator())
            {
                if (!iterator.MoveNext())
                    yield break;
                List<T> batch = new List<T> { iterator.Current };
                while (iterator.MoveNext())
                {
                    if (!predicate(batch[batch.Count - 1], iterator.Current))
                    {
                        yield return batch;
                        batch = new List<T>();
                    }
                    batch.Add(iterator.Current);
                }
                if (batch.Any())
                    yield return batch;
            }
        }
    }
}
