﻿using GotTable.Common.Constants;
using System;

namespace GotTable.Common.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class BookingDateTimeExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingDateTime"></param>
        /// <returns></returns>
        public static long ToDateTimeInteger(this DateTime bookingDateTime)
        {
            return Convert.ToInt64(bookingDateTime.ToString(GotTableCommanConstant.BookingDateTimeIntegerFormat));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingDateTime"></param>
        /// <returns></returns>
        public static long ToLockInDateTimeInteger(this DateTime bookingDateTime)
        {
            return Convert.ToInt64(bookingDateTime.AddMinutes(-45).ToString(GotTableCommanConstant.BookingDateTimeIntegerFormat));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingDateTime"></param>
        /// <returns></returns>
        public static long ToLockOutDateTimeInteger(this DateTime bookingDateTime)
        {
            return Convert.ToInt64(bookingDateTime.AddMinutes(120).ToString(GotTableCommanConstant.BookingDateTimeIntegerFormat));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingTime"></param>
        /// <returns></returns>
        public static int ToInteger(this string bookingTime)
        {
            return Convert.ToInt16(bookingTime.Replace(":", ""));
        }
    }
}
