﻿using System;

namespace GotTable.Common.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class DisplayAttributeExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(true);
            return (attributes.Length > 0) ? ((System.ComponentModel.DataAnnotations.DisplayAttribute)attributes[0]).Name : null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static string GetPromptString(this Enum enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(true);
            return (attributes.Length > 0) ? ((System.ComponentModel.DataAnnotations.DisplayAttribute)attributes[0]).Prompt : null;
        }
    }
}
