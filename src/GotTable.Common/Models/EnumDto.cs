﻿using System;

namespace GotTable.Common.Models
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class EnumDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
