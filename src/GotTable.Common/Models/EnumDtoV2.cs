﻿namespace GotTable.Common
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class EnumDtoV2<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public T Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
