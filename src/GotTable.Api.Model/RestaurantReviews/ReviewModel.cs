﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantReviews
{
    /// <summary>
    /// Review model
    /// </summary>
    public sealed class ReviewModel
    {
        public ReviewModel(string id = null, string userId = null, string userName = null, string phoneNumber = null, string branchId = null, string title = null, string comment = null, int rating = 0, int musicRating = 0, int ambienceRating = 0, int foodRating = 0, int priceRating = 0, int serviceRating = 0, string createdDate = null)
        {
            Id = id;
            UserId = userId;
            UserName = userName;
            PhoneNumber = phoneNumber;
            BranchId = branchId;
            Title = title;
            Comment = comment;
            Rating = rating;
            MusicRating = musicRating;
            AmbienceRating = ambienceRating;
            FoodRating = foodRating;
            PriceRating = priceRating;
            ServiceRating = serviceRating;
            CreatedDate = createdDate;
        }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        [JsonProperty("userid")]
        public string UserId { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        [JsonProperty("username")]
        public string UserName { get; set; }

        /// <summary>
        /// Useremailaddress
        /// </summary>
        [JsonProperty("PhoneNumber")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// BranchId
        /// </summary>
        [JsonProperty("branchid")]
        public string BranchId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        [JsonProperty("comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Rating
        /// </summary>
        [JsonProperty("rating")]
        public int Rating { get; set; }

        /// <summary>
        /// MusicRating
        /// </summary>
        [JsonProperty("musicrating")]
        public int MusicRating { get; set; }

        /// <summary>
        /// AmbienceRating
        /// </summary>
        [JsonProperty("ambiencerating")]
        public int AmbienceRating { get; set; }

        /// <summary>
        /// FoodRating
        /// </summary>
        [JsonProperty("foodrating")]
        public int FoodRating { get; set; }

        /// <summary>
        /// PriceRating
        /// </summary>
        [JsonProperty("pricerating")]
        public int PriceRating { get; set; }

        /// <summary>
        /// ServiceRating
        /// </summary>
        [JsonProperty("servicerating")]
        public int ServiceRating { get; set; }

        /// <summary>
        /// CreatedDate
        /// </summary>
        [JsonProperty("createddate")]
        public string CreatedDate { get; set; }
    }
}
