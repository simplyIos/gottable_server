﻿using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantReviews
{
    /// <summary>
    /// ReviewResponse
    /// </summary>
    public sealed class ReviewResponse
    {
        /// <summary>
        /// Error message
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// ReviewId
        /// </summary>
        [JsonProperty("reviewid")]
        public string ReviewId { get; set; }
    }
}
