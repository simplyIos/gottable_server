﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Cuisines.V1
{
    /// <summary>
    /// Model uses for cuisine.
    /// </summary>
    public sealed class CuisineModel
    {
        /// <summary>
        /// Unique id of the model.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name of the cuisine name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
