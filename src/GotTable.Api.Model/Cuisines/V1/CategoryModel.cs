﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Cuisines.V1
{
    /// <summary>
    /// Model uses to get all cusines list
    /// </summary>
    public sealed class CategoryModel
    {
        public CategoryModel()
        {
            this.CuisineList = new List<CuisineModel>();
        }

        /// <summary>
        /// Unique Id for the model.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name for the cusine name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// list for the cuisine
        /// </summary>
        [JsonProperty("cuisinelist")]
        public List<CuisineModel> CuisineList { get; set; }
    }
}
