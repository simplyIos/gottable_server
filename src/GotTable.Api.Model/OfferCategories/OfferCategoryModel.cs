﻿using Newtonsoft.Json;

namespace GotTable.API.Model.OfferCategories
{
    /// <summary>
    /// OfferCategoryModel
    /// </summary>
    public sealed class OfferCategoryModel
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("Name")]
        public string Name { get; set; }

        /// <summary>
        /// ImagePath
        /// </summary>
        [JsonProperty("ImagePath")]
        public string ImagePath { get; set; }
    }
}
