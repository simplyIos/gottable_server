﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.OTPTransactions
{
    /// <summary>
    /// RequestOTPModel
    /// </summary>
    public sealed class RequestOTPModel
    {
        /// <summary>
        /// Phonenumber
        /// </summary>
        [JsonProperty("phonenumber")]
        public long PhoneNumber { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// ExternalId
        /// </summary>
        [JsonProperty("externalId")]
        public string ExternalId { get; set; }
    }
}
