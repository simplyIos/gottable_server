﻿using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantTables
{
    /// <summary>
    /// Table model
    /// </summary>
    public sealed class TableModel
    {
        public TableModel(string id = null, string name = null, int total = 0, bool vacant = false)
        {
            Id = id;
            Name = name;
            Total = total;
            Vacant = vacant;
        }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Total
        /// </summary>
        [JsonProperty("total")]
        public int Total { get; set; }

        /// <summary>
        /// Vacant
        /// </summary>
        [JsonProperty("vacant")]
        public bool Vacant { get; set; }
    }
}
