﻿using GotTable.API.Model.UserAddresses;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Users
{
    public sealed class DetailModel
    {
        public DetailModel()
        {

        }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("usertype")]
        public string UserType { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }

        [JsonProperty("rewardcount")]
        public int RewardCount { get; set; }

        [JsonProperty("addresslist")]
        public List<AddressModel> AddressList { get; set; }
    }
}
