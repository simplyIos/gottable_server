﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Users
{
    public sealed class PasswordModel
    {
        public PasswordModel()
        {
            this.UserId = string.Empty;
            this.OldPassword = string.Empty;
            this.NewPassword = string.Empty;
        }

        [JsonProperty("userid")]
        public string UserId { get; set; }

        [JsonProperty("oldpassword")]
        public string OldPassword { get; set; }

        [JsonProperty("newpassword")]
        public string NewPassword { get; set; }
    }
}
