﻿namespace GotTable.API.Model.Users
{
    public class EmailAddressUpdateModel
    {
        public decimal UserId { get; set; }

        public string EmailAddress { get; set; }
    }
}