﻿using Newtonsoft.Json;

namespace GotTable.API.Model.Users
{
    public sealed class UserResponse<T>
    {
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("user_detail")]
        public T UserDetail { get; set; }
    }
}
