﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.UserAddresses
{
    public sealed class AddressModel
    {
        public AddressModel()
        {

        }

        [JsonProperty("addressid")]
        public string AddressId { get; set; }


        [JsonProperty("userid")]
        public string UserId { get; set; }


        [JsonProperty("type")]
        public string Type { get; set; }


        [JsonProperty("line1")]
        public string AddressLine1 { get; set; }


        [JsonProperty("line2")]
        public string AddressLine2 { get; set; }


        [JsonProperty("city")]
        public string City { get; set; }


        [JsonProperty("state")]
        public string State { get; set; }


        [JsonProperty("zip")]
        public string Zip { get; set; }


        [JsonProperty("latitude")]
        public string Latitude { get; set; }


        [JsonProperty("longitude")]
        public string Longitude { get; set; }
    }
}
