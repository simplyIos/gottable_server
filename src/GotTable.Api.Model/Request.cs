﻿
using Newtonsoft.Json;

namespace GotTable.API.Model
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Request<T>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("deviceid")]
        public string DeviceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("devicetype")]
        public string DeviceType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("detail")]
        public T Detail { get; set; }
    }
}
