﻿namespace GotTable.API.Model.RestaurantBookings.BillUploads
{
    /// <summary>
    /// Model uses to upload the bill.
    /// </summary>
    public sealed class BillUploadModel
    {
        /// <summary>
        /// Required.
        /// </summary>
        public decimal BookingId { get; set; }

        /// <summary>
        /// Required.
        /// </summary>
        public decimal UserId { get; set; }

        /// <summary>
        /// Required.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Required.
        /// </summary>
        public string BillPath { get; set; }

        /// <summary>
        /// This will returns you the error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// This will returns you the success message.
        /// </summary>
        public string SuccessMessage { get; set; }

        /// <summary>
        /// This will returns you the status.
        /// </summary>
        public bool Status { get; set; }
    }
}
