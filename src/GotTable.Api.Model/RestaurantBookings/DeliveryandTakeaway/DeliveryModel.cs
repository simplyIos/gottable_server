﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantBookings.DeliveryandTakeaway
{
    public sealed class DeliveryModel
    {
        [JsonProperty("addressline1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("addressline2")]
        public string AddressLine2 { get; set; }

        [JsonProperty("landmark")]
        public string LandMark { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }
    }
}
