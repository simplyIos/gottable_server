﻿using System.Collections.Generic;

namespace GotTable.API.Model.UserBookings
{
    public sealed class DeliveryandTakeawayBookingModel
    {
        public DeliveryandTakeawayBookingModel()
        {
            this.RestaurantDetail = new RestaurantModel();
            this.CartList = new List<CartModel>();
            this.DeliveryDetail = new DeliveryAddressModel();
            this.ContactList = new List<RestaurantContactModel>();
        }

        public string BookingId { get; set; }

        public string BookingType { get; set; }

        public string DateForBooking { get; set; }

        public string Status { get; set; }

        public string TotalAmount { get; set; }

        public string CentralGST { get; set; }

        public string StateGST { get; set; }

        public string DeliveryCharges { get; set; }

        public string PackingCharges { get; set; }

        public string ServiceCharges { get; set; }

        public List<CartModel> CartList { get; set; }

        public RestaurantModel RestaurantDetail { get; set; }

        public DeliveryAddressModel DeliveryDetail { get; set; }

        public List<RestaurantContactModel> ContactList { get; set; }
    }
}
