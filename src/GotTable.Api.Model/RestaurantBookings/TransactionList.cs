﻿using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantBookings
{
    /// <summary>
    /// TransactionList
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class TransactionList<T>
    {
        /// <summary>
        /// ErrorMessage
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// TransactionDetail
        /// </summary>
        [JsonProperty("transactiondetail")]
        public T TransactionDetail { get; set; }
    }
}
