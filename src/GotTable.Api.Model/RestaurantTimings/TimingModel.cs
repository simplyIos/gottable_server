﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantTimings
{
    /// <summary>
    /// Timing model
    /// </summary>
    public sealed class TimingModel
    {
        public TimingModel(bool isClosed = default, string name = default, string lunchEndTime = default, string lunchStartTime = default, string dinnerEndTime = default, string dinnerStartTime = default)
        {
            IsClosed = isClosed;
            Name = name;
            LunchEndTime = lunchEndTime;
            LunchStartTime = lunchStartTime;
            DinnerStartTime = dinnerStartTime;
            DinnerEndTime = dinnerEndTime;
        }

        /// <summary>
        /// Isclosed
        /// </summary>
        [JsonProperty("isclosed")]
        public bool IsClosed { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Lunch start time
        /// </summary>
        [JsonProperty("lunchstarttime")]
        public string LunchStartTime { get; set; }

        /// <summary>
        /// Lunch end time
        /// </summary>
        [JsonProperty("lunchendtime")]
        public string LunchEndTime { get; set; }

        /// <summary>
        /// Dinner start time
        /// </summary>
        [JsonProperty("dinnerstarttime")]
        public string DinnerStartTime { get; set; }

        /// <summary>
        /// Dinner end time
        /// </summary>
        [JsonProperty("dinnerendtime")]
        public string DinnerEndTime { get; set; }
    }
}
