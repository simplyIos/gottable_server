﻿

namespace GotTable.API.Model.Versions
{
    /// <summary>
    /// Version model
    /// </summary>
    public sealed class VersionModel
    {
        /// <summary>
        /// Error message
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Display message
        /// </summary>
        public string DisplayMessage { get; set; }

        //Version number
        public string VersionNumber { get; set; }

        /// <summary>
        /// Device type
        /// </summary>
        public string DeviceType { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        public bool Active { get; set; }
    }
}
