﻿using GotTable.DalEF.Localities;
using GotTable.Library.Localities;
using GotTable.Library.OperationExceptionLogs;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;


namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LocaltyListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        public readonly ILocaltyList localtyList;

        public LocaltyListController(ILocaltyList localtyList, IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="active"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(bool? active = null, int? cityId = null)
        {
            var model = new List<LocaltyModel>();
            var localtyDAOs = await localtyList.Get(active, true);
            foreach (var item in localtyDAOs)
            {
                model.Add(new LocaltyModel()
                {
                    Id = int.Parse(item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(model);
        }
    }
}
