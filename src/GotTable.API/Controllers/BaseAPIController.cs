﻿using GotTable.API.Filters;
using System;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    [Exception]
    [Authorize]
    [Log]
    public class BaseAPIController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        public BaseAPIController()
        {

        }
    }
}
