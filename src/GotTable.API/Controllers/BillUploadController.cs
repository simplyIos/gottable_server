﻿using GotTable.API.Model.RestaurantBookings.BillUploads;
using GotTable.DAO.RestaurantBookings.BillUploads;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class BillUploadController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInBookingBillUpload dineInBookingBillUpload;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public BillUploadController(IDineInBookingBillUpload dineInBookingBillUpload, IOperationExceptionLog operationExceptionLog)
        {
            this.dineInBookingBillUpload = dineInBookingBillUpload;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody] BillUploadModel model)
        {
            var dineInBookingBillUploadDAO = new DineInBookingBillUploadDAO()
            {
                Amount = model.Amount,
                BookingId = model.BookingId,
                UserId = model.UserId,
                ImagePath = model.BillPath
            };
            await dineInBookingBillUpload.New(dineInBookingBillUploadDAO);
            if (dineInBookingBillUploadDAO.Status)
            {
                model.Status = true;
                model.SuccessMessage = "Bill uploaded successfully.";
                return Ok(model);
            }
            else
            {
                model.Status = false;
                model.ErrorMessage = dineInBookingBillUploadDAO.ErrorMessage;
                return BadRequest(model.ErrorMessage);
            }
        }
    }
}
