﻿using GotTable.API.Model.Cuisines.V1;
using GotTable.API.Model.Images;
using GotTable.API.Model.RestaurantAmenities;
using GotTable.API.Model.RestaurantContacts;
using GotTable.API.Model.RestaurantReviews;
using GotTable.API.Model.Restaurants;
using GotTable.API.Model.RestaurantTimings;
using GotTable.API.Model.Resturants;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantAmenities;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Library.RestaurantContacts;
using GotTable.Library.RestaurantCuisines;
using GotTable.Library.RestaurantDocuments;
using GotTable.Library.RestaurantRatings;
using GotTable.Library.Restaurants;
using GotTable.Library.RestaurantTimings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRatingList ratingList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ITimingList timingList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IContactList contactList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        private readonly IAmenityList amenityList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationRestaurantList applicationRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="ratingList"></param>
        /// <param name="timingList"></param>
        /// <param name="branchConfiguration"></param>
        /// <param name="cuisineList"></param>
        /// <param name="document"></param>
        /// <param name="contactList"></param>
        /// <param name="applicationRestaurantList"></param>
        /// <param name="amenityList"></param>
        public RestaurantController(IOperationExceptionLog operationExceptionLog, IRatingList ratingList, ITimingList timingList, IBranchConfiguration branchConfiguration, ICuisineList cuisineList, IDocument document, IContactList contactList, IApplicationRestaurantList applicationRestaurantList, IAmenityList amenityList)
        {
            this.branchConfiguration = branchConfiguration;
            this.ratingList = ratingList;
            this.timingList = timingList;
            this.operationExceptionLog = operationExceptionLog;
            this.cuisineList = cuisineList;
            this.document = document;
            this.contactList = contactList;
            this.applicationRestaurantList = applicationRestaurantList;
            this.amenityList = amenityList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="deviceType"></param>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        [Route("api/restaurant/get")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(Enumeration.RestaurantTypes restaurantType, string deviceId = default, string deviceType = default, decimal restaurantId = default, double latitude = default, double longitude = default)
        {
            var model = new ResturantDetailResponse<ResturantDetailModel>
            {
                ResturantDetail = new ResturantDetailModel()
            };
            if (restaurantId == default)
            {
                model.ErrorMessage = "Missing restaurantId, Invalid request";
                return BadRequest(model.ErrorMessage);
            }

            if (restaurantType == default)
            {
                model.ErrorMessage = "Missing resturant type, Invalid request";
                return BadRequest(model.ErrorMessage);
            }

            if (latitude == default)
            {
                model.ErrorMessage = "Missing latitude, Invalid request";
                return BadRequest(model.ErrorMessage);
            }

            if (longitude == default)
            {
                model.ErrorMessage = "Missing longitude, Invalid request";
                return BadRequest(model.ErrorMessage);
            }

            var resuturantList = await applicationRestaurantList.Get(latitude, longitude, restaurantId, restaurantType);

            if (resuturantList != null)
            {
                model.ErrorMessage = string.Empty;
                foreach (var item in resuturantList)
                {
                    model.ResturantDetail.Id = item.Id.ToString("G29");
                    model.ResturantDetail.AddressLine1 = item.AddressLine1;
                    model.ResturantDetail.AddressLine2 = item.AddressLine2;
                    model.ResturantDetail.City = item.City;
                    model.ResturantDetail.Latitude = item.Latitude;
                    model.ResturantDetail.Longitude = item.Longitude;
                    model.ResturantDetail.Name = item.Name;
                    model.ResturantDetail.State = item.State;
                    model.ResturantDetail.Zip = item.ZipCode.ToString("G29");
                    model.ResturantDetail.Rating = item.AvgRating;
                    model.ResturantDetail.AmbienceRating = item.AmbienceRating;
                    model.ResturantDetail.FoodRating = item.FoodRating;
                    model.ResturantDetail.ServiceRating = item.ServiceRating;
                    model.ResturantDetail.PriceRating = item.PriceRating;
                    model.ResturantDetail.MusicRating = item.MusicRating;
                    model.ResturantDetail.Distance = item.Distance.ToString("G29");
                    model.ResturantDetail.IsOfferAvailable = item.IsOfferAvailable;
                    model.ResturantDetail.RestaurantTag = item.RestaurantTag;
                    model.ResturantDetail.SEODescription = item.SEODescription;
                    model.ResturantDetail.SEOKeyword = item.SEOKeyword;
                    model.ResturantDetail.SEOTitle = item.SEOTitle;
                    model.ResturantDetail.Description = item.Description;
                    model.ResturantDetail.CostForTwo = item.CostForTwo;
                    model.ResturantDetail.Tag = item.SelectedTag.ToString();

                    var configuration = await branchConfiguration.Get(item.Id);
                    if (configuration != null)
                    {
                        if (restaurantType == Enumeration.RestaurantTypes.Delivery)
                        {
                            model.ResturantDetail.CentralGST = configuration.CentralGST.Value.ToString("G29");
                            model.ResturantDetail.StateGST = configuration.StateGST.Value.ToString("G29");
                            model.ResturantDetail.DeliveryCharges = configuration.DeliveryCharges.Value.ToString("G29");
                            model.ResturantDetail.StandByTime = configuration.DeliveryStandBy;
                        }
                        else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
                        {
                            model.ResturantDetail.CentralGST = configuration.CentralGST.Value.ToString("G29");
                            model.ResturantDetail.StateGST = configuration.StateGST.Value.ToString("G29");
                            model.ResturantDetail.StandByTime = configuration.TakeawayStandBy;
                            model.ResturantDetail.DeliveryCharges = string.Empty;
                        }
                        else
                        {
                            model.ResturantDetail.CentralGST = string.Empty;
                            model.ResturantDetail.StateGST = string.Empty;
                            model.ResturantDetail.DeliveryCharges = string.Empty;
                            model.ResturantDetail.StandByTime = string.Empty;
                        }
                    }

                    var cuisines = await cuisineList.Get(item.Id, activeCuisines: true);
                    model.ResturantDetail.Cuisines = cuisines.Select(m => new CuisineModel()
                    {
                        Id = m.Id.ToString("G29"),
                        Name = m.SelectedCuisine
                    }).ToList();

                    var contacts = await contactList.Get(item.Id);
                    model.ResturantDetail.Contacts = contacts.OrderBy(x => x.SelectedTypeId).Select(m => new ContactModel()
                    {
                        EmailAddress = m.EmailAddress,
                        Id = m.Id.ToString("G29"),
                        Name = m.Name,
                        PhoneNumber = Convert.ToInt64(m.PhoneNumber)
                    }).ToList();

                    var ratingDAO = await ratingList.Get(restaurantId);
                    model.ResturantDetail.Reviews = ratingDAO.Select(ratingItem => new ReviewModel(
                        userId: ratingItem.UserId.ToString("G29"),
                        userName: ratingItem.UserName,
                        phoneNumber: ratingItem.PhoneNumber,
                        priceRating: ratingItem.PriceRating ?? 0,
                        musicRating: ratingItem.MusicRating ?? 0,
                        serviceRating: ratingItem.ServiceRating ?? 0,
                        foodRating: ratingItem.FoodRating ?? 0,
                        ambienceRating: ratingItem.AmbienceRating ?? 0,
                        rating: ratingItem.Rating ?? 0,
                        title: ratingItem.Title,
                        comment: ratingItem.Comment,
                        createdDate: ratingItem.CreationDate.Value.ToString("dd-MMM-yyyy hh:mm")
                    )).ToList();

                    var timingDAO = await timingList.Get(item.Id, Enumeration.RestaurantTypes.DineIn == restaurantType, Enumeration.RestaurantTypes.Delivery == restaurantType, Enumeration.RestaurantTypes.Takeaway == restaurantType);
                    model.ResturantDetail.Timings = timingDAO.DefaultIfEmpty().FirstOrDefault().DayTiming.Select(m => new TimingModel()
                    {
                        DinnerEndTime = m.DinnerEndTimeIn24HourFormat,
                        DinnerStartTime = m.DinnerStartTimeIn24HourFormat,
                        Name = m.SelectedDay.ToString(),
                        IsClosed = m.IsClosed,
                        LunchStartTime = m.LunchStartTimeIn24HourFormat,
                        LunchEndTime = m.LunchEndTimeIn24HourFormat
                    }).ToList();

                    var amenities = await amenityList.Get(item.Id);
                    model.ResturantDetail.Amenities = amenities.Select(amenity => new AmenityModel()
                    {
                        AmenityId = amenity.AmenityId,
                        Name = amenity.AmenityName
                    }).ToList();

                    model.ResturantDetail.Images = await MapObjectToModel(item.Id, false);
                }
            }
            return Ok(model);
        }

        #region Helper method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        internal async Task<List<ImageModel>> MapObjectToModel(decimal restaurantId, bool listType)
        {
            var model = new List<ImageModel>();
            var restaurantImages = await document.Get(restaurantId);
            if (listType)
            {
                if (restaurantImages.FrontImage != null && restaurantImages.FrontImage.FilePath != null)
                {
                    model.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = restaurantImages.FrontImage.FilePath
                    });
                }
                else
                {
                    model.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            else
            {
                if (restaurantImages.BackImage != null && restaurantImages.BackImage.FilePath != null)
                {
                    model.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = restaurantImages.BackImage.FilePath
                    });
                }
                else
                {
                    model.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            return model;
        }

        #endregion
    }
}
