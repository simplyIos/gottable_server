﻿using GotTable.Library.Users;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CheckUserController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public CheckUserController(IUser user)
        {
            this.user = user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return BadRequest("Missing phone number");
            }
            var dao = await user.Get(phoneNumber);
            if (dao != null && dao.UserId == 0)
            {
                return Ok(new { message = "INVALID_USER" });
            }
            if (dao.UserType.ToLower() != "enduser")
            {
                return Ok(new { message = "INVALID_USER" });
            }
            return Ok(new { message = "VALID_USER" });
        }
    }
}
