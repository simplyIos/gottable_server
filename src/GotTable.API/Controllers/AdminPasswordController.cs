﻿using GotTable.API.Model.Users;
using GotTable.Library.Administrators;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class AdminPasswordController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAdministrator<DAO.Administrators.AdminDAO> administrator;

        public AdminPasswordController(IAdministrator<DAO.Administrators.AdminDAO> administrator)
        {
            this.administrator = administrator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(PasswordModel model)
        {
            if (string.IsNullOrEmpty(model.UserId))
            {
                return BadRequest("Missing userid");
            }
            if (string.IsNullOrEmpty(model.OldPassword) || string.IsNullOrEmpty(model.NewPassword))
            {
                return BadRequest("Missing password");
            }
            if (model.NewPassword.Length <= 5)
            {
                return BadRequest("Password minimum of 6 characters");
            }
            var adminDAO = await administrator.Get(decimal.Parse(model.UserId));
            if (adminDAO != null && adminDAO.UserId == 0)
            {
                return BadRequest("Invalid user");
            }
            if (adminDAO.Password != model.OldPassword)
            {
                return BadRequest("Invalid Old Password");
            }
            adminDAO.Password = model.NewPassword;
            await administrator.Save(adminDAO);
            return Ok(new { message = "Password changed" });
        }
    }
}
