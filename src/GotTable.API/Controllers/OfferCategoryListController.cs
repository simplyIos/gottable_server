﻿using GotTable.API.Model.OfferCategories;
using GotTable.Common.Enumerations;
using GotTable.DAO.OfferCategories;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.OfferCategories;
using GotTable.Library.OperationExceptionLogs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class OfferCategoryListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryList"></param>
        /// <param name="operationExceptionLog"></param>
        public OfferCategoryListController(ICategoryList categoryList, IOperationExceptionLog operationExceptionLog)
        {
            this.categoryList = categoryList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(Enumeration.RestaurantTypes? restaurantType = null)
        {
            var model = MapListToModel(await categoryList.Get(0, 10, restaurantType));
            return Ok(model);
        }

        #region Helper method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal List<OfferCategoryModel> MapListToModel(List<OfferCategoryDAO> list)
        {
            return list.Where(x => x.Active == true).Select(m => new OfferCategoryModel()
            {
                Name = m.Name,
                Id = m.Id,
                ImagePath = AppSettingKeys.AdminApplicationBaseUrl + "/" + m.ImagePath
            }).ToList();
        }

        #endregion

    }
}
