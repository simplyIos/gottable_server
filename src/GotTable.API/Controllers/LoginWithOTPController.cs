﻿using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.Users;
using GotTable.Common.Enumerations;
using GotTable.Library.Communications.Message;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Users;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LoginWithOTPController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        public LoginWithOTPController(IUser user, IOperationExceptionLog operationExceptionLog, IOTPCommand oTPCommand)
        {
            this.user = user;
            this.operationExceptionLog = operationExceptionLog;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="transactionId"></param>
        /// <param name="otpNumber"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(string phoneNumber, decimal transactionId, string otpNumber)
        {
            var model = new UserResponse<DetailModel>()
            {
                UserDetail = new DetailModel()
            };

            var oTPDetail = await oTPCommand.Get(transactionId);
            if (oTPDetail.IsUsed)
            {
                model.ErrorMessage = "Invalid OTP number, please try again";
                return BadRequest(model.ErrorMessage);
            }
            if (oTPDetail.OTP != otpNumber)
            {
                model.ErrorMessage = "Invalid OTP number, please try again";
                return BadRequest(model.ErrorMessage);
            }
            oTPDetail.StatusId = 1;
            await oTPCommand.Save(oTPDetail);
            var userDAO = await user.Get(phoneNumber);
            if (userDAO == null || userDAO.UserId == 0)
            {
                model.ErrorMessage = "This phone number does not exists. please check.!";
                return BadRequest(model.ErrorMessage);
            }
            if (!userDAO.IsActive)
            {
                model.ErrorMessage = "Your credentials has been locked.";
                return BadRequest(model.ErrorMessage);
            }
            if (userDAO.UserType == Enumeration.UserType.Guest.ToString())
            {
                userDAO.UserType = Enumeration.UserType.EndUser.ToString();
                await user.Save(userDAO);
            }
            model.ErrorMessage = string.Empty;
            model.UserDetail.Password = userDAO.Password;
            model.UserDetail.UserId = userDAO.UserId.ToString("G29");
            model.UserDetail.UserType = userDAO.UserType;
            model.UserDetail.FirstName = userDAO.FirstName;
            model.UserDetail.Gender = userDAO.Gender;
            model.UserDetail.Prefix = userDAO.Prefix;
            model.UserDetail.LastName = userDAO.LastName;
            model.UserDetail.PhoneNumber = userDAO.PhoneNumber;
            model.UserDetail.EmailAddress = userDAO.EmailAddress;
            model.UserDetail.AddressList = new System.Collections.Generic.List<AddressModel>();
            return Ok(model);
        }
    }
}
