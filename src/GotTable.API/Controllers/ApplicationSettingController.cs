﻿using GotTable.API.Model.ApplicationSettings;
using GotTable.Library.ApplicationSettings;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// ApplicationSettingController
    /// </summary>
    [AllowAnonymous]
    public sealed class ApplicationSettingController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationSetting applicationSetting;

        public ApplicationSettingController(IApplicationSetting applicationSetting)
        {
            this.applicationSetting = applicationSetting;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var model = new ApplicationSettingModel();
            var applicationSettingDAO = await applicationSetting.Get();
            model.AdminApplicationBaseUrl = applicationSettingDAO.AdminApplicationBaseUrl;
            model.UserApplicationBaseUrl = applicationSettingDAO.UserApplicationBaseUrl;
            model.DineInActive = applicationSettingDAO.DineInActive;
            model.DeliveryActive = applicationSettingDAO.DeliveryActive;
            model.TakeawayActive = applicationSettingDAO.TakeawayActive;
            return Ok(model);
        }
    }
}
