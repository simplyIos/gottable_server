﻿using GotTable.API.Model.DeviceTokens;
using GotTable.Common.Enumerations;
using GotTable.DAO.Devices;
using GotTable.Library.Devices;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DeviceTokenController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDevice device;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDeviceList deviceList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="tokenList"></param>
        /// <param name="operationExceptionLog"></param>
        public DeviceTokenController(IDevice device, IDeviceList deviceList, IOperationExceptionLog operationExceptionLog)
        {
            this.device = device;
            this.deviceList = deviceList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(DeviceToken request)
        {
            var model = new DeviceResponse
            {
                ErrorMessage = string.Empty
            };
            var objDevice = new DeviceDAO()
            {
                DeviceId = request.Token,
                IsActive = request.IsActive,
                SelectedDevice = (Enumeration.Device)Enum.Parse(typeof(Enumeration.Device), request.DeviceType),
                UserId = decimal.Parse(request.UserId),
                Latitude = string.IsNullOrEmpty(request.Latitude) ? string.Empty : request.Latitude,
                Longitude = string.IsNullOrEmpty(request.Longitude) ? string.Empty : request.Longitude,
                City = string.Empty
            };
            await device.Save(objDevice);
            return Ok(model);
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(Enumeration.Device deviceType)
        {
            var model = new DeviceListResponse<List<DeviceToken>>
            {
                ErrorMessage = string.Empty,
                List = new List<DeviceToken>()
            };
            foreach (var item in await deviceList.Get(deviceType))
            {
                model.List.Add(new DeviceToken()
                {
                    Token = item.DeviceId,
                    DeviceType = item.SelectedDevice.ToString(),
                    IsActive = true,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    UserId = item.UserId?.ToString("G29")
                });
            }
            return Ok(model);
        }
    }
}
