﻿using GotTable.API.Model.Documents;
using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantDocuments;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantDocuments;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DocumentListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocumentList documentList;

        public DocumentListController(IDocumentList documentList, IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.documentList = documentList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategory"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(decimal restaurantId, Enumeration.ImageCategory? imageCategory = null)
        {
            var model = MapListToModel(await documentList.Get(restaurantId, imageCategory));
            return Ok(model);
        }

        #region Helper method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<DocumentInfoModel> MapListToModel(List<DocumentInfoDAO> list)
        {
            return list.Where(x => x.Active == true).Select(m => new DocumentInfoModel()
            {
                DocumentId = m.DocumentId,
                Path = AppSettingKeys.AdminApplicationBaseUrl + "/" + m.Path.Replace("\\", "/")
            }).ToList();
        }

        #endregion
    }
}
