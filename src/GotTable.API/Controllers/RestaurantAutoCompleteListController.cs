﻿using GotTable.Library.Restaurants;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantAutoCompleteListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAutoCompleteList autoCompleteList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="autoCompleteList"></param>
        public RestaurantAutoCompleteListController(IAutoCompleteList autoCompleteList)
        {
            this.autoCompleteList = autoCompleteList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(string queryExpression)
        {
            var model = await autoCompleteList.Get(queryExpression);
            return Ok(model);
        }
    }
}
