﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantBookings;
using GotTable.API.Model.RestaurantBookings.DineIn;
using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DineIn;
using GotTable.DAO.Communications.Message;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DineInConfirmationController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineIn dineIn;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineIn"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="oTPCommand"></param>
        public DineInConfirmationController(IDineIn dineIn, IOperationExceptionLog operationExceptionLog, IOTPCommand oTPCommand)
        {
            this.dineIn = dineIn;
            this.operationExceptionLog = operationExceptionLog;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(Request<DineInBookingModel> request)
        {
            var model = new BookingResponse();
            if (!string.IsNullOrEmpty(request.Detail.TransactionId) && string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                model.ErrorMessage = "Invalid request, Please check.!";
                return BadRequest(model.ErrorMessage);
            }
            if (string.IsNullOrEmpty(request.Detail.TransactionId) && !string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                model.ErrorMessage = "Invalid request, Please check.!";
                return BadRequest(model.ErrorMessage);
            }
            var otpDetail = new OTPDetailDAO();

            if (!string.IsNullOrEmpty(request.Detail.TransactionId))
            {
                otpDetail = await oTPCommand.Get(Convert.ToDecimal(request.Detail.TransactionId));
                if (otpDetail == null)
                {
                    model.ErrorMessage = "Invalid OTP";
                    return BadRequest(model.ErrorMessage);
                }
                if (otpDetail.TransactionId == 0)
                {
                    model.ErrorMessage = "Invalid OTP";
                    return BadRequest(model.ErrorMessage);
                }
                if (otpDetail.IsUsed)
                {
                    model.ErrorMessage = "This OTP is already used";
                    return BadRequest(model.ErrorMessage);
                }
                if (otpDetail.OTP != request.Detail.OtpNumber)
                {
                    model.ErrorMessage = "Invalid OTP";
                    return BadRequest(model.ErrorMessage);
                }
            }

            var bookingDAO = new DineInBookingDAO()
            {
                DeviceId = request.DeviceId,
                DeviceType = request.DeviceType,
                Id = String.IsNullOrEmpty(request.Detail.BookingId) ? 0 : decimal.Parse(request.Detail.BookingId),
                BookingDate = DateTime.ParseExact(request.Detail.BookingDate, "MM-dd-yyyy", null),
                BookingTime = request.Detail.BookingTime,
                BranchId = String.IsNullOrEmpty(request.Detail.BranchId) ? 0 : decimal.Parse(request.Detail.BranchId),
                Comment = request.Detail.Comment,
                CreatedDate = DateTime.Now,
                TableId = String.IsNullOrEmpty(request.Detail.TableId) ? 0 : decimal.Parse(request.Detail.TableId),
                UserId = String.IsNullOrEmpty(request.Detail.UserId) ? 0 : decimal.Parse(request.Detail.UserId),
                UserDetail = new UserDetailDAO()
                {
                    FirstName = request.Detail.FirstName,
                    LastName = request.Detail.LastName,
                    PhoneNumber = request.Detail.PhoneNumber.ToString(),
                    IsActive = true,
                    IsEmailOptedForCommunication = request.Detail.IsEmailOptedForCommunication,
                    IsEmailVerified = false
                },
                EmailAddress = request.Detail.EmailAddress,
                OfferId = String.IsNullOrEmpty(request.Detail.OfferId) ? 0 : decimal.Parse(request.Detail.OfferId),
                PromoCode = request.Detail.PromoCode
            };

            await dineIn.New(bookingDAO);

            model.ErrorMessage = bookingDAO.ErrorMessage;
            model.ConfirmationMessage = bookingDAO.ConfirmationMessage;
            model.BookingId = bookingDAO.Id.ToString("G29");
            model.BookingType = Enumeration.BookingType.DineIn.ToString();

            if (otpDetail != null && !string.IsNullOrEmpty(request.Detail.TransactionId))
            {
                otpDetail.IsUsed = true;
                await oTPCommand.Save(otpDetail);
            }
            return Ok(model);
        }
    }
}
