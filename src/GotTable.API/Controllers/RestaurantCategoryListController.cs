﻿using GotTable.API.Model.RestaurantCategories;
using GotTable.Common.Models;
using GotTable.Library.OperationExceptionLogs;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantCategoryListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public RestaurantCategoryListController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var model = MapListToModel(Common.List.RestaurantCategoryList.Get());
            return Ok(model);
        }

        #region Helper method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal List<RestaurantCategoryModel> MapListToModel(List<EnumDto> list)
        {
            var returnlist = new List<RestaurantCategoryModel>();
            foreach (var item in list)
            {
                returnlist.Add(new RestaurantCategoryModel()
                {
                    Id = item.Id,
                    Name = item.Name.Replace("_", " ")
                });
            }
            return returnlist;
        }

        #endregion
    }
}
