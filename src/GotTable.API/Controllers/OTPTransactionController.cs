﻿using GotTable.API.Model.OTPTransactions;
using GotTable.DAO.Communications.Message;
using GotTable.Library.Communications.Message;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class OTPTransactionController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oTPCommand"></param>
        public OTPTransactionController(IOTPCommand oTPCommand)
        {
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(RequestOTPModel request)
        {
            if (string.IsNullOrEmpty(request.ExternalId))
            {
                return BadRequest("Missing ExternalId");
            }
            if (request.PhoneNumber == 0)
            {
                return BadRequest("Missing PhoneNumber");
            }
            if (request.Type == "")
            {
                return BadRequest("Missing Type");
            }

            var otpDetail = new OTPDetailDAO()
            {
                TransactionId = 0,
                IsUsed = false,
                OTP = string.Empty,
                PhoneNumber = request.PhoneNumber,
                Type = request.Type,
                ExternalId = string.IsNullOrEmpty(request.ExternalId) ? 0 : decimal.Parse(request.ExternalId),
                CreatedDate = DateTime.Now
            };

            await oTPCommand.New(otpDetail);
            if (otpDetail.TransactionId > 0)
            {
                return Ok(new { transactionid = otpDetail.TransactionId.ToString("G29"), message = "OTP Sent" });
            }
            else
            {
                return Ok(new { message = "Issue" });
            }
        }
    }
}
