﻿using GotTable.API.Model;
using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.Users;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message;
using GotTable.Library.Users;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class UserProfileController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="oTPCommand"></param>
        public UserProfileController(IUser user, IOTPCommand oTPCommand)
        {
            this.user = user;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(decimal userid)
        {
            var model = new UserResponse<DetailModel>
            {
                UserDetail = new DetailModel()
            };
            if (userid == 0)
            {
                model.ErrorMessage = "missing userId";
                model.UserDetail = default;
                return BadRequest(model.ErrorMessage);
            }
            var dao = await user.Get(userid);
            if (dao != null && dao.UserType != "EndUser")
            {
                model.ErrorMessage = "Invalid user type";
                return BadRequest(model.ErrorMessage);
            }
            if (!dao.IsActive)
            {
                model.ErrorMessage = "Inactive user";
                return BadRequest(model.ErrorMessage);
            }

            model.ErrorMessage = string.Empty;
            model.UserDetail.Password = dao.Password;
            model.UserDetail.UserId = dao.UserId.ToString("G29");
            model.UserDetail.UserType = dao.UserType;
            model.UserDetail.FirstName = dao.FirstName;
            model.UserDetail.Gender = dao.Gender;
            model.UserDetail.Prefix = dao.Prefix;
            model.UserDetail.LastName = dao.LastName;
            model.UserDetail.PhoneNumber = dao.PhoneNumber;
            model.UserDetail.RewardCount = dao.RewardCount ?? 0;
            model.UserDetail.EmailAddress = dao.EmailAddress;
            model.UserDetail.AddressList = new System.Collections.Generic.List<AddressModel>();
            return Ok(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(Request<ProfileModel> request)
        {
            var model = new UserResponse<DetailModel>();
            if (string.IsNullOrEmpty(request.Detail.TransactionId))
            {
                model.ErrorMessage = "Invalid request";
                return BadRequest(model.ErrorMessage);
            }
            if (string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                model.ErrorMessage = "Invalid request";
                return BadRequest(model.ErrorMessage);
            }
            if (string.IsNullOrEmpty(request.Detail.PhoneNumber))
            {
                model.ErrorMessage = "Please provide phone number.";
                return BadRequest(model.ErrorMessage);
            }
            if (string.IsNullOrEmpty(request.Detail.Password))
            {
                model.ErrorMessage = "Please provide password.";
                return BadRequest(model.ErrorMessage);
            }
            if (string.IsNullOrEmpty(request.Detail.FirstName))
            {
                model.ErrorMessage = "Please provide firstname.";
                return BadRequest(model.ErrorMessage);
            }
            if (string.IsNullOrEmpty(request.Detail.LastName))
            {
                model.ErrorMessage = "Please provide lastname.";
                return BadRequest(model.ErrorMessage);
            }

            var dao = await user.Get(request.Detail.PhoneNumber);
            if (dao != null && dao.UserId != 0)
            {
                model.ErrorMessage = "This phone number is already exists";
                return BadRequest(model.ErrorMessage);
            }
            var otpDetail = await oTPCommand.Get(Convert.ToInt16(request.Detail.TransactionId));
            if (otpDetail != null && otpDetail.TransactionId == 0)
            {
                model.ErrorMessage = "Invalid OTP";
                return BadRequest(model.ErrorMessage);
            }
            if (otpDetail.IsUsed)
            {
                model.ErrorMessage = "Invalid OTP";
                return BadRequest(model.ErrorMessage);
            }
            if (otpDetail.OTP != request.Detail.OtpNumber)
            {
                model.ErrorMessage = "Invalid OTP";
                return BadRequest(model.ErrorMessage);
            }
            if (otpDetail.OTP == request.Detail.OtpNumber)
            {
                otpDetail.IsUsed = true;
            }

            dao = new UserDetailDAO()
            {
                FirstName = request.Detail.FirstName,
                UserType = "EndUser",
                Password = request.Detail.Password,
                LastName = request.Detail.LastName,
                Gender = "NotDefined",
                Prefix = "NotDefined",
                IsActive = true,
                UserId = decimal.Parse(request.Detail.UserId),
                PhoneNumber = request.Detail.PhoneNumber,
                EmailAddress = request.Detail.EmailAddress
            };

            if (request.Detail.UserId == "0")
            {
                await user.New(dao);
            }
            else
            {
                await user.Save(dao);
            }

            if (dao.UserId != 0)
            {
                model.ErrorMessage = string.Empty;
                model.UserDetail = new DetailModel()
                {
                    FirstName = dao.FirstName,
                    UserId = dao.UserId.ToString("G29"),
                    Gender = dao.Gender,
                    LastName = dao.LastName,
                    PhoneNumber = dao.PhoneNumber,
                    Password = dao.Password,
                    Prefix = dao.Prefix,
                    UserType = dao.UserType,
                    AddressList = new System.Collections.Generic.List<AddressModel>()
                };

                await oTPCommand.Save(otpDetail);
                return Ok(model);
            }
            else
            {
                model.ErrorMessage = dao.ErrorMessage;
                return BadRequest(model.ErrorMessage);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<IHttpActionResult> Patch(Request<EmailAddressUpdateModel> requestModel)
        {
            var model = new UserResponse<EmailAddressUpdateModel>();
            if (string.IsNullOrEmpty(requestModel.Detail.EmailAddress))
            {
                model.ErrorMessage = "Please provide email address.";
                return BadRequest(model.ErrorMessage);
            }
            if (requestModel.Detail.UserId == 0)
            {
                model.ErrorMessage = "Invalid request model, please check.!";
                return BadRequest(model.ErrorMessage);
            }
            var dao = await user.Get(requestModel.Detail.UserId);
            if (dao == null || dao.UserId == 0)
            {
                model.ErrorMessage = "Invalid request model, please check.!";
                return BadRequest(model.ErrorMessage);
            }
            dao.EmailAddress = requestModel.Detail.EmailAddress;
            await user.Save(dao);
            if (dao.UserId != 0)
            {
                model.ErrorMessage = string.Empty;
                model.UserDetail = new EmailAddressUpdateModel()
                {
                    UserId = dao.UserId,
                    EmailAddress = dao.EmailAddress
                };
                return Ok(model);
            }
            else
            {
                model.ErrorMessage = dao.ErrorMessage;
                return BadRequest(model.ErrorMessage);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(decimal userId)
        {
            var dao = await user.Get(userId);
            if (dao == null || dao.UserId == 0)
            {
                return BadRequest("Invalid request model, please check.!");
            }
            await user.Delete(dao);
            return Ok(new { message = "Profile deleted successfully.!" });
        }
    }
}
