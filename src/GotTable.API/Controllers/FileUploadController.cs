﻿using GotTable.API.Model;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class FileUploadController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public FileUploadController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(FileUploadModel model)
        {
            string errorMessage = "Invalid model properties, Please check";
            if (model.Content == null && string.IsNullOrEmpty(model.StringContent))
            {
                return BadRequest(errorMessage);
            }
            if (string.IsNullOrEmpty(model.FileExtension))
            {
                return BadRequest(errorMessage);
            }
            if (string.IsNullOrEmpty(model.FileName))
            {
                return BadRequest(errorMessage);
            }
            if (string.IsNullOrEmpty(model.FileName))
            {
                return BadRequest(errorMessage);
            }
            if (model.Content == null && model.StringContent != null)
            {
                model.Content = Convert.FromBase64String(model.StringContent);
            }
            string directoryPath = HttpContext.Current.Server.MapPath("~/Uploads/" + model.UploadType + @"//");
            string filePath = directoryPath + model.FileName;
            File.WriteAllBytes(filePath, model.Content);
            string imagePath = @"/Uploads/" + model.UploadType + @"/" + model.FileName;
            return Ok(new { Status = 1, FilePath = imagePath, ErrorMessage = string.Empty });
        }
    }
}
