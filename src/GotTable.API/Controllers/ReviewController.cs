﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantReviews;
using GotTable.DAO.RestaurantRatings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantRatings;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ReviewController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchRating branchRating;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchRating"></param>
        /// <param name="operationExceptionLog"></param>
        public ReviewController(IBranchRating branchRating, IOperationExceptionLog operationExceptionLog)
        {
            this.branchRating = branchRating;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(Request<ReviewModel> request)
        {
            var model = new ReviewResponse();
            var dao = new BranchRatingDAO()
            {
                BranchId = Convert.ToDecimal(request.Detail.BranchId),
                Comment = request.Detail.Comment,
                CreationDate = DateTime.Now,
                Id = Convert.ToDecimal(request.Detail.Id),
                IsActive = true,
                Rating = request.Detail.Rating,
                Title = request.Detail.Title,
                AmbienceRating = request.Detail.AmbienceRating,
                FoodRating = request.Detail.FoodRating,
                MusicRating = request.Detail.MusicRating,
                PriceRating = request.Detail.PriceRating,
                ServiceRating = request.Detail.ServiceRating,
                UserId = Convert.ToDecimal(request.Detail.UserId)
            };
            await branchRating.New(dao);
            if (dao.Status)
            {
                model.ReviewId = dao.StatusId.ToString();
                model.ErrorMessage = string.Empty;
                return Ok(model);
            }
            else
            {
                model.ReviewId = string.Empty;
                model.ErrorMessage = dao.ErrorMessage;
                return BadRequest(model.ErrorMessage);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restuarantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Check(decimal restuarantId, decimal userId)
        {
            string errorMessage = "Invalid request, please check.!";
            if (restuarantId == 0)
            {
                return BadRequest(errorMessage);
            }
            if (userId == 0)
            {
                return BadRequest(errorMessage);
            }
            if (await branchRating.CheckAuthencity(userId, restuarantId))
            {
                return Ok(new { message = "true" });
            }
            else
            {
                return Ok(new { message = "false" });
            }
        }
    }
}
