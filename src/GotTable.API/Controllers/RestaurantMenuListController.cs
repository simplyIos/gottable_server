﻿using GotTable.API.Model.Images;
using GotTable.API.Model.RestaurantMenus;
using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantMenus;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.MenuCategories;
using GotTable.Library.RestaurantMenus;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantMenuListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMenuList menuList;

        public RestaurantMenuListController(ICategoryList categoryList, IMenuList menuList)
        {
            this.categoryList = categoryList;
            this.menuList = menuList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(decimal restaurantId = 0, Enumeration.RestaurantTypes? restaurantType = null, int currentPage = 0, int pageSize = 10)
        {
            var model = new ResturantMenuModel<List<MenuCuisineModel>>();

            if (restaurantId == 0)
            {
                model.ErrorMessage = "missing restaurantId";
                return BadRequest(model.ErrorMessage);
            }
            if (restaurantType == null && restaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                model.ErrorMessage = "missing restaurantType";
                return BadRequest(model.ErrorMessage);
            }

            var categoryDAOs = await categoryList.Get(0, 10);
            if (categoryDAOs != null && categoryDAOs.Any())
            {
                categoryDAOs = categoryDAOs.OrderBy(x => x.Id).ToList();
                model.List = new List<MenuCuisineModel>();
                foreach (var item in categoryDAOs)
                {
                    var menuItems = await menuList.Get(restaurantId, item.Id, restaurantType, currentPage, pageSize);
                    model.List.Add(new MenuCuisineModel()
                    {
                        CategoryId = item.Id.ToString("G29"),
                        CategoryName = item.Name,
                        MenuItem = MapObject2Model(menuItems),
                    });
                }
            }

            model.ErrorMessage = string.Empty;
            return Ok(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectItem"></param>
        /// <returns></returns>
        internal List<MenuModel> MapObject2Model(List<BranchMenuDAO> objectItem)
        {
            var model = new List<MenuModel>();
            foreach (var item in objectItem)
            {
                if (item.IsActive)
                {
                    model.Add(new MenuModel()
                    {
                        CategoryName = item.SelectedCategory,
                        MenuId = item.Id.ToString("G29"),
                        Name = item.Name,
                        Price = item.Cost,
                        Description = item.Description,
                        Logo = new ImageModel()
                        {
                            Name = item.MenuLogo.Name,
                            Url = item.MenuLogo.Url ?? string.Format("{0}/{1}", AppSettingKeys.AdminApplicationBaseUrl, "images/logo.png")
                        }
                    });
                }
            }
            return model;
        }
    }
}
