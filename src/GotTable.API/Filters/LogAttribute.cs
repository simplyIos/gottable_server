﻿using GotTable.Common.Enumerations;
using GotTable.DAO.ApplicationLogs;
using GotTable.Library.ApplicationLogs;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GotTable.API.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LogAttribute : Attribute, IActionFilter
    {
        /// <summary>
        /// 
        /// </summary>
        private IApplicationLog applicationLog { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LogAttribute()
        {
            this.applicationLog = new ApplicationLog();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool AllowMultiple
        {
            get { return true; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="continuation"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var parameters = Newtonsoft.Json.JsonConvert.SerializeObject(actionContext.ActionArguments);
            object deviceType = null;
            object deviceId = null;
            var deviceTypeId = Enumeration.Device.NotDefined;
            if (actionContext.ActionArguments.ContainsKey("deviceid"))
            {
                actionContext.ActionArguments.TryGetValue("deviceid", out deviceId);
            }
            if (actionContext.ActionArguments.ContainsKey("deviceId"))
            {
                actionContext.ActionArguments.TryGetValue("deviceId", out deviceId);
            }
            if (actionContext.ActionArguments.ContainsKey("devicetype"))
            {
                actionContext.ActionArguments.TryGetValue("devicetype", out deviceType);
                if (deviceType != null)
                {
                    var parsedValue = char.ToUpper(deviceType.ToString()[0]) + deviceType.ToString().Substring(1);
                    Enum.TryParse(parsedValue, out deviceTypeId);
                }
            }
            if (actionContext.ActionArguments.ContainsKey("deviceType"))
            {
                actionContext.ActionArguments.TryGetValue("deviceType", out deviceType);
                if (deviceType != null)
                {
                    var parsedValue = char.ToUpper(deviceType.ToString()[0]) + deviceType.ToString().Substring(1);
                    Enum.TryParse(parsedValue, out deviceTypeId);
                }
            }
            var result = continuation();
            int.TryParse(identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier.ToString()).Value, out int userId);
            var log = new ApplicationLogDAO()
            {
                DeviceId = deviceId != null ? deviceId.ToString() : string.Empty,
                DeviceTypeId = (int)deviceTypeId,
                Error = string.Empty,
                Params = parameters,
                StatusCode = (int)result.Result.StatusCode,
                ControllerName = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                Url = actionContext.Request.RequestUri.ToString(),
                UserId = userId
            };
            await applicationLog.New(log);
            return await result;
        }
    }
}