﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Events
{
    /// <summary>
    /// EventModel
    /// </summary>
    public sealed class EventModel
    {
        /// <summary>
        /// ImageUrl
        /// </summary>
        [JsonProperty("imageurl")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        [JsonProperty("active")]
        public bool Active { get; set; }
    }
}
