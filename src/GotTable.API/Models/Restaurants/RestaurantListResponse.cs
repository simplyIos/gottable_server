﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Restaurants
{
    public sealed class ResturantListResponse<T>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("resturant_list")]
        public T RestaurantList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("total_restaurant")]
        public int TotalRestaurants { get; set; }
    }
}
