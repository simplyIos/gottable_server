﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Restaurants
{
    public sealed class ResturantDetailResponse<T>
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("resturant_detail")]
        public T ResturantDetail { get; set; }
    }
}
