﻿using Newtonsoft.Json;

namespace GotTable.API.Model.Restaurants
{
    public sealed class FilterItemModel<T>
    {
        [JsonProperty("id")]
        public T Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
