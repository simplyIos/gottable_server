﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Restaurants
{
    public class FilterModel
    {
        [JsonProperty("index")]
        public int Index { get; set; }


        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("key")]
        public string Key { get; set; }


        [JsonProperty("iconurl")]
        public string IconUrl { get; set; }


        [JsonProperty("list")]
        public List<FilterItemModel<decimal>> List { get; set; }
    }
}
