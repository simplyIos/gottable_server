﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Images
{
    /// <summary>
    /// ImageModel
    /// </summary>
    public sealed class ImageModel
    {
        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }


        /// <summary>
        /// Url
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
