﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Cuisines
{
    /// <summary>
    /// Model uses to get all cuisines.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class CuisinesListResponse<T>
    {
        /// <summary>
        /// Provide error message. 
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// list for all cuisines.
        /// </summary>
        [JsonProperty("cuisines_list")]
        public T CuisinesList { get; set; }
    }
}
