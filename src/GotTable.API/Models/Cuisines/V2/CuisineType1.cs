﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Cuisines.V2
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CuisineType1
    {
        [JsonProperty("id")]
        public string Id { get; set; }


        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
