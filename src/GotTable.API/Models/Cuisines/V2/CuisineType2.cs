﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Cuisines.V2
{
    public sealed class CuisineType2
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("list")]
        public List<CuisineType1> List { get; set; }
    }
}
