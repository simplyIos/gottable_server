﻿
namespace GotTable.DalEF.Localities
{
    /// <summary>
    /// LocaltyModel
    /// </summary>
    public sealed class LocaltyModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
