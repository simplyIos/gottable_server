﻿namespace GotTable.API.Model.RestaurantCategories
{
    /// <summary>
    /// RestaurantCategoryModel
    /// </summary>
    public sealed class RestaurantCategoryModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
