﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantBookings
{
    /// <summary>
    /// BookingResponse
    /// </summary>
    public sealed class BookingResponse
    {
        /// <summary>
        /// ConfirmationMessage
        /// </summary>
        [JsonProperty("confirmationmessage")]
        public string ConfirmationMessage { get; set; }

        /// <summary>
        /// ErrorMessage
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// BookingType
        /// </summary>
        [JsonProperty("bookingtype")]
        public string BookingType { get; set; }

        /// <summary>
        /// BookingId
        /// </summary>
        [JsonProperty("bookingid")]
        public string BookingId { get; set; }
    }
}
