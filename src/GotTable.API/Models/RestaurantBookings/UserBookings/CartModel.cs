﻿namespace GotTable.API.Model.UserBookings
{
    public sealed class CartModel
    {
        public string MenuName { get; set; }

        public int Quantity { get; set; }

        public string Price { get; set; }
    }
}
