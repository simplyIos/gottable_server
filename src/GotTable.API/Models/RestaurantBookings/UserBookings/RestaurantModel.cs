﻿namespace GotTable.API.Model.UserBookings
{
    public sealed class RestaurantModel
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
    }
}
