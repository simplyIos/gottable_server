﻿using System.Collections.Generic;

namespace GotTable.API.Model.UserBookings
{
    public sealed class UserBookingModel
    {
        public UserBookingModel()
        {
            this.DineInList = new List<DineInBookingModel>();
            this.DeliveryandTakeawayList = new List<DeliveryandTakeawayBookingModel>();
        }

        public string UserId { get; set; }

        public List<DineInBookingModel> DineInList { get; set; }

        public List<DeliveryandTakeawayBookingModel> DeliveryandTakeawayList { get; set; }
    }
}
