﻿namespace GotTable.API.Model.UserBookings
{
    public sealed class DeliveryAddressModel
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Landmark { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Latitiude { get; set; }

        public string Longitude { get; set; }
    }
}
