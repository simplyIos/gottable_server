﻿namespace GotTable.API.Model.UserBookings
{
    public sealed class RestaurantContactModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public long PhoneNumber { get; set; }
    }
}
