﻿using System.Collections.Generic;

namespace GotTable.API.Model.RestaurantOffers
{
    /// <summary>
    /// OfferList model
    /// </summary>
    public sealed class OfferListModel
    {
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// List
        /// </summary>
        public List<OfferModel> List { get; set; }
    }
}
