﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.DeviceTokens
{
    /// <summary>
    /// DeviceListResponse
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class DeviceListResponse<T>
    {
        /// <summary>
        /// ErrorMessage
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// List
        /// </summary>
        [JsonProperty("list")]
        public T List { get; set; }
    }
}
