﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.DeviceTokens
{
    /// <summary>
    /// DeviceToken
    /// </summary>
    public sealed class DeviceToken
    {
        /// <summary>
        /// UserId
        /// </summary>
        [JsonProperty("userid")]
        public string UserId { get; set; }

        /// <summary>
        /// DeviceType
        /// </summary>
        [JsonProperty("devicetype")]
        public string DeviceType { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        [JsonProperty("devicetoken")]
        public string Token { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [JsonProperty("longitude")]
        public string Longitude { get; set; }
    }
}
