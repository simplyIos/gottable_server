﻿using Newtonsoft.Json;

namespace GotTable.API.Model.PreferredRestaurants
{
    public sealed class PreferredRestaurantListModel<T>
    {
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("restaurantlist")]
        public T RestaurantList { get; set; }
    }
}
