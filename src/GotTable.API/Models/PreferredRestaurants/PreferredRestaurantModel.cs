﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.PreferredRestaurants
{
    /// <summary>
    /// PreferredRestaurantModel
    /// </summary>
    public class PreferredRestaurantModel
    {
        public PreferredRestaurantModel(string restaurantId = null, string name = null, string longitude = null, string latitude = null, string state = null, string zipCode = null, string address = null, string distance = null, string imageUrl = null, string seoKeyword = null, string seoTitle = null, string seoDescription = null, string tagLine = null)
        {
            RestaurantId = restaurantId;
            Name = name;
            Longitude = longitude;
            Latitude = latitude;
            State = state;
            Zipcode = zipCode;
            Address = address;
            Distance = distance;
            ImageUrl = imageUrl;
            SEOKeyword = seoKeyword;
            SEODescription = seoDescription;
            TagLine = tagLine;
        }

        /// <summary>
        /// RestaurantId
        /// </summary>
        [JsonProperty("RestaurantId")]
        public string RestaurantId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        [JsonProperty("Address")]
        public string Address { get; set; }

        /// <summary>
        /// City
        /// </summary>
        [JsonProperty("City")]
        public string City { get; set; }

        /// <summary>
        /// State
        /// </summary>
        [JsonProperty("State")]
        public string State { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        /// <summary>
        /// Zipcode
        /// </summary>
        [JsonProperty("Zipcode")]
        public string Zipcode { get; set; }

        /// <summary>
        /// Distance
        /// </summary>
        [JsonProperty("Distance")]
        public string Distance { get; set; }

        /// <summary>
        /// TagLine
        /// </summary>
        [JsonProperty("TagLine")]
        public string TagLine { get; set; }

        /// <summary>
        /// SEOTitle
        /// </summary>
        [JsonProperty("SEOTitle")]
        public string SEOTitle { get; set; }

        /// <summary>
        /// SEODescription
        /// </summary>
        [JsonProperty("SEODescription")]
        public string SEODescription { get; set; }

        /// <summary>
        /// SEOKeyword
        /// </summary>
        [JsonProperty("SEOKeyword")]
        public string SEOKeyword { get; set; }

        /// <summary>
        /// ImageUrl
        /// </summary>
        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Rating
        /// </summary>
        [JsonProperty("Rating")]
        public int Rating { get; set; }
    }
}
