﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantMenus
{
    /// <summary>
    /// ResturantMenuModel
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ResturantMenuModel<T>
    {
        /// <summary>
        /// Error Message
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// List Detail
        /// </summary>
        [JsonProperty("listdetail")]
        public T List { get; set; }
    }
}
