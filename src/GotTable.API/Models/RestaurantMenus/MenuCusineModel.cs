﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.RestaurantMenus
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class MenuCuisineModel
    {
        public MenuCuisineModel()
        {
            MenuItem = new List<MenuModel>();
        }

        /// <summary>
        /// CategoryId
        /// </summary>
        [JsonProperty("categoryid")]
        public string CategoryId { get; set; }

        /// <summary>
        /// CategoryName
        /// </summary>
        [JsonProperty("categoryname")]
        public string CategoryName { get; set; }

        /// <summary>
        /// MenuItemList
        /// </summary>
        [JsonProperty("menuitem")]
        public List<MenuModel> MenuItem { get; set; }
    }
}
