﻿
using GotTable.API.Model.Images;
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantMenus
{
    /// <summary>
    /// MenuModel
    /// </summary>
    public sealed class MenuModel
    {
        public MenuModel()
        {
            Logo = new ImageModel();
        }

        /// <summary>
        /// MenuId
        /// </summary>
        [JsonProperty("menuid")]
        public string MenuId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// CategoryName
        /// </summary>
        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        [JsonProperty("price")]
        public decimal Price { get; set; }

        /// <summary>
        /// Logo
        /// </summary>
        [JsonProperty("logo")]
        public ImageModel Logo { get; set; }
    }
}
