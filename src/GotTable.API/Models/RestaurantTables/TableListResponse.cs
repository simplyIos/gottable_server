﻿using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantTables
{
    /// <summary>
    /// ResturantTablesResponse
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ResturantTablesResponse<T>
    {
        /// <summary>
        /// Error message
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Table list
        /// </summary>
        [JsonProperty("tablelist")]
        public T TableList { get; set; }
    }
}
