﻿namespace GotTable.API.Model
{
    /// <summary>
    /// Model uses for uploading the file(s).
    /// </summary>
    public sealed class FileUploadModel
    {
        /// <summary>
        /// String content for android device.
        /// </summary>
        public string StringContent { get; set; }

        /// <summary>
        /// Bytes content for IOS device.
        /// </summary>
        public byte[] Content { get; set; }

        /// <summary>
        /// Provide the file name extension.
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Provide the file name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Provide the upload type.
        /// </summary>
        public string UploadType { get; set; }
    }
}
