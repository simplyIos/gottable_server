﻿using Autofac;
using Autofac.Integration.WebApi;
using GotTable.API.Providers;
using GotTable.Library.Extensions;
using GotTable.Library.Security;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Reflection;
using System.Web.Http;

[assembly: OwinStartup(typeof(GotTable.API.Startup))]

namespace GotTable.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // INJECTING DEPENDENCIES FOR AUTOFAC
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);
            ContainerExtension.Add(builder);
            var container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // SETTING TOKEN BASED AUTHENTICATION
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            var options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(5),
                Provider = new AuthorizationServerProvider(container.Resolve<IIdentityManager>())
            };
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            HttpConfiguration config = new HttpConfiguration();

            // CONFIGURING WEBAPI
            WebApiConfig.Register(config);
        }
    }
}