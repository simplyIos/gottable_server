﻿using Autofac;
using GotTable.Library.Restaurants;
using NUnit.Framework;
using System.Threading.Tasks;

namespace GotTable.Test.Restaurants
{
    [TestFixture]
    public class Restaurant : TestBase
    {
        [Test]
        public async Task Get()
        {
            var restaurant = AutofacContainer.Resolve<IRestaurant>();
            var restaurantDetail = await restaurant.Get(1);
            Assert.IsNotNull(restaurantDetail);
            Assert.IsNotNull(restaurantDetail.BranchId);
        }
    }
}
