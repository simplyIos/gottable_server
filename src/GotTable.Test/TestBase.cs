﻿using Autofac;
using GotTable.Library.ApplicationSettings;

namespace GotTable.Test
{
    public class TestBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected IContainer AutofacContainer;

        /// <summary>
        /// 
        /// </summary>
        public TestBase()
        {
            _ = AppSettingKeys.Init();
            var builder = new ContainerBuilder();
            Library.Extensions.ContainerExtension.Add(builder);
            AutofacContainer = builder.Build();
        }
    }
}
