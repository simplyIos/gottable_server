﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace GotTable.Test.RestaurantBookings
{
    [TestFixture]
    public class RestaurantBooking : TestBase
    {
        [Test]
        public async Task NewDineInBooking()
        {
            await Task.FromResult(1);
        }

        [Test]
        public async Task NewTakeawayBooking()
        {
            await Task.FromResult(1);
        }
    }
}
