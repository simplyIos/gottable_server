﻿
namespace GotTable.DAO.Users
{
    [System.Serializable]
    public sealed class PhoneNumberDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public decimal UserId { get; set; }

        public string Type { get; set; }

        public decimal Value { get; set; }

        public bool isActive { get; set; }

        public bool isVerified { get; set; }

        public string VerificationDate { get; set; }

    }
}
