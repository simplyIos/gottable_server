﻿
namespace GotTable.DAO.Users
{
    [System.Serializable]
    public sealed class UserAddressDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public decimal UserId { get; set; }

        public string Type { get; set; }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public bool isActive { get; set; }
    }
}
