﻿using System;

namespace GotTable.DAO.Versions
{
    [Serializable]
    public sealed class VersionDAO : EditModelDAO
    {
        public int VersionId { get; set; }

        public string Number { get; set; }

        public int TypeId { get; set; }

        public string TypeName { get; set; }

        public string Message { get; set; }

        public bool Active { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid SyncName { get; set; }

        public Guid SyncPassword { get; set; }

        public int AttachedDeviceCount { get; set; }
    }
}
