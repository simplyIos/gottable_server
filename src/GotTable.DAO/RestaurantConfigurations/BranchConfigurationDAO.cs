﻿
namespace GotTable.DAO.RestaurantConfigurations
{
    [System.Serializable]
    public sealed class BranchConfigurationDAO : EditModelDAO
    {
        public decimal BranchId { get; set; }

        public string TakeawayStandBy { get; set; }

        public string DeliveryStandBy { get; set; }

        public string DeliveryCoverage { get; set; }

        public decimal? DeliveryCharges { get; set; }

        public decimal? CentralGST { get; set; }

        public decimal? StateGST { get; set; }

        public bool DineInAutoCompleteActive { get; set; }

        public int? DineInAutoCompleteHours { get; set; }

        public bool DineInRewardActive { get; set; }

        public int? DineInReward { get; set; }
    }
}
