﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.DAO.RestaurantOffers
{
    [Serializable]
    public sealed class BranchOfferDAO : EditModelDAO
    {
        public BranchOfferDAO()
        {

        }

        public BranchOfferDAO(decimal restaurantId)
        {
            this.Id = 0;
            this.StartDate = DateTime.Now;
            this.EndDate = DateTime.Now.AddMinutes(30);
            this.IsActive = true;
            this.IsDelivery = true;
            this.IsDineIn = true;
            this.IsTakeAway = true;
            this.BranchId = restaurantId;
            this.Description = string.Empty;
            this.OfferType = null;
        }

        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public string StartDateInString { get; set; }

        public DateTime EndDate { get; set; }

        public string EndDateInString { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelivery { get; set; }

        public bool IsTakeAway { get; set; }

        public bool IsDineIn { get; set; }

        public int TypeId { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public Enumeration.OfferType? OfferType { get; set; }
    }
}
