﻿using GotTable.Common.Enumerations;

namespace GotTable.DAO.RestaurantContacts
{
    [System.Serializable]
    public sealed class BranchContactDAO : EditModelDAO
    {
        public BranchContactDAO()
        {
            this.Id = 0;
        }
        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public Enumeration.ContactType SelectedType { get; set; }
        public int SelectedTypeId
        {
            get { return (int)SelectedType; }
        }
        public string Name { get; set; }
        public decimal PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailAlert { get; set; }
        public bool IsPhoneAlert { get; set; }
    }
}
