﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.DAO.RestaurantInvoices
{
    [Serializable]
    public sealed class BranchInvoiceDAO : EditModelDAO
    {
        public BranchInvoiceDAO()
        {

        }

        public Guid InvoiceId { get; set; }

        public decimal BranchId { get; set; }

        public decimal Amount { get; set; }

        public string SelectedPrescription { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool? IsActive { get; set; }

        public bool DineIn { get; set; }

        public bool Delivery { get; set; }

        public bool Takeaway { get; set; }

        public bool IsPaymentDone { get; set; }

        public DateTime? PaymentDate { get; set; }

        public int? PaymentMethodId { get; set; }

        public Enumeration.PaymentMethod SelectedMethod
        {
            get
            {
                if (PaymentMethodId == null || PaymentMethodId == 0)
                {
                    return Enumeration.PaymentMethod.Pending;
                }
                else
                {
                    return (Enumeration.PaymentMethod)PaymentMethodId;
                }
            }
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public decimal UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool FirstInvoice { get; set; }
    }
}
