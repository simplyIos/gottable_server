﻿
namespace GotTable.DAO
{
    public class EditModelDAO
    {
        public EditModelDAO()
        {
            this.StatusId = 1;
            this.ErrorMessage = string.Empty;
        }
        public int StatusId { get; set; }

        public bool Status
        {
            get
            {
                if (StatusId == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string ErrorMessage { get; set; }
    }
}
