﻿using System;

namespace GotTable.DAO.GeoCordinates
{
    [Serializable]
    public sealed class GeoCoordinateDAO : EditModelDAO
    {
        public string Address { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
}
