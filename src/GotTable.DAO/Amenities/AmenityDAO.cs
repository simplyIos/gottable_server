﻿
using System;

namespace GotTable.DAO.Amenities
{
    [Serializable]
    public sealed class AmenityDAO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
