﻿
namespace GotTable.DAO.RestaurantDocuments
{
    public sealed class DocumentInfoDAO : ListModelDAO
    {
        public DocumentInfoDAO(int documentId = default, decimal restaurantId = default, string categoryName = default, string name = default, string extension = default, bool active = default, string restaurantDirectory = default)
        {
            DocumentId = documentId;
            CategoryName = categoryName;
            Active = active;
            Name = name;
            Extension = extension;
            Path = string.Format("{0}{1}/{2}{3}", restaurantDirectory, restaurantId, documentId, extension);
        }

        public int DocumentId { get; private set; }

        public string CategoryName { get; private set; }

        public string Name { get; private set; }

        public string Extension { get; private set; }

        public bool Active { get; private set; }

        public string Path { get; private set; }
    }
}
