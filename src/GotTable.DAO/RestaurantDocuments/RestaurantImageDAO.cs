﻿using GotTable.Common.Enumerations;

namespace GotTable.DAO.RestaurantDocuments
{
    [System.Serializable]
    public sealed class RestaurantImageDAO : ListModelDAO
    {
        public RestaurantImageDAO()
        {

        }

        public RestaurantImageDAO(decimal restaurantId)
        {
            this.FrontImage = new FileModelDAO(restaurantId, Enumeration.ImageType.Front.ToString());
            this.BackImage = new FileModelDAO(restaurantId, Enumeration.ImageType.Inside.ToString());
            this.LogoImage = new FileModelDAO(restaurantId, Enumeration.ImageType.Logo.ToString());
        }

        public FileModelDAO FrontImage { get; set; }

        public FileModelDAO BackImage { get; set; }

        public FileModelDAO LogoImage { get; set; }
    }
}
