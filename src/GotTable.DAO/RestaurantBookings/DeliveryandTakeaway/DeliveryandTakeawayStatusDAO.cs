﻿using System;

namespace GotTable.DAO.Bookings.DeliveryandTakeaway
{
    public sealed class DeliveryandTakeawayStatusDAO : EditModelDAO
    {
        public decimal BookingId { get; set; }

        public string StatusName { get; set; }

        public decimal? UserId { get; set; }

        public string UserName { get; set; }

        public decimal? AdminId { get; set; }

        public string AdminName { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
