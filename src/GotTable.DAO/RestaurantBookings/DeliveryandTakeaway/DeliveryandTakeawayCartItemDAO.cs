﻿
namespace GotTable.DAO.Bookings.DeliveryandTakeaway
{
    public sealed class DeliveryandTakeawayCartItemDAO : EditModelDAO
    {
        public decimal ItemId { get; set; }

        public decimal MenuId { get; set; }

        public string MenuName { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public decimal Amount
        {
            get
            {
                return Price * Quantity;
            }
        }

        public string Remark { get; set; }
    }
}
