﻿using System;

namespace GotTable.DAO.Bookings.DeliveryandTakeaway
{
    public sealed class DeliveryandTakeawayListModelDAO : ListModelDAO
    {
        public string BookingId { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingType { get; set; }

        public string BranchId { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string OfferId { get; set; }

        public string CurentStatus { get; set; }

        public long PhoneNumber { get; set; }

        public decimal Cartamount { get; set; }

        public bool IsRead { get; set; }
    }
}
