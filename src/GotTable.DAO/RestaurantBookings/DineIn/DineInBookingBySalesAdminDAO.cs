﻿using GotTable.DAO.RestaurantOffers;
using GotTable.DAO.RestaurantTables;
using System;
using System.Collections.Generic;

namespace GotTable.DAO.Bookings.DineIn
{
    public sealed class DineInBookingBySalesAdminDAO : EditModelDAO
    {
        public DineInBookingBySalesAdminDAO()
        {
            Tables = new List<BranchTableDAO>();
            Offers = new List<BranchOfferDAO>();
        }

        public decimal BookingId { get; set; }

        public decimal RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public decimal UserId { get; set; }

        public string BookingDateTime { get; set; }

        public string BookingDate
        {
            get { return DateTime.ParseExact(this.BookingDateTime, "MM-dd-yyyy hh:mm tt", null).ToString("MM-dd-yyyy"); }
        }

        public string BookingTime
        {
            get { return DateTime.ParseExact(this.BookingDateTime, "MM-dd-yyyy hh:mm tt", null).ToString("HH:mm"); }
        }

        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public long PhoneNumber { get; set; }

        public string Comment { get; set; }

        public string PromoCode { get; set; }

        public string ConfirmationMessage { get; set; }

        public decimal SelectedTableId { get; set; }

        public List<BranchTableDAO> Tables { get; set; }

        public decimal SelectedOfferId { get; set; }

        public List<BranchOfferDAO> Offers { get; set; }
    }
}
