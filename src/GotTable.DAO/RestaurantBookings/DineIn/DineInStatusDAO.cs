﻿using System;

namespace GotTable.DAO.Bookings.DineIn
{
    [Serializable]
    public sealed class DineInStatusDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public decimal BookingId { get; set; }

        public string Comment { get; set; }

        public string StatusName { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal? UserId { get; set; }

        public string UserName { get; set; }

        public decimal? AdminId { get; set; }

        public string AdminName { get; set; }
    }
}
