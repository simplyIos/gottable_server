﻿using System.Collections.Generic;

namespace GotTable.DAO.Bookings.Users
{
    public sealed class UserBookingDAO : ListModelDAO
    {
        public UserBookingDAO()
        {
            this.DineInList = new List<DineInListDAO>();
            this.DeliveryandTakeawayList = new List<DeliveryandTakeawayListDAO>();
        }

        public decimal UserId { get; set; }

        public List<DineInListDAO> DineInList { get; set; }

        public List<DeliveryandTakeawayListDAO> DeliveryandTakeawayList { get; set; }
    }
}
