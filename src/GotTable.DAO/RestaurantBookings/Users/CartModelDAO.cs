﻿
namespace GotTable.DAO.Bookings.Users
{
    public sealed class CartModelDAO : EditModelDAO
    {
        public string MenuName { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
