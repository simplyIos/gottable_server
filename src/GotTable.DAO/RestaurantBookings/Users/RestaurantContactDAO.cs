﻿
namespace GotTable.DAO.Bookings.Users
{
    public sealed class RestaurantContactDAO : ListModelDAO
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
