﻿
namespace GotTable.DAO.Bookings.Users
{
    public sealed class DeliveryAddressDAO : ListModelDAO
    {
        public DeliveryAddressDAO()
        {

        }

        public DeliveryAddressDAO(string line1, string line2, string landmark, string city, string state, decimal zip, string latitude, string longitude)
        {
            this.Line1 = line1;
            this.Line2 = line2;
            this.Landmark = landmark;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Latitiude = latitude;
            this.Longitude = longitude;
        }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Landmark { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public string Latitiude { get; set; }

        public string Longitude { get; set; }
    }
}
