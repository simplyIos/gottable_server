﻿
namespace GotTable.DAO.Bookings.Users
{
    public sealed class RestaurantDAO : ListModelDAO
    {
        public RestaurantDAO()
        {

        }

        public RestaurantDAO(string name, string address, string city, string state, string zipcode)
        {
            this.Name = name;
            this.Address = address;
            this.City = city;
            this.State = state;
            this.Zip = zipcode;
        }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
    }
}
