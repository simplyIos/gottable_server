﻿using System;

namespace GotTable.DAO.PushNotifications
{
    [Serializable]
    public sealed class NotificationExceptionLogDAO : ListModelDAO
    {
        public int ExceptionId { get; set; }

        public string DeviceId { get; set; }

        public int DeviceTypeId { get; set; }

        public int NotificationId { get; set; }

        public string Source { get; set; }

        public string Message { get; set; }

        public string InnerException { get; set; }

        public string StackTrace { get; set; }
    }
}
