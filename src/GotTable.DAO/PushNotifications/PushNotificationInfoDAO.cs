﻿using System;

namespace GotTable.DAO.PushNotifications
{
    [Serializable]
    public sealed class PushNotificationInfoDAO
    {
        public PushNotificationInfoDAO()
        {
            this.Id = 0;
        }

        public int Id { get; set; }

        public string Message { get; set; }

        public string Title { get; set; }

        public bool Active { get; set; }

        public string StatusName { get; set; }

        public decimal? CityId { get; set; }

        public decimal UserId { get; set; }

        public string ScheduleTime { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public string CityName { get; set; }

        public string NotificationType { get; set; }

        public string OfferTypeName { get; set; }

        public string CategoryTypeName { get; set; }

        public int FailureCount { get; set; }

        public int SuccessCount { get; set; }

        public string IosPayLoad { get; set; }

        public string AndroidPayLoad { get; set; }
    }
}
