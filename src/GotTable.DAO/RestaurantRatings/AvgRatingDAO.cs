﻿using System;

namespace GotTable.DAO.RestaurantRatings
{
    [Serializable]
    public sealed class AvgRatingDAO : ListModelDAO
    {
        public int Rating { get; set; }

        public int AmbienceRating { get; set; }

        public int FoodRating { get; set; }

        public int MusicRating { get; set; }

        public int PriceRating { get; set; }

        public int ServiceRating { get; set; }
    }
}
