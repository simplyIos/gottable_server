﻿
namespace GotTable.DAO.Localities
{
    public sealed class LocaltyDAO
    {
        public decimal Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
