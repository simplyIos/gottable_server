﻿using System;

namespace GotTable.DAO.Reports
{
    [Serializable]
    public sealed class RestaurantDAO
    {
        public decimal BranchId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}
