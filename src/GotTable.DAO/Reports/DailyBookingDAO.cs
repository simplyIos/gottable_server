﻿
using System;

namespace GotTable.DAO.Reports
{
    [Serializable]
    public sealed class DailyBookingDAO : ListModelDAO
    {
        public decimal PhoneNumber { get; set; }

        public string BookingType { get; set; }

        public decimal BranchId { get; set; }

        public string BranchName { get; set; }

        public string BranchAddress { get; set; }

        public string BranchCity { get; set; }

        public string BranchState { get; set; }

        public decimal BranchZipCode { get; set; }

        public string UserName { get; set; }

        public string BookingDate { get; set; }

        public string BookingTime { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
