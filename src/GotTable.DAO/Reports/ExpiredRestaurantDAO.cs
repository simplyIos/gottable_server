﻿using System;

namespace GotTable.DAO.Reports
{
    [Serializable]
    public class ExpiredRestaurantDAO : ListModelDAO
    {
        public decimal RestaurantId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}
