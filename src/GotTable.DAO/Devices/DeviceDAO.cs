﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.DAO.Devices
{
    [Serializable]
    public sealed class DeviceDAO : EditModelDAO
    {
        public string DeviceId { get; set; }

        public Enumeration.Device SelectedDevice { get; set; }

        public decimal? UserId { get; set; }

        public bool IsActive { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string City { get; set; }

        public string VersionNumber { get; set; }
    }
}
