﻿
namespace GotTable.DAO.RestaurantTables
{
    [System.Serializable]
    public sealed class BranchTableDAO : EditModelDAO
    {
        public BranchTableDAO()
        {

        }

        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public string SelectedTable { get; set; }

        public int Value { get; set; }

        public bool Active { get; set; }
    }
}
