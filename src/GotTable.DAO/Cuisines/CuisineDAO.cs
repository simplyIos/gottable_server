﻿using System;

namespace GotTable.DAO.Cuisines
{
    [Serializable]
    public sealed class CuisineDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public string CuisineName { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int DisplayLevel { get; set; }

        public int EngagedRestaurant { get; set; }
    }
}
