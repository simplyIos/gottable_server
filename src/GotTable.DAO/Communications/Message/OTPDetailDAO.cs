﻿using System;

namespace GotTable.DAO.Communications.Message
{
    [Serializable]
    public sealed class OTPDetailDAO : EditModelDAO
    {
        public decimal TransactionId { get; set; }

        public decimal ExternalId { get; set; }

        public string Type { get; set; }

        public long PhoneNumber { get; set; }

        public string OTP { get; set; }

        public bool IsUsed { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
