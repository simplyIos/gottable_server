﻿
using System;
using System.Collections.Generic;

namespace GotTable.DAO.Communications.Message
{
    [Serializable]
    public sealed class SMSModelDAO : EditModelDAO
    {
        public SMSModelDAO()
        {
            this.to = new List<long>();
        }

        public string message { get; set; }

        public List<long> to { get; set; }
    }
}
