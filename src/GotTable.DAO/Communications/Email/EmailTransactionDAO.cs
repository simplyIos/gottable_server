﻿using System;

namespace GotTable.DAO.Communications.Email
{
    [Serializable]
    public sealed class EmailTransactionDAO : EditModelDAO
    {
        public int TemplateTypeId { get; set; }

        public string ExternalId { get; set; }
    }
}
