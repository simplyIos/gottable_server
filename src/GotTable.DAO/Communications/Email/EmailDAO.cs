﻿using System;

namespace GotTable.DAO.Communications.Email
{
    [Serializable]
    public sealed class EmailDAO : EditModelDAO
    {
        public string Email_To { get; set; }

        public string Email_CC { get; set; }

        public string Email_BCC { get; set; }

        public string Email_Body { get; set; }

        public string Email_Subject { get; set; }
    }
}
