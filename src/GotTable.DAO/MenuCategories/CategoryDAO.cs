﻿using System;

namespace GotTable.DAO.MenuCategories
{
    [Serializable]
    public sealed class CategoryDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public string Name { get; set; }
    }
}
