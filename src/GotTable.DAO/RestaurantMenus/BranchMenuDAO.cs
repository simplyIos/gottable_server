﻿
using GotTable.DAO.Images;

namespace GotTable.DAO.RestaurantMenus
{
    [System.Serializable]
    public sealed class BranchMenuDAO : EditModelDAO
    {
        public BranchMenuDAO()
        {
            this.MenuLogo = new ImageDAO();
        }

        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public string Description { get; set; }

        public string SelectedMenuCategory { get; set; }

        public string SelectedCuisine { get; set; }

        public decimal SelectedBranchCuisineId { get; set; }

        public string SelectedCategory { get; set; }

        public string Name { get; set; }

        public decimal Cost { get; set; }

        public bool IsActive { get; set; }

        public bool IsDineAvailable { get; set; }

        public bool IsTakeAwayAvailable { get; set; }

        public bool IsDeliveryAvailable { get; set; }

        public ImageDAO MenuLogo { get; set; }
    }
}
