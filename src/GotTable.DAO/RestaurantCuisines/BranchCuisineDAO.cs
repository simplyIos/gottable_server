﻿
namespace GotTable.DAO.RestaurantCuisines
{
    [System.Serializable]
    public sealed class BranchCuisineDAO : EditModelDAO
    {
        public BranchCuisineDAO()
        {

        }

        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public string SelectedCuisine { get; set; }
        public bool IsActive { get; set; }
    }
}
