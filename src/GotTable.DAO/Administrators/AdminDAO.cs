﻿using System;

namespace GotTable.DAO.Administrators
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class AdminDAO : EditModelDAO
    {
        /// <summary>
        /// UserId
        /// </summary>
        public decimal UserId { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public string UserType { get; set; }

        public string Prefix { get; set; }

        public bool IsActive { get; set; }

        public decimal? CityId { get; set; }

        public bool? EnableButtonForRestaurantLogin { get; set; }
    }
}
