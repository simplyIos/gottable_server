﻿using System;

namespace GotTable.DAO.Administrators
{
    [Serializable]
    public sealed class AdminInfoDAO : ListModelDAO
    {
        public decimal UserId { get; set; }

        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserType { get; set; }

        public bool IsActive { get; set; }

        public decimal? CityId { get; set; }

        public string CityName { get; set; }

        public bool? EnableButtonForRestaurantLogin { get; set; }

        public string FullName { get { return string.Format("{0} {1} - {2}", FirstName, LastName, EmailAddress); } }
    }
}
