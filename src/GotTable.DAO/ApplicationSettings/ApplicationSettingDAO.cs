﻿using System;

namespace GotTable.DAO.ApplicationSettings
{
    /// <summary>
    /// ApplicationSettingDAO
    /// </summary>
    [Serializable]
    public sealed class ApplicationSettingDAO : EditModelDAO
    {
        public string GeoUrl { get; set; }

        public string OTPAuthUrl { get; set; }

        public string OTPAuthKey { get; set; }

        public string TwoFactorAPIURl { get; set; }

        public string TwoFactorAPIAuthKey { get; set; }

        public byte[] ApnsCertificateContent { get; set; }

        public string ApnsCeritificatePassword { get; set; }

        public string AdminApplicationBaseUrl { get; set; }

        public string UserApplicationBaseUrl { get; set; }

        public string FCMServerKey { get; set; }

        public string FCMSenderId { get; set; }

        public string ApplicationIdPackageName { get; set; }

        public string FCMUrl { get; set; }

        public bool DineInActive { get; set; }

        public bool DeliveryActive { get; set; }

        public bool TakeawayActive { get; set; }

        public string RestaurantDirectory { get; set; }

        public string OfferDirectory { get; set; }

        public string NotificationDirectory { get; set; }

        public bool ServiceBusFeatureEnable { get; set; }

        public bool HangfireFeatureEnable { get; set; }

        public bool? DineInEmailTransmission { get; set; } = false;

        public bool? DeliveryEmailTransmission { get; set; } = false;

        public bool? TakeawayEmailTransmission { get; set; } = false;

        public bool? DineInMessageTransmission { get; set; } = false;

        public bool? DeliveryMessageTransmission { get; set; } = false;

        public bool? TakeawayMessageTransmission { get; set; } = false;
    }
}
