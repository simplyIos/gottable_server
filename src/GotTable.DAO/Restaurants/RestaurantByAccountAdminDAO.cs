﻿
using GotTable.Common.Enumerations;

namespace GotTable.DAO.Restaurants
{
    [System.Serializable]
    public sealed class RestaurantByAccountAdminDAO : ListModelDAO
    {
        public decimal SalesPersonId { get; set; }

        public string SalesAdminName { get; set; }

        public string SalesAdminEmailAddress { get; set; }

        public decimal RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public bool IsActive { get; set; }

        public bool Open4AccountAdmin { get; set; }

        public Enumeration.RestaurantCategories Category { get; set; }
    }
}
