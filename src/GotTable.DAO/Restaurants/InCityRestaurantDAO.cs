﻿
namespace GotTable.DAO.Restaurants
{
    [System.Serializable]
    public class InCityRestaurantDAO : ListModelDAO
    {
        public decimal BranchId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }
    }
}
