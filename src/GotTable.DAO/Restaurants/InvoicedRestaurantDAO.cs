﻿
using GotTable.Common.Enumerations;
using System;

namespace GotTable.DAO.Restaurants
{
    [Serializable]
    public sealed class InvoicedRestaurantDAO : ListModelDAO
    {
        public string SalesPerson { get; set; }

        public decimal? SalesPersonId { get; set; }

        public decimal RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal Amount { get; set; }

        public string Subscription { get; set; }

        public bool? IsActive { get; set; }

        public double? RemainingDays
        {
            get
            {
                if (this.EndDate != null)
                {
                    return (Convert.ToDateTime(this.EndDate).Date - DateTime.Now.Date).TotalDays;
                }
                else
                {
                    return null;
                }
            }
        }

        public Enumeration.RestaurantCategories Category { get; set; }
    }
}
