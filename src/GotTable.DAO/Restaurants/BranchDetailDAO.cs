﻿
namespace GotTable.DAO.Restaurants
{
    [System.Serializable]
    public sealed class BranchDetailDAO : EditModelDAO
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal BranchId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BranchName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TagLine { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Zip { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal SalesPersonId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal AccountAdminPersonId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Open4AccountAdmin { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SEOKeyword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SEOTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SEODescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LocalityId1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LocalityName1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LocalityId2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LocalityName2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal? LocalityId3 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LocalityName3 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LocalityId4 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LocalityName4 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CostForTwo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? AdminId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminEmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool DineInActive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool DeliveryActive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool TakeawayActive { get; set; }
    }
}
