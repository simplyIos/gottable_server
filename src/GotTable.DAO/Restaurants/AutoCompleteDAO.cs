﻿using System.Collections.Generic;

namespace GotTable.DAO.Restaurants
{
    public sealed class AutoCompleteDAO
    {
        public IEnumerable<AutoCompleteItemDAO> Locations { get; set; }

        public IEnumerable<AutoCompleteItemDAO> Names { get; set; }

        public IEnumerable<AutoCompleteItemDAO> Cuisines { get; set; }
    }
}
