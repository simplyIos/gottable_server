﻿
namespace GotTable.DAO.Restaurants.PreferredRestaurants
{
    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    public sealed class PreferredRestaurantDAO : EditModelDAO
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RestaurantId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IndexValue { get; set; }
    }
}
