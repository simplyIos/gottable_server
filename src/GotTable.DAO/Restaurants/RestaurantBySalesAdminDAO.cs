﻿
using GotTable.Common.Enumerations;
using System;

namespace GotTable.DAO.Restaurants
{
    [Serializable]
    public sealed class RestaurantBySalesAdminDAO : ListModelDAO
    {
        public decimal RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public Guid InvoiceId { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public int PrescriptionTypeId { get; set; }

        public double? RemainingDays
        {
            get
            {
                if (this.EndDate != "")
                {
                    return (Convert.ToDateTime(this.EndDate).Date - DateTime.Now.Date).TotalDays;
                }
                else
                {
                    return null;
                }
            }
        }

        public Enumeration.PrescriptionTypes? SelectedPrescription
        {
            get { if (this.PrescriptionTypeId == 0) return null; else return (Enumeration.PrescriptionTypes)this.PrescriptionTypeId; }
        }

        public bool RestaurantStatus { get; set; }

        public bool InvoiceStatus { get; set; }

        public decimal Amount { get; set; }

        public bool DineIn { get; set; }

        public bool Delivery { get; set; }

        public bool TakeAway { get; set; }

        public string AccountAdminName { get; set; }

        public string AccountAdminEmail { get; set; }

        public decimal AccountAdminPersonId { get; set; }

        public Enumeration.RestaurantCategories Category { get; set; }
    }
}
