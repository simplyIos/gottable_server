﻿
namespace GotTable.DAO.Restaurants.PreferredRestaurants
{
    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    public sealed class PreferredRestaurantSwapDAO : ListModelDAO
    {
        /// <summary>
        /// FirstRestaurantId
        /// </summary>
        public decimal FirstRestaurantId { get; set; }

        /// <summary>
        /// FirstRestaurantIndex
        /// </summary>
        public int FirstRestaurantIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal SecondRestaurantId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SecondRestaurantIndex { get; set; }
    }
}
