﻿using System;

namespace GotTable.DAO.OperationExceptions
{
    [Serializable]
    public sealed class OperationExceptionLogDAO : ListModelDAO
    {
        public Guid ExceptionId { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string InnerException { get; set; }

        public string ActionName { get; set; }

        public string ControllerName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
