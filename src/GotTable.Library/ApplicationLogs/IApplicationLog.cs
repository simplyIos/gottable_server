﻿using GotTable.DAO.ApplicationLogs;
using System.Threading.Tasks;

namespace GotTable.Library.ApplicationLogs
{
    /// <summary>
    /// IApplicationLog
    /// </summary>
    public interface IApplicationLog
    {
        /// <summary>
        /// New
        /// </summary>
        /// <param name="logDAO"></param>
        /// <returns></returns>
        Task New(ApplicationLogDAO logDAO);
    }
}
