﻿using GotTable.Dal;
using GotTable.Dal.Cities;
using GotTable.DAO.Cities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Cities
{
    /// <summary>
    /// CityList
    /// </summary>
    [Serializable]
    public sealed class CityList : ICityList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        public async Task<List<CityInfoDAO>> Get()
        {
            var dal = DalFactory.Create<ICityDal>();
            var dtos = await dal.FetchList();
            var model = new List<CityInfoDAO>
            {
                new CityInfoDAO()
                {
                    Id = null,
                    Name = "All"
                }
            };
            dtos.ForEach(row =>
            {
                model.Add(new CityInfoDAO()
                {
                    Id = row.Id,
                    Name = row.Name
                });
            });
            return model;
        }
    }
}
