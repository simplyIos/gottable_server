﻿using GotTable.Dal;
using GotTable.Dal.Security;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Security
{
    [Serializable]
    public class IdentityManager : IIdentityManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<ApplicationIdentityDto> Get(string userName, string password)
        {
            var dal = DalFactory.Create<IApplicationIdentityDal>();
            return await dal.Get(userName, password);
        }
    }
}
