﻿using GotTable.Dal.Security;
using System.Threading.Tasks;

namespace GotTable.Library.Security
{
    public interface IIdentityManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ApplicationIdentityDto> Get(string userName, string password);
    }
}