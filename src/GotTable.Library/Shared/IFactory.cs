﻿namespace GotTable.Library.Shared
{
    public interface IFactory
    {
        void Get<T>(T factoryObject);

        void Create<T>(T factoryObject);

        void New<T>(T factoryObject);

        void Save<T>(T factoryObject);

        void Delete<T>(T factoryObject);
    }
}
