﻿using NServiceBus;
using System.Threading.Tasks;

namespace GotTable.Library.Shared
{
    public sealed class ServiceBus
    {
        private static IEndpointInstance bus;

        public static async void Init()
        {
            var endpointConfiguration = new EndpointConfiguration("GotTableServiceBus");
            endpointConfiguration.MakeInstanceUniquelyAddressable("1");
            endpointConfiguration.EnableCallbacks();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                endpointConfiguration.LimitMessageProcessingConcurrencyTo(1);
            }
            endpointConfiguration.UsePersistence<LearningPersistence>();
            endpointConfiguration.UseTransport<LearningTransport>();

            bus = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);
        }

        public static async Task PublishAsync<T>(T @event)
        {
            await bus.Publish(@event).ConfigureAwait(false);
        }
    }
}
