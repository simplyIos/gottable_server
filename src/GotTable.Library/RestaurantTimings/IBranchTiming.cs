﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantTimings;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTimings
{
    public interface IBranchTiming
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        Task Default(decimal branchId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timingDAO"></param>
        /// <returns></returns>
        Task Save(BranchTimingDAO timingDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        Task<BranchTimingDAO> Get(decimal restaurantId, Enumeration.RestaurantTypes restaurantType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <returns></returns>
        Task<bool> CheckAvailability(decimal restaurantId, Enumeration.RestaurantTypes restaurantType, DateTime bookingDate, string bookingTime);
    }
}