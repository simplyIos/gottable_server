﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantTimings;
using GotTable.DAO.RestaurantTimings;
using GotTable.Library.ApplicationSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTimings
{
    [Serializable]
    public sealed class TimingList : ITimingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="dineIn"></param>
        /// <param name="delivery"></param>
        /// <param name="takeAway"></param>
        /// <returns></returns>
        public async Task<List<BranchTimingDAO>> Get(decimal restaurantId, bool? dineIn, bool? delivery, bool? takeAway)
        {
            var timingList = new List<BranchTimingDAO>();
            if (dineIn == true && AppSettingKeys.DineInActive.Value)
            {
                var timingItem = new BranchTimingDAO
                {
                    BranchId = restaurantId,
                    SelectedCategory = Enumeration.RestaurantTypes.DineIn
                };
                var dal = DalFactory.Create<ITimingDal>();
                foreach (var item in await dal.Fetch(restaurantId, (int)Enumeration.RestaurantTypes.DineIn))
                {
                    timingItem.DayTiming.Add(MapObject2Model(item));
                }
                timingList.Add(timingItem);
            }
            if (delivery == true && AppSettingKeys.DeliveryActive.Value)
            {
                var timingItem = new BranchTimingDAO();
                timingItem.BranchId = restaurantId;
                timingItem.SelectedCategory = Enumeration.RestaurantTypes.Delivery;
                var dal = DalFactory.Create<ITimingDal>();
                foreach (var item in await dal.Fetch(restaurantId, (int)Enumeration.RestaurantTypes.Delivery))
                {
                    timingItem.DayTiming.Add(MapObject2Model(item));
                }
                timingList.Add(timingItem);
            }
            if (takeAway == true && AppSettingKeys.TakeawayActive.Value)
            {
                var timingItem = new BranchTimingDAO();
                timingItem.BranchId = restaurantId;
                timingItem.SelectedCategory = Enumeration.RestaurantTypes.Takeaway;
                var dal = DalFactory.Create<ITimingDal>();
                foreach (var item in await dal.Fetch(restaurantId, (int)Enumeration.RestaurantTypes.Takeaway))
                {
                    timingItem.DayTiming.Add(MapObject2Model(item));
                }
                timingList.Add(timingItem);
            }
            return timingList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<List<BranchTimingDAO>> Get(decimal restaurantId)
        {
            var timingList = new List<BranchTimingDAO>();
            var timingItem = new BranchTimingDAO();
            var dal = DalFactory.Create<ITimingDal>();
            var dto = await dal.FetchList(restaurantId);
            foreach (var item in dto.Where(x => x.CategoryId == (int)Enumeration.RestaurantTypes.DineIn))
            {
                timingItem.BranchId = (decimal)item.BranchId;
                timingItem.SelectedCategory = (Enumeration.RestaurantTypes)item.CategoryId;
                timingItem.DayTiming.Add(MapObject2Model(item));
            }
            timingList.Add(timingItem);
            timingItem = null;
            timingItem = new BranchTimingDAO();
            foreach (var item in dto.Where(x => x.CategoryId == (int)Enumeration.RestaurantTypes.Delivery))
            {
                timingItem.BranchId = (decimal)item.BranchId;
                timingItem.SelectedCategory = (Enumeration.RestaurantTypes)item.CategoryId;
                timingItem.DayTiming.Add(MapObject2Model(item));
            }
            timingList.Add(timingItem);
            timingItem = null;
            timingItem = new BranchTimingDAO();
            foreach (var item in dto.Where(x => x.CategoryId == (int)Enumeration.RestaurantTypes.Takeaway))
            {
                timingItem.BranchId = (decimal)item.BranchId;
                timingItem.SelectedCategory = (Enumeration.RestaurantTypes)item.CategoryId;
                timingItem.DayTiming.Add(MapObject2Model(item));
            }
            timingList.Add(timingItem);
            timingItem = null;
            return timingList;
        }

        #region Helper

        internal static TimingDAO MapObject2Model(TimingDto dto)
        {
            return new TimingDAO()
            {
                BranchId = dto.BranchId,
                DinnerEndTime = dto.DinnerEndTime,
                DinnerStartTime = dto.DinnerStartTime,
                Id = dto.Id,
                IsClosed = dto.IsClosed ?? false,
                LunchEndTime = dto.LunchEndTime,
                LunchStartTime = dto.LunchStartTime,
                SelectedDay = (Enumeration.WeekDays)dto.DayId
            };
        }

        #endregion
    }
}
