﻿using GotTable.Common.Enumerations;
using GotTable.Common.List;
using GotTable.Dal;
using GotTable.Dal.RestaurantTimings;
using GotTable.DAO.RestaurantTimings;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTimings
{
    [Serializable]
    public sealed class BranchTiming : IBranchTiming
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public async Task Default(decimal branchId)
        {
            var dal = DalFactory.Create<ITimingDal>();
            foreach (var categoryItem in RestaurantTypeList.Get())
            {
                foreach (var dayItem in WeekDayList.Get())
                {
                    var dto = new TimingDto()
                    {
                        BranchId = branchId,
                        CategoryId = categoryItem.Id,
                        IsClosed = true,
                        DayId = dayItem.Id
                    };
                    await dal.Insert(dto);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timingDAO"></param>
        public async Task Save(BranchTimingDAO timingDAO)
        {
            var dal = DalFactory.Create<ITimingDal>();
            foreach (var timingItem in timingDAO.DayTiming)
            {
                var dto = new TimingDto()
                {
                    Id = timingItem.Id,
                    DinnerEndTime = timingItem.IsClosed ? "00:00" : timingItem.DinnerEndTime ?? "00:00",
                    DinnerStartTime = timingItem.IsClosed ? "00:00" : timingItem.DinnerStartTime ?? "00:00",
                    LunchEndTime = timingItem.IsClosed ? "00:00" : timingItem.LunchEndTime ?? "00:00",
                    LunchStartTime = timingItem.IsClosed ? "00:00" : timingItem.LunchStartTime ?? "00:00",
                    IsClosed = timingItem.IsClosed
                };
                await dal.Update(dto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        public async Task<BranchTimingDAO> Get(decimal restaurantId, Enumeration.RestaurantTypes restaurantType)
        {
            var branchTiming = new BranchTimingDAO
            {
                BranchId = restaurantId,
                SelectedCategory = restaurantType
            };
            var dal = DalFactory.Create<ITimingDal>();
            foreach (var item in await dal.Fetch(restaurantId, (int)restaurantType))
            {
                branchTiming.DayTiming.Add(MapObject2Model(item));
            }
            return branchTiming;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="bookingTime"></param>
        /// <param name="bookingDate"></param>
        /// <returns></returns>
        public async Task<bool> CheckAvailability(decimal restaurantId, Enumeration.RestaurantTypes restaurantType, DateTime bookingDate, string bookingTime)
        {
            var timeSpanInInteger = int.Parse(bookingTime.Replace(":", ""));
            var dayOfWeek = (int)bookingDate.DayOfWeek + 1;
            var branchTiming = await Get(restaurantId, restaurantType);
            return branchTiming.DayTiming.Any(x => x.IsClosed == false && (int)x.SelectedDay == dayOfWeek && (x.LunchStartTimeIn24HourIntegerFormat >= timeSpanInInteger && x.LunchEndTimeIn24HourIntegerFormat < timeSpanInInteger) || (x.DinnerStartTimeIn24HourIntegerFormat >= timeSpanInInteger && x.DinnerEndTimeIn24HourIntegerFormat < timeSpanInInteger));
        }


        #region Helper

        private TimingDAO MapObject2Model(TimingDto dto)
        {
            return new TimingDAO()
            {
                BranchId = dto.BranchId,
                DinnerEndTime = dto.DinnerEndTime,
                DinnerStartTime = dto.DinnerStartTime,
                Id = dto.Id,
                IsClosed = dto.IsClosed ?? false,
                LunchEndTime = dto.LunchEndTime,
                LunchStartTime = dto.LunchStartTime,
                SelectedDay = (Enumeration.WeekDays)dto.DayId
            };
        }

        #endregion
    }
}
