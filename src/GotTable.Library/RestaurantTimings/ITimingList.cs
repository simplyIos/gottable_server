﻿using GotTable.DAO.RestaurantTimings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTimings
{
    public interface ITimingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="dineIn"></param>
        /// <param name="delivery"></param>
        /// <param name="takeAway"></param>
        /// <returns></returns>
        Task<List<BranchTimingDAO>> Get(decimal restaurantId, bool? dineIn, bool? delivery, bool? takeAway);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<List<BranchTimingDAO>> Get(decimal restaurantId);
    }
}