﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantMenus;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantMenus
{
    public interface IMenuList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchMenuDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchMenuDAO>> Get(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="typeId"></param>
        /// <param name="type"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchMenuDAO>> Get(decimal restaurantId, decimal typeId, Enumeration.RestaurantTypes? type, int currentPage = 1, int pageSize = 10);
    }
}