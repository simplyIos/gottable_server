﻿using GotTable.DAO.RestaurantMenus;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantMenus
{
    public interface IBranchMenu
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<BranchMenuDAO> Create(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchMenuDAO"></param>
        Task New(BranchMenuDAO branchMenuDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchMenuDAO"></param>
        Task Save(BranchMenuDAO branchMenuDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        Task<BranchMenuDAO> Get(decimal menuId);
    }
}