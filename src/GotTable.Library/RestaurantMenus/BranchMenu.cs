﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.MenuCategories;
using GotTable.Dal.RestaurantMenus;
using GotTable.DAO.RestaurantMenus;
using GotTable.Library.RestaurantCuisines;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantMenus
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class BranchMenu : IBranchMenu
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchCuisine branchCuisine;

        public BranchMenu(IBranchCuisine branchCuisine)
        {
            this.branchCuisine = branchCuisine;
        }

        #region Factory method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<BranchMenuDAO> Create(decimal restaurantId)
        {
            await Task.FromResult(1);
            var branchMenu = new BranchMenuDAO()
            {
                BranchId = restaurantId,
                Id = 0,
                StatusId = 1,
                ErrorMessage = string.Empty
            };
            return branchMenu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchMenuDAO"></param>
        public async Task New(BranchMenuDAO branchMenuDAO)
        {
            if (branchMenuDAO.BranchId == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (String.IsNullOrEmpty(branchMenuDAO.Name))
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Menu name can not be blank, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchMenuDAO.Description))
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Menu description can not be blank, Please check.!";
                return;
            }
            if (branchMenuDAO.Cost == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Menu cost can not be 0, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchMenuDAO.SelectedCategory))
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid menu category, Please check.!";
                return;
            }
            if (branchMenuDAO.SelectedBranchCuisineId == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid menu cuisine, Please check.!";
                return;
            }
            var categoryDal = DalFactory.Create<IMenuCategoryDal>();
            var categoryDto = await categoryDal.Fetch(branchMenuDAO.SelectedCategory);
            if (categoryDto == null)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid category, Please check.!";
                return;
            }
            var cusineDao = await branchCuisine.Get(branchMenuDAO.SelectedBranchCuisineId);
            if (cusineDao == null)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid cuisine, Please check.!";
                return;
            }
            var dto = new MenuDto()
            {
                Id = branchMenuDAO.Id,
                BranchId = branchMenuDAO.BranchId,
                Cost = branchMenuDAO.Cost,
                CuisineId = cusineDao.Id,
                IsDeliveryAvailable = branchMenuDAO.IsDeliveryAvailable,
                IsDineAvailable = branchMenuDAO.IsDineAvailable,
                IsTakeAwayAvailable = branchMenuDAO.IsTakeAwayAvailable,
                Name = branchMenuDAO.Name,
                CategoryId = categoryDto.Id,
                Description = branchMenuDAO.Description,
                IsActive = branchMenuDAO.IsActive,
                MenuTypeId = (int)(Enumeration.MenuType.NotDefined)
            };
            var dal = DalFactory.Create<IMenuDal>();
            await dal.Insert(dto);
            if (dto.Id == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time.!";
                return;
            }
            branchMenuDAO.Id = dto.Id;
            branchMenuDAO.StatusId = 1;
            branchMenuDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchMenuDAO"></param>
        public async Task Save(BranchMenuDAO branchMenuDAO)
        {
            if (branchMenuDAO.Id == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (branchMenuDAO.BranchId == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (String.IsNullOrEmpty(branchMenuDAO.Name))
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Menu name can not be blank, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchMenuDAO.Description))
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Menu description can not be blank, Please check.!";
                return;
            }
            if (branchMenuDAO.Cost == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Menu cost can not be 0, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchMenuDAO.SelectedCategory))
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid menu category, Please check.!";
                return;
            }
            if (branchMenuDAO.SelectedBranchCuisineId == 0)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid menu cuisine, Please check.!";
                return;
            }
            var categoryDal = DalFactory.Create<IMenuCategoryDal>();
            var categoryDto = await categoryDal.Fetch(branchMenuDAO.SelectedCategory);
            if (categoryDto == null)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid category, Please check.!";
                return;
            }
            var cusineDao = await branchCuisine.Get(branchMenuDAO.SelectedBranchCuisineId);
            if (cusineDao == null)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "Invalid cuisine, Please check.!";
                return;
            }
            var dto = new MenuDto()
            {
                Id = branchMenuDAO.Id,
                BranchId = branchMenuDAO.BranchId,
                Cost = branchMenuDAO.Cost,
                CuisineId = cusineDao.Id,
                IsDeliveryAvailable = branchMenuDAO.IsDeliveryAvailable,
                IsDineAvailable = branchMenuDAO.IsDineAvailable,
                IsTakeAwayAvailable = branchMenuDAO.IsTakeAwayAvailable,
                Name = branchMenuDAO.Name,
                CategoryId = categoryDto.Id,
                Description = branchMenuDAO.Description,
                IsActive = branchMenuDAO.IsActive,
                MenuTypeId = (int)(Enumeration.MenuType.NotDefined)
            };
            var dal = DalFactory.Create<IMenuDal>();
            await dal.Update(dto);
            if (dto.Id < 1)
            {
                branchMenuDAO.StatusId = 0;
                branchMenuDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time.!";
                return;
            }
            branchMenuDAO.StatusId = 1;
            branchMenuDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public async Task<BranchMenuDAO> Get(decimal menuId)
        {
            var dal = DalFactory.Create<IMenuDal>();
            var dto = await dal.Fetch(menuId);
            return new BranchMenuDAO()
            {
                BranchId = dto.BranchId,
                Cost = dto.Cost,
                SelectedCuisine = dto.CuisineName,
                SelectedBranchCuisineId = dto.CuisineId,
                Id = dto.Id,
                IsActive = (bool)dto.IsActive,
                IsDeliveryAvailable = dto.IsDeliveryAvailable,
                IsDineAvailable = dto.IsDineAvailable,
                SelectedCategory = dto.CategoryName,
                IsTakeAwayAvailable = dto.IsTakeAwayAvailable,
                Name = dto.Name,
                Description = dto.Description
            };
        }

        #endregion
    }
}
