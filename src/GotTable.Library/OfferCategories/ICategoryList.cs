﻿using GotTable.Common.Enumerations;
using GotTable.DAO.OfferCategories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.OfferCategories
{
    public interface ICategoryList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        Task<List<OfferCategoryDAO>> Get(int currentPage = 0, int pageSize = 10, Enumeration.RestaurantTypes? restaurantType = null);
    }
}
