﻿using GotTable.DAO.OfferCategories;
using System.Threading.Tasks;

namespace GotTable.Library.OfferCategories
{
    /// <summary>
    /// IOfferCategory
    /// </summary>
    public interface IOfferCategory
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Task<OfferCategoryDAO> Get(int categoryId);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="categoryDAO"></param>
        Task Save(OfferCategoryDAO categoryDAO);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="offerCategoryId"></param>
        Task Update(int offerCategoryId);
    }
}
