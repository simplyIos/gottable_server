﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.OfferCategories;
using GotTable.DAO.OfferCategories;
using GotTable.Library.ApplicationSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.OfferCategories
{
    [Serializable]
    public sealed class CategoryList : ICategoryList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        public async Task<List<OfferCategoryDAO>> Get(int currentPage = 0, int pageSize = 10, Enumeration.RestaurantTypes? restaurantType = null)
        {
            var dal = DalFactory.Create<IOfferCategoryDal>();
            var dtos = await dal.FetchList(currentPage, pageSize);
            return dtos.Select(m => new OfferCategoryDAO(m.Id, m.Name, m.Active, m.Extension, AppSettingKeys.OfferDirectory)).ToList();
        }
    }
}
