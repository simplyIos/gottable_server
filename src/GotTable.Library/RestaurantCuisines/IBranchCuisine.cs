﻿using GotTable.DAO.RestaurantCuisines;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantCuisines
{
    public interface IBranchCuisine
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterCuisineId"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        Task<BranchCuisineDAO> Get(decimal masterCuisineId, decimal branchId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantCuisineId"></param>
        /// <returns></returns>
        Task<BranchCuisineDAO> Get(decimal restaurantCuisineId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchCuisineDAO"></param>
        Task Save(BranchCuisineDAO branchCuisineDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchCuisineDAO"></param>
        Task New(BranchCuisineDAO branchCuisineDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        Task<BranchCuisineDAO> Create(decimal branchId);
    }
}