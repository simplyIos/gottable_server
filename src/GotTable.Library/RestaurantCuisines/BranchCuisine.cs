﻿using GotTable.Dal;
using GotTable.DAO.RestaurantCuisines;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantCuisines
{
    [Serializable]
    public sealed class BranchCuisine : IBranchCuisine
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public async Task<BranchCuisineDAO> Create(decimal branchId)
        {
            await Task.FromResult(1);
            var branchCuisine = new BranchCuisineDAO()
            {
                BranchId = branchId,
                StatusId = 1,
                ErrorMessage = string.Empty
            };
            return branchCuisine;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchCuisineDAO"></param>
        public async Task New(BranchCuisineDAO branchCuisineDAO)
        {
            if (branchCuisineDAO.BranchId == 0)
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (String.IsNullOrEmpty(branchCuisineDAO.SelectedCuisine))
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid cuisine name, Please check.!";
                return;
            }

            var masterCuisineDal = DalFactory.Create<Dal.Cuisines.ICuisineDal>();
            var cuisineDto = await masterCuisineDal.Fetch(branchCuisineDAO.SelectedCuisine);
            if (cuisineDto == null || cuisineDto.Id == 0)
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid cuisine name, Please check.!";
                return;
            }

            var dal = DalFactory.Create<Dal.RestaurantCuisines.ICuisineDal>();
            var dto = new Dal.RestaurantCuisines.CuisineDto()
            {
                Id = branchCuisineDAO.Id,
                BranchId = branchCuisineDAO.BranchId,
                CuisineId = cuisineDto.Id,
                IsActive = branchCuisineDAO.IsActive,
                CuisineName = branchCuisineDAO.SelectedCuisine
            };

            if (await dal.IsExist(dto))
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "This selected cuisine is already part of your list, please check.!";
                return;
            }

            await dal.Insert(dto);

            if (dto.Id == 0)
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "There is some issue while processsing your request, Please try after some time.!";
                return;
            }

            branchCuisineDAO.Id = dto.Id;
            branchCuisineDAO.StatusId = 1;
            branchCuisineDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchCuisineDAO"></param>
        public async Task Save(BranchCuisineDAO branchCuisineDAO)
        {
            if (branchCuisineDAO.Id == 0)
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid request, Please check.!";
                return;
            }
            if (branchCuisineDAO.BranchId == 0)
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid request, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchCuisineDAO.SelectedCuisine))
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid request, Please check.!";
                return;
            }

            var masterCuisineDal = DalFactory.Create<Dal.Cuisines.ICuisineDal>();
            var cuisineDto = await masterCuisineDal.Fetch(branchCuisineDAO.SelectedCuisine);
            if (cuisineDto == null || cuisineDto.Id == 0)
            {
                branchCuisineDAO.StatusId = 0;
                branchCuisineDAO.ErrorMessage = "Invalid cuisine name, Please check.!";
                return;
            }

            var dto = new Dal.RestaurantCuisines.CuisineDto()
            {
                Id = branchCuisineDAO.Id,
                BranchId = branchCuisineDAO.BranchId,
                CuisineId = cuisineDto.Id,
                IsActive = branchCuisineDAO.IsActive,
            };
            var dal = DalFactory.Create<Dal.RestaurantCuisines.ICuisineDal>();
            await dal.Update(dto);
            branchCuisineDAO.StatusId = 1;
            branchCuisineDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantCuisineId"></param>
        /// <returns></returns>
        public async Task<BranchCuisineDAO> Get(decimal restaurantCuisineId)
        {
            var dal = DalFactory.Create<Dal.RestaurantCuisines.ICuisineDal>();
            var dto = await dal.Fetch(restaurantCuisineId);
            var branchCuisine = new BranchCuisineDAO()
            {
                BranchId = dto.BranchId,
                Id = dto.Id,
                IsActive = dto.IsActive,
                SelectedCuisine = dto.CuisineName
            };
            return branchCuisine;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterCuisineId"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public async Task<BranchCuisineDAO> Get(decimal masterCuisineId, decimal branchId)
        {
            var dal = DalFactory.Create<Dal.RestaurantCuisines.ICuisineDal>();
            var dto = await dal.Fetch(masterCuisineId, branchId);
            return dto != null ? new BranchCuisineDAO()
            {
                BranchId = dto.BranchId,
                Id = dto.Id,
                IsActive = dto.IsActive,
                SelectedCuisine = dto.CuisineName
            } : null;
        }
    }
}
