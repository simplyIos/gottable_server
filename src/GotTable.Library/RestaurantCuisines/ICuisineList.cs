﻿using GotTable.DAO.RestaurantCuisines;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantCuisines
{
    public interface ICuisineList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchCuisineDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="activeCuisines"></param>
        /// <returns></returns>
        Task<List<BranchCuisineDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10, bool activeCuisines = true);
    }
}
