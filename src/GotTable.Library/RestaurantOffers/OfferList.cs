﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantOffers;
using GotTable.DAO.RestaurantOffers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantOffers
{
    [Serializable]
    public sealed class OfferList : IOfferList
    {
        #region Factory methods

        public async Task<List<BranchOfferDAO>> Get(decimal? restaurantId = null, Enumeration.RestaurantTypes? restaurantType = null, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.OfferType? offerType = null, DateTime? bookingDate = null, bool? expiredOffers = null, bool? validNow = null, int currentPage = 0, int pageSize = 10)
        {
            var dal = DalFactory.Create<IOfferDal>();
            var dtos = await dal.FetchList(restaurantId, restaurantType, restaurantCategory, offerType, bookingDate, expiredOffers, validNow, currentPage, pageSize);
            return await MapObjectToModel(dtos);
        }

        #endregion

        #region Helper methods

        internal async Task<List<BranchOfferDAO>> MapObjectToModel(List<OfferDto> dtos)
        {
            await Task.FromResult(1);
            var branchOffers = new List<BranchOfferDAO>();
            foreach (var item in dtos)
            {
                branchOffers.Add(new BranchOfferDAO()
                {
                    BranchId = item.BranchId,
                    Description = item.Description,
                    EndDate = item.EndDate,
                    ErrorMessage = string.Empty,
                    Id = item.Id,
                    IsActive = item.IsActive ?? false,
                    IsDelivery = item.IsDelivery ?? false,
                    IsDineIn = item.IsDineIn ?? false,
                    IsTakeAway = item.IsTakeAway ?? false,
                    Name = item.Name,
                    StartDate = item.StartDate,
                    OfferType = item.OfferType,
                    TypeId = item.TypeId
                });
            }
            return branchOffers;
        }

        #endregion
    }
}
