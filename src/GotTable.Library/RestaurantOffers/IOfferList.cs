﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantOffers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantOffers
{
    public interface IOfferList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="offerType"></param>
        /// <param name="bookingDate"></param>
        /// <param name="expiredOffers"></param>
        /// <param name="validNow"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchOfferDAO>> Get(decimal? restaurantId = null, Enumeration.RestaurantTypes? restaurantType = null, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.OfferType? offerType = null, DateTime? bookingDate = null, bool? expiredOffers = null, bool? validNow = null, int currentPage = 0, int pageSize = 10);
    }
}