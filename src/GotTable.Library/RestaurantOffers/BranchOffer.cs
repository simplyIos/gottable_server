﻿using GotTable.Dal;
using GotTable.Dal.RestaurantOffers;
using GotTable.DAO.RestaurantOffers;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantOffers
{
    [Serializable]
    public sealed class BranchOffer : IBranchOffer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<BranchOfferDAO> Create(decimal restaurantId)
        {
            await Task.FromResult(1);
            var branchOffer = new BranchOfferDAO()
            {
                Id = 0,
                BranchId = restaurantId,
                StatusId = 1,
                ErrorMessage = string.Empty,
                OfferType = null
            };
            return branchOffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerDAO"></param>
        /// <returns></returns>
        public async Task New(BranchOfferDAO offerDAO)
        {
            if (offerDAO.BranchId == 0)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Invalid request, Please try after some time.";
                return;
            }
            if (offerDAO.CategoryId == null)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Invalid request, Please try after some time.";
                return;
            }
            if (String.IsNullOrEmpty(offerDAO.StartDateInString))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer start date can not be blank, Please check.";
                return;
            }
            offerDAO.StartDate = DateTime.ParseExact(offerDAO.StartDateInString, "MM-dd-yyyy hh:mm tt", null);
            if (String.IsNullOrEmpty(offerDAO.EndDateInString))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer end date can not be blank, Please check.";
                return;
            }
            offerDAO.EndDate = DateTime.ParseExact(offerDAO.EndDateInString, "MM-dd-yyyy hh:mm tt", null);
            if (String.IsNullOrEmpty(Convert.ToString(offerDAO.Name)))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer title can not be blank, Please check.";
                return;
            }
            if (String.IsNullOrEmpty(Convert.ToString(offerDAO.Description)))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer description can not be blank, Please check.";
                return;
            }
            if (offerDAO.StartDate >= offerDAO.EndDate)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer end date should be greater than start date, Please check.";
                return;
            }
            if (offerDAO.Name.Length > 500)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Characters allowed for title is 500, Please check.";
                return;
            }
            if (offerDAO.Description.Length > 500)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Characters allowed for description is 2000, Please check.";
                return;
            }
            var dto = new OfferDto()
            {
                BranchId = offerDAO.BranchId,
                EndDate = offerDAO.EndDate,
                Id = offerDAO.Id,
                IsActive = offerDAO.IsActive,
                IsDelivery = offerDAO.IsDelivery,
                IsDineIn = offerDAO.IsDineIn,
                IsTakeAway = offerDAO.IsTakeAway,
                Name = offerDAO.Name,
                StartDate = offerDAO.StartDate,
                Description = offerDAO.Description,
                TypeId = offerDAO.TypeId,
                CategoryId = offerDAO.CategoryId
            };
            var dal = DalFactory.Create<IOfferDal>();
            await dal.Insert(dto);
            if (dto.Id < 1)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time.!";
                return;
            }
            offerDAO.StatusId = 1;
            offerDAO.ErrorMessage = string.Empty;
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerDAO"></param>
        public async Task Save(BranchOfferDAO offerDAO)
        {
            if (offerDAO.BranchId == 0)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Invalid request, Please try after some time.";
                return;
            }
            if (offerDAO.CategoryId == 0)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Invalid request, Please try after some time.";
                return;
            }
            if (String.IsNullOrEmpty(offerDAO.StartDateInString))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer start date can not be blank, Please check.";
                return;
            }
            offerDAO.StartDate = DateTime.ParseExact(offerDAO.StartDateInString, "MM-dd-yyyy hh:mm tt", null);
            if (String.IsNullOrEmpty(offerDAO.EndDateInString))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer end date can not be blank, Please check.";
                return;
            }
            offerDAO.EndDate = DateTime.ParseExact(offerDAO.EndDateInString, "MM-dd-yyyy hh:mm tt", null);
            if (String.IsNullOrEmpty(Convert.ToString(offerDAO.Name)))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer title can not be blank, Please check.";
                return;
            }
            if (String.IsNullOrEmpty(Convert.ToString(offerDAO.Description)))
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer description can not be blank, Please check.";
                return;
            }
            if (offerDAO.StartDate >= offerDAO.EndDate)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Offer end date should be greater than start date, Please check.";
                return;
            }
            if (offerDAO.Name.Length > 500)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Characters allowed for title is 500, Please check.";
                return;
            }
            if (offerDAO.Description.Length > 500)
            {
                offerDAO.StatusId = 0;
                offerDAO.ErrorMessage = "Characters allowed for description is 2000, Please check.";
                return;
            }
            var dto = new OfferDto()
            {
                BranchId = offerDAO.BranchId,
                EndDate = offerDAO.EndDate,
                Id = offerDAO.Id,
                IsActive = offerDAO.IsActive,
                IsDelivery = offerDAO.IsDelivery,
                IsDineIn = offerDAO.IsDineIn,
                IsTakeAway = offerDAO.IsTakeAway,
                Name = offerDAO.Name,
                StartDate = offerDAO.StartDate,
                Description = offerDAO.Description,
                TypeId = offerDAO.TypeId,
                CategoryId = offerDAO.CategoryId
            };
            var dal = DalFactory.Create<IOfferDal>();
            await dal.Update(dto);
            if (dto.Id > 1)
            {
                offerDAO.StatusId = 1;
                offerDAO.ErrorMessage = string.Empty;
                return;
            }
            offerDAO.StatusId = 0;
            offerDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time.!";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        public async Task<BranchOfferDAO> Get(decimal offerId)
        {
            var dal = DalFactory.Create<IOfferDal>();
            var dto = await dal.Fetch(offerId);
            return new BranchOfferDAO()
            {
                BranchId = dto.BranchId,
                Description = dto.Description,
                EndDate = dto.EndDate,
                Id = dto.Id,
                IsActive = dto.IsActive ?? false,
                IsDelivery = dto.IsDelivery ?? false,
                IsDineIn = dto.IsDineIn ?? false,
                IsTakeAway = dto.IsTakeAway ?? false,
                Name = dto.Name,
                StartDate = dto.StartDate,
                EndDateInString = dto.EndDate.ToString("MM-dd-yyyy hh:mm tt"),
                StartDateInString = dto.StartDate.ToString("MM-dd-yyyy hh:mm tt"),
                TypeId = dto.TypeId,
                OfferType = (Common.Enumerations.Enumeration.OfferType?)dto.TypeId,
                CategoryId = dto.CategoryId,
                CategoryName = dto.CategoryName
            };
        }
    }
}
