﻿using GotTable.DAO.RestaurantAmenities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantAmenities
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAmenityList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<AmenityDAO>> Get(decimal restaurantId, int currentPage = 0, int pageSize = 10);
    }
}
