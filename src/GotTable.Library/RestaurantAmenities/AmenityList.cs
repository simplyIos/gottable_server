﻿using GotTable.Dal;
using GotTable.Dal.RestaurantAmenities;
using GotTable.DAO.RestaurantAmenities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantAmenities
{
    [Serializable]
    public sealed class AmenityList : IAmenityList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<AmenityDAO>> Get(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var amenityDAOS = new List<AmenityDAO>();
            var dal = DalFactory.Create<IAmenityDal>();
            var dtos = await dal.FetchList(restaurantId, currentPage, pageSize);
            dtos.ForEach(item => amenityDAOS.Add(new AmenityDAO()
            {
                AmenityId = item.AmenityId,
                AmenityName = item.AmenityName,
                BranchId = item.BranchId,
                Id = item.Id,
                Checked = item.Checked
            }));
            return amenityDAOS;
        }
    }
}
