﻿using GotTable.DAO.RestaurantAmenities;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantAmenities
{
    public interface IAmenity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityDAO"></param>
        /// <returns></returns>
        Task New(AmenityDAO amenityDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityId"></param>
        /// <param name="restaurantId"></param>
        Task Delete(decimal amenityId, decimal restaurantId);
    }
}
