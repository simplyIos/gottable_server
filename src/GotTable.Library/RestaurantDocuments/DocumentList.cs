﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantDocuments;
using GotTable.DAO.RestaurantDocuments;
using GotTable.Library.ApplicationSettings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantDocuments
{
    [Serializable]
    public sealed class DocumentList : IDocumentList
    {
        #region Factory method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategory"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<DocumentInfoDAO>> Get(decimal restaurantId, Enumeration.ImageCategory? imageCategory = default, int currentPage = default, int pageSize = 10)
        {
            var documentList = new List<DocumentInfoDAO>();
            var dal = DalFactory.Create<IDocumentDal>();
            var documentCategoryId = imageCategory != null ? (int?)imageCategory : (int?)null;
            var dtos = await dal.FetchList(restaurantId, documentCategoryId, currentPage, pageSize);
            dtos.ForEach(item =>
            {
                var categoryName = item.CategoryId != null ? ((Enumeration.ImageCategory)item.CategoryId).ToString() : null;
                documentList.Add(new DocumentInfoDAO(item.DocumentId, item.BranchId, categoryName, item.Name, item.Extension, item.Active, AppSettingKeys.RestaurantDirectory));
            });
            return documentList;
        }

        #endregion
    }
}
