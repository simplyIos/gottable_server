﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantDocuments;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantDocuments
{
    public interface IDocument
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentDAO"></param>
        /// <returns></returns>
        Task Post(DocumentDAO documentDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileModelDAO"></param>
        /// <returns></returns>
        Task Save(FileModelDAO fileModelDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageType"></param>
        /// <returns></returns>
        Task<RestaurantImageDAO> Get(decimal restaurantId, Enumeration.ImageType? imageType = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        Task Delete(int documentId);
    }
}