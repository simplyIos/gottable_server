﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantDocuments;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantDocuments
{
    public interface IDocumentList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategory"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<DocumentInfoDAO>> Get(decimal restaurantId, Enumeration.ImageCategory? imageCategory = default, int currentPage = default, int pageSize = 10);
    }
}