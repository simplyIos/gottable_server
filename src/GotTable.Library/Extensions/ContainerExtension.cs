﻿using Autofac;

namespace GotTable.Library.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ContainerExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void Add(this ContainerBuilder builder)
        {
            builder.RegisterType<Security.IdentityManager>().As<Security.IIdentityManager>();

            builder.RegisterType<Localities.Localty>().As<Localities.ILocalty>();
            builder.RegisterType<Localities.LocaltyList>().As<Localities.ILocaltyList>();

            builder.RegisterType<OperationExceptionLogs.OperationExceptionLog>().As<OperationExceptionLogs.IOperationExceptionLog>();

            builder.RegisterType<Amenities.AmenityList>().As<Amenities.IAmenityList>();

            builder.RegisterType<ApplicationSettings.ApplicationSetting>().As<ApplicationSettings.IApplicationSetting>();

            builder.RegisterType<ApplicationLogs.ApplicationLog>().As<ApplicationLogs.IApplicationLog>();

            builder.RegisterType<MenuCategories.CategoryList>().As<MenuCategories.ICategoryList>();

            builder.RegisterType<Cities.CityList>().As<Cities.ICityList>();

            builder.RegisterType<Cuisines.Cuisine>().As<Cuisines.ICuisine>();
            builder.RegisterType<Cuisines.CuisineList>().As<Cuisines.ICuisineList>();

            builder.RegisterType<OfferCategories.CategoryList>().As<OfferCategories.ICategoryList>();
            builder.RegisterType<OfferCategories.OfferCategory>().As<OfferCategories.IOfferCategory>();
            builder.RegisterType<Administrators.Administrator>().As<Administrators.IAdministrator<DAO.Administrators.AdminDAO>>();
            builder.RegisterType<Administrators.AdministratorList>().As<Administrators.IAdministratorList<DAO.Administrators.AdminInfoDAO, Administrators.ListCriteria>>();

            builder.RegisterType<Users.User>().As<Users.IUser>();
            builder.RegisterType<Users.UserList>().As<Users.IUserList>();

            builder.RegisterType<Devices.Device>().As<Devices.IDevice>();
            builder.RegisterType<Devices.DeviceList>().As<Devices.IDeviceList>();

            builder.RegisterType<PushNotifications.PushNotification>().As<PushNotifications.IPushNotification>();
            builder.RegisterType<PushNotifications.NotificationList>().As<PushNotifications.INotificationList>();
            builder.RegisterType<PushNotifications.NotificationExceptionLogList>().As<PushNotifications.INotificationExceptionLogList>();

            builder.RegisterType<Reports.CustomBookingList>().As<Reports.IReportList<DAO.Reports.CustomBookingDAO, Reports.Criterias.CustomBookingListCriteria>>();
            builder.RegisterType<Reports.DailyBookingList>().As<Reports.IReportList<DAO.Reports.DailyBookingDAO, Reports.Criterias.DailyBookingListCriteria>>();
            builder.RegisterType<Reports.ExpiredRestaurantList>().As<Reports.IReportList<DAO.Reports.ExpiredRestaurantDAO, Reports.Criterias.ExpiredRestaurantListCriteria>>();
            builder.RegisterType<Reports.DeletedRestaurantList>().As<Reports.IReportList<DAO.Reports.RestaurantDAO, Reports.Criterias.DeletedRestaurantListCriteria>>();

            builder.RegisterType<Restaurants.PreferredRestaurantList>().As<Restaurants.IPreferredRestaurantList>();
            builder.RegisterType<Restaurants.AccountAdminRestaurantList>().As<Restaurants.IAccountAdminRestaurantList>();
            builder.RegisterType<Restaurants.PipelineRestaurantList>().As<Restaurants.IPipelineRestaurantList>();
            builder.RegisterType<Restaurants.SalesAdminRestaurantList>().As<Restaurants.ISalesAdminRestaurantList>();
            builder.RegisterType<Restaurants.ApplicationRestaurantList>().As<Restaurants.IApplicationRestaurantList>();
            builder.RegisterType<Restaurants.InCityRestaurantList>().As<Restaurants.IInCityRestaurantList>();
            builder.RegisterType<Restaurants.InvoicedRestaurantList>().As<Restaurants.IInvoicedRestaurantList>();
            builder.RegisterType<Restaurants.Restaurant>().As<Restaurants.IRestaurant>();
            builder.RegisterType<Restaurants.RestaurantSetting>().As<Restaurants.IRestaurantSetting>();
            builder.RegisterType<Restaurants.RestaurantTag>().As<Restaurants.IRestaurantTag<Restaurants.RestaurantTag>>();
            builder.RegisterType<Restaurants.RestaurantListTag>().As<Restaurants.IRestaurantTag<Restaurants.RestaurantListTag>>();
            builder.RegisterType<Restaurants.AutoCompleteList>().As<Restaurants.IAutoCompleteList>();

            builder.RegisterType<Restaurants.Filters.FilterList>().As<Restaurants.Filters.IFilterList>();
            builder.RegisterType<Restaurants.PreferredRestaurants.PreferredRestaurant>().As<Restaurants.PreferredRestaurants.IPreferredRestaurant>();

            builder.RegisterType<RestaurantTables.BranchTable>().As<RestaurantTables.IBranchTable>();
            builder.RegisterType<RestaurantTables.TableList>().As<RestaurantTables.ITableList>();

            builder.RegisterType<RestaurantTimings.TimingList>().As<RestaurantTimings.ITimingList>();
            builder.RegisterType<RestaurantTimings.BranchTiming>().As<RestaurantTimings.IBranchTiming>();
            builder.RegisterType<RestaurantTimings.TimingList>().As<RestaurantTimings.ITimingList>();
            builder.RegisterType<RestaurantTimings.BranchTiming>().As<RestaurantTimings.IBranchTiming>();

            builder.RegisterType<RestaurantRatings.BranchRating>().As<RestaurantRatings.IBranchRating>();
            builder.RegisterType<RestaurantRatings.RatingList>().As<RestaurantRatings.IRatingList>();

            builder.RegisterType<RestaurantBookings.UserBookingList>().As<RestaurantBookings.IUserBookingList>();
            builder.RegisterType<RestaurantBookings.RestaurantBookingList>().As<RestaurantBookings.IRestaurantBookingList>();
            builder.RegisterType<RestaurantBookings.DeliveryandTakeaway>().As<RestaurantBookings.IDeliveryandTakeaway>();
            builder.RegisterType<RestaurantBookings.DeliveryandTakeawayList>().As<RestaurantBookings.IDeliveryandTakeawayList>();
            builder.RegisterType<RestaurantBookings.DineIn>().As<RestaurantBookings.IDineIn>();
            builder.RegisterType<RestaurantBookings.DineInList>().As<RestaurantBookings.IDineInList>();
            builder.RegisterType<RestaurantBookings.DineInBookingBillUpload>().As<RestaurantBookings.IDineInBookingBillUpload>();
            builder.RegisterType<RestaurantBookings.DineInBookingBillUploadList>().As<RestaurantBookings.IDineInBookingBillUploadList>();

            builder.RegisterType<RestaurantConfigurations.BranchConfiguration>().As<RestaurantConfigurations.IBranchConfiguration>();

            builder.RegisterType<RestaurantContacts.BranchContact>().As<RestaurantContacts.IBranchContact>();
            builder.RegisterType<RestaurantContacts.ContactList>().As<RestaurantContacts.IContactList>();

            builder.RegisterType<RestaurantCuisines.BranchCuisine>().As<RestaurantCuisines.IBranchCuisine>();
            builder.RegisterType<RestaurantCuisines.CuisineList>().As<RestaurantCuisines.ICuisineList>();

            builder.RegisterType<RestaurantDocuments.Document>().As<RestaurantDocuments.IDocument>();
            builder.RegisterType<RestaurantDocuments.DocumentList>().As<RestaurantDocuments.IDocumentList>();

            builder.RegisterType<RestaurantInvoices.InvoiceList>().As<RestaurantInvoices.IInvoiceList>();
            builder.RegisterType<RestaurantInvoices.BranchInvoice>().As<RestaurantInvoices.IBranchInvoice>();

            builder.RegisterType<RestaurantMenus.BranchMenu>().As<RestaurantMenus.IBranchMenu>();
            builder.RegisterType<RestaurantMenus.MenuList>().As<RestaurantMenus.IMenuList>();

            builder.RegisterType<RestaurantOffers.BranchOffer>().As<RestaurantOffers.IBranchOffer>();
            builder.RegisterType<RestaurantOffers.OfferList>().As<RestaurantOffers.IOfferList>();

            builder.RegisterType<RestaurantAmenities.AmenityList>().As<RestaurantAmenities.IAmenityList>();
            builder.RegisterType<RestaurantAmenities.Amenity>().As<RestaurantAmenities.IAmenity>();

            // MESSAGE TRANSMISSION
            builder.RegisterType<Communications.Message.DineInBookingConfirmation>().As<Communications.Message.IMessageTransmission<Communications.Message.DineInBookingConfirmation, string>>();
            builder.RegisterType<Communications.Message.DeliveryandTakeawayBookingConfirmation>().As<Communications.Message.IMessageTransmission<Communications.Message.DeliveryandTakeawayBookingConfirmation, string>>();
            builder.RegisterType<Communications.Message.OTPCommand>().As<Communications.Message.IOTPCommand>();


            // EMAIL TRANSMISSION
            builder.RegisterType<Communications.Email.ForgetPassword>().As<Communications.Email.IEmailTransmission<Communications.Email.ForgetPassword, DAO.Communications.Email.ForgetPasswordDAO>>();
            builder.RegisterType<Communications.Email.RestaurantCredential>().As<Communications.Email.IEmailTransmission<Communications.Email.RestaurantCredential, decimal>>();


            // JOBS
            builder.RegisterType<Communications.Generics.JobFactory>().As<Communications.Generics.IJobFactory>();
            builder.RegisterType<Communications.Jobs.AndroidPushNotificationJob>().As<Communications.Generics.IJob<Communications.Jobs.AndroidPushNotificationJob, DAO.PushNotifications.PushNotificationDAO>>();
            builder.RegisterType<Communications.Jobs.IOSPushNotificationJob>().As<Communications.Generics.IJob<Communications.Jobs.IOSPushNotificationJob, DAO.PushNotifications.PushNotificationDAO>>();
            builder.RegisterType<Communications.Jobs.DineInBookingAutoCompleteJob>().As<Communications.Generics.IJob<Communications.Jobs.DineInBookingAutoCompleteJob, DAO.Bookings.DineIn.DineInBookingDAO>>();
            builder.RegisterType<Communications.Jobs.DineInBookingConfirmationJob>().As<Communications.Generics.IJob<Communications.Jobs.DineInBookingConfirmationJob, DAO.Bookings.DineIn.DineInBookingDAO>>();
            builder.RegisterType<Communications.Jobs.DeliveryandTakeawayBookingConfirmationJob>().As<Communications.Generics.IJob<Communications.Jobs.DeliveryandTakeawayBookingConfirmationJob, DAO.Bookings.DeliveryandTakeaway.DeliveryandTakeawayDAO>>();

            // ELASTICSERVICE
            builder.RegisterType<ElasticSearch.Services.IndexRepositoryService>().As<ElasticSearch.Interfaces.IIndexRepository>();
            builder.RegisterType<ElasticSearch.Services.RestaurantService>().As<ElasticSearch.Interfaces.IIndexService<ElasticSearch.Services.RestaurantService>>();
        }
    }
}
