﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public interface IReportList<T1, T2>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        Task<List<T1>> Get(T2 criteria);
    }
}
