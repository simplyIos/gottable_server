﻿namespace GotTable.Library.Reports.Criterias
{
    public class ExpiredRestaurantListCriteria
    {
        public ExpiredRestaurantListCriteria(int currentPage = 1, int pageSize = 10)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
        }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
