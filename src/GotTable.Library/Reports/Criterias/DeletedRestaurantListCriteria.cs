﻿using GotTable.Library.Shared;

namespace GotTable.Library.Reports.Criterias
{
    public class DeletedRestaurantListCriteria : ICriteria
    {
        public DeletedRestaurantListCriteria(int currentPage = 1, int pageSize = 10)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
        }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
