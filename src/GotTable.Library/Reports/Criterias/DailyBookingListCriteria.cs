﻿
using GotTable.Library.Shared;
using System;

namespace GotTable.Library.Reports.Criterias
{
    public class DailyBookingListCriteria : ICriteria
    {
        public DailyBookingListCriteria(DateTime bookingDate = default, int currentPage = 1, int pageSize = 10)
        {
            BookingDate = bookingDate;
            CurrentPage = currentPage;
            PageSize = pageSize;
        }

        public DateTime BookingDate { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
