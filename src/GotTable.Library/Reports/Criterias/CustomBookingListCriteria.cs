﻿
using GotTable.Library.Shared;
using System;

namespace GotTable.Library.Reports.Criterias
{
    public class CustomBookingListCriteria : ICriteria
    {
        public CustomBookingListCriteria(DateTime bookingStartDate = default, DateTime bookingEndDate = default, string restaurantName = default, string restaurantCategory = default, int currentPage = 1, int pageSize = 10)
        {
            BookingStartDate = bookingStartDate;
            BookingEndDate = bookingEndDate;
            RestaurantName = restaurantName;
            RestaurantCategory = restaurantCategory;
            CurrentPage = currentPage;
            PageSize = pageSize;
        }

        public DateTime BookingStartDate { get; set; }

        public DateTime BookingEndDate { get; set; }

        public string RestaurantName { get; set; }

        public string RestaurantCategory { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
