﻿using GotTable.Dal;
using GotTable.Dal.Reports.Restaurants;
using GotTable.DAO.Reports;
using GotTable.Library.Reports.Criterias;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Reports
{
    [Serializable]
    public sealed class ExpiredRestaurantList : IReportList<ExpiredRestaurantDAO, ExpiredRestaurantListCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<ExpiredRestaurantDAO>> Get(ExpiredRestaurantListCriteria criteria)
        {
            var restaurants = new List<ExpiredRestaurantDAO>();
            var dal = DalFactory.Create<IRestaurantDal>();
            foreach (var restaurant in await dal.FetchExpiredList(criteria.CurrentPage, criteria.PageSize))
            {
                restaurants.Add(new ExpiredRestaurantDAO()
                {
                    Address = restaurant.Address,
                    ExpireDate = restaurant.ExpireDate,
                    Name = restaurant.Name,
                    RestaurantId = restaurant.RestaurantId
                });
            }
            return restaurants;
        }
    }
}
