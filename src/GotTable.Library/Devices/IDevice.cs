﻿using GotTable.DAO.Devices;
using System.Threading.Tasks;

namespace GotTable.Library.Devices
{
    public interface IDevice
    {
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="deviceDAO"></param>
        Task Save(DeviceDAO deviceDAO);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        Task<DeviceDAO> Get(string deviceId);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="deviceId"></param>
        Task Delete(string deviceId);
    }
}
