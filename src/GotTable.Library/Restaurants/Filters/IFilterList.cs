﻿using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants.Filters
{
    public interface IFilterList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<FilterDAO>> Get();
    }
}
