﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IApplicationRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        Task<List<ResuturantModelDAO>> Get(double latitude, double longitude, decimal restaurantId, Enumeration.RestaurantTypes restaurantType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="cuisineIds"></param>
        /// <param name="areaCoverage"></param>
        /// <param name="offerType"></param>
        /// <param name="queryExpression"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategoryId"></param>
        /// <param name="OfferCategories"></param>
        /// <param name="localtyId"></param>
        /// <returns></returns>
        Task<List<ResuturantModelDAO>> Get(double latitude, double longitude, Enumeration.RestaurantTypes restaurantType, List<decimal> cuisineIds = default, int areaCoverage = 0, Enumeration.OfferType? offerType = null, string queryExpression = "", int currentPage = 0, int pageSize = 10, int? restaurantCategoryId = null, List<int> OfferCategories = default, List<decimal> localtyId = default);
    }
}