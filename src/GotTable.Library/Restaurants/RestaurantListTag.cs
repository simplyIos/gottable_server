﻿using GotTable.Common.Enumerations;
using GotTable.Library.RestaurantOffers;
using GotTable.Library.RestaurantTables;
using GotTable.Library.RestaurantTimings;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class RestaurantListTag : IRestaurantTag<RestaurantListTag>
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITableList tableList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTiming branchTiming;

        public RestaurantListTag(ITableList tableList, IOfferList offerList, IBranchTiming branchTiming)
        {
            this.tableList = tableList;
            this.offerList = offerList;
            this.branchTiming = branchTiming;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <param name="offerType"></param>
        /// <returns></returns>
        public async Task<(Enumeration.RestaurantTags, string)> Get(Enumeration.RestaurantTypes restaurantType, decimal restaurantId, DateTime bookingDate, string bookingTime, Enumeration.OfferType? offerType = null)
        {
            var timeSpanInInteger = int.Parse(DateTime.Now.ToString("HHmm"));
            var dayOfWeek = (int)DateTime.Now.DayOfWeek + 1;
            var timingListDao = await branchTiming.Get(restaurantId, restaurantType);
            if (restaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                bool returnTable = false;
                if (timingListDao.DayTiming.Any(x => x.IsClosed == false && (int)x.SelectedDay == dayOfWeek))
                {
                    var tableListDaos = await tableList.Get(restaurantId, 1, 6);
                    returnTable = tableListDaos.Any(x => x.Active == true && x.Value > 0);
                    if (returnTable)
                    {
                        var dtos = await offerList.Get(restaurantId, restaurantType, null, offerType, null, null, true, 0, 10);
                        if (dtos != null && dtos.Any())
                        {
                            return (Enumeration.RestaurantTags.Yellow, dtos.OrderBy(x => x.TypeId).FirstOrDefault().Name);
                        }
                        return (Enumeration.RestaurantTags.Green, string.Empty);
                    }
                }
                return (Enumeration.RestaurantTags.Red, string.Empty);
            }
            else
            {
                if (timingListDao.DayTiming.Any(x => x.IsClosed == false && (int)x.SelectedDay == dayOfWeek))
                {
                    var dtos = await offerList.Get(restaurantId, restaurantType, null, offerType, null, null, true, 0, 10);
                    if (dtos.Any() || dtos.Any())
                    {
                        return (Enumeration.RestaurantTags.Yellow, dtos.OrderBy(x => x.TypeId).FirstOrDefault().Name);
                    }
                    return (Enumeration.RestaurantTags.Green, string.Empty);
                }
                return (Enumeration.RestaurantTags.Red, string.Empty);
            }
        }
    }
}
