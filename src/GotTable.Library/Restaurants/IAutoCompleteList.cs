﻿using GotTable.DAO.Restaurants;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IAutoCompleteList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <returns></returns>
        Task<AutoCompleteDAO> Get(string queryExpression);
    }
}