﻿using GotTable.Dal;
using GotTable.Dal.Restaurants.PreferredRestaurants;
using GotTable.DAO.Restaurants.PreferredRestaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants.PreferredRestaurants
{
    [Serializable]
    public sealed class PreferredRestaurant : IPreferredRestaurant
    {
        #region Factory method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurant"></param>
        /// <returns></returns>
        public async Task New(PreferredRestaurantDAO preferredRestaurant)
        {
            var dal = DalFactory.Create<IPreferredRestaurantDal>();
            var dto = new RestaurantDto()
            {
                CityId = preferredRestaurant.CityId,
                IndexValue = 1,
                RestaurantId = preferredRestaurant.RestaurantId,
                Status = true
            };
            await dal.Insert(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        /// <returns></returns>
        public async Task Delete(decimal preferredRestaurantId)
        {
            var dal = DalFactory.Create<IPreferredRestaurantDal>();
            await dal.Delete(preferredRestaurantId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantIds"></param>
        /// <returns></returns>
        public async Task Update(List<decimal> restaurantIds)
        {
            var dal = DalFactory.Create<IPreferredRestaurantDal>();
            await dal.Update(restaurantIds);
        }

        #endregion
    }
}
