﻿using GotTable.DAO.Restaurants.PreferredRestaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IPreferredRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<PreferredRestaurantInfoDAO>> Get(double latitude, double longitude, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<PreferredRestaurantDAO>> Get(decimal? cityId, int currentPage = 1, int pageSize = 12);
    }
}