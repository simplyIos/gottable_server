﻿using GotTable.DAO.Restaurants.PreferredRestaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants.PreferredRestaurants
{
    public interface IPreferredRestaurant
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurant"></param>
        /// <returns></returns>
        Task New(PreferredRestaurantDAO preferredRestaurant);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantIds"></param>
        /// <returns></returns>
        Task Update(List<decimal> restaurantIds);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        Task Delete(decimal preferredRestaurantId);
    }
}