﻿using GotTable.DAO.RestaurantSettings;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IRestaurantSetting
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<RestaurantSettingDAO> Get(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantSettingDAO"></param>
        /// <returns></returns>
        Task Save(RestaurantSettingDAO restaurantSettingDAO);
    }
}