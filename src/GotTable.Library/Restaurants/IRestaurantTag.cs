﻿using GotTable.Common.Enumerations;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IRestaurantTag<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <param name="offerType"></param>
        /// <returns></returns>
        Task<(Enumeration.RestaurantTags, string)> Get(Enumeration.RestaurantTypes restaurantType, decimal restaurantId, DateTime bookingDate, string bookingTime, Enumeration.OfferType? offerType = null);
    }
}
