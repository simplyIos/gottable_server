﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface ISalesAdminRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesAdminPersonId"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="restaurantType"></param>
        /// <param name="searchExpression"></param>
        /// <param name="restaurantOwnerEmailAddress"></param>
        /// <param name="expiredRestaurant"></param>
        /// <param name="inactiveRestaurant"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantBySalesAdminDAO>> Get(decimal salesAdminPersonId, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.RestaurantTypes? restaurantType = null, string searchExpression = "", string restaurantOwnerEmailAddress = "", bool? expiredRestaurant = null, bool? inactiveRestaurant = null, int currentPage = 1, int pageSize = 10);
    }
}