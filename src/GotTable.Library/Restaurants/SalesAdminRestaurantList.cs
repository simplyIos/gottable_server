﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants.SalesAdmin;
using GotTable.DAO.Restaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class SalesAdminRestaurantList : ISalesAdminRestaurantList
    {
        public async Task<List<RestaurantBySalesAdminDAO>> Get(decimal salesAdminPersonId, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.RestaurantTypes? restaurantType = null, string searchExpression = "", string restaurantOwnerEmailAddress = "", bool? expiredRestaurant = null, bool? inactiveRestaurant = null, int currentPage = 1, int pageSize = 10)
        {
            var restaurantList = new List<RestaurantBySalesAdminDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var dtos = await dal.FetchList(salesAdminPersonId, restaurantCategory, restaurantType, searchExpression, restaurantOwnerEmailAddress, expiredRestaurant, inactiveRestaurant, currentPage, pageSize);

            foreach (var item in dtos)
            {
                restaurantList.Add(new RestaurantBySalesAdminDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    City = item.City,
                    RestaurantStatus = item.RestaurantStatus ?? false,
                    InvoiceStatus = item.InvoiceStatus ?? false,
                    RestaurantId = item.RestaurantId,
                    RestaurantName = item.RestaurantName,
                    AccountAdminName = item.AccountAdminName,
                    AccountAdminEmail = item.AccountAdminEmailAddress,
                    AccountAdminPersonId = item.AccountAdminId ?? 0,
                    Amount = item.Amount ?? 0,
                    Delivery = item.Delivery ?? false,
                    TakeAway = item.TakeAway ?? false,
                    DineIn = item.DineIn ?? false,
                    EndDate = item.EndDate.ToString(),
                    StartDate = item.StartDate.ToString(),
                    State = item.State,
                    Zip = item.Zip,
                    InvoiceId = item.InvoiceId ?? Guid.Empty,
                    PrescriptionTypeId = item.PrescriptionTypeId ?? 0,
                    Category = item.Category
                });
            }

            return restaurantList;
        }
    }
}
