﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants.ApplicationRestaurants;
using GotTable.DAO.Restaurants;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class AutoCompleteList : IAutoCompleteList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <returns></returns>
        public async Task<AutoCompleteDAO> Get(string queryExpression)
        {
            var dal = DalFactory.Create<IAutoCompleteDal>();
            var cuisineDto = await dal.FetchList(queryExpression, Enumeration.RestaurantAutoComplete.Cuisine);
            var nameDto = await dal.FetchList(queryExpression, Enumeration.RestaurantAutoComplete.RestaurantName);
            var locationDto = await dal.FetchList(queryExpression, Enumeration.RestaurantAutoComplete.Location);
            var autoCompleteList = new AutoCompleteDAO
            {
                Cuisines = cuisineDto.Select(m => new AutoCompleteItemDAO()
                {
                    Id = Convert.ToInt32(m.Id),
                    Name = m.Name
                }).ToList(),
                Names = nameDto.Select(m => new AutoCompleteItemDAO()
                {
                    Id = Convert.ToInt32(m.Id),
                    Name = m.Name
                }).ToList(),
                Locations = locationDto.Select(m => new AutoCompleteItemDAO()
                {
                    Id = Convert.ToInt32(m.Id),
                    Name = m.Name
                }).ToList()
            };

            return autoCompleteList;
        }
    }
}
