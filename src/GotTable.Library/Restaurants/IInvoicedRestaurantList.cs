﻿using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    public interface IInvoicedRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonAdminId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<InvoicedRestaurantDAO>> Get(decimal? salesPersonAdminId = null, int currentPage = 1, int pageSize = 10);
    }
}