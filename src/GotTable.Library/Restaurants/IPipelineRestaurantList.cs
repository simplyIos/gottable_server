﻿using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPipelineRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonAdminId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<PipelineRestaurantDAO>> Get(decimal salesPersonAdminId, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<PipelineRestaurantDAO>> Get(int currentPage = 1, int pageSize = 10);
    }
}