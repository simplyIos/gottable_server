﻿using GotTable.DAO.MenuCategories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.MenuCategories
{
    public interface ICategoryList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<CategoryDAO>> Get(int currentPage = 0, int pageSize = 10);
    }
}
