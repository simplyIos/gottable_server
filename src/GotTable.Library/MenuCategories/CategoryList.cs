﻿using GotTable.Dal;
using GotTable.Dal.MenuCategories;
using GotTable.DAO.MenuCategories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.MenuCategories
{
    [Serializable]
    public sealed class CategoryList : ICategoryList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<CategoryDAO>> Get(int currentPage = 0, int pageSize = 10)
        {
            var dal = DalFactory.Create<IMenuCategoryDal>();
            var categoryList = new List<CategoryDAO>();
            foreach (var p in await dal.FetchList(currentPage, pageSize))
            {
                categoryList.Add(new CategoryDAO()
                {
                    Name = p.Name,
                    Id = p.Id
                });
            }
            return categoryList;
        }
    }
}
