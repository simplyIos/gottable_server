﻿using GotTable.DAO.ApplicationSettings;
using System.Threading.Tasks;

namespace GotTable.Library.ApplicationSettings
{
    /// <summary>
    /// IApplicationSetting
    /// </summary>
    public interface IApplicationSetting
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        Task<ApplicationSettingDAO> Get();

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="settingDAO"></param>
        Task Save(ApplicationSettingDAO settingDAO);
    }
}
