﻿using GotTable.Dal;
using GotTable.Dal.ApplicationSettings;
using GotTable.DAO.ApplicationSettings;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.ApplicationSettings
{
    [Serializable]
    public sealed class ApplicationSetting : IApplicationSetting
    {
        public ApplicationSetting()
        {

        }

        public async Task<ApplicationSettingDAO> Get()
        {
            var dal = DalFactory.Create<IApplicationSettingDal>();
            var dto = await dal.Fetch();
            var applicationSetting = new ApplicationSettingDAO()
            {
                AdminApplicationBaseUrl = dto.AdminApplicationBaseUrl,
                ApnsCeritificatePassword = dto.ApnsCeritificatePassword,
                ApnsCertificateContent = dto.ApnsCertificateContent,
                ApplicationIdPackageName = dto.ApplicationIdPackageName,
                DeliveryActive = dto.DeliveryActive ?? false,
                DineInActive = dto.DineInActive ?? false,
                FCMSenderId = dto.FCMSenderId,
                FCMServerKey = dto.FCMServerKey,
                FCMUrl = dto.FCMUrl,
                GeoUrl = dto.GeoUrl,
                OTPAuthKey = dto.OTPAuthKey,
                OTPAuthUrl = dto.OTPAuthUrl,
                TakeawayActive = dto.TakeawayActive ?? false,
                TwoFactorAPIAuthKey = dto.TwoFactorAPIAuthKey,
                TwoFactorAPIURl = dto.TwoFactorAPIURl,
                UserApplicationBaseUrl = dto.UserApplicationBaseUrl,
                HangfireFeatureEnable = dto.HangfireFeatureEnable ?? false,
                NotificationDirectory = dto.NotificationDirectory,
                OfferDirectory = dto.OfferDirectory,
                RestaurantDirectory = dto.RestaurantDirectory,
                ServiceBusFeatureEnable = dto.ServiceBusFeatureEnable ?? false,
                DeliveryEmailTransmission = dto.DeliveryEmailTransmission,
                DeliveryMessageTransmission = dto.DeliveryMessageTransmission,
                DineInEmailTransmission = dto.DineInEmailTransmission,
                DineInMessageTransmission = dto.DineInMessageTransmission,
                TakeawayEmailTransmission = dto.TakeawayEmailTransmission,
                TakeawayMessageTransmission = dto.TakeawayMessageTransmission
            };
            return applicationSetting;
        }

        public async Task Save(ApplicationSettingDAO applicationSetting)
        {
            var dal = DalFactory.Create<IApplicationSettingDal>();
            var dto = new ApplicationSettingDto()
            {
                AdminApplicationBaseUrl = applicationSetting.AdminApplicationBaseUrl,
                ApnsCeritificatePassword = applicationSetting.ApnsCeritificatePassword,
                ApplicationIdPackageName = applicationSetting.ApplicationIdPackageName,
                DeliveryActive = applicationSetting.DeliveryActive,
                DineInActive = applicationSetting.DineInActive,
                FCMSenderId = applicationSetting.FCMSenderId,
                FCMServerKey = applicationSetting.FCMServerKey,
                FCMUrl = applicationSetting.FCMUrl,
                GeoUrl = applicationSetting.GeoUrl,
                OTPAuthKey = applicationSetting.OTPAuthKey,
                OTPAuthUrl = applicationSetting.OTPAuthUrl,
                TakeawayActive = applicationSetting.TakeawayActive,
                TwoFactorAPIAuthKey = applicationSetting.TwoFactorAPIAuthKey,
                TwoFactorAPIURl = applicationSetting.TwoFactorAPIURl,
                UserApplicationBaseUrl = applicationSetting.UserApplicationBaseUrl,
                HangfireFeatureEnable = applicationSetting.HangfireFeatureEnable,
                ServiceBusFeatureEnable = applicationSetting.ServiceBusFeatureEnable,
                OfferDirectory = applicationSetting.OfferDirectory,
                NotificationDirectory = applicationSetting.NotificationDirectory,
                RestaurantDirectory = applicationSetting.RestaurantDirectory,
                DineInEmailTransmission = applicationSetting.DineInEmailTransmission,
                DineInMessageTransmission = applicationSetting.DineInMessageTransmission,
                DeliveryEmailTransmission = applicationSetting.DeliveryEmailTransmission,
                DeliveryMessageTransmission = applicationSetting.DeliveryMessageTransmission,
                TakeawayEmailTransmission = applicationSetting.TakeawayEmailTransmission,
                TakeawayMessageTransmission = applicationSetting.TakeawayMessageTransmission
            };
            await dal.Update(dto);

            // This is to reintialize the app setting keys
            await AppSettingKeys.Init();
        }
    }
}
