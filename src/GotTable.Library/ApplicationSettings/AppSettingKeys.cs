﻿using System;
using System.Threading.Tasks;

namespace GotTable.Library.ApplicationSettings
{
    [Serializable]
    public static class AppSettingKeys
    {
        public async static Task Init()
        {
            var dto = await new ApplicationSetting().Get();

            GeoUrl = dto?.GeoUrl;

            Two2FactorApiAuthKey = dto?.TwoFactorAPIAuthKey;
            TwoFactorAPIURl = dto?.TwoFactorAPIURl;

            AdminApplicationBaseUrl = dto?.AdminApplicationBaseUrl;
            UserApplicationBaseUrl = dto?.UserApplicationBaseUrl;

            FCMSenderId = dto?.FCMSenderId;
            FCMServerKey = dto?.FCMServerKey;
            FCMUrl = dto?.FCMUrl;

            ApplicationIdPackageName = dto?.ApplicationIdPackageName;
            ApnsCertificatePassword = dto?.ApnsCeritificatePassword;
            ApnsCertificateContent = dto?.ApnsCertificateContent;

            ServiceBusFeatureEnable = dto?.ServiceBusFeatureEnable ?? false;
            HangfireFeatureEnable = dto?.HangfireFeatureEnable ?? false;

            NotificationDirectory = dto?.NotificationDirectory;
            OfferDirectory = dto?.OfferDirectory;
            RestaurantDirectory = dto?.RestaurantDirectory;

            DineInActive = dto?.DineInActive;
            DeliveryActive = dto?.DeliveryActive;
            TakeawayActive = dto?.TakeawayActive;

            DineInEmailTransmission = dto?.DineInEmailTransmission ?? false;
            DineInMessageTransmission = dto?.DineInMessageTransmission ?? false;

            DeliveryEmailTransmission = dto?.DeliveryEmailTransmission ?? false;
            DeliveryMessageTransmission = dto?.DeliveryMessageTransmission ?? false;

            TakeawayEmailTransmission = dto?.TakeawayEmailTransmission ?? false;
            TakeawayMessageTransmission = dto?.TakeawayMessageTransmission ?? false;
        }


        public static string TwoFactorAPIURl
        {
            get; private set;
        }

        public static string Two2FactorApiAuthKey
        {
            get; private set;
        }

        public static string GeoUrl
        {
            get; private set;
        }

        public static string ApnsCertificatePassword
        {
            get; private set;
        }

        public static byte[] ApnsCertificateContent
        {
            get; private set;
        }

        public static string FCMServerKey
        {
            get; private set;
        }

        public static string AdminApplicationBaseUrl
        {
            get; private set;
        }

        public static string UserApplicationBaseUrl
        {
            get; private set;
        }

        public static string FCMSenderId
        {
            get; private set;
        }

        public static string FCMUrl
        {
            get; private set;
        }

        public static string ApplicationIdPackageName
        {
            get; private set;
        }

        public static bool? DineInActive
        {
            get; private set;
        }

        public static bool? DeliveryActive
        {
            get; private set;
        }

        public static bool? TakeawayActive
        {
            get; private set;
        }

        public static string RestaurantDirectory
        {
            get; private set;
        }

        public static string OfferDirectory
        {
            get; private set;
        }

        public static bool ServiceBusFeatureEnable
        {
            get; private set;
        }

        public static bool HangfireFeatureEnable
        {
            get; private set;
        }

        public static string NotificationDirectory
        {
            get; private set;
        }

        public static bool DineInEmailTransmission
        {
            get; private set;
        }

        public static bool DineInMessageTransmission
        {
            get; private set;
        }

        public static bool DeliveryEmailTransmission
        {
            get; private set;
        }

        public static bool DeliveryMessageTransmission
        {
            get; private set;
        }

        public static bool TakeawayEmailTransmission
        {
            get; private set;
        }

        public static bool TakeawayMessageTransmission
        {
            get; private set;
        }
    }
}
