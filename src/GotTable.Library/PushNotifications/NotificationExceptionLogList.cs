﻿using GotTable.Dal;
using GotTable.Dal.PushNotifications;
using GotTable.DAO.PushNotifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.PushNotifications
{
    [Serializable]
    public sealed class NotificationExceptionLogList : INotificationExceptionLogList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public async Task<List<NotificationExceptionLogDAO>> Get(decimal notificationId)
        {
            var model = new List<NotificationExceptionLogDAO>();
            var dal = DalFactory.Create<IPushNotificationDal>();
            var dtos = await dal.FetchList(notificationId);

            foreach (var item in dtos)
            {
                model.Add(new NotificationExceptionLogDAO()
                {
                    DeviceId = item.DeviceId,
                    DeviceTypeId = item.DeviceTypeId,
                    ExceptionId = item.ExceptionId,
                    InnerException = item.InnerException,
                    Message = item.Message,
                    NotificationId = item.NotificationId,
                    Source = item.Source,
                    StackTrace = item.StackTrace
                });
            }

            return model;
        }
    }
}
