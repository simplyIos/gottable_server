﻿using GotTable.DAO.PushNotifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.PushNotifications
{
    public interface INotificationList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="scheduleDate"></param>
        /// <param name="notificationCity"></param>
        /// <param name="notificationType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<PushNotificationInfoDAO>> Get(decimal userId, DateTime? scheduleDate = null, string notificationCity = "", string notificationType = "", int currentPage = 1, int pageSize = 10);
    }
}
