﻿using GotTable.DAO.PushNotifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.PushNotifications
{
    /// <summary>
    /// 
    /// </summary>
    public interface INotificationExceptionLogList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        Task<List<NotificationExceptionLogDAO>> Get(decimal notificationId);
    }
}
