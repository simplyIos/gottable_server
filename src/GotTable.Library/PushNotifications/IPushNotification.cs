﻿using GotTable.DAO.PushNotifications;
using System.Threading.Tasks;

namespace GotTable.Library.PushNotifications
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPushNotification
    {
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<PushNotificationDAO> Create(decimal userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        Task New(PushNotificationDAO pushNotificationDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        /// <returns></returns>
        Task Save(PushNotificationDAO pushNotificationDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        Task<PushNotificationDAO> Get(int notificationId);
    }
}
