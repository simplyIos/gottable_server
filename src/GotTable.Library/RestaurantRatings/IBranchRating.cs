﻿using GotTable.DAO.RestaurantRatings;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantRatings
{
    public interface IBranchRating
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratingDAO"></param>
        /// <returns></returns>
        Task New(BranchRatingDAO ratingDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<AvgRatingDAO> GetAvgRating(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="restuarantId"></param>
        /// <returns></returns>
        Task<bool> CheckAuthencity(decimal userId, decimal restuarantId);
    }
}