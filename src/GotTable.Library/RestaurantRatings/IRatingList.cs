﻿using GotTable.DAO.RestaurantRatings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantRatings
{
    public interface IRatingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchRatingDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10);
    }
}