﻿using GotTable.Dal;
using GotTable.Dal.Localities;
using GotTable.DAO.Localities;
using System;

namespace GotTable.Library.Localities
{
    [Serializable]
    public sealed class Localty : ILocalty
    {
        public void Insert(LocaltyDAO localtyDAO)
        {
            var dal = DalFactory.Create<ILocaltyDal>();
            var dto = new LocaltyDto()
            {
                Active = true,
                Id = 0,
                Name = localtyDAO.Name
            };
            dal.Insert(dto);
        }
    }
}
