﻿using PushSharp.Apple;

namespace GotTable.Library.Communications.Device
{
    public sealed class ApnsServiceBrokerManager
    {
        #region Thread safe singleton

        private ApnsServiceBrokerManager()
        {
        }

        public static ApnsServiceBrokerManager Instance
        {
            get { return Nested.instance; }
        }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly ApnsServiceBrokerManager instance = new ApnsServiceBrokerManager();
        }

        #endregion

        #region Properties

        private ApnsConfiguration _configuration = null;
        private ApnsServiceBroker _serviceBroker = null;

        #endregion

        #region Configuration

        private void Init(ApnsConfiguration configuration)
        {
            _configuration = configuration;
            _serviceBroker = new ApnsServiceBroker(_configuration);
            //TODO event handler

            _serviceBroker.Start();
        }

        private ApnsServiceBroker GetCurrentApnsServiceBroker()
        {
            if (_serviceBroker == null)
            {
                _serviceBroker = new ApnsServiceBroker(_configuration);
            }

            return _serviceBroker;
        }

        private ApnsServiceBroker ResetApnsServiceBroker(ApnsConfiguration configuration = null)
        {
            _serviceBroker.Stop();
            _serviceBroker = null;

            if (configuration != null)
            {
                _configuration = configuration;
            }

            return GetCurrentApnsServiceBroker();
        }

        #endregion

        #region Public methods

        public static void Initialize(ApnsConfiguration configuration)
        {
            var serviceBrokerManager = ApnsServiceBrokerManager.Instance;

            serviceBrokerManager.Init(configuration);
        }

        public static ApnsServiceBroker Reset(ApnsConfiguration configuration = null)
        {
            var serviceBrokerManager = ApnsServiceBrokerManager.Instance;

            return serviceBrokerManager.ResetApnsServiceBroker(configuration);
        }

        public static ApnsServiceBroker GetApnsServiceBroker()
        {
            var serviceBrokerManager = ApnsServiceBrokerManager.Instance;

            return serviceBrokerManager.GetCurrentApnsServiceBroker();
        }

        #endregion
    }
}