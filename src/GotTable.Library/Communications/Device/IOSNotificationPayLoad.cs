﻿using System;

namespace GotTable.Library.Communications.Device.PayLoad
{
    [Serializable]
    public sealed class IOSNotificationPayLoad
    {
        public IOSNotificationPayLoad(string title, string message, string categoryName, string offerName)
        {
            JsonString = "{\"aps\": {\"alert\" : { \"title\": " + "\"" + title + "\"" + ", \"body\": " + "\"" + message + "\"" + " }, \"badge\": \"0\", \"category\": " + "\"" + categoryName + "\"" + ", \"purpose\": " + "\"" + offerName + "\"" + " }}";
        }

        public IOSNotificationPayLoad(string title, string message, string categoryName, string offerName, string filePath)
        {
            JsonString = "{\"aps\": {\"alert\" : { \"title\": " + "\"" + title + "\"" + ", \"body\": " + "\"" + message + "\"" + " }, \"badge\": \"0\", \"category\": " + "\"" + categoryName + "\"" + ", \"purpose\": " + "\"" + offerName + "\"" + " , \"mutable-content\": 1 } , \"data\" : {  \"media-url\" : " + "\"" + filePath + "\"" + "} }";
        }

        public string JsonString { get; private set; }
    }
}
