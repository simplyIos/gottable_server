﻿using PushSharp.Apple;
using System;
using System.Security.Cryptography.X509Certificates;

namespace GotTable.Library.Communications.Device
{
    internal sealed class ApnsConfigurator
    {
        #region Thread safe singleton

        private ApnsConfigurator()
        {
        }

        public static ApnsConfigurator Instance
        {
            get { return Nested.instance; }
        }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly ApnsConfigurator instance = new ApnsConfigurator();
        }

        #endregion

        #region Properties

        private ApnsConfiguration.ApnsServerEnvironment _environment;
        private string _certificateFriendlyName;
        private string _certificatePassword;
        private ApnsConfiguration _configuration = null;

        #endregion

        #region Configuration

        private void Init(ApnsConfiguration.ApnsServerEnvironment environment, string certificateFriendlyName, string certificatePassword)
        {
            _environment = environment;
            _certificateFriendlyName = certificateFriendlyName;
            _certificatePassword = certificatePassword;
        }

        private ApnsConfiguration GetCurrentConfiguration()
        {
            if (_configuration == null)
            {
                _configuration = new ApnsConfiguration(_environment, GetServerCertificate().ToString(), _certificatePassword);
            }

            return _configuration;
        }

        private ApnsConfiguration ResetCurrentConfiguration()
        {
            _configuration = null;

            return GetCurrentConfiguration();
        }

        #endregion

        #region Public methods

        public static void Initialize(ApnsConfiguration.ApnsServerEnvironment environment, string certificateFriendlyName, string certificatePassword)
        {
            var configurator = ApnsConfigurator.Instance;

            configurator.Init(environment, certificateFriendlyName, certificatePassword);
        }

        public static ApnsConfiguration ResetConfiguration()
        {
            var configurator = ApnsConfigurator.Instance;

            return configurator.ResetCurrentConfiguration();
        }

        public static ApnsConfiguration GetConfiguration()
        {
            var configurator = ApnsConfigurator.Instance;

            return configurator.GetCurrentConfiguration();
        }

        #endregion

        private X509Certificate2 GetServerCertificate()
        {
            var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);

            if (store == null)
            {
                throw new InvalidOperationException(ApnsConfigurationResources.InvalidLocation);
            }

            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            var certificates = store.Certificates;

            if (certificates == null)
            {
                throw new InvalidOperationException(ApnsConfigurationResources.CertificateNotFound);
            }

            var certificate = new X509Certificate2();

            if (certificates.Count > 0)
            {
                int i;
                for (i = 0; i < certificates.Count; i++)
                {
                    certificate = certificates[i];

                    if (certificate.FriendlyName == _certificateFriendlyName)
                    {
                        return certificate;
                    }
                }
            }

            throw new InvalidOperationException(ApnsConfigurationResources.CertificateNotFound);
        }
    }
}