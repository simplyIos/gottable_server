﻿using GotTable.DAO.Bookings.DineIn;
using GotTable.Library.Communications.Generics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Jobs
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class ReminderConfirmationJob : IJob<ReminderConfirmationJob, DineInBookingDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Execute(DineInBookingDAO dao)
        {

        }

        #region

        private async Task SendReminderEmail(DineInBookingDAO bookingDAO)
        {

        }

        private async Task SendReminderMessage(DineInBookingDAO bookingDAO)
        {

        }

        private async Task SendReminderNOtification(DineInBookingDAO bookingDAO)
        {

        }

        #endregion
    }
}
