﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DineIn;
using GotTable.Library.Communications.Generics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Jobs
{
    [Serializable]
    public sealed class DineInBookingConfirmationJob : IJob<DineInBookingConfirmationJob, DineInBookingDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        private const string RestaurantUser = "user";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Execute(DineInBookingDAO dao)
        {
            var emailAttributes = new List<KeyValuePair<string, string>>();
            var dal = Dal.DalFactory.Create<Dal.RestaurantContacts.IContactDal>();
            var dtos = await dal.FetchList(dao.BranchId, 1, 10);
            if (dtos.Any())
            {
                var emailAddresses = dtos.Where(x => x.IsEmailAlert && !string.IsNullOrEmpty(x.EmailAddress)).Select(x => x.EmailAddress).ToList();
                if (emailAddresses.Any())
                {
                    emailAttributes = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("xxxNamexxx", RestaurantUser),
                        new KeyValuePair<string, string>("xxxRestaurantNamexxx", dao.RestaurantName),
                        new KeyValuePair<string, string>("xxxBookingDatexxx", dao.BookingDate.ToString("dd-MM-yyyy")),
                        new KeyValuePair<string, string>("xxxBookingDatexxx", dao.BookingDate.ToString("dd-MM-yyyy")),
                        new KeyValuePair<string, string>("xxxBookingTimexxx", dao.BookingTime),
                        new KeyValuePair<string, string>("xxxDateTimexxx", dao.BookingDate.ToString("dd-MMM-yyyy")),
                        new KeyValuePair<string, string>("xxxTableNamexxx", dao.SelectedTable),
                        new KeyValuePair<string, string>("xxxOfferNamexxx", dao.OfferTitle ?? string.Empty),
                        new KeyValuePair<string, string>("xxxUserNamexxx", string.Format("{0} {1},", dao.UserDetail.FirstName, dao.UserDetail.LastName)),
                        new KeyValuePair<string, string>("xxxUserEmailAddressxxx", dao.EmailAddress),
                        new KeyValuePair<string, string>("xxxUserPhoneNumberxxx", dao.UserDetail.PhoneNumber)
                    };
                    await Generics.Email.Invoke(Enumeration.EmailAction.DineInConfirmationToRestaurant, Enumeration.CredentialType.Info, emailAttributes, emailAddresses);
                }
            }

            var contact = dtos.Where(x => x.TypeId == (int)Enumeration.ContactType.GotTableRepersentative).FirstOrDefault();
            emailAttributes = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("xxxNamexxx", RestaurantUser),
                new KeyValuePair<string, string>("xxxRestaurantNamexxx", dao.RestaurantName),
                new KeyValuePair<string, string>("xxxBookingDatexxx", dao.BookingDate.ToString("dd-MM-yyyy")),
                new KeyValuePair<string, string>("xxxBookingTimexxx", dao.BookingTime),
                new KeyValuePair<string, string>("xxxDateTimexxx", dao.BookingDate.ToString("dd-MMM-yyyy")),
                new KeyValuePair<string, string>("xxxTableNamexxx", dao.SelectedTable),
                new KeyValuePair<string, string>("xxxOfferNamexxx", dao.OfferTitle ?? string.Empty),
                new KeyValuePair<string, string>("xxxUserNamexxx", string.Format("{0} {1},", dao.UserDetail.FirstName, dao.UserDetail.LastName)),
                new KeyValuePair<string, string>("xxxUserEmailAddressxxx", dao.EmailAddress),
                new KeyValuePair<string, string>("xxxUserPhoneNumberxxx", dao.UserDetail.PhoneNumber.ToString().Replace(".0", "")),
                new KeyValuePair<string, string>("xxxGTRepNamexxx", contact?.Name ?? string.Empty),
                new KeyValuePair<string, string>("xxxGTRepEmailAddressxxx", contact?.EmailAddress ?? string.Empty),
                new KeyValuePair<string, string>("xxxGTRepPhoneNumberxxx", contact?.PhoneNumber.ToString().Replace(".0", "") ?? string.Empty),
            };
            await Generics.Email.Invoke(Enumeration.EmailAction.DineInConfirmationToUser, Enumeration.CredentialType.Info, emailAttributes, new List<string>() {
                dao.EmailAddress });
        }
    }
}
