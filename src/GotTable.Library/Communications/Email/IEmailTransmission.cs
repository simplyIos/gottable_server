﻿
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public interface IEmailTransmission<T1, T2>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task Execute(T2 dao);
    }
}
