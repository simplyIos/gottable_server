﻿using NServiceBus;

namespace GotTable.Library.Communications.Events
{
    public class NotificationEvent : IEvent
    {
        public int ScheduleJobId { get; set; }

        public int NotificationId { get; set; }
    }
}
