﻿using GotTable.Library.OperationExceptionLogs;
using NServiceBus;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Events
{
    public class NotificationEventHandler : IHandleMessages<NotificationEvent>
    {
        private readonly IOperationExceptionLog operationExceptionLog;

        public NotificationEventHandler(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        public async Task Handle(NotificationEvent message, IMessageHandlerContext context)
        {
            var notificationEvent = new NotificationEvent
            {
                NotificationId = message.NotificationId,
                ScheduleJobId = message.ScheduleJobId
            };
            var exception = new System.NotImplementedException();
            await operationExceptionLog.New(exception);
        }
    }
}
