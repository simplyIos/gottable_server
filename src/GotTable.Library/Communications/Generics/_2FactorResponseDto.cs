﻿using System;

namespace GotTable.Library.Communications.Generics
{
    [Serializable]
    public sealed class _2FactorResponseDto
    {
        public string Status { get; set; }
        public string Details { get; set; }
    }
}
