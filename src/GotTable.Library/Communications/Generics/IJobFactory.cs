﻿using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Generics
{
    public interface IJobFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="job"></param>
        /// <param name="jobContent"></param>
        /// <param name="scheduleDateTime"></param>
        Task CreateJob<T1, T2>(IJob<T1, T2> job, T2 jobContent, DateTime scheduleDateTime);
    }
}