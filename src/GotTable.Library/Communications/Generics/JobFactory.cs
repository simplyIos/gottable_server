﻿using Hangfire;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Generics
{
    [Serializable]
    public sealed class JobFactory : IJobFactory
    {
        #region Create Jobs

        public async Task CreateJob<T1, T2>(IJob<T1, T2> job, T2 jobContent, DateTime scheduleDateTime)
        {
            await Task.FromResult(1);
            var timeSpan = scheduleDateTime - DateTime.Now;
            BackgroundJob.Schedule(() => job.Execute(jobContent), TimeSpan.FromSeconds(timeSpan.TotalSeconds));
        }

        #endregion
    }
}
