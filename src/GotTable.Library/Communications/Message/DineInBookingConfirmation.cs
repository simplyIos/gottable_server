﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Communications.Message.Content;
using GotTable.Dal.Communications.Message.TwoFactor;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Generics;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Library.RestaurantContacts;
using GotTable.Library.Restaurants;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Message
{
    [Serializable]
    public sealed class DineInBookingConfirmation : IMessageTransmission<DineInBookingConfirmation, string>
    {
        /// <summary>
        /// IRestaurant
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// IContactList
        /// </summary>
        private readonly IContactList contactList;

        /// <summary>
        /// IContactList
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        public DineInBookingConfirmation(IRestaurant restaurant, IContactList contactList, IBranchConfiguration branchConfiguration)
        {
            this.contactList = contactList;
            this.restaurant = restaurant;
            this.branchConfiguration = branchConfiguration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingType"></param>
        /// <param name="bookingDateTime"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="restaurantId"></param>
        /// <param name="selectedTable"></param>
        /// <param name="deliveryAddress"></param>
        /// <param name="userId"></param>
        /// <param name="bookingId"></param>
        /// <param name="rewardStatus"></param>
        /// <returns></returns>
        public async Task<string> Execute(Enumeration.BookingType bookingType, DateTime bookingDateTime, string phoneNumber, decimal restaurantId, string selectedTable, string deliveryAddress, decimal userId, decimal bookingId, bool rewardStatus = false, string restaurantName = null)
        {
            _2FactorRequestDto factorRequestDto;
            TwoFactorDto messageTemplate;
            TwoFactorLogDto twoFactorTransactionLogDto;
            var dal = DalFactory.Create<ITwoFactorDal>();
            var contactListDao = await contactList.Get(restaurantId, 0, 10);
            if (contactListDao != null && contactListDao.Any())
            {
                messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.NewDineConfirmation2Restaurant);
                foreach (var contactItem in contactListDao)
                {
                    if (contactItem.IsPhoneAlert && contactItem.IsActive)
                    {
                        var staffName = contactItem.Name;
                        if (contactItem.PhoneNumber.ToString().Length == 10)
                        {
                            factorRequestDto = new _2FactorRequestDto()
                            {
                                BaseUrl = AppSettingKeys.TwoFactorAPIURl,
                                ServiceUrl = string.Format(messageTemplate.QueryString, AppSettingKeys.Two2FactorApiAuthKey, contactItem.PhoneNumber, messageTemplate.SenderId, messageTemplate.Name, staffName, selectedTable, restaurantName),
                                TemplateName = messageTemplate.Name,
                                LogId = 0
                            };
                            twoFactorTransactionLogDto = new TwoFactorLogDto()
                            {
                                TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                                TemplateId = messageTemplate.TemplateId,
                                ExternalId = contactItem.Id,
                                PhoneNumber = contactItem.PhoneNumber,
                                LogId = 0
                            };
                            await dal.Insert(twoFactorTransactionLogDto);
                            factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
                            _2Factor.Send(factorRequestDto);
                        }
                    }
                }
            }
            messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.NewDineConfirmation2User);
            factorRequestDto = new _2FactorRequestDto()
            {
                BaseUrl = AppSettingKeys.TwoFactorAPIURl,
                ServiceUrl = string.Format(messageTemplate.QueryString, AppSettingKeys.Two2FactorApiAuthKey, phoneNumber, messageTemplate.SenderId, messageTemplate.Name, bookingDateTime.ToString("dd-MMM-yyyy hh:mm tt"), selectedTable, restaurantName),
                TemplateName = messageTemplate.Name,
            };
            twoFactorTransactionLogDto = new TwoFactorLogDto()
            {
                TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                TemplateId = messageTemplate.TemplateId,
                ExternalId = 0,
                PhoneNumber = decimal.Parse(phoneNumber)
            };
            await dal.Insert(twoFactorTransactionLogDto);
            factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
            _2Factor.Send(factorRequestDto);
            if (rewardStatus)
            {
                messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.DineInLoyalty4User);
                factorRequestDto = new _2FactorRequestDto()
                {
                    BaseUrl = AppSettingKeys.TwoFactorAPIURl,
                    ServiceUrl = string.Format(messageTemplate.QueryString, AppSettingKeys.Two2FactorApiAuthKey, phoneNumber, messageTemplate.SenderId, messageTemplate.Name, restaurantName, bookingDateTime.ToString("dd-MMM-yyyy hh:mm tt")),
                    TemplateName = messageTemplate.Name,
                };
                twoFactorTransactionLogDto = new TwoFactorLogDto()
                {
                    LogId = 0,
                    ExternalId = 0,
                    PhoneNumber = decimal.Parse(phoneNumber),
                    TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                    TemplateId = messageTemplate.TemplateId
                };
                await dal.Insert(twoFactorTransactionLogDto);
                factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
                _2Factor.Send(factorRequestDto);
            }
            var contentDal = DalFactory.Create<IContentDal>();
            var content = await contentDal.Fetch((int)Enumeration.TransmissionContent.NewDineConfirmation2User);
            var returnMessage = string.Format(content.Message, bookingDateTime.ToString("dd-MMM-yyyy hh:mm tt"), selectedTable, restaurantName);
            return returnMessage;
        }
    }
}
