﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantContacts;
using GotTable.DAO.RestaurantContacts;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantContacts
{
    [Serializable]
    public sealed class BranchContact : IBranchContact
    {
        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public async Task<BranchContactDAO> Create(decimal branchId)
        {
            await Task.FromResult(1);
            var branchContact = new BranchContactDAO()
            {
                BranchId = branchId,
                Id = 0,
                ErrorMessage = string.Empty,
                StatusId = 1
            };
            return branchContact;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchContactDAO"></param>
        /// <returns></returns>
        public async Task New(BranchContactDAO branchContactDAO)
        {
            if (branchContactDAO.BranchId == 0)
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (String.IsNullOrEmpty(branchContactDAO.Name))
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid contact name, Please check.!";
                return;
            }
            if (branchContactDAO.SelectedType.ToString() == "0")
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid selected type, Please check.!";
                return;
            }
            if (branchContactDAO.PhoneNumber.ToString().Length != 10)
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid contact number, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchContactDAO.EmailAddress))
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid email address, Please check.!";
                return;
            }
            var dto = new ContactDto()
            {
                BranchId = branchContactDAO.BranchId,
                EmailAddress = branchContactDAO.EmailAddress,
                CreatedDate = DateTime.Now,
                TypeId = (int)branchContactDAO.SelectedType,
                IsEmailAlert = branchContactDAO.IsEmailAlert,
                IsPhoneAlert = branchContactDAO.IsPhoneAlert,
                Name = branchContactDAO.Name,
                PhoneNumber = branchContactDAO.PhoneNumber.ToString(),
                IsActive = branchContactDAO.IsActive,
            };
            var dal = DalFactory.Create<IContactDal>();
            await dal.Insert(dto);
            if (dto.Id == 0)
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "There is some issue while processing your request, Please try afer some time.";
                return;
            }
            branchContactDAO.StatusId = 1;
            branchContactDAO.ErrorMessage = string.Empty;
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchContactDAO"></param>
        public async Task Save(BranchContactDAO branchContactDAO)
        {
            if (branchContactDAO.Id == 0)
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (branchContactDAO.BranchId == 0)
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (branchContactDAO.SelectedType.ToString() == "0")
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid selected type, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchContactDAO.Name))
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid contact name, Please check.!";
                return;
            }
            if (branchContactDAO.PhoneNumber.ToString().Length != 10)
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid contact number, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchContactDAO.EmailAddress))
            {
                branchContactDAO.StatusId = 0;
                branchContactDAO.ErrorMessage = "Invalid email address, Please check.!";
                return;
            }
            var dto = new ContactDto()
            {
                Id = branchContactDAO.Id,
                BranchId = branchContactDAO.BranchId,
                EmailAddress = branchContactDAO.EmailAddress,
                CreatedDate = DateTime.Now,
                TypeId = (int)branchContactDAO.SelectedType,
                IsEmailAlert = branchContactDAO.IsEmailAlert,
                IsPhoneAlert = branchContactDAO.IsPhoneAlert,
                Name = branchContactDAO.Name,
                PhoneNumber = branchContactDAO.PhoneNumber.ToString(),
                IsActive = branchContactDAO.IsActive,
            };
            var dal = DalFactory.Create<IContactDal>();
            await dal.Update(dto);
            branchContactDAO.StatusId = 1;
            branchContactDAO.ErrorMessage = string.Empty;
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public async Task<BranchContactDAO> Get(decimal contactId)
        {
            var dal = DalFactory.Create<IContactDal>();
            var dto = await dal.Fetch(contactId);
            if (dto != null)
            {
                return new BranchContactDAO()
                {
                    BranchId = dto.BranchId,
                    EmailAddress = dto.EmailAddress,
                    Id = dto.Id,
                    IsActive = dto.IsActive,
                    IsEmailAlert = dto.IsEmailAlert,
                    IsPhoneAlert = dto.IsPhoneAlert,
                    Name = dto.Name,
                    PhoneNumber = Convert.ToDecimal(dto.PhoneNumber),
                    SelectedType = (Enumeration.ContactType)dto.TypeId
                };
            }
            return null;
        }

        #endregion
    }
}
