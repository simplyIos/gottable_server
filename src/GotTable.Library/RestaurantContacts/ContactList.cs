﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantContacts;
using GotTable.DAO.RestaurantContacts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantContacts
{
    [Serializable]
    public sealed class ContactList : IContactList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchContactDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var contactList = new List<BranchContactDAO>();
            var dal = DalFactory.Create<IContactDal>();
            foreach (var contactItem in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                contactList.Add(new BranchContactDAO()
                {
                    BranchId = contactItem.BranchId,
                    Id = contactItem.Id,
                    IsActive = contactItem.IsActive,
                    PhoneNumber = Convert.ToDecimal(contactItem.PhoneNumber),
                    EmailAddress = contactItem.EmailAddress,
                    Name = contactItem.Name,
                    SelectedType = (Enumeration.ContactType)contactItem.TypeId,
                    IsPhoneAlert = contactItem.IsPhoneAlert,
                    IsEmailAlert = contactItem.IsEmailAlert
                });
            }
            return contactList;
        }
    }
}
