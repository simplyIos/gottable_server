﻿using GotTable.DAO.RestaurantContacts;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantContacts
{
    public interface IBranchContact
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        Task<BranchContactDAO> Create(decimal branchId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchContactDAO"></param>
        /// <returns></returns>
        Task New(BranchContactDAO branchContactDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchContactDAO"></param>
        /// <returns></returns>
        Task Save(BranchContactDAO branchContactDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        Task<BranchContactDAO> Get(decimal contactId);
    }
}