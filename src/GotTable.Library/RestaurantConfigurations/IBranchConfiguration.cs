﻿using GotTable.DAO.RestaurantConfigurations;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantConfigurations
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBranchConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restuarantId"></param>
        /// <returns></returns>
        Task Default(decimal restuarantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationDAO"></param>
        /// <returns></returns>
        Task Save(BranchConfigurationDAO configurationDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<BranchConfigurationDAO> Get(decimal restaurantId);
    }
}