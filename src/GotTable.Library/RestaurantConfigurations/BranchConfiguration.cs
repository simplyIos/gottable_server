﻿using GotTable.Dal;
using GotTable.Dal.RestaurantConfigurations;
using GotTable.DAO.RestaurantConfigurations;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantConfigurations
{
    [Serializable]
    public sealed class BranchConfiguration : IBranchConfiguration
    {
        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restuarantId"></param>
        /// <returns></returns>
        public async Task Default(decimal restuarantId)
        {
            var dal = DalFactory.Create<IConfigurationDal>();
            var dto = new ConfigurationDto()
            {
                BranchId = restuarantId,
            };
            await dal.Insert(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationDAO"></param>
        public async Task Save(BranchConfigurationDAO configurationDAO)
        {
            var dto = new ConfigurationDto()
            {
                BranchId = configurationDAO.BranchId,
                DeliveryCoverage = configurationDAO.DeliveryCoverage,
                DeliveryStandBy = configurationDAO.DeliveryStandBy,
                DineInReward = configurationDAO.DineInReward,
                TakeawayStandBy = configurationDAO.TakeawayStandBy,
                CentralGST = configurationDAO.CentralGST,
                StateGST = configurationDAO.StateGST,
                DeliveryCharges = configurationDAO.DeliveryCharges,
                DineInAutoCompleteActive = configurationDAO.DineInAutoCompleteActive,
                DineInAutoCompleteHours = configurationDAO.DineInAutoCompleteHours,
                DineInRewardActive = configurationDAO.DineInRewardActive
            };
            var dal = DalFactory.Create<IConfigurationDal>();
            await dal.Update(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<BranchConfigurationDAO> Get(decimal restaurantId)
        {
            var dal = DalFactory.Create<IConfigurationDal>();
            var dto = await dal.Fetch(restaurantId);
            return new BranchConfigurationDAO()
            {
                BranchId = dto.BranchId,
                DeliveryCoverage = dto.DeliveryCoverage,
                DeliveryStandBy = dto.DeliveryStandBy,
                DeliveryCharges = dto.DeliveryCharges,
                DineInReward = dto.DineInReward,
                TakeawayStandBy = dto.DeliveryStandBy,
                CentralGST = dto.CentralGST,
                StateGST = dto.StateGST,
                DineInRewardActive = dto.DineInRewardActive ?? false,
                DineInAutoCompleteHours = dto.DineInAutoCompleteHours,
                DineInAutoCompleteActive = dto.DineInAutoCompleteActive ?? false
            };
        }

        #endregion
    }
}
