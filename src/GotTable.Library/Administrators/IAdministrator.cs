﻿using System.Threading.Tasks;

namespace GotTable.Library.Administrators
{
    /// <summary>
    /// IAdministrator
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    public interface IAdministrator<T1>
    {
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        Task<T1> Create();

        /// <summary>
        /// New
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task New(T1 t);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task Save(T1 t);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<T1> Get(string emailAddress, string password);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        Task<T1> Get(decimal objectId);
    }
}
