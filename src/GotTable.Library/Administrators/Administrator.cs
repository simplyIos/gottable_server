﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Administrators;
using GotTable.DAO.Administrators;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Administrators
{
    /// <summary>
    /// Administrator
    /// </summary>
    [Serializable]
    public sealed class Administrator : IAdministrator<AdminDAO>
    {
        #region Factory method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<AdminDAO> Create()
        {
            await Task.FromResult(1);
            return new AdminDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                UserId = 0
            };
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="adminDAO"></param>
        public async Task New(AdminDAO adminDAO)
        {
            if (String.IsNullOrEmpty(adminDAO.FirstName))
            {
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "Invalid firstname, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(adminDAO.LastName))
            {
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "Invalid lastname, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(adminDAO.EmailAddress))
            {
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "Invalid emailaddress, Please check.!";
                return;
            }
            if (adminDAO.Password != null && adminDAO.Password.Length < 8)
            {
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "Invalid password length, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(adminDAO.UserType))
            {
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "Invalid user type, Please check.!";
                return;
            }
            var dal = DalFactory.Create<IAdministratorDal>();
            var dto = await dal.Fetch(adminDAO.EmailAddress);
            if (dto == null)
            {
                if (String.IsNullOrEmpty(adminDAO.Gender))
                {
                    adminDAO.Gender = Enumeration.Gender.NotDefined.ToString();
                }
                if (String.IsNullOrWhiteSpace(adminDAO.Prefix))
                {
                    adminDAO.Prefix = Enumeration.Prefix.NotDefined.ToString();
                }
                dto = new AdminDto()
                {
                    FirstName = adminDAO.FirstName,
                    LastName = adminDAO.LastName,
                    EmailAddress = adminDAO.EmailAddress,
                    Password = adminDAO.Password,
                    IsActive = adminDAO.IsActive,
                    TypeId = (int)Enum.Parse(typeof(Enumeration.AdminType), adminDAO.UserType),
                    GenderId = (int)Enum.Parse(typeof(Enumeration.Gender), adminDAO.Gender),
                    PrefixId = (int)Enum.Parse(typeof(Enumeration.Prefix), adminDAO.Prefix),
                    CityId = adminDAO.CityId,
                    EnableButtonForRestaurantLogin = adminDAO.EnableButtonForRestaurantLogin
                };
                await dal.Insert(dto);
                if (dto.UserId > 0)
                {
                    adminDAO.StatusId = 1;
                    adminDAO.UserId = dto.UserId;
                    adminDAO.ErrorMessage = string.Empty;
                }
                else
                {
                    adminDAO.UserId = 0;
                    adminDAO.StatusId = 0;
                    adminDAO.ErrorMessage = "There is some issue while registering the user, Please contact support team!";
                }
            }
            else
            {
                adminDAO.UserId = 0;
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "This email is already exists";
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="adminDAO"></param>
        /// <returns></returns>
        public async Task Save(AdminDAO adminDAO)
        {
            var dal = DalFactory.Create<IAdministratorDal>();
            var dto = await dal.Fetch(adminDAO.UserId);
            if (dto != null)
            {
                dto.FirstName = adminDAO.FirstName;
                dto.LastName = adminDAO.LastName;
                dto.Password = adminDAO.Password;
                dto.EmailAddress = adminDAO.EmailAddress;
                dto.IsActive = adminDAO.IsActive;
                dto.CityId = adminDAO.CityId;
                dto.EnableButtonForRestaurantLogin = adminDAO.EnableButtonForRestaurantLogin;
                await dal.Update(dto);
                if (dto.UserId > 0)
                {
                    adminDAO.StatusId = 1;
                    adminDAO.ErrorMessage = string.Empty;
                }
                else
                {
                    adminDAO.StatusId = 0;
                    adminDAO.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
                }
            }
            else
            {
                adminDAO.StatusId = 0;
                adminDAO.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<AdminDAO> Get(string emailAddress, string password)
        {
            var dal = DalFactory.Create<IAdministratorDal>();
            var dto = await dal.Fetch(emailAddress, password);
            if (dto == null)
            {
                return new AdminDAO()
                {
                    StatusId = 0,
                    ErrorMessage = "Invalid emailAddress and password"
                };
            }
            else
            {
                return new AdminDAO()
                {
                    StatusId = 1,
                    ErrorMessage = string.Empty,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    EmailAddress = dto.EmailAddress,
                    Gender = dto.GenderName,
                    Prefix = dto.PrefixName,
                    UserId = dto.UserId,
                    Password = dto.Password,
                    UserType = dto.TypeName,
                    IsActive = dto.IsActive ?? false,
                    CityId = dto.CityId,
                    EnableButtonForRestaurantLogin = dto.EnableButtonForRestaurantLogin
                };
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="adminId"></param>
        /// <returns></returns>
        public async Task<AdminDAO> Get(decimal adminId)
        {
            var dal = DalFactory.Create<IAdministratorDal>();
            var dto = await dal.Fetch(adminId);
            return new AdminDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                EmailAddress = dto.EmailAddress,
                Gender = dto.GenderName,
                Prefix = dto.PrefixName,
                UserId = dto.UserId,
                Password = dto.Password ?? string.Empty,
                UserType = dto.TypeName,
                IsActive = dto.IsActive ?? false,
                CityId = dto.CityId,
                EnableButtonForRestaurantLogin = dto.EnableButtonForRestaurantLogin
            };
        }

        #endregion
    }
}
