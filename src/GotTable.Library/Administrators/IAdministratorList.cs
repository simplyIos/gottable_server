﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Administrators
{
    /// <summary>
    /// IAdministratorList
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public interface IAdministratorList<T1, T2>
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        Task<List<T1>> Get(T2 criteria);
    }
}
