﻿using GotTable.Dal;
using GotTable.Dal.Administrators;
using GotTable.DAO.Administrators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Administrators
{
    /// <summary>
    /// AdministratorList
    /// </summary>
    [Serializable]
    public sealed class AdministratorList : IAdministratorList<AdminInfoDAO, ListCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<AdminInfoDAO>> Get(ListCriteria criteria)
        {
            var dal = DalFactory.Create<IAdministratorDal>();
            var dtos = await dal.FetchAdminList(criteria.AdminType, criteria.EmailAddress, criteria.AdminName, criteria.Status, criteria.CurrentPage, criteria.PageSize);
            var userList = await ModelToObjectListMapping(dtos);
            return userList;
        }


        #region Model mapping

        /// <summary>
        /// ModelToObjectListMapping
        /// </summary>
        /// <param name="adminDtos"></param>
        /// <returns></returns>
        private async Task<List<AdminInfoDAO>> ModelToObjectListMapping(List<AdminDto> adminDtos)
        {
            await Task.FromResult(1);
            var list = new List<AdminInfoDAO>();
            adminDtos.ForEach(x =>
            {
                list.Add(new AdminInfoDAO()
                {
                    CityId = x.CityId,
                    CityName = x.CityName,
                    EmailAddress = x.EmailAddress,
                    FirstName = x.FirstName,
                    IsActive = x.IsActive ?? false,
                    LastName = x.LastName,
                    UserId = x.UserId,
                    UserType = x.TypeName,
                    EnableButtonForRestaurantLogin = x.EnableButtonForRestaurantLogin
                });
            });
            return list;
        }

        #endregion
    }
}
