﻿using GotTable.Common.Enumerations;
using GotTable.Library.Shared;
using System;

namespace GotTable.Library.Administrators
{
    /// <summary>
    /// ListCriteria
    /// </summary>
    [Serializable]
    public sealed class ListCriteria : ICriteria
    {
        public ListCriteria(string adminName = default, string emailAddress = default, Enumeration.AdminType? adminType = default, bool getAll = default, int currentPage = default, int pageSize = default)
        {
            AdminName = adminName;
            EmailAddress = emailAddress;
            AdminType = adminType;
            GetAll = getAll;
            CurrentPage = currentPage;
            PageSize = pageSize;
        }

        public bool? Status { get; set; }

        public string AdminName { get; set; }

        public string EmailAddress { get; set; }

        public Enumeration.AdminType? AdminType { get; set; } = null;

        public bool GetAll { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
