﻿using GotTable.Dal;
using GotTable.Dal.RestaurantTables;
using GotTable.DAO.RestaurantTables;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTables
{
    [Serializable]
    public sealed class TableList : ITableList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchTableDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var branchTables = new List<BranchTableDAO>();
            var dal = DalFactory.Create<ITableDal>();
            foreach (var tableItem in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                branchTables.Add(new BranchTableDAO()
                {
                    BranchId = tableItem.RestaurantId,
                    Id = tableItem.Id,
                    SelectedTable = tableItem.TableName,
                    Value = tableItem.TableCount,
                    Active = tableItem.IsActive
                }); ;
            }
            return branchTables;
        }
    }
}
