﻿using GotTable.DAO.RestaurantTables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTables
{
    public interface ITableList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchTableDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10);
    }
}