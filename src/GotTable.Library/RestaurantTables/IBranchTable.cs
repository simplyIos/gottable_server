﻿using GotTable.DAO.RestaurantTables;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTables
{
    public interface IBranchTable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        Task<BranchTableDAO> Get(decimal tableId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTableDAO"></param>
        Task Save(BranchTableDAO branchTableDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task Default(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTableDAO"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <returns></returns>
        Task<bool> CheckAvailablity(BranchTableDAO branchTableDAO, DateTime bookingDate, string bookingTime);
    }
}