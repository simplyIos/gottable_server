﻿using GotTable.DAO.Cuisines;
using System.Threading.Tasks;

namespace GotTable.Library.Cuisines
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICuisine
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        Task<CuisineDAO> Get(decimal cuisineId);
    }
}
