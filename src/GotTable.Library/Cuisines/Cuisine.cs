﻿using GotTable.Dal;
using GotTable.Dal.Cuisines;
using GotTable.DAO.Cuisines;
using System.Threading.Tasks;

namespace GotTable.Library.Cuisines
{
    /// <summary>
    /// Cuisine
    /// </summary>
    public sealed class Cuisine : ICuisine
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        public async Task<CuisineDAO> Get(decimal cuisineId)
        {
            var dal = DalFactory.Create<ICuisineDal>();
            var dto = await dal.Fetch(cuisineId);
            var cuisineDAO = new CuisineDAO()
            {
                CuisineName = dto.Name,
                Id = dto.Id,
                CategoryId = dto.CategoryId,
                CategoryName = dto.CategoryName,
                DisplayLevel = dto.DisplayLevel
            };
            return cuisineDAO;
        }
    }
}
