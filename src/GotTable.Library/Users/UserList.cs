﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Users;
using GotTable.DAO.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace GotTable.Library.Users
{
    [Serializable]
    public sealed class UserList : IUserList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task<List<UserDetailDAO>> Get(string query)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dtos = await dal.FetchList(query);
            return dtos.Select(x => new UserDetailDAO()
            {
                FirstName = x.FirstName,
                LastName = x.LastName,
                EmailAddress = x.EmailAddress,
                UserId = x.UserId,
                PhoneNumber = x.PhoneNumber
            }).ToList();
        }
    }
}
