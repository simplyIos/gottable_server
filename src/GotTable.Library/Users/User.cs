﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Users;
using GotTable.DAO.Users;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    [Serializable]
    public sealed class User : IUser
    {

        #region Factory method
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Create(decimal userId)
        {
            await Task.FromResult(1);
            return new UserDetailDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                UserId = userId
            };
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Get(decimal userId)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(userId);
            return new UserDetailDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PhoneNumber = dto.PhoneNumber,
                Gender = dto.GenderName,
                Prefix = dto.PrefixName,
                UserId = dto.UserId,
                Password = dto.Password ?? string.Empty,
                UserType = dto.TypeName,
                IsActive = dto.IsActive ?? false,
                IsEmailVerified = dto.IsEmailVerified ?? false,
                IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication ?? false,
                RewardCount = dto.RewardCount,
                DoubleTheDealBookingId = dto.DoubleTheDealBookingId,
                Archeived = dto.Archeived
            };
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Get(string phoneNumber, string password)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(phoneNumber, password);
            if (dto == null)
            {
                return new UserDetailDAO()
                {
                    StatusId = 0,
                    ErrorMessage = "Invalid emailAddress and password"
                };
            }
            else
            {
                return new UserDetailDAO()
                {
                    StatusId = 1,
                    ErrorMessage = string.Empty,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    PhoneNumber = dto.PhoneNumber,
                    Gender = dto.GenderName,
                    Prefix = dto.PrefixName,
                    UserId = dto.UserId,
                    Password = dto.Password,
                    UserType = dto.TypeName,
                    IsActive = dto.IsActive ?? false,
                    IsEmailVerified = dto.IsEmailVerified ?? false,
                    IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication ?? false,
                    RewardCount = dto.RewardCount,
                    DoubleTheDealBookingId = dto.DoubleTheDealBookingId,
                    EmailAddress = dto.EmailAddress,
                    Archeived = dto.Archeived
                };
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Get(string phoneNumber)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(phoneNumber);
            if (dto == null)
            {
                return new UserDetailDAO()
                {
                    UserId = 0,
                    StatusId = 0,
                    ErrorMessage = "Invalid emailAddress"
                };
            }
            else
            {
                return new UserDetailDAO()
                {
                    StatusId = 1,
                    ErrorMessage = string.Empty,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    PhoneNumber = dto.PhoneNumber,
                    Gender = dto.GenderName,
                    Prefix = dto.PrefixName,
                    UserId = dto.UserId,
                    Password = dto.Password ?? string.Empty,
                    UserType = dto.TypeName,
                    IsActive = dto.IsActive ?? false,
                    IsEmailVerified = dto.IsEmailVerified ?? false,
                    IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication ?? false,
                    RewardCount = dto.RewardCount,
                    DoubleTheDealBookingId = dto.DoubleTheDealBookingId,
                    EmailAddress = dto.EmailAddress,
                    Archeived = dto.Archeived
                };
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="userDetailDAO"></param>
        /// <returns></returns>
        public async Task Save(UserDetailDAO dao)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(dao.UserId);
            if (dto != null)
            {
                dto.FirstName = dao.FirstName;
                dto.LastName = dao.LastName;
                dto.EmailAddress = dao.EmailAddress;
                dto.Archeived = null;
                await dal.Update(dto);
                if (dto.UserId > 0)
                {
                    dao.StatusId = 1;
                    dao.ErrorMessage = string.Empty;
                }
                else
                {
                    dao.StatusId = 0;
                    dao.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
                }
            }
            else
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
            }
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dao"></param>
        public async Task New(UserDetailDAO dao)
        {
            if (string.IsNullOrEmpty(dao.FirstName))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid firstname, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(dao.LastName))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid lastname, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(dao.PhoneNumber))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid phone number, Please check.!";
                return;
            }
            if (dao.Password != null && dao.Password.Length < 8)
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid password length, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(dao.UserType) && !Enum.IsDefined(typeof(Enumeration.UserType), dao.UserType))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid user type, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(dao.UserType) && !Enum.IsDefined(typeof(Enumeration.UserType), dao.UserType))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid user type, Please check.!";
                return;
            }
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(dao.PhoneNumber);
            if (dto == null)
            {
                if (string.IsNullOrEmpty(dao.Gender))
                {
                    dao.Gender = Enumeration.Gender.NotDefined.ToString();
                }
                if (string.IsNullOrWhiteSpace(dao.Prefix))
                {
                    dao.Prefix = Enumeration.Prefix.NotDefined.ToString();
                }
                dto = new UserDto()
                {
                    FirstName = dao.FirstName,
                    LastName = dao.LastName,
                    PhoneNumber = dao.PhoneNumber,
                    Password = dao.Password,
                    IsActive = dao.IsActive,
                    TypeId = (int)Enum.Parse(typeof(Enumeration.UserType), dao.UserType),
                    GenderId = (int)Enum.Parse(typeof(Enumeration.Gender), dao.Gender),
                    PrefixId = (int)Enum.Parse(typeof(Enumeration.Prefix), dao.Prefix),
                    IsEmailVerified = dao.IsEmailVerified,
                    IsEmailOptedForCommunication = dao.IsEmailOptedForCommunication,
                    EmailAddress = dao.EmailAddress,
                    Archeived = false
                };
                await dal.Insert(dto);
                if (dto.UserId > 0)
                {
                    dao.StatusId = 1;
                    dao.UserId = dto.UserId;
                    dao.ErrorMessage = string.Empty;
                }
                else
                {
                    dao.UserId = 0;
                    dao.StatusId = 0;
                    dao.ErrorMessage = "There is some issue while registering the user, Please contact support team!";
                }
            }
            else
            {
                dao.UserId = 0;
                dao.StatusId = 0;
                dao.ErrorMessage = "This phone number is already exists";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Delete(UserDetailDAO dao)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(dao.UserId);
            dto.Archeived = true;
            await dal.Update(dto);
            dao.StatusId = 1;
            dao.ErrorMessage = string.Empty;
        }

        #endregion
    }
}
