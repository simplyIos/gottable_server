﻿using GotTable.DAO.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    /// <summary>
    /// IUser
    /// </summary>
    public interface IUserList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<UserDetailDAO>> Get(string query);
    }
}
