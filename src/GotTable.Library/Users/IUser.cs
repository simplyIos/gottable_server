﻿using GotTable.DAO.Users;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    /// <summary>
    /// IUser
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Get(decimal userId);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Get(string emailAddress, string password);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Get(string phoneNumber);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="dao"></param>
        Task Save(UserDetailDAO dao);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="dao"></param>
        Task Delete(UserDetailDAO dao);

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dao"></param>
        Task New(UserDetailDAO dao);

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Create(decimal userId);
    }
}
