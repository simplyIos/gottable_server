﻿
using GotTable.Dal;
using GotTable.Dal.RestaurantInvoices;
using GotTable.DAO.RestaurantInvoices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantInvoices
{
    [Serializable]
    public sealed class InvoiceList : IInvoiceList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchInvoiceDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var dal = DalFactory.Create<IInvoiceDal>();
            var invoiceList = new List<BranchInvoiceDAO>();
            foreach (var dto in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                invoiceList.Add(new BranchInvoiceDAO()
                {
                    Amount = dto.Amount,
                    BranchId = dto.BranchId,
                    EndDate = dto.EndDate,
                    InvoiceId = dto.InvoiceId,
                    IsActive = dto.IsActive,
                    SelectedPrescription = dto.PrescriptionTypeName,
                    StartDate = dto.StartDate,
                    Delivery = dto.Delivery ?? false,
                    Takeaway = dto.Takeaway ?? false,
                    DineIn = dto.DineIn ?? false,
                    IsPaymentDone = dto.IsPaymentDone ?? false,
                    PaymentDate = dto.PaymentDate,
                    PaymentMethodId = dto.PaymentMethodId,
                    UserId = dto.UserId,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    EmailAddress = dto.EmailAddress,
                    CreatedDate = dto.CreatedDate.Value
                });
            }
            return invoiceList;
        }
    }
}
