﻿using GotTable.DAO.RestaurantInvoices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantInvoices
{
    public interface IInvoiceList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchInvoiceDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10);
    }
}