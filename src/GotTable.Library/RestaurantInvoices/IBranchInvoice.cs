﻿using GotTable.DAO.RestaurantInvoices;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantInvoices
{
    public interface IBranchInvoice
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<BranchInvoiceDAO> Create(decimal restaurantId);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchInvoiceDAO"></param>
        /// <returns></returns>
        Task New(BranchInvoiceDAO branchInvoiceDAO);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        Task<BranchInvoiceDAO> Get(Guid invoiceId);
    }
}