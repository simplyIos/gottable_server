﻿using GotTable.DAO.Amenities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Amenities
{
    /// <summary>
    /// IAmenityList
    /// </summary>
    public interface IAmenityList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<AmenityDAO>> Get(int currentPage, int pageSize);
    }
}
