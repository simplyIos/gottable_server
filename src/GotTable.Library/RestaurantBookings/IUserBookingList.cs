﻿using GotTable.DAO.Bookings.Users;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    public interface IUserBookingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<UserBookingDAO> Get(decimal userId, int currentPage = 1, int pageSize = 10);
    }
}