﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.DineIn;
using GotTable.DAO.Bookings.DineIn;
using GotTable.DAO.Users;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Jobs;
using GotTable.Library.Communications.Generics;
using GotTable.Library.Communications.Message;
using GotTable.Library.Devices;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Library.RestaurantTables;
using GotTable.Library.Users;
using System;
using System.Linq;
using System.Threading.Tasks;
using GotTable.Library.Restaurants;

namespace GotTable.Library.RestaurantBookings
{
    /// <summary>
    /// DineIn
    /// </summary>
    [Serializable]
    public sealed class DineIn : IDineIn
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDevice device;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJobFactory jobFactory;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageTransmission<DineInBookingConfirmation, string> messageTransmission;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<DineInBookingAutoCompleteJob, DineInBookingDAO> autoCompleteDineInJob;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<DineInBookingConfirmationJob, DineInBookingDAO> emailJob;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// 
        /// </summary>
        private const string confirmationMessage = "Your {0} reservation for {1} has been confirmed. Thank you for using Got Table!";

        public DineIn(IBranchTable branchTable, IMessageTransmission<DineInBookingConfirmation, string> messageTransmission, IUser user, IBranchConfiguration branchConfiguration, IJobFactory jobFactory, IJob<DineInBookingAutoCompleteJob, DineInBookingDAO> autoCompleteDineInJob, IJob<DineInBookingConfirmationJob, DineInBookingDAO> emailJob, IDevice device, IRestaurant restaurant)
        {
            this.jobFactory = jobFactory;
            this.branchTable = branchTable;
            this.messageTransmission = messageTransmission;
            this.user = user;
            this.branchConfiguration = branchConfiguration;
            this.autoCompleteDineInJob = autoCompleteDineInJob;
            this.emailJob = emailJob;
            this.device = device;
            this.restaurant = restaurant;
        }


        #region Factory methods

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(DineInBookingDAO dao)
        {
            if (dao.UserId == 0 && String.IsNullOrEmpty(dao.UserDetail.FirstName))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "First name should not be blank, Please check.!";
                return;
            }
            if (dao.UserId == 0 && String.IsNullOrEmpty(dao.UserDetail.LastName))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Last name should not be blank, Please check.!";
                return;
            }
            if (dao.UserId == 0 && String.IsNullOrEmpty(dao.UserDetail.PhoneNumber))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Phone number should not be blank, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(dao.EmailAddress))
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Email address can not be blank, Please check.!";
                return;
            }
            if (dao.TableId == 0)
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Please select one table.!";
                return;
            }
            var bookingDateTimeForDineIn = DateTime.ParseExact(dao.BookingDate.ToString("MM-dd-yyyy") + " " + dao.BookingTime, "MM-dd-yyyy HH:mm", null);
            if (DateTime.Now.AddMinutes(15) > bookingDateTimeForDineIn)
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Booking time should be 15 minutes more than current date and time.!";
                return;
            }
            var tableDetail = await branchTable.Get(dao.TableId);
            if (tableDetail == null)
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Invalid table, Please check.!";
                return;
            }
            if (await branchTable.CheckAvailablity(tableDetail, dao.BookingDate, dao.BookingTime) == false)
            {
                dao.StatusId = 0;
                dao.ErrorMessage = "Table you are looking for is not available, Please try with different table";
                return;
            }
            dynamic userDetail = null;
            if (dao.UserId != 0)
            {
                userDetail = await user.Get(dao.UserId);
                if (userDetail.UserType != Enumeration.UserType.Guest.ToString() && userDetail.UserType != Enumeration.UserType.EndUser.ToString())
                {
                    dao.StatusId = 0;
                    dao.ErrorMessage = "Invalid email address, Please check.!";
                    return;
                }
            }

            if (dao.UserId == 0)
            {
                userDetail = await user.Get(dao.UserDetail.PhoneNumber);
                if (userDetail.UserId == 0)
                {
                    userDetail = new UserDetailDAO()
                    {
                        UserId = dao.UserId,
                        PhoneNumber = dao.UserDetail.PhoneNumber,
                        FirstName = dao.UserDetail.FirstName,
                        LastName = dao.UserDetail.LastName,
                        UserType = Enumeration.UserType.Guest.ToString(),
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        IsEmailOptedForCommunication = dao.UserDetail.IsEmailOptedForCommunication,
                        Password = Guid.NewGuid().ToString().Substring(1, 10),
                        IsActive = dao.UserDetail.IsActive
                    };
                    await user.New(userDetail);
                }
            }
            dao.UserDetail = userDetail;

            var dto = new DineInDto()
            {
                BookingDate = dao.BookingDate,
                BookingTime = dao.BookingTime,
                BranchId = dao.BranchId,
                Comment = dao.Comment,
                CreatedDate = dao.CreatedDate,
                Id = dao.Id,
                TableId = dao.TableId,
                UserId = userDetail.UserId,
                EmailAddress = dao.EmailAddress,
                OfferId = dao.OfferId == 0 ? null : dao.OfferId,
                CurrentStatusId = (int)Enumeration.DineInStatusType.Confirm,
                PromoCode = dao.PromoCode,
                BillUploadStatus = (int)Enumeration.BillUpload.Pending,
                DoubleTheDealActive = false
            };

            var dal = DalFactory.Create<IDineInDal>();
            await dal.Insert(dto);

            dao.Id = dto.Id;
            dao.OfferTitle = dto.OfferTitle;
            dao.SelectedTable = dto.TableName;

            var restaurantDetail = await restaurant.Get(dao.BranchId);

            dao.RestaurantName = restaurantDetail.BranchName + ", " + restaurantDetail.City;
            await CreateJob(dao);

            string bookingConfirmationMessage;
            if (AppSettingKeys.DineInMessageTransmission)
            {
                bookingConfirmationMessage = await messageTransmission.Execute(Enumeration.BookingType.DineIn, bookingDateTimeForDineIn, dao.UserDetail.PhoneNumber.ToString(), dao.BranchId, tableDetail.SelectedTable, null, dao.UserId, dao.Id, dto.RewardApplied, dao.RestaurantName);
            }
            else
            {
                bookingConfirmationMessage = string.Format(confirmationMessage, dao.BookingDate, dao.SelectedTable.ToString());
            }

            dao.StatusId = 1;
            dao.ConfirmationMessage = bookingConfirmationMessage;
            dao.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dineInStatusDAO"></param>
        public async Task New(DineInStatusDAO dineInStatusDAO)
        {
            var dto = new DineInStatusDto()
            {
                BookingId = dineInStatusDAO.BookingId,
                Comment = dineInStatusDAO.Comment ?? string.Empty,
                StatusId = (int)Enum.Parse(typeof(Enumeration.DineInStatusType), dineInStatusDAO.StatusName),
                UserId = null,
                AdminId = dineInStatusDAO.UserId
            };
            var dal = DalFactory.Create<IDineInDal>();
            await dal.Insert(dto);
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DineInBookingDAO> Get(decimal bookingId)
        {
            var dal = DalFactory.Create<IDineInDal>();

            var dto = await dal.Fetch(bookingId);

            return new DineInBookingDAO()
            {
                BookingDate = DateTime.ParseExact(dto.BookingDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", null),
                BookingTime = dto.BookingTime,
                BranchId = dto.BranchId,
                Comment = dto.Comment,
                CreatedDate = dto.CreatedDate,
                Id = dto.Id,
                TableId = dto.TableId,
                SelectedTable = dto.TableName,
                UserId = dto.UserId,
                EmailAddress = dto.EmailAddress,
                UserDetail = await user.Get(dto.UserId),
                StatusList = dto.StatusList.Select(item => new DineInStatusDAO()
                {
                    BookingId = item.BookingId,
                    Comment = item.Comment,
                    CreatedDate = item.CreatedDate,
                    Id = item.Id,
                    StatusName = ((Enumeration.DineInStatusType)item.StatusId).ToString(),
                    UserId = item.UserId,
                    UserName = item.UserName,
                    AdminId = item.AdminId,
                    AdminName = item.AdminName
                }).ToList(),
                OfferId = dto.OfferId,
                OfferTitle = dto.OfferTitle,
                OfferDescription = dto.OfferDescription,
                OfferType = dto.OfferTypeId != null ? ((Enumeration.OfferType)dto.OfferTypeId).ToString() : default,
                PromoCode = dto.PromoCode ?? "No promo code",
                RewardApplied = dto.RewardApplied,
                BillUploadStatus = ((Enumeration.BillUpload)dto.BillUploadStatus).ToString(),
                DoubleTheDeal = dto.DoubleTheDealActive ?? false
            };
        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task MarkAsRead(decimal bookingId)
        {
            var dal = DalFactory.Create<IDineInDal>();
            await dal.MarkAsRead(bookingId);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// CreateJob
        /// </summary>
        /// <param name="dineInBookingDAO"></param>
        /// <returns></returns>
        private async Task CreateJob(DineInBookingDAO dineInBookingDAO)
        {
            var branchConfigurationDao = await branchConfiguration.Get(dineInBookingDAO.BranchId);

            if (branchConfigurationDao.DineInAutoCompleteActive && branchConfigurationDao.DineInAutoCompleteHours != null)
            {
                var scheduleDateTime = DateTime.ParseExact(dineInBookingDAO.BookingDate.Date.ToString("MM-dd-yyyy") + " " + dineInBookingDAO.BookingTime, "MM-dd-yyyy HH:mm", null).AddHours(Convert.ToDouble(branchConfigurationDao.DineInAutoCompleteHours));
                await jobFactory.CreateJob(autoCompleteDineInJob, dineInBookingDAO, scheduleDateTime);
            }

            if (AppSettingKeys.DineInEmailTransmission)
            {
                var scheduleDateTime = DateTime.Now.AddMinutes(2);
                await jobFactory.CreateJob(emailJob, dineInBookingDAO, scheduleDateTime);
            }
        }

        #endregion
    }
}
