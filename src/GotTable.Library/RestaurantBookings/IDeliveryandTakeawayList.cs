﻿using GotTable.DAO.Bookings.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    public interface IDeliveryandTakeawayList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<DeliveryandTakeawayListDAO>> Get(decimal userId, int currentPage = 1, int pageSize = 10);
    }
}