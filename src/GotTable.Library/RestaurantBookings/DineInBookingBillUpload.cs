﻿using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.BillUploads;
using GotTable.DAO.RestaurantBookings.BillUploads;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class DineInBookingBillUpload : IDineInBookingBillUpload
    {
        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInBookingBillUpload"></param>
        /// <returns></returns>
        public async Task New(DineInBookingBillUploadDAO dineInBookingBillUpload)
        {
            if (dineInBookingBillUpload.Amount == 0)
            {
                dineInBookingBillUpload.StatusId = 0;
                dineInBookingBillUpload.ErrorMessage = "Invalid amount, Please provide the bill amount";
                return;
            }
            if (string.IsNullOrEmpty(dineInBookingBillUpload.ImagePath))
            {
                dineInBookingBillUpload.StatusId = 0;
                dineInBookingBillUpload.ErrorMessage = "Invalid path, Please provide the bill file";
                return;
            }
            if (dineInBookingBillUpload.BookingId <= 0)
            {
                dineInBookingBillUpload.StatusId = 0;
                dineInBookingBillUpload.ErrorMessage = "Invalid request, Please check with GotTable";
                return;
            }
            if (dineInBookingBillUpload.UserId <= 0)
            {
                dineInBookingBillUpload.StatusId = 0;
                dineInBookingBillUpload.ErrorMessage = "Invalid request, Please check with GotTable";
                return;
            }

            var dto = new BillUploadDto()
            {
                Amount = dineInBookingBillUpload.Amount,
                BookingId = dineInBookingBillUpload.BookingId,
                UserId = dineInBookingBillUpload.UserId,
                ImagePath = dineInBookingBillUpload.ImagePath,
                Authorized = false,
                AuthorizedComment = string.Empty,
                AuthorizedRewardPoint = 0,
                Redeem = false,
                RedeemComment = string.Empty,
                AdminId = dineInBookingBillUpload.AdminId
            };

            var dal = DalFactory.Create<IBillUploadDal>();
            var booking = await dal.Fetch((int)dto.BookingId);
            if (booking == null)
            {
                await dal.Insert(dto);
                dineInBookingBillUpload.StatusId = 1;
                dineInBookingBillUpload.ErrorMessage = string.Empty;
            }
            else
            {
                dineInBookingBillUpload.StatusId = 0;
                dineInBookingBillUpload.ErrorMessage = "Bill already uploaded, Please check.";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInBookingBillUpload"></param>
        /// <returns></returns>
        public async Task Save(DineInBookingBillUploadDAO dineInBookingBillUpload)
        {
            if (dineInBookingBillUpload.Authorized == false && !string.IsNullOrEmpty(dineInBookingBillUpload.OfferName))
            {
                if (string.IsNullOrEmpty(dineInBookingBillUpload.AuthorizedComment))
                {
                    dineInBookingBillUpload.StatusId = 0;
                    dineInBookingBillUpload.ErrorMessage = "Invalid comment, Please provide the comment for rejecting this bill.!";
                    return;
                }
                dineInBookingBillUpload.AuthorizedRewardPoint = 0;
                dineInBookingBillUpload.Redeem = false;
                dineInBookingBillUpload.RedeemComment = string.Empty;
            }
            else if (dineInBookingBillUpload.Authorized == true)
            {
                dineInBookingBillUpload.AuthorizedDate = DateTime.Now;
            }

            var dto = new BillUploadDto()
            {
                Amount = dineInBookingBillUpload.Amount,
                BookingId = dineInBookingBillUpload.BookingId,
                UserId = dineInBookingBillUpload.UserId,
                ImagePath = dineInBookingBillUpload.ImagePath,
                Authorized = dineInBookingBillUpload.Authorized,
                AuthorizedComment = dineInBookingBillUpload.AuthorizedComment,
                AuthorizedRewardPoint = dineInBookingBillUpload.AuthorizedRewardPoint,
                Redeem = dineInBookingBillUpload.Redeem,
                RedeemComment = dineInBookingBillUpload.RedeemComment,
                AuthorizedDate = dineInBookingBillUpload.AuthorizedDate,
                RedeemDate = dineInBookingBillUpload.RedeemDate,
                AdminId = dineInBookingBillUpload.AdminId
            };

            var dal = DalFactory.Create<IBillUploadDal>();
            await dal.Update(dto);

            dineInBookingBillUpload.StatusId = 1;
            dineInBookingBillUpload.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DineInBookingBillUploadDAO> Get(int bookingId)
        {
            var dal = DalFactory.Create<IBillUploadDal>();
            var dto = await dal.Fetch(bookingId);

            var dineInBookingBillUpload = new DineInBookingBillUploadDAO()
            {
                Amount = dto.Amount,
                Authorized = dto.Authorized,
                AuthorizedComment = dto.AuthorizedComment,
                AuthorizedDate = dto.AuthorizedDate,
                AuthorizedRewardPoint = dto.AuthorizedRewardPoint != null ? dto.AuthorizedRewardPoint : dto.Amount > 2499 ? 100 : 75,
                BookingId = dto.BookingId,
                ImagePath = dto.ImagePath,
                Redeem = dto.Redeem ?? false,
                RedeemComment = dto.RedeemComment,
                RedeemDate = dto.RedeemDate,
                UploadedDate = dto.UploadedDate,
                UserId = dto.UserId,
                EmailAddress = dto.EmailAddress,
                PhoneNumber = dto.PhoneNumber,
                RestaurantAddress = dto.RestaurantAddress,
                RestaurantName = dto.RestaurantName,
                UserName = dto.UserName,
                OfferName = dto.OfferName,
                OfferType = dto.OfferType,
                BookingDate = dto.BookingDate,
                AdminId = dto.AdminId,
                AdminName = dto.AdminName
            };

            return dineInBookingBillUpload;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<bool> Delete(int bookingId)
        {
            var deleteStatus = true;
            var dal = DalFactory.Create<IBillUploadDal>();
            await dal.Delete(bookingId);
            return deleteStatus;
        }

        #endregion
    }
}
