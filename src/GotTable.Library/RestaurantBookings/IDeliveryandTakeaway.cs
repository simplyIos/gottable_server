﻿using GotTable.DAO.Bookings.DeliveryandTakeaway;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    /// <summary>
    /// IDeliveryandTakeaway
    /// </summary>
    public interface IDeliveryandTakeaway
    {
        #region Factory methods

        /// <summary>
        /// New
        /// </summary>
        /// <param name="deliveryandTakeawayDAO"></param>
        /// <returns></returns>
        Task New(DeliveryandTakeawayDAO deliveryandTakeawayDAO);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<DeliveryandTakeawayDAO> Get(decimal bookingId);

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="bookingId"></param>
        Task MarkAsRead(decimal bookingId);

        /// <summary>
        /// New
        /// </summary>
        /// <param name="deliveryandTakeawayStatusDAO"></param>
        Task New(DeliveryandTakeawayStatusDAO deliveryandTakeawayStatusDAO);

        #endregion
    }
}
