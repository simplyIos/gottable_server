﻿using GotTable.DAO.Bookings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    public interface IRestaurantBookingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingDAO>> Get(decimal restaurantId, DateTime bookingDate, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingStartDate"></param>
        /// <param name="bookingEndDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingDAO>> Get(decimal restaurantId, DateTime bookingStartDate, DateTime bookingEndDate, int currentPage = 1, int pageSize = 10);
    }
}