﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.DeliveryandTakeaway;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.DAO.Users;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Jobs;
using GotTable.Library.Communications.Generics;
using GotTable.Library.Communications.Message;
using GotTable.Library.Users;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class DeliveryandTakeaway : IDeliveryandTakeaway
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IJobFactory jobFactory;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageTransmission<DeliveryandTakeawayBookingConfirmation, string> messageTransmission;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<DeliveryandTakeawayBookingConfirmationJob, DeliveryandTakeawayDAO> emailJob;

        /// <summary>
        /// 
        /// </summary>
        private const string confirmationMessage = "Your order has been confirmed. Thank you for using Got Table!";

        public DeliveryandTakeaway(IJobFactory jobFactory, IUser user, IMessageTransmission<DeliveryandTakeawayBookingConfirmation, string> messageTransmission, IJob<DeliveryandTakeawayBookingConfirmationJob, DeliveryandTakeawayDAO> emailJob)
        {
            this.jobFactory = jobFactory;
            this.user = user;
            this.messageTransmission = messageTransmission;
            this.emailJob = emailJob;
        }

        #region Factory methods

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(DeliveryandTakeawayDAO dao)
        {
            dao.ConfirmationMessage = string.Empty;
            dao.ErrorMessage = string.Empty;
            dao.StatusId = 0;

            if (dao.BranchId == 0)
            {
                dao.ErrorMessage = "Invalid request, Please check.!";
                return;
            }
            if (dao.UserId == 0 && dao.UserDetail.FirstName == string.Empty)
            {
                dao.ErrorMessage = "First name can not be blank, Please check.!";
                return;
            }
            if (dao.UserId == 0 && dao.UserDetail.LastName == string.Empty)
            {
                dao.ErrorMessage = "Last name can not be blank, Please check.!";
                return;
            }
            if (dao.UserId == 0 && dao.UserDetail.PhoneNumber == string.Empty)
            {
                dao.ErrorMessage = "Phone number can not be blank, Please check.!";
                return;
            }
            if (dao.EmailAddress == string.Empty)
            {
                dao.ErrorMessage = "Email address can not be blank, Please check.!";
                return;
            }
            if (dao.BookingDate == null)
            {
                dao.ErrorMessage = "Invalid booking date, Please check.!";
                return;
            }
            if (dao.BookingTime == string.Empty)
            {
                dao.ErrorMessage = "Invalid booking time, Please check.!";
                return;
            }
            if (dao.Cartamount == 0)
            {
                dao.ErrorMessage = "Invalid cartamount, Please check.!";
                return;
            }
            if (dao.CentralGST == 0)
            {
                dao.ErrorMessage = "Invalid central gst, Please check.!";
                return;
            }
            if (dao.StateGST == 0)
            {
                dao.ErrorMessage = "Invalid state gst, Please check.!";
                return;
            }

            UserDetailDAO userDao = null;

            if (dao.UserId != 0)
            {
                userDao = await user.Get(dao.UserId);
                if (userDao.UserType != Enumeration.UserType.EndUser.ToString() && userDao.UserType != Enumeration.UserType.Guest.ToString())
                {
                    dao.ErrorMessage = "Invalid request, You can not use this emailaddress for booking.";
                    return;
                }
            }
            else if (dao.UserId == 0)
            {
                userDao = await user.Get(dao.UserDetail.PhoneNumber);
                if (userDao.UserId == 0)
                {
                    userDao = new UserDetailDAO()
                    {
                        UserId = dao.UserDetail.UserId,
                        PhoneNumber = dao.UserDetail.PhoneNumber,
                        FirstName = dao.UserDetail.FirstName,
                        LastName = dao.UserDetail.LastName,
                        UserType = Enumeration.UserType.Guest.ToString(),
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        IsActive = dao.UserDetail.IsActive,
                        IsEmailVerified = dao.UserDetail.IsEmailVerified,
                        IsEmailOptedForCommunication = dao.UserDetail.IsEmailOptedForCommunication,
                        Password = Guid.NewGuid().ToString().Substring(1, 10),
                        EmailAddress = dao.EmailAddress
                    };
                    await user.New(userDao);
                }
            }

            var dto = new DeliveryandTakeawayDto()
            {
                BookingDate = dao.BookingDate,
                BookingTime = dao.BookingTime,
                BookingId = dao.BookingId,
                BookingTypeId = (int)dao.BookingType,
                BranchId = dao.BranchId,
                CartAmount = dao.CartItem.Sum(x => x.Price * x.Quantity),
                Comment = string.Empty,
                CreatedDate = DateTime.Now,
                OfferId = dao.OfferId,
                EmailAddress = dao.EmailAddress,
                CentralGST = dao.CentralGST,
                StateGST = dao.StateGST,
                UserId = userDao.UserId,
                CurrentStatusId = (int)Enumeration.DeliveryandTakeawayStatusType.Confirm,
                PromoCode = dao.PromoCode,
                DeliveryCharges = dao.DeliveryCharges,
                DeliveryAddress = dao.BookingType == Enumeration.BookingType.Takeaway ? null : new DeliveryAddressDto()
                {
                    City = dao.AddressDetail.City,
                    Id = dao.AddressDetail.Id,
                    Landmark = dao.AddressDetail.Landmark,
                    Latitude = dao.AddressDetail.Latitude,
                    Line1 = dao.AddressDetail.AddressLine1,
                    Line2 = dao.AddressDetail.AddressLine2,
                    Longitude = dao.AddressDetail.Longitude,
                    State = dao.AddressDetail.State,
                    Zip = dao.AddressDetail.Zip
                },
                ItemList = dao.CartItem.Select(m => new DeliveryandTakeawayCartIttemDto()
                {
                    MenuId = m.MenuId,
                    Quantity = m.Quantity,
                    Cost = m.Price,
                    Remark = m.Remark
                }).ToList()
            };

            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            await dal.Insert(dto);

            if (dto.BookingId != 0)
            {
                var deliveryAddress = string.Empty;
                if (dao.BookingType == Enumeration.BookingType.Delivery)
                    deliveryAddress = dao.AddressDetail.AddressLine1 + " " + dao.AddressDetail.AddressLine2 + " " + dao.AddressDetail.Landmark + ", " + dao.AddressDetail.City + " " + dao.AddressDetail.State;

                var dateTimeForDeliveryandTakeaway = DateTime.ParseExact(dao.BookingDate.ToString("MM-dd-yyyy") + " " + dao.BookingTime, "MM-dd-yyyy HH:mm", null);

                dao.StatusId = 1;

                await CreateJob(dao);

                var bookingConfirmationMessage = confirmationMessage;
                if (dao.BookingType == Enumeration.BookingType.Delivery && AppSettingKeys.DeliveryMessageTransmission)
                {
                    bookingConfirmationMessage = await messageTransmission.Execute(dao.BookingType, dateTimeForDeliveryandTakeaway, dao.UserDetail.PhoneNumber, dao.BranchId, null, deliveryAddress, dao.UserId, dao.BookingId);
                }
                if (dao.BookingType == Enumeration.BookingType.Takeaway && AppSettingKeys.TakeawayMessageTransmission)
                {
                    bookingConfirmationMessage = await messageTransmission.Execute(dao.BookingType, dateTimeForDeliveryandTakeaway, dao.UserDetail.PhoneNumber, dao.BranchId, null, deliveryAddress, dao.UserId, dao.BookingId);
                }
                dao.ConfirmationMessage = bookingConfirmationMessage;
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DeliveryandTakeawayDAO> Get(decimal bookingId)
        {
            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            var dto = await dal.Fetch(bookingId);
            return new DeliveryandTakeawayDAO()
            {
                BookingDate = DateTime.ParseExact(dto.BookingDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", null),
                BookingTime = dto.BookingTime,
                BookingId = dto.BookingId,
                BookingTypeId = dto.BookingTypeId,
                BranchId = dto.BranchId,
                OfferId = dto.OfferId ?? 0,
                EmailAddress = dto.EmailAddress,
                StateGST = dto.StateGST,
                OfferTitle = dto.OfferTitle,
                OfferDescription = dto.OfferDescription,
                CartItem = dto.ItemList.Select(item => new DeliveryandTakeawayCartItemDAO()
                {
                    ItemId = item.ItemId,
                    MenuId = item.MenuId,
                    MenuName = item.MenuName,
                    Price = item.Cost,
                    Quantity = item.Quantity,
                    Remark = item.Remark
                }).ToList(),
                StatusList = dto.StatusList.Select(item => new DeliveryandTakeawayStatusDAO()
                {
                    Comment = item.Comment,
                    CreatedDate = item.CreatedDate,
                    StatusId = item.StatusId,
                    StatusName = item.StatusName,
                    UserId = item.UserId,
                    UserName = item.UserName,
                    AdminId = item.AdminId,
                    AdminName = item.AdminName
                }).ToList(),
                CurrentStatusId = dto.CurrentStatusId,
                CurrentStatus = ((Enumeration.DeliveryandTakeawayStatusType)dto.CurrentStatusId).ToString(),
                AddressDetail = dto.DeliveryAddress == null ? null : MapModelToObject(dto.DeliveryAddress),
                UserId = dto.UserId,
                UserDetail = await user.Get(dto.UserId),
                NewStatus = new DeliveryandTakeawayStatusDAO()
                {
                    BookingId = dto.BookingId,
                    Comment = string.Empty,
                },
                CentralGST = dto.CentralGST,
                PromoCode = dto.PromoCode ?? "No promo code",
                DeliveryCharges = dto.DeliveryCharges
            };
        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="bookingId"></param>
        public async Task MarkAsRead(decimal bookingId)
        {
            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            await dal.MarkAsRead(bookingId);
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="deliveryandTakeawayStatusDAO"></param>
        public async Task New(DeliveryandTakeawayStatusDAO deliveryandTakeawayStatusDAO)
        {
            var dto = new DeliveryandTakeawayStatusDto()
            {
                BookingId = deliveryandTakeawayStatusDAO.BookingId,
                Comment = deliveryandTakeawayStatusDAO.Comment == null ? string.Empty : deliveryandTakeawayStatusDAO.Comment,
                StatusId = (int)Enum.Parse(typeof(Enumeration.DineInStatusType), deliveryandTakeawayStatusDAO.StatusName),
                UserId = deliveryandTakeawayStatusDAO.UserId,
                AdminId = deliveryandTakeawayStatusDAO.AdminId
            };
            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            await dal.Insert(dto);
        }

        #endregion

        #region Helper Method

        private DeliveryAddressDAO MapModelToObject(DeliveryAddressDto dto)
        {
            return new DeliveryAddressDAO()
            {
                AddressLine1 = dto.Line1,
                AddressLine2 = dto.Line2,
                City = dto.City,
                Id = dto.Id ?? 0,
                Landmark = dto.Landmark,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                State = dto.State,
                Zip = dto.Zip
            };
        }

        /// <summary>
        /// CreateJob
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        private async Task CreateJob(DeliveryandTakeawayDAO dao)
        {

            if (dao.BookingType == Enumeration.BookingType.Delivery && AppSettingKeys.DeliveryEmailTransmission)
            {
                var scheduleDateTime = DateTime.Now.AddMinutes(2);
                await jobFactory.CreateJob(emailJob, dao, scheduleDateTime);
            }

            if (dao.BookingType == Enumeration.BookingType.Takeaway && AppSettingKeys.TakeawayEmailTransmission)
            {
                var scheduleDateTime = DateTime.Now.AddMinutes(2);
                await jobFactory.CreateJob(emailJob, dao, scheduleDateTime);
            }
        }

        #endregion  
    }
}
