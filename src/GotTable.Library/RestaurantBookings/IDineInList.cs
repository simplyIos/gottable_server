﻿using GotTable.Dal.RestaurantBookings;
using GotTable.DAO.Bookings.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    public interface IDineInList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingInfoDto>> Get(decimal restaurantTableId, DateTime bookingDate, int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<DineInListDAO>> Get(decimal userId, int currentPage = 1, int pageSize = 10);
    }
}