﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings;
using GotTable.DAO.Bookings.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class DeliveryandTakeawayList : IDeliveryandTakeawayList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<DeliveryandTakeawayListDAO>> Get(decimal userId, int currentPage = 1, int pageSize = 10)
        {
            var dal = DalFactory.Create<IBookingListDal>();
            var deliveryandTakeawayList = new List<DeliveryandTakeawayListDAO>();
            var dtos = await dal.FetchDeliveryandTakeawayList(userId, currentPage, pageSize);
            foreach (var item in dtos)
            {
                deliveryandTakeawayList.Add(new DeliveryandTakeawayListDAO()
                {
                    BookingId = item.BookingId,
                    BookingType = ((Enumeration.BookingType)item.BookingTypeId).ToString(),
                    CentralGST = item.CentralGST,
                    DateForBooking = item.BookingDate.ToString("MM-dd-yyyy") + " " + item.BookingTime,
                    DeliveryCharges = item.DeliveryCharges,
                    PackingCharges = item.PackingCharges,
                    ServiceCharges = item.ServiceCharges,
                    StateGST = item.StateGST,
                    TotalAmount = item.CartAmount + item.CentralGST + item.StateGST,
                    Status = ((Enumeration.DeliveryandTakeawayStatusType)item.CurrentStatusId).ToString(),
                    RestaurantDetail = new RestaurantDAO(item.BranchName, item.BranchAddress, item.BranchCity, item.BranchState, item.BranchZipCode.ToString()),
                    DeliveryDetail = item.Delivery == null ? null : new DeliveryAddressDAO(item.Delivery.Address1, item.Delivery.Address2, string.Empty, item.Delivery.City, item.Delivery.State, item.Delivery.Zip, item.Delivery.Latitude, item.Delivery.Longitude),
                    ContactList = item.ContactList.Select(m => new RestaurantContactDAO()
                    {
                        Email = m.Email,
                        Name = m.Name,
                        PhoneNumber = m.PhoneNumber
                    }).ToList(),
                    CartList = item.CartItem.Select(m => new CartModelDAO()
                    {
                        MenuName = m.MenuName,
                        Price = m.Rate,
                        Quantity = m.Quantity
                    }).ToList()
                });
            }
            return deliveryandTakeawayList;
        }
    }
}
