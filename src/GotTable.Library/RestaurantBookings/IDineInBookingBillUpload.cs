﻿using GotTable.DAO.RestaurantBookings.BillUploads;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    public interface IDineInBookingBillUpload
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInBookingBillUpload"></param>
        /// <returns></returns>
        Task New(DineInBookingBillUploadDAO dineInBookingBillUpload);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInBookingBillUpload"></param>
        Task Save(DineInBookingBillUploadDAO dineInBookingBillUpload);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<DineInBookingBillUploadDAO> Get(int bookingId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<bool> Delete(int bookingId);
    }
}