﻿using GotTable.DAO.Bookings.Users;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class UserBookingList : IUserBookingList
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInList dineInList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeawayList deliveryandTakeawayList;

        public UserBookingList(IDineInList dineInList, IDeliveryandTakeawayList deliveryandTakeawayList)
        {
            this.dineInList = dineInList;
            this.deliveryandTakeawayList = deliveryandTakeawayList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<UserBookingDAO> Get(decimal userId, int currentPage = 1, int pageSize = 10)
        {
            return new UserBookingDAO()
            {
                UserId = userId,
                DineInList = await dineInList.Get(userId, currentPage, pageSize),
                DeliveryandTakeawayList = await deliveryandTakeawayList.Get(userId, currentPage, pageSize)
            };
        }
    }
}
