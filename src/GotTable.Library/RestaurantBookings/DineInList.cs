﻿using GotTable.Dal;
using GotTable.Dal.RestaurantBookings;
using GotTable.DAO.Bookings.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class DineInList : IDineInList
    {
        public DineInList()
        {

        }

        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingInfoDto>> Get(decimal restaurantTableId, DateTime bookingDate, int currentPage = 1, int pageSize = 10)
        {
            var dal = DalFactory.Create<IBookingListDal>();
            return await dal.FetchDineInBookingList(restaurantTableId, bookingDate, currentPage, pageSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<DineInListDAO>> Get(decimal userId, int currentPage = 1, int pageSize = 10)
        {
            var dal = DalFactory.Create<IBookingListDal>();
            var dineInList = new List<DineInListDAO>();
            var dtos = await dal.FetchDineInList(userId, currentPage, pageSize);

            foreach (var item in dtos)
            {
                dineInList.Add(new DineInListDAO()
                {
                    BookingId = item.BookingId,
                    DateForBooking = item.BookingDate.ToString("MM-dd-yyyy") + " " + item.BookingTime,
                    RestaurantDetail = new RestaurantDAO(item.BranchName, item.BranchAddress, item.BranchCity, item.BranchState, item.BranchZipCode.ToString("G29")),
                    SelectedOffer = item.OfferName,
                    SelectedTable = ((Common.Enumerations.Enumeration.Tables)item.TableId).ToString(),
                    Status = ((Common.Enumerations.Enumeration.DineInStatusType)item.CurrentStatusId).ToString(),
                    TimeForBooking = item.BookingTime,
                    ContactList = item.ContactList.Select(m => new RestaurantContactDAO()
                    {
                        Email = m.Email,
                        Name = m.Name,
                        PhoneNumber = m.PhoneNumber
                    }).ToList(),
                    BillUploadStatus = ((Common.Enumerations.Enumeration.BillUpload)item.BillUploadStatus).ToString()
                });
            }

            return dineInList;
        }

        #endregion
    }
}
