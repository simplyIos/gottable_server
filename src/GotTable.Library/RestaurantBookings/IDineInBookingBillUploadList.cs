﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantBookings.BillUploads;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    public interface IDineInBookingBillUploadList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadStatus"></param>
        /// <param name="emailAddress"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BillUploadListItemInfoDAO>> Get(Enumeration.BillUpload? uploadStatus = null, string emailAddress = "", string phoneNumber = "", int currentPage = 1, int pageSize = 10);
    }
}