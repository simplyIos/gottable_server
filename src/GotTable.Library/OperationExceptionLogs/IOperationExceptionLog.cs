﻿using GotTable.DAO.OperationExceptions;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.OperationExceptionLogs
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOperationExceptionLog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLog"></param>
        /// <returns></returns>
        Task<Guid> New(Exception exceptionLog);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLogId"></param>
        /// <returns></returns>
        Task<OperationExceptionLogDAO> Get(Guid exceptionLogId);
    }
}
