SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Localities](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Latitude] [varchar](50) NOT NULL,
	[Longitude] [varchar](50) NOT NULL,
	[CityId] [numeric](18, 0) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Localities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Localities]  WITH CHECK ADD  CONSTRAINT [FK_Localities_Cities] FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([CityId])
GO

ALTER TABLE [dbo].[Localities] CHECK CONSTRAINT [FK_Localities_Cities]
GO