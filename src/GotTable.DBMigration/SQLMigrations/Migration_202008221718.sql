Alter View [dbo].[BranchAvgRating]
As
Select BranchId, 
       Sum(Rating) / COUNT(BranchId) as AvgRating, 
	   Sum(AmbienceRating) / COUNT(BranchId) as AvgAmbienceRating,
	   Sum(FoodRating) / COUNT(BranchId) as AvgFoodRating,
	   Sum(MusicRating) / COUNT(BranchId) as AvgMusicRating,
	   Sum(PriceRating) / COUNT(BranchId) as AvgPriceRating,
	   Sum(ServiceRating) / COUNT(BranchId) as AvgServiceRating
	   From BranchRatings
Group By BranchId
GO


