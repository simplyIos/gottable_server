/****** Object:  Table [dbo].[BranchAmenities]    Script Date: 24-07-2020 16:07:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BranchAmenities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [numeric](18, 0) NOT NULL,
	[AmenityId] [int] NOT NULL,
 CONSTRAINT [PK_BranchAmenities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[BranchAmenities]  WITH CHECK ADD  CONSTRAINT [FK_BranchAmenities_Amenities] FOREIGN KEY([AmenityId])
REFERENCES [dbo].[Amenities] ([Id])
GO

ALTER TABLE [dbo].[BranchAmenities] CHECK CONSTRAINT [FK_BranchAmenities_Amenities]
GO

ALTER TABLE [dbo].[BranchAmenities]  WITH CHECK ADD  CONSTRAINT [FK_BranchAmenities_HotelBranches] FOREIGN KEY([BranchId])
REFERENCES [dbo].[HotelBranches] ([BranchId])
GO

ALTER TABLE [dbo].[BranchAmenities] CHECK CONSTRAINT [FK_BranchAmenities_HotelBranches]
GO


