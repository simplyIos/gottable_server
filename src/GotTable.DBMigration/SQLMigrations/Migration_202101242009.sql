ALTER TABLE [dbo].[HotelBranches]  WITH CHECK ADD  CONSTRAINT [FK_HotelBranches_Localities1] FOREIGN KEY([LocalityId1])
REFERENCES [dbo].[Localities] ([Id])
GO

ALTER TABLE [dbo].[HotelBranches] CHECK CONSTRAINT [FK_HotelBranches_Localities1]
GO


ALTER TABLE [dbo].[HotelBranches]  WITH CHECK ADD  CONSTRAINT [FK_HotelBranches_Localities2] FOREIGN KEY([LocalityId2])
REFERENCES [dbo].[Localities] ([Id])
GO

ALTER TABLE [dbo].[HotelBranches] CHECK CONSTRAINT [FK_HotelBranches_Localities2]
GO


ALTER TABLE [dbo].[HotelBranches]  WITH CHECK ADD  CONSTRAINT [FK_HotelBranches_Localities3] FOREIGN KEY([LocalityId3])
REFERENCES [dbo].[Localities] ([Id])
GO

ALTER TABLE [dbo].[HotelBranches] CHECK CONSTRAINT [FK_HotelBranches_Localities3]
GO


ALTER TABLE [dbo].[HotelBranches]  WITH CHECK ADD  CONSTRAINT [FK_HotelBranches_Localities4] FOREIGN KEY([LocalityId4])
REFERENCES [dbo].[Localities] ([Id])
GO

ALTER TABLE [dbo].[HotelBranches] CHECK CONSTRAINT [FK_HotelBranches_Localities4]
GO
